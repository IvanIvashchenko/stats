-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: zeptostats
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rank`
--

DROP TABLE IF EXISTS `rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` bigint(20) NOT NULL,
  `rank` int(11) DEFAULT NULL,
  `country_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK354C2C37770F28` (`game_id`),
  KEY `FK354C2C230CDCF` (`country_id`),
  CONSTRAINT `FK354C2C230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `FK354C2C37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank`
--

LOCK TABLES `rank` WRITE;
/*!40000 ALTER TABLE `rank` DISABLE KEYS */;
INSERT INTO `rank` (`id`, `rank`, `date`, `country_id`, `game_id`) VALUES
(1, 8841, 315878400000, 1, 1),
(2, NULL, 315878400000, 2, 2);

/*!40000 ALTER TABLE `rank` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `date` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB AUTO_INCREMENT=31449600001 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
INSERT INTO `calendar` (`date`) VALUES
    (0),
    (86400000),
    (172800000),
    (259200000),
    (345600000),
    (432000000),
    (518400000),
    (604800000),
    (691200000),
    (777600000),
    (864000000),
    (950400000),
    (1036800000),
    (1123200000),
    (1209600000),
    (1296000000),
    (1382400000),
    (1468800000),
    (1555200000),
    (1641600000),
    (1728000000),
    (1814400000),
    (1900800000),
    (1987200000),
    (2073600000),
    (2160000000),
    (2246400000),
    (2332800000),
    (2419200000),
    (2505600000),
    (2592000000),
    (2678400000),
    (2764800000),
    (2851200000),
    (2937600000),
    (3024000000),
    (3110400000),
    (3196800000),
    (3283200000),
    (3369600000),
    (3456000000),
    (3542400000),
    (3628800000),
    (3715200000),
    (3801600000),
    (3888000000),
    (3974400000),
    (4060800000),
    (4147200000),
    (4233600000),
    (4320000000),
    (4406400000),
    (4492800000),
    (4579200000),
    (4665600000),
    (4752000000),
    (4838400000),
    (4924800000),
    (5011200000),
    (5097600000),
    (5184000000),
    (5270400000),
    (5356800000),
    (5443200000),
    (5529600000),
    (5616000000),
    (5702400000),
    (5788800000),
    (5875200000),
    (5961600000),
    (6048000000),
    (6134400000),
    (6220800000),
    (6307200000),
    (6393600000),
    (6480000000),
    (6566400000),
    (6652800000),
    (6739200000),
    (6825600000),
    (6912000000),
    (6998400000),
    (7084800000),
    (7171200000),
    (7257600000),
    (7344000000),
    (7430400000),
    (7516800000),
    (7603200000),
    (7689600000),
    (7776000000),
    (7862400000),
    (7948800000),
    (8035200000),
    (8121600000),
    (8208000000),
    (8294400000),
    (8380800000),
    (8467200000),
    (8553600000),
    (8640000000),
    (8726400000),
    (8812800000),
    (8899200000),
    (8985600000),
    (9072000000),
    (9158400000),
    (9244800000),
    (9331200000),
    (9417600000),
    (9504000000),
    (9590400000),
    (9676800000),
    (9763200000),
    (9849600000),
    (9936000000),
    (10022400000),
    (10108800000),
    (10195200000),
    (10281600000),
    (10368000000),
    (10454400000),
    (10540800000),
    (10627200000),
    (10713600000),
    (10800000000),
    (10886400000),
    (10972800000),
    (11059200000),
    (11145600000),
    (11232000000),
    (11318400000),
    (11404800000),
    (11491200000),
    (11577600000),
    (11664000000),
    (11750400000),
    (11836800000),
    (11923200000),
    (12009600000),
    (12096000000),
    (12182400000),
    (12268800000),
    (12355200000),
    (12441600000),
    (12528000000),
    (12614400000),
    (12700800000),
    (12787200000),
    (12873600000),
    (12960000000),
    (13046400000),
    (13132800000),
    (13219200000),
    (13305600000),
    (13392000000),
    (13478400000),
    (13564800000),
    (13651200000),
    (13737600000),
    (13824000000),
    (13910400000),
    (13996800000),
    (14083200000),
    (14169600000),
    (14256000000),
    (14342400000),
    (14428800000),
    (14515200000),
    (14601600000),
    (14688000000),
    (14774400000),
    (14860800000),
    (14947200000),
    (15033600000),
    (15120000000),
    (15206400000),
    (15292800000),
    (15379200000),
    (15465600000),
    (15552000000),
    (15638400000),
    (15724800000),
    (15811200000),
    (15897600000),
    (15984000000),
    (16070400000),
    (16156800000),
    (16243200000),
    (16329600000),
    (16416000000),
    (16502400000),
    (16588800000),
    (16675200000),
    (16761600000),
    (16848000000),
    (16934400000),
    (17020800000),
    (17107200000),
    (17193600000),
    (17280000000),
    (17366400000),
    (17452800000),
    (17539200000),
    (17625600000),
    (17712000000),
    (17798400000),
    (17884800000),
    (17971200000),
    (18057600000),
    (18144000000),
    (18230400000),
    (18316800000),
    (18403200000),
    (18489600000),
    (18576000000),
    (18662400000),
    (18748800000),
    (18835200000),
    (18921600000),
    (19008000000),
    (19094400000),
    (19180800000),
    (19267200000),
    (19353600000),
    (19440000000),
    (19526400000),
    (19612800000),
    (19699200000),
    (19785600000),
    (19872000000),
    (19958400000),
    (20044800000),
    (20131200000),
    (20217600000),
    (20304000000),
    (20390400000),
    (20476800000),
    (20563200000),
    (20649600000),
    (20736000000),
    (20822400000),
    (20908800000),
    (20995200000),
    (21081600000),
    (21168000000),
    (21254400000),
    (21340800000),
    (21427200000),
    (21513600000),
    (21600000000),
    (21686400000),
    (21772800000),
    (21859200000),
    (21945600000),
    (22032000000),
    (22118400000),
    (22204800000),
    (22291200000),
    (22377600000),
    (22464000000),
    (22550400000),
    (22636800000),
    (22723200000),
    (22809600000),
    (22896000000),
    (22982400000),
    (23068800000),
    (23155200000),
    (23241600000),
    (23328000000),
    (23414400000),
    (23500800000),
    (23587200000),
    (23673600000),
    (23760000000),
    (23846400000),
    (23932800000),
    (24019200000),
    (24105600000),
    (24192000000),
    (24278400000),
    (24364800000),
    (24451200000),
    (24537600000),
    (24624000000),
    (24710400000),
    (24796800000),
    (24883200000),
    (24969600000),
    (25056000000),
    (25142400000),
    (25228800000),
    (25315200000),
    (25401600000),
    (25488000000),
    (25574400000),
    (25660800000),
    (25747200000),
    (25833600000),
    (25920000000),
    (26006400000),
    (26092800000),
    (26179200000),
    (26265600000),
    (26352000000),
    (26438400000),
    (26524800000),
    (26611200000),
    (26697600000),
    (26784000000),
    (26870400000),
    (26956800000),
    (27043200000),
    (27129600000),
    (27216000000),
    (27302400000),
    (27388800000),
    (27475200000),
    (27561600000),
    (27648000000),
    (27734400000),
    (27820800000),
    (27907200000),
    (27993600000),
    (28080000000),
    (28166400000),
    (28252800000),
    (28339200000),
    (28425600000),
    (28512000000),
    (28598400000),
    (28684800000),
    (28771200000),
    (28857600000),
    (28944000000),
    (29030400000),
    (29116800000),
    (29203200000),
    (29289600000),
    (29376000000),
    (29462400000),
    (29548800000),
    (29635200000),
    (29721600000),
    (29808000000),
    (29894400000),
    (29980800000),
    (30067200000),
    (30153600000),
    (30240000000),
    (30326400000),
    (30412800000),
    (30499200000),
    (30585600000),
    (30672000000),
    (30758400000),
    (30844800000),
    (30931200000),
    (31017600000),
    (31104000000),
    (31190400000),
    (31276800000),
    (31363200000),
    (31449600000),

    (315532800000),
    (315619200000),

    (315705600000),
    (315792000000),
    (315878400000);
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_bin NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `is_top` tinyint(1) NOT NULL,
  `region_id` bigint(20) NOT NULL,
  `ranking_title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK3917579627BCC2C5` (`region_id`),
  CONSTRAINT `FK3917579627BCC2C5` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`, `code`, `name`, `is_top`, `region_id`, `ranking_title`) VALUES
    (1,'BR','BRZ',0,2,NULL),
    (2,'FR','FRC',1,3,NULL),
    (3,'US','USA',1,1,NULL);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extra` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `finance_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  `title` varchar(256) COLLATE utf8_bin NOT NULL,
  `unit_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  `parent_game_id` bigint(20) DEFAULT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `partner_id` bigint(20) DEFAULT NULL,
  `default_price` double DEFAULT NULL,
  `default_currency_id` bigint(20) DEFAULT NULL,
  `is_required_in_report` tinyint(1) NOT NULL DEFAULT '1',
  `ranking_sku` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK304BF2133D4251` (`report_ui_id`),
  KEY `FK304BF2469B7493` (`parent_game_id`),
  KEY `FK304BF2E9E03AED` (`game_type_id`),
  KEY `FK304BF23E50EF93` (`meta_game_id`),
  KEY `FK304BF287E4952C` (`store_id`),
  KEY `FK304BF235543B4C` (`partner_id`),
  KEY `FK304BF227A4E268` (`resolution_id`),
  KEY `FK304BF22AD7ACA` (`default_currency_id`),
  CONSTRAINT `FK304BF2133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`),
  CONSTRAINT `FK304BF227A4E268` FOREIGN KEY (`resolution_id`) REFERENCES `resolution` (`id`),
  CONSTRAINT `FK304BF22AD7ACA` FOREIGN KEY (`default_currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `FK304BF235543B4C` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`),
  CONSTRAINT `FK304BF23E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FK304BF2469B7493` FOREIGN KEY (`parent_game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK304BF287E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FK304BF2E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` (`id`, `extra`, `finance_sku`, `game_type_id`, `resolution_id`, `title`, `unit_sku`, `meta_game_id`, `parent_game_id`, `report_ui_id`, `store_id`, `partner_id`, `default_price`, `default_currency_id`, `is_required_in_report`, `ranking_sku`) VALUES
    (1,NULL,'finance',1,1,'Cut the Rope 1','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (2,NULL,'finance',1,1,'Cut the Rope 2','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (3,NULL,'finance',1,1,'Cut the Rope 3','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (4,NULL,'finance',1,2,'Cut the Rope 4','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (5,NULL,'finance',1,2,'Cut the Rope 5','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (6,NULL,'finance',1,2,'Cut the Rope 6','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (7,NULL,'finance',1,3,'Cut the Rope 7','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (8,NULL,'finance',1,3,'Cut the Rope 8','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (9,NULL,'finance',1,3,'Cut the Rope 9','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (10,NULL,'finance',2,1,'Cut the Rope 10','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (11,NULL,'finance',2,1,'Cut the Rope 11','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (12,NULL,'finance',2,1,'Cut the Rope 12','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (13,NULL,'finance',2,2,'Cut the Rope 13','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (14,NULL,'finance',2,2,'Cut the Rope 14','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (15,NULL,'finance',2,2,'Cut the Rope 15','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (16,NULL,'finance',2,3,'Cut the Rope 16','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (17,NULL,'finance',2,3,'Cut the Rope 17','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (18,NULL,'finance',2,3,'Cut the Rope 18','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (19,NULL,'finance',3,1,'Cut the Rope 19','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (20,NULL,'finance',3,1,'Cut the Rope 20','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (21,NULL,'finance',3,1,'Cut the Rope 21','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (22,NULL,'finance',3,2,'Cut the Rope 22','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (23,NULL,'finance',3,2,'Cut the Rope 23','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (24,NULL,'finance',3,2,'Cut the Rope 24','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (25,NULL,'finance',3,3,'Cut the Rope 25','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (26,NULL,'finance',3,3,'Cut the Rope 26','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (27,NULL,'finance',3,3,'Cut the Rope 27','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (28,NULL,'finance',3,3,'0 commision 0 partner','unit',1,NULL,1,1,NULL,1,NULL,1,NULL),
    (29,NULL,'finance',3,3,'0 commision 1 partner','unit',1,NULL,1,1,1,1,NULL,1,NULL),
    (30,NULL,'finance',3,3,'1 commision 0 partner','unit',1,NULL,1,4,NULL,1,NULL,1,NULL),
    (31,NULL,'finance',3,3,'1 commision 1 partner','unit',1,NULL,1,4,1,1,NULL,1,NULL),
    (32,'8C','finance',3,3,'inapp','unit',1,1,1,1,1,1,NULL,1,NULL),
    (33,NULL,'finance',3,3,'no game_period','unit',1,1,1,1,1,1,NULL,1,NULL),

    (34,NULL,'finance',1,2,'game name A','unit',5,NULL,1,1,1,1,NULL,1,NULL),
    (35,NULL,'finance',1,1,'game name A','unit',5,NULL,1,1,1,1,NULL,1,NULL),
    (36,NULL,'finance',2,2,'game name C','unit',6,NULL,1,1,1,1,NULL,1,NULL),
    (37,NULL,'finance',2,1,'game name C','unit',6,NULL,1,1,1,1,NULL,1,NULL),
    (38,NULL,'finance',2,2,'game name B','unit',7,34,1,1,1,1,NULL,1,NULL),
    (39,NULL,'finance',2,1,'game name B','unit',7,35,1,1,1,1,NULL,1,NULL),
    (40,NULL,'finance',2,2,'game name D','unit',8,36,1,1,1,1,NULL,1,NULL),
    (41,NULL,'finance',2,1,'game name D','unit',8,37,1,1,1,1,NULL,1,NULL),

    (42,NULL,'finance',2,1,'Android app','unit',8,NULL,1,1,1,1,NULL,1,NULL),
    (43,NULL,'finance',2,1,'Android inapp','unit',8,42,1,1,1,1,NULL,1,NULL),
    (44,NULL,'finance',2,1,'BlackBerry app','unit',8,NULL,1,2,1,1,NULL,1,NULL),
    (45,NULL,'finance',2,1,'BlackBerry inapp','unit',8,44,1,2,1,1,NULL,1,NULL);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_period`
--

DROP TABLE IF EXISTS `game_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_period` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `start_date` bigint(20) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `game_id` (`game_id`,`start_date`,`end_date`),
  KEY `FKFB2532AE39799FC8` (`currency_id`),
  KEY `FKFB2532AE37770F28` (`game_id`),
  CONSTRAINT `FKFB2532AE37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FKFB2532AE39799FC8` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_period`
--

LOCK TABLES `game_period` WRITE;
/*!40000 ALTER TABLE `game_period` DISABLE KEYS */;
INSERT INTO `game_period` (`id`, `is_active`, `end_date`, `price`, `start_date`, `currency_id`, `game_id`) VALUES
    (1,1,NULL,0.99,NULL,1,1),
    (2,1,NULL,0.99,NULL,1,2),
    (3,1,NULL,0.99,NULL,1,3),
    (4,1,NULL,0.99,NULL,1,4),
    (5,1,NULL,0.99,NULL,1,5),
    (6,1,NULL,0.99,NULL,1,6),
    (7,1,NULL,0.99,NULL,1,7),
    (8,1,NULL,0.99,NULL,1,8),
    (9,1,NULL,0.99,NULL,1,9),
    (10,1,NULL,0.99,NULL,1,10),
    (11,1,NULL,0.99,NULL,1,11),
    (12,1,NULL,0.99,NULL,1,12),
    (13,1,NULL,0.99,NULL,1,13),
    (14,1,NULL,0.99,NULL,1,14),
    (15,1,NULL,0.99,NULL,1,15),
    (16,1,NULL,0.99,NULL,1,16),
    (17,1,NULL,0.99,NULL,1,17),
    (18,1,NULL,0.99,NULL,1,18),
    (19,1,NULL,0.99,NULL,1,19),
    (20,1,NULL,0.99,NULL,1,20),
    (21,1,NULL,0.99,NULL,1,21),
    (22,1,NULL,0.99,NULL,1,22),
    (23,1,NULL,0.99,NULL,1,23),
    (24,1,NULL,0.99,NULL,1,24),
    (25,1,NULL,0.99,NULL,1,25),
    (26,1,NULL,0.99,NULL,1,26),
    (27,1,NULL,0.99,NULL,1,27),
    (28,1,NULL,1.0,NULL,1,28),
    (29,1,NULL,1.0,NULL,1,29),
    (30,1,NULL,1.0,NULL,1,30),
    (31,1,NULL,1.0,NULL,1,31),
    (32,1,NULL,1.0,NULL,1,32),

    (33,1,NULL,88.88,NULL,1,34),
    (34,1,NULL,88.88,NULL,1,35),
    (35,1,NULL,88.88,NULL,1,36),
    (36,1,NULL,88.88,NULL,1,37),
    (37,1,NULL,88.88,NULL,1,38),
    (38,1,NULL,88.88,NULL,1,39),
    (39,1,NULL,88.88,NULL,1,40),
    (40,1,NULL,88.88,NULL,1,41),

    (41,1,NULL,88.88,NULL,1,42),
    (42,1,NULL,88.88,NULL,1,43),
    (43,1,NULL,88.88,NULL,1,44),
    (44,1,NULL,88.88,NULL,1,45);
/*!40000 ALTER TABLE `game_period` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `game_type`
--

DROP TABLE IF EXISTS `game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_type`
--

LOCK TABLES `game_type` WRITE;
/*!40000 ALTER TABLE `game_type` DISABLE KEYS */;
INSERT INTO `game_type` (`id`, `name`, `is_default`) VALUES
    (1,'FREE',0),
    (2,'PAID',1),
    (3,'LITE',0);
/*!40000 ALTER TABLE `game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game`
--

DROP TABLE IF EXISTS `meta_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_inapp` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game`
--

LOCK TABLES `meta_game` WRITE;
/*!40000 ALTER TABLE `meta_game` DISABLE KEYS */;
INSERT INTO `meta_game` (`id`, `name`, `is_inapp`) VALUES
    (1,'Cut the Rope',0),
    (2,'Experiments',0),
    (3,'Pudding monsters',0),
    (4,'inapp',1),

    (5,'name A',0),
    (6,'name C',0),
    (7,'name B',1),
    (8,'name D',1);
/*!40000 ALTER TABLE `meta_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` (`id`, `name`) VALUES
    (1,'Android'),
    (2,'BlackBerry'),
    (3,'iOS');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` (`id`, `name`) VALUES
    (1,'North'),
    (2,'South'),
    (3,'Europe');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolution`
--

LOCK TABLES `resolution` WRITE;
/*!40000 ALTER TABLE `resolution` DISABLE KEYS */;
INSERT INTO `resolution` (`id`, `name`) VALUES
    (1,'HD'),
    (2,'SD'),
    (3,'DESKTOP');
/*!40000 ALTER TABLE `resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commission` double NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `platform_id` bigint(20) NOT NULL,
  `store_logo_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68AF8E1AE932C88` (`platform_id`),
  KEY `FK68AF8E110A0530F` (`store_logo_id`),
  CONSTRAINT `FK68AF8E110A0530F` FOREIGN KEY (`store_logo_id`) REFERENCES `store_logo` (`id`),
  CONSTRAINT `FK68AF8E1AE932C88` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`id`, `commission`, `name`, `platform_id`, `store_logo_id`) VALUES
    (1,0,'Ovi',1,1),
    (2,0,'BlackBerry',2,2),
    (3,0,'GetJar',3,3),
    (4,0.3,'Commission',1,1);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `date` bigint(20) NOT NULL,
  `downloads` int(11) NOT NULL,
  `refunds` int(11) NOT NULL,
  `updates` int(11) NOT NULL,
  `country_id` bigint(20) NOT NULL DEFAULT '0',
  `game_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`date`,`game_id`,`country_id`),
  KEY `FK36D98437770F28` (`game_id`),
  KEY `FK36D984DE6C1897` (`report_id`),
  KEY `FK36D984230CDCF` (`country_id`),
  CONSTRAINT `FK36D984230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `FK36D98437770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK36D984DE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` (`date`,`report_id`, `downloads`, `refunds`, `updates`, `country_id`, `game_id`) VALUES
(0,1,1,1,1,1,1),
(0,1,1,1,1,1,2),
(0,1,1,1,1,1,3),
(0,1,1,1,1,1,4),
(0,1,1,1,1,1,5),
(0,1,1,1,1,1,6),
(0,1,1,1,1,1,7),
(0,1,1,1,1,1,8),
(0,1,1,1,1,1,9),
(0,1,1,1,1,1,10),
(0,1,1,1,1,1,11),
(0,1,1,1,1,1,12),
(0,1,1,1,1,1,13),
(0,1,1,1,1,1,14),
(0,1,1,1,1,1,15),
(0,1,1,1,1,1,16),
(0,1,1,1,1,1,17),
(0,1,1,1,1,1,18),
(0,1,1,1,1,1,19),
(0,1,1,1,1,1,20),
(0,1,1,1,1,1,21),
(0,1,1,1,1,1,22),
(0,1,1,1,1,1,23),
(0,1,1,1,1,1,24),
(0,1,1,1,1,1,25),
(0,1,1,1,1,1,26),
(0,1,1,1,1,1,27),

(0,1,1,1,1,2,1),
(0,1,1,1,1,2,2),
(0,1,1,1,1,2,3),
(0,1,1,1,1,2,4),
(0,1,1,1,1,2,5),
(0,1,1,1,1,2,6),
(0,1,1,1,1,2,7),
(0,1,1,1,1,2,8),
(0,1,1,1,1,2,9),
(0,1,1,1,1,2,10),
(0,1,1,1,1,2,11),
(0,1,1,1,1,2,12),
(0,1,1,1,1,2,13),
(0,1,1,1,1,2,14),
(0,1,1,1,1,2,15),
(0,1,1,1,1,2,16),
(0,1,1,1,1,2,17),
(0,1,1,1,1,2,18),
(0,1,1,1,1,2,19),
(0,1,1,1,1,2,20),
(0,1,1,1,1,2,21),
(0,1,1,1,1,2,22),
(0,1,1,1,1,2,23),
(0,1,1,1,1,2,24),
(0,1,1,1,1,2,25),
(0,1,1,1,1,2,26),
(0,1,1,1,1,2,27),

(0,1,1,1,1,3,1),
(0,1,1,1,1,3,2),
(0,1,1,1,1,3,3),
(0,1,1,1,1,3,4),
(0,1,1,1,1,3,5),
(0,1,1,1,1,3,6),
(0,1,1,1,1,3,7),
(0,1,1,1,1,3,8),
(0,1,1,1,1,3,9),
(0,1,1,1,1,3,10),
(0,1,1,1,1,3,11),
(0,1,1,1,1,3,12),
(0,1,1,1,1,3,13),
(0,1,1,1,1,3,14),
(0,1,1,1,1,3,15),
(0,1,1,1,1,3,16),
(0,1,1,1,1,3,17),
(0,1,1,1,1,3,18),
(0,1,1,1,1,3,19),
(0,1,1,1,1,3,20),
(0,1,1,1,1,3,21),
(0,1,1,1,1,3,22),
(0,1,1,1,1,3,23),
(0,1,1,1,1,3,24),
(0,1,1,1,1,3,25),
(0,1,1,1,1,3,26),
(0,1,1,1,1,3,27),

(86400000,1,3,5,7,1,1),

(172800000,1,1,1,1,1,28),
(172800000,1,1,1,1,1,29),
(172800000,1,1,1,1,1,30),
(172800000,1,1,1,1,1,31),

(259200000,1,1,1,1,1,32),

(432000000,1,2,4,6,1,1),
(604800000,1,7,8,9,1,1),

(691200000,1,3,5,7,1,1),
(691200000,1,4,6,8,1,33),

(777600000,1,2,4,6,1,1),
(777600000,1,3,5,7,1,33),

(950400000,1,2,1,3,1,1),
(1036800000,1,2,1,3,1,1),
(1123200000,1,2,1,3,1,1),
(1209600000,1,2,1,3,1,1),
(1296000000,1,2,1,3,1,1),
(1382400000,1,2,1,3,1,1),
(1468800000,1,2,1,3,1,1),
(1641600000,1,2,1,3,1,1),
(1814400000,1,2,1,3,1,1),
(1987200000,1,2,1,3,1,1),

(5097600000,1,1,2,3,1,1),
(5184000000,1,1,2,3,1,1),
(5270400000,1,1,2,3,1,1),
(5356800000,1,1,2,3,1,1),
(5443200000,1,1,2,3,1,1),
(5529600000,1,1,2,3,1,1),
(5616000000,1,1,2,3,1,1),
(5702400000,1,1,2,3,1,1),
(5788800000,1,1,2,3,1,1),
(5875200000,1,1,2,3,1,1),
(5961600000,1,1,2,3,1,1),
(6048000000,1,1,2,3,1,1),
(6134400000,1,1,2,3,1,1),
(6220800000,1,1,2,3,1,1),
(6307200000,1,1,2,3,1,1),
(6393600000,1,1,2,3,1,1),
(6480000000,1,1,2,3,1,1),
(6566400000,1,1,2,3,1,1),
(6652800000,1,1,2,3,1,1),
(6739200000,1,1,2,3,1,1),
(6825600000,1,1,2,3,1,1),
(6912000000,1,1,2,3,1,1),
(6998400000,1,1,2,3,1,1),
(7084800000,1,1,2,3,1,1),
(7171200000,1,1,2,3,1,1),
(7257600000,1,1,2,3,1,1),
(7344000000,1,1,2,3,1,1),
(7430400000,1,1,2,3,1,1),
(7516800000,1,1,2,3,1,1),
(7603200000,1,1,2,3,1,1),
(7689600000,1,1,2,3,1,1),
(7862400000,1,1,2,3,1,1),
(8553600000,1,1,2,3,1,1),
(9417600000,1,1,2,3,1,1),

(9504000000,1,0,0,0,1,1),

(315532800000,1,7,7,7,1,34),
(315532800000,1,7,7,7,1,35),
(315532800000,1,7,7,7,1,36),
(315532800000,1,7,7,7,1,37),
(315532800000,1,7,7,7,1,38),
(315532800000,1,7,7,7,1,39),
(315532800000,1,7,7,7,1,40),
(315532800000,1,7,7,7,1,41),
(315532800000,1,8,8,8,2,34),
(315532800000,1,8,8,8,2,35),
(315532800000,1,8,8,8,2,36),
(315532800000,1,8,8,8,2,37),
(315532800000,1,9,9,9,3,38),
(315532800000,1,9,9,9,3,39),
(315532800000,1,9,9,9,3,40),
(315532800000,1,9,9,9,3,41),

(315619200000,1,2,2,2,1,34),
(315619200000,1,2,2,2,1,35),
(315619200000,1,2,2,2,1,36),
(315619200000,1,2,2,2,1,37),
(315619200000,1,2,2,2,1,38),
(315619200000,1,2,2,2,1,39),
(315619200000,1,2,2,2,1,40),
(315619200000,1,2,2,2,1,41),
(315619200000,1,3,3,3,2,34),
(315619200000,1,3,3,3,2,35),
(315619200000,1,3,3,3,2,36),
(315619200000,1,3,3,3,2,37),
(315619200000,1,4,4,4,3,38),
(315619200000,1,4,4,4,3,39),
(315619200000,1,4,4,4,3,40),
(315619200000,1,4,4,4,3,41),

(315705600000,1,4,4,4,3,42),
(315705600000,1,5,5,5,3,43),
(315705600000,1,6,6,6,3,44),
(315705600000,1,7,7,7,3,45),
(315792000000,1,8,8,8,3,42),
(315792000000,1,9,9,9,3,43),
(315792000000,1,3,3,3,3,44),
(315792000000,1,2,2,2,3,45),

(315878400000,1,1,1,1,1,1),
(315878400000,1,2,2,2,2,2),
(315878400000,1,3,3,3,3,3)
;
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `royalty` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `name`, `royalty`) VALUES
    (1,'Chillingo',0.2);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_ui`
--

DROP TABLE IF EXISTS `report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_ui` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `login_page_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `unit_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `finance_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `code` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'fake',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_ui`
--

LOCK TABLES `report_ui` WRITE;
/*!40000 ALTER TABLE `report_ui` DISABLE KEYS */;
INSERT INTO `report_ui` (`id`, `login`, `name`, `password`, `login_page_domain`, `unit_domain`, `finance_domain`, `code`) VALUES
    (1,'ZeptoLab','Chillingo','OmNom123','http://www.clickgamer.com','http://www.clickgamer.com','http://www.clickgamer.com','chillingo'),
    (2,'konstantin.kolotyuk@7bits.it','ITunes','7bitsitunes','https://itunesconnect.apple.com','https://itunesconnect.apple.com','https://itunesconnect.apple.com','itunes'),
    (3,'accounts@zeptolab.com','GetJar','ZLGetJarPass','https://developer.getjar.com','https://developer.getjar.com','https://developer.getjar.com','getjar');
/*!40000 ALTER TABLE `report_ui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `sign` varchar(5) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `name`, `sign`, `is_default`) VALUES
    (1,'USD','USD',1),
    (2,'GBP','GBP',0),
    (3,'CAD','CAD',0);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_logo`
--

DROP TABLE IF EXISTS `store_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_logo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_logo`
--

LOCK TABLES `store_logo` WRITE;
/*!40000 ALTER TABLE `store_logo` DISABLE KEYS */;
INSERT INTO `store_logo` (`id`, `name`, `path`) VALUES
    (1,'app-store.png','/uploads/images/appstore/'),
    (2,'mac-app-store.png','/uploads/images/macappstore/'),
    (3,'google-play.png','/uploads/images/gp/');
/*!40000 ALTER TABLE `store_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cause` longtext COLLATE utf8_bin,
  `requested_date` bigint(20) NOT NULL,
  `manager_info_id` bigint(20) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `start_date` bigint(20) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL,
  `games_lack_check` tinyint(1) NOT NULL DEFAULT '0',
  `games_excess_check` tinyint(1) NOT NULL DEFAULT '0',
  `zero_check` tinyint(1) NOT NULL DEFAULT '0',
  `custom_validation` text COLLATE utf8_bin,
  `launch_type` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'RANGE',
  `parent_report_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC84C55344B312E68` (`manager_info_id`),
  KEY `FKC84C5534B60CCF42` (`parent_report_id`),
  CONSTRAINT `FKC84C5534B60CCF42` FOREIGN KEY (`parent_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FKC84C55344B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` (`id`, `cause`, `requested_date`, `manager_info_id`, `is_visible`, `start_date`, `end_date`, `status`, `games_lack_check`, `games_excess_check`, `zero_check`, `custom_validation`, `launch_type`, `parent_report_id`) VALUES
    (1,'cause',0,1,1,1350459410000,1350459510000,'SUCCESS',0,0,1,NULL,'USUAL',NULL),
    (2,NULL,2678400000,1,1,1350459410000,1350459510000,'SUCCESS',0,0,1,NULL,'USUAL',NULL);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_info`
--

DROP TABLE IF EXISTS `manager_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `report_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `required_product_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `cron` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `lag` bigint(20) DEFAULT NULL,
  `is_full_active` tinyint(1) NOT NULL,
  `comment` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `commentForSku` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `report_ui_id` (`report_ui_id`,`report_type`,`required_product_type`),
  KEY `FK492AD200133D4251` (`report_ui_id`),
  CONSTRAINT `FK492AD200133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_info`
--

LOCK TABLES `manager_info` WRITE;
/*!40000 ALTER TABLE `manager_info` DISABLE KEYS */;
INSERT INTO `manager_info` (`id`, `is_active`, `name`, `report_type`, `report_ui_id`, `required_product_type`, `cron`, `is_deleted`, `lag`, `is_full_active`, `comment`, `commentForSku`) VALUES
    (1,0,'itunes_finance_manager','FINANCE',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (2,0,'itunes_unit_manager','UNIT',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (3,0,'google_finance_manager','FINANCE',3,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (4,0,'google_app_unit_manager','UNIT',3,'APP',NULL,0,NULL,1,NULL,NULL),
    (5,0,'amazon_finance_manager','FINANCE',4,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (6,0,'amazon_app_unit_manager','UNIT',4,'APP',NULL,0,NULL,1,NULL,NULL),
    (7,0,'chillingo_finance_manager','FINANCE',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (8,0,'chillingo_unit_manager','UNIT',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (9,0,'ovi_finance_manager','FINANCE',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (10,0,'ovi_unit_manager','UNIT',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (11,0,'nook_finance_manager','FINANCE',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (12,0,'nook_unit_manager','UNIT',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (13,0,'blackberry_finance_manager','FINANCE',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (14,0,'blackberry_unit_manager','UNIT',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (15,0,'getjar_unit_manager','UNIT',8,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (16,0,'google_in_app_unit_manager','UNIT',3,'IN_APP',NULL,0,NULL,1,NULL,NULL),
    (17,0,'amazon_in_app_unit_manager','UNIT',4,'IN_APP',NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `manager_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-14 11:50:35
