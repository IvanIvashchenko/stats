-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: zeptostats
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_bin NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `is_top` tinyint(1) NOT NULL,
  `region_id` bigint(20) NOT NULL,
  `ranking_title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK3917579627BCC2C5` (`region_id`),
  CONSTRAINT `FK3917579627BCC2C5` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`, `code`, `name`, `is_top`, `region_id`, `ranking_title`) VALUES
    (1,'BR','BRZ',0,2,NULL),
    (2,'FR','FRC',1,3,NULL),
    (3,'US','USA',1,1,NULL);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extra` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `finance_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  `title` varchar(256) COLLATE utf8_bin NOT NULL,
  `unit_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  `parent_game_id` bigint(20) DEFAULT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `partner_id` bigint(20) DEFAULT NULL,
  `default_price` double DEFAULT NULL,
  `default_currency_id` bigint(20) DEFAULT NULL,
  `is_required_in_report` tinyint(1) NOT NULL DEFAULT '1',
  `ranking_sku` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK304BF2133D4251` (`report_ui_id`),
  KEY `FK304BF2469B7493` (`parent_game_id`),
  KEY `FK304BF2E9E03AED` (`game_type_id`),
  KEY `FK304BF23E50EF93` (`meta_game_id`),
  KEY `FK304BF287E4952C` (`store_id`),
  KEY `FK304BF235543B4C` (`partner_id`),
  KEY `FK304BF227A4E268` (`resolution_id`),
  KEY `FK304BF22AD7ACA` (`default_currency_id`),
  CONSTRAINT `FK304BF2133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`),
  CONSTRAINT `FK304BF227A4E268` FOREIGN KEY (`resolution_id`) REFERENCES `resolution` (`id`),
  CONSTRAINT `FK304BF22AD7ACA` FOREIGN KEY (`default_currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `FK304BF235543B4C` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`),
  CONSTRAINT `FK304BF23E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FK304BF2469B7493` FOREIGN KEY (`parent_game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK304BF287E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FK304BF2E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` (`id`, `extra`, `finance_sku`, `game_type_id`, `resolution_id`, `title`, `unit_sku`, `meta_game_id`, `parent_game_id`, `report_ui_id`, `store_id`, `partner_id`, `default_price`, `default_currency_id`, `is_required_in_report`, `ranking_sku`) VALUES
    (1,NULL,'finance',1,1,'Cut the Rope 1','unit',1,NULL,1,1,1,0.99,NULL,1,NULL),
    (2,NULL,'finance',1,1,'Cut the Rope 2','unit',1,NULL,1,2,1,0.99,NULL,1,NULL),
    (3,NULL,'finance',1,1,'Cut the Rope 3','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (4,NULL,'finance',1,2,'Cut the Rope 4','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (5,NULL,'finance',1,2,'Cut the Rope 5','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (6,NULL,'finance',1,2,'Cut the Rope 6','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (7,NULL,'finance',1,3,'Cut the Rope 7','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (8,NULL,'finance',1,3,'Cut the Rope 8','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (9,NULL,'finance',1,3,'Cut the Rope 9','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (10,NULL,'finance',2,1,'Cut the Rope 10','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (11,NULL,'finance',2,1,'Cut the Rope 11','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (12,NULL,'finance',2,1,'Cut the Rope 12','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (13,NULL,'finance',2,2,'Cut the Rope 13','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (14,NULL,'finance',2,2,'Cut the Rope 14','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (15,NULL,'finance',2,2,'Cut the Rope 15','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (16,NULL,'finance',2,3,'Cut the Rope 16','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (17,NULL,'finance',2,3,'Cut the Rope 17','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (18,NULL,'finance',2,3,'Cut the Rope 18','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (19,NULL,'finance',3,1,'Cut the Rope 19','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (20,NULL,'finance',3,1,'Cut the Rope 20','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (21,NULL,'finance',3,1,'Cut the Rope 21','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (22,NULL,'finance',3,2,'Cut the Rope 22','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (23,NULL,'finance',3,2,'Cut the Rope 23','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (24,NULL,'finance',3,2,'Cut the Rope 24','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL),
    (25,NULL,'finance',3,3,'Cut the Rope 25','unit',1,NULL,1,1,NULL,0.99,NULL,1,NULL),
    (26,NULL,'finance',3,3,'Cut the Rope 26','unit',1,NULL,1,2,NULL,0.99,NULL,1,NULL),
    (27,NULL,'finance',3,3,'Cut the Rope 27','unit',1,NULL,1,3,NULL,0.99,NULL,1,NULL);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_period`
--

DROP TABLE IF EXISTS `game_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_period` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `start_date` bigint(20) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `game_id` (`game_id`,`start_date`,`end_date`),
  KEY `FKFB2532AE39799FC8` (`currency_id`),
  KEY `FKFB2532AE37770F28` (`game_id`),
  CONSTRAINT `FKFB2532AE37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FKFB2532AE39799FC8` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=589 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_period`
--

LOCK TABLES `game_period` WRITE;
/*!40000 ALTER TABLE `game_period` DISABLE KEYS */;
INSERT INTO `game_period` (`id`, `is_active`, `end_date`, `price`, `start_date`, `currency_id`, `game_id`) VALUES
    (1,1,NULL,0.99,NULL,1,1),
    (2,1,NULL,0.99,NULL,1,2),
    (3,1,NULL,0.99,NULL,1,3),
    (4,1,NULL,0.99,NULL,1,4),
    (5,1,NULL,0.99,NULL,1,5),
    (6,1,NULL,0.99,NULL,1,6),
    (7,1,NULL,0.99,NULL,1,7),
    (8,1,NULL,0.99,NULL,1,8),
    (9,1,NULL,0.99,NULL,1,9),
    (10,1,NULL,0.99,NULL,1,10),
    (11,1,NULL,0.99,NULL,1,11),
    (12,1,NULL,0.99,NULL,1,12),
    (13,1,NULL,0.99,NULL,1,13),
    (14,1,NULL,0.99,NULL,1,14),
    (15,1,NULL,0.99,NULL,1,15),
    (16,1,NULL,0.99,NULL,1,16),
    (17,1,NULL,0.99,NULL,1,17),
    (18,1,NULL,0.99,NULL,1,18),
    (19,1,NULL,0.99,NULL,1,19),
    (20,1,NULL,0.99,NULL,1,20),
    (21,1,NULL,0.99,NULL,1,21),
    (22,1,NULL,0.99,NULL,1,22),
    (23,1,NULL,0.99,NULL,1,23),
    (24,1,NULL,0.99,NULL,1,24),
    (25,1,NULL,0.99,NULL,1,25),
    (26,1,NULL,0.99,NULL,1,26),
    (27,1,NULL,0.99,NULL,1,27);
/*!40000 ALTER TABLE `game_period` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `game_type`
--

DROP TABLE IF EXISTS `game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_type`
--

LOCK TABLES `game_type` WRITE;
/*!40000 ALTER TABLE `game_type` DISABLE KEYS */;
INSERT INTO `game_type` (`id`, `name`, `is_default`) VALUES
    (1,'FREE',0),
    (2,'PAID',1),
    (3,'LITE',0);
/*!40000 ALTER TABLE `game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game`
--

DROP TABLE IF EXISTS `meta_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_inapp` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game`
--

LOCK TABLES `meta_game` WRITE;
/*!40000 ALTER TABLE `meta_game` DISABLE KEYS */;
INSERT INTO `meta_game` (`id`, `name`, `is_inapp`) VALUES
    (1,'Cut the Rope',0),
    (2,'Experiments',0),
    (3,'Pudding monsters',0);
/*!40000 ALTER TABLE `meta_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` (`id`, `name`) VALUES
    (1,'Android'),
    (2,'BlackBerry'),
    (3,'iOS');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` (`id`, `name`) VALUES
    (1,'North'),
    (2,'South'),
    (3,'Europe');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolution`
--

LOCK TABLES `resolution` WRITE;
/*!40000 ALTER TABLE `resolution` DISABLE KEYS */;
INSERT INTO `resolution` (`id`, `name`) VALUES
    (1,'HD'),
    (2,'SD'),
    (3,'DESKTOP');
/*!40000 ALTER TABLE `resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commission` double NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `platform_id` bigint(20) NOT NULL,
  `store_logo_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68AF8E1AE932C88` (`platform_id`),
  KEY `FK68AF8E110A0530F` (`store_logo_id`),
  CONSTRAINT `FK68AF8E110A0530F` FOREIGN KEY (`store_logo_id`) REFERENCES `store_logo` (`id`),
  CONSTRAINT `FK68AF8E1AE932C88` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`id`, `commission`, `name`, `platform_id`, `store_logo_id`) VALUES
    (1,0.3,'Ovi',1,1),
    (2,0.3,'BlackBerry',2,2),
    (3,0.3,'GetJar',3,3);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `date` bigint(20) NOT NULL,
  `downloads` int(11) NOT NULL,
  `refunds` int(11) NOT NULL,
  `updates` int(11) NOT NULL,
  `country_id` bigint(20) NOT NULL DEFAULT '0',
  `game_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`date`,`game_id`,`country_id`),
  KEY `FK36D98437770F28` (`game_id`),
  KEY `FK36D984DE6C1897` (`report_id`),
  KEY `FK36D984230CDCF` (`country_id`),
  CONSTRAINT `FK36D984230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `FK36D98437770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK36D984DE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` (`date`,`report_id`, `downloads`, `refunds`, `updates`, `country_id`, `game_id`) VALUES
(0,1,1,1,1,1,1),
(0,1,1,1,1,1,2),
(0,1,1,1,1,1,3),
(0,1,1,1,1,1,4),
(0,1,1,1,1,1,5),
(0,1,1,1,1,1,6),
(0,1,1,1,1,1,7),
(0,1,1,1,1,1,8),
(0,1,1,1,1,1,9),
(0,1,1,1,1,1,10),
(0,1,1,1,1,1,11),
(0,1,1,1,1,1,12),
(0,1,1,1,1,1,13),
(0,1,1,1,1,1,14),
(0,1,1,1,1,1,15),
(0,1,1,1,1,1,16),
(0,1,1,1,1,1,17),
(0,1,1,1,1,1,18),
(0,1,1,1,1,1,19),
(0,1,1,1,1,1,20),
(0,1,1,1,1,1,21),
(0,1,1,1,1,1,22),
(0,1,1,1,1,1,23),
(0,1,1,1,1,1,24),
(0,1,1,1,1,1,25),
(0,1,1,1,1,1,26),
(0,1,1,1,1,1,27),

(0,1,1,1,1,2,1),
(0,1,1,1,1,2,2),
(0,1,1,1,1,2,3),
(0,1,1,1,1,2,4),
(0,1,1,1,1,2,5),
(0,1,1,1,1,2,6),
(0,1,1,1,1,2,7),
(0,1,1,1,1,2,8),
(0,1,1,1,1,2,9),
(0,1,1,1,1,2,10),
(0,1,1,1,1,2,11),
(0,1,1,1,1,2,12),
(0,1,1,1,1,2,13),
(0,1,1,1,1,2,14),
(0,1,1,1,1,2,15),
(0,1,1,1,1,2,16),
(0,1,1,1,1,2,17),
(0,1,1,1,1,2,18),
(0,1,1,1,1,2,19),
(0,1,1,1,1,2,20),
(0,1,1,1,1,2,21),
(0,1,1,1,1,2,22),
(0,1,1,1,1,2,23),
(0,1,1,1,1,2,24),
(0,1,1,1,1,2,25),
(0,1,1,1,1,2,26),
(0,1,1,1,1,2,27),

(0,1,1,1,1,3,1),
(0,1,1,1,1,3,2),
(0,1,1,1,1,3,3),
(0,1,1,1,1,3,4),
(0,1,1,1,1,3,5),
(0,1,1,1,1,3,6),
(0,1,1,1,1,3,7),
(0,1,1,1,1,3,8),
(0,1,1,1,1,3,9),
(0,1,1,1,1,3,10),
(0,1,1,1,1,3,11),
(0,1,1,1,1,3,12),
(0,1,1,1,1,3,13),
(0,1,1,1,1,3,14),
(0,1,1,1,1,3,15),
(0,1,1,1,1,3,16),
(0,1,1,1,1,3,17),
(0,1,1,1,1,3,18),
(0,1,1,1,1,3,19),
(0,1,1,1,1,3,20),
(0,1,1,1,1,3,21),
(0,1,1,1,1,3,22),
(0,1,1,1,1,3,23),
(0,1,1,1,1,3,24),
(0,1,1,1,1,3,25),
(0,1,1,1,1,3,26),
(0,1,1,1,1,3,27),

(2710741000,1,10,20,30,1,1),

(5097600000,1,10,10,10,1,1),
(5184000000,1,20,20,20,1,1),
(5270400000,1,30,30,30,1,1),
(5356800000,1,40,40,40,1,1),
(5443200000,1,50,50,50,1,1),
(5529600000,1,60,60,60,1,1),
(5616000000,1,70,70,70,1,1),
(5702400000,1,80,80,80,1,1),
(5788800000,1,90,90,90,1,1),
(5875200000,1,100,100,100,1,1),
(5961600000,1,110,110,110,1,1),
(6048000000,1,120,120,120,1,1),
(6134400000,1,130,130,130,1,1),
(6220800000,1,10,10,10,2,2),
(6307200000,1,20,20,20,2,2),
(6393600000,1,30,30,30,2,2),
(6480000000,1,40,40,40,2,2),
(6566400000,1,50,50,50,2,2),
(6652800000,1,60,60,60,2,2),
(6739200000,1,70,70,70,2,2),
(6825600000,1,80,80,80,2,2),
(6912000000,1,90,90,90,2,2),
(6998400000,1,100,100,100,2,2),
(7084800000,1,110,110,110,2,2),
(7171200000,1,120,120,120,2,2),
(7257600000,1,130,130,130,2,2),
(7344000000,1,140,140,140,2,2),
(7430400000,1,150,150,150,2,2),
(7516800000,1,160,160,160,2,2),
(7603200000,1,170,170,170,2,2),
(7689600000,1,180,180,180,2,2),

(7776000000,1,10,10,10,1,1),
(7862400000,1,20,20,20,1,1),
(7948800000,1,30,30,30,1,1),
(8035200000,1,40,40,40,1,1),
(8121600000,1,50,50,50,1,1),
(8208000000,1,60,60,60,1,1),
(8294400000,1,70,70,70,1,1),
(8380800000,1,80,80,80,1,1),
(8467200000,1,90,90,90,1,1),
(8553600000,1,100,100,100,1,1),
(8640000000,1,110,110,110,1,1),
(8726400000,1,120,120,120,1,1),
(8812800000,1,130,130,130,1,1),
(8899200000,1,140,140,140,1,1),
(8985600000,1,150,150,150,1,1),
(9072000000,1,160,160,160,1,1),
(9158400000,1,170,170,170,1,1),
(9244800000,1,180,180,180,1,1),
(9331200000,1,190,190,190,1,1),
(9417600000,1,200,200,200,1,1),
(9504000000,1,10,10,10,2,2),
(9590400000,1,20,20,20,2,2),
(9676800000,1,30,30,30,2,2),
(9763200000,1,40,40,40,2,2),
(9849600000,1,50,50,50,2,2),
(9936000000,1,60,60,60,2,2),
(10022400000,1,70,70,70,2,2),
(10108800000,1,80,80,80,2,2),
(10195200000,1,90,90,90,2,2),
(10281600000,1,100,100,100,2,2)
;
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `royalty` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `name`, `royalty`) VALUES
    (1,'Chillingo',0.3);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_ui`
--

DROP TABLE IF EXISTS `report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_ui` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `login_page_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `unit_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `finance_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `code` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'fake',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_ui`
--

LOCK TABLES `report_ui` WRITE;
/*!40000 ALTER TABLE `report_ui` DISABLE KEYS */;
INSERT INTO `report_ui` (`id`, `login`, `name`, `password`, `login_page_domain`, `unit_domain`, `finance_domain`, `code`) VALUES
    (1,'ZeptoLab','Chillingo','OmNom123','http://www.clickgamer.com','http://www.clickgamer.com','http://www.clickgamer.com','chillingo'),
    (2,'konstantin.kolotyuk@7bits.it','ITunes','7bitsitunes','https://itunesconnect.apple.com','https://itunesconnect.apple.com','https://itunesconnect.apple.com','itunes'),
    (3,'accounts@zeptolab.com','GetJar','ZLGetJarPass','https://developer.getjar.com','https://developer.getjar.com','https://developer.getjar.com','getjar');
/*!40000 ALTER TABLE `report_ui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `sign` varchar(5) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `name`, `sign`, `is_default`) VALUES
    (1,'USD','USD',1),
    (2,'GBP','GBP',0),
    (3,'CAD','CAD',0);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_logo`
--

DROP TABLE IF EXISTS `store_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_logo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_logo`
--

LOCK TABLES `store_logo` WRITE;
/*!40000 ALTER TABLE `store_logo` DISABLE KEYS */;
INSERT INTO `store_logo` (`id`, `name`, `path`) VALUES
    (1,'app-store.png','/uploads/images/appstore/'),
    (2,'mac-app-store.png','/uploads/images/macappstore/'),
    (3,'google-play.png','/uploads/images/gp/');
/*!40000 ALTER TABLE `store_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cause` longtext COLLATE utf8_bin,
  `requested_date` bigint(20) NOT NULL,
  `manager_info_id` bigint(20) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `start_date` bigint(20) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL,
  `games_lack_check` tinyint(1) NOT NULL DEFAULT '0',
  `games_excess_check` tinyint(1) NOT NULL DEFAULT '0',
  `zero_check` tinyint(1) NOT NULL DEFAULT '0',
  `custom_validation` text COLLATE utf8_bin,
  `launch_type` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'RANGE',
  `parent_report_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC84C55344B312E68` (`manager_info_id`),
  KEY `FKC84C5534B60CCF42` (`parent_report_id`),
  CONSTRAINT `FKC84C5534B60CCF42` FOREIGN KEY (`parent_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FKC84C55344B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` (`id`, `cause`, `requested_date`, `manager_info_id`, `is_visible`, `start_date`, `end_date`, `status`, `games_lack_check`, `games_excess_check`, `zero_check`, `custom_validation`, `launch_type`, `parent_report_id`) VALUES
    (1,'cause',0,1,1,1350459410000,1350459510000,'SUCCESS',0,0,1,NULL,'USUAL',NULL),
    (2,NULL,2678400000,1,1,1350459410000,1350459510000,'SUCCESS',0,0,1,NULL,'USUAL',NULL);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_info`
--

DROP TABLE IF EXISTS `manager_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `report_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `required_product_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `cron` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `lag` bigint(20) DEFAULT NULL,
  `is_full_active` tinyint(1) NOT NULL,
  `comment` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `commentForSku` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `report_ui_id` (`report_ui_id`,`report_type`,`required_product_type`),
  KEY `FK492AD200133D4251` (`report_ui_id`),
  CONSTRAINT `FK492AD200133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_info`
--

LOCK TABLES `manager_info` WRITE;
/*!40000 ALTER TABLE `manager_info` DISABLE KEYS */;
INSERT INTO `manager_info` (`id`, `is_active`, `name`, `report_type`, `report_ui_id`, `required_product_type`, `cron`, `is_deleted`, `lag`, `is_full_active`, `comment`, `commentForSku`) VALUES
    (1,0,'itunes_finance_manager','FINANCE',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (2,0,'itunes_unit_manager','UNIT',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (3,0,'google_finance_manager','FINANCE',3,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (4,0,'google_app_unit_manager','UNIT',3,'APP',NULL,0,NULL,1,NULL,NULL),
    (5,0,'amazon_finance_manager','FINANCE',4,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (6,0,'amazon_app_unit_manager','UNIT',4,'APP',NULL,0,NULL,1,NULL,NULL),
    (7,0,'chillingo_finance_manager','FINANCE',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (8,0,'chillingo_unit_manager','UNIT',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (9,0,'ovi_finance_manager','FINANCE',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (10,0,'ovi_unit_manager','UNIT',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (11,0,'nook_finance_manager','FINANCE',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (12,0,'nook_unit_manager','UNIT',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (13,0,'blackberry_finance_manager','FINANCE',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (14,0,'blackberry_unit_manager','UNIT',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (15,0,'getjar_unit_manager','UNIT',8,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (16,0,'google_in_app_unit_manager','UNIT',3,'IN_APP',NULL,0,NULL,1,NULL,NULL),
    (17,0,'amazon_in_app_unit_manager','UNIT',4,'IN_APP',NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `manager_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-14 12:04:36
