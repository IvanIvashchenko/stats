-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: zeptostats
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

SET FOREIGN_KEY_CHECKS = 0;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `date` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_bin NOT NULL,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  `is_top` tinyint(1) NOT NULL,
  `region_id` bigint(20) NOT NULL,
  `ranking_title` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`),
  KEY `FK3917579627BCC2C5` (`region_id`),
  CONSTRAINT `FK3917579627BCC2C5` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=256 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` (`id`, `code`, `name`, `is_top`, `region_id`, `ranking_title`) VALUES
    (1,'US','United States',1,1,NULL),
    (2,'AF','Afghanistan',0,8,NULL),
    (3,'AL','Albania',1,5,NULL),
    (4,'DZ','Algeria',1,4,NULL),
    (5,'AS','American Samoa',0,3,NULL),
    (6,'AD','Andorra',1,5,NULL),
    (7,'AO','Angola',0,4,NULL),
    (8,'AI','Anguilla',0,7,NULL),
    (9,'AG','Antigua and Barbuda',0,7,NULL),
    (10,'AR','Argentina',1,2,NULL),
    (11,'AM','Armenia',1,6,NULL),
    (12,'AW','Aruba',0,7,NULL),
    (13,'AU','Australia',1,3,NULL),
    (14,'AT','Austria',1,5,NULL),
    (15,'AZ','Azerbaijan',1,6,NULL),
    (16,'BS','Bahamas',0,7,NULL),
    (17,'BH','Bahrain',1,8,NULL),
    (18,'BD','Bangladesh',0,6,NULL),
    (19,'BB','Barbados',0,7,NULL),
    (20,'BY','Belarus',1,5,NULL),
    (21,'BE','Belgium',1,5,NULL),
    (22,'BZ','Belize',1,7,NULL),
    (23,'BJ','Benin',0,4,NULL),
    (24,'BM','Bermuda',0,7,NULL),
    (25,'BT','Bhutan',0,6,NULL),
    (26,'BO','Bolivia',1,2,NULL),
    (27,'BA','Bosnia and Herzegovina',0,5,NULL),
    (28,'BW','Botswana',0,4,NULL),
    (29,'BR','Brazil',1,2,NULL),
    (30,'VG','British Virgin Islands',0,7,NULL),
    (31,'BN','Brunei Darussalam',0,6,NULL),
    (32,'BG','Bulgaria',1,5,NULL),
    (33,'BF','Burkina Faso',0,4,NULL),
    (34,'BI','Burundi',0,4,NULL),
    (35,'KH','Cambodia',0,6,NULL),
    (36,'CM','Cameroon',0,4,NULL),
    (37,'CA','Canada',1,1,NULL),
    (38,'CV','Cape Verde',0,4,NULL),
    (39,'KY','Cayman Islands',0,7,NULL),
    (40,'CF','Central African Republic',0,4,NULL),
    (41,'TD','Chad',0,4,NULL),
    (42,'CL','Chile',1,2,NULL),
    (43,'CN','China',1,6,NULL),
    (44,'CO','Colombia',1,2,NULL),
    (45,'KM','Comoros',0,4,NULL),
    (46,'CD','Congo, Democratic Republic',0,9,NULL),
    (47,'CK','Cook Islands',0,3,NULL),
    (48,'CR','Costa Rica',0,7,NULL),
    (49,'CI','Cote d`Ivoire',0,4,NULL),
    (50,'HR','Croatia',1,5,NULL),
    (51,'CU','Cuba',0,7,NULL),
    (52,'CY','Cyprus',1,5,NULL),
    (53,'CZ','Czech Republic',1,5,NULL),
    (54,'DK','Denmark',1,5,NULL),
    (55,'DJ','Djibouti',0,4,NULL),
    (56,'DM','Dominica',0,7,NULL),
    (57,'DO','Dominican Republic',1,7,NULL),
    (58,'EC','Ecuador',1,2,NULL),
    (59,'EG','Egypt',1,4,NULL),
    (60,'SV','El Salvador',1,7,NULL),
    (61,'GQ','Equatorial Guinea',0,4,NULL),
    (62,'ER','Eritrea',0,4,NULL),
    (63,'EE','Estonia',1,5,NULL),
    (64,'ET','Ethiopia',0,4,NULL),
    (65,'FO','Faeroe Islands',0,5,NULL),
    (66,'FK','Falkland Islands',0,2,NULL),
    (67,'FJ','Fiji',0,3,NULL),
    (68,'FI','Finland',1,5,NULL),
    (69,'FR','France',1,5,NULL),
    (70,'PF','French Polynesia',0,3,NULL),
    (71,'GA','Gabon',0,4,NULL),
    (72,'GM','Gambia',0,4,NULL),
    (73,'GE','Georgia',1,6,NULL),
    (74,'DE','Germany',1,5,NULL),
    (75,'GH','Ghana',1,4,NULL),
    (76,'GI','Gibraltar',0,5,NULL),
    (77,'GR','Greece',1,5,NULL),
    (78,'GL','Greenland',0,1,NULL),
    (79,'GD','Grenada',0,7,NULL),
    (80,'GP','Guadaloupe',0,7,NULL),
    (81,'GU','Guam',0,3,NULL),
    (82,'GN','Guinea',0,4,NULL),
    (83,'GW','Guinea-Bissau',0,4,NULL),
    (84,'GY','Guyana',0,2,NULL),
    (85,'HT','Haiti',0,7,NULL),
    (86,'VA','Vatican City',0,5,NULL),
    (87,'HN','Honduras',1,7,NULL),
    (88,'HK','Hong Kong',1,6,NULL),
    (89,'HU','Hungary',1,5,NULL),
    (90,'IS','Iceland',1,5,NULL),
    (91,'IN','India',1,6,NULL),
    (92,'ID','Indonesia',1,6,NULL),
    (93,'IR','Iran',0,8,NULL),
    (94,'IQ','Iraq',0,8,NULL),
    (95,'IE','Ireland',1,5,NULL),
    (96,'IL','Israel',1,8,NULL),
    (97,'IT','Italy',1,5,NULL),
    (98,'JM','Jamaica',0,7,NULL),
    (99,'JP','Japan',1,6,NULL),
    (100,'JO','Jordan',1,8,NULL),
    (101,'KZ','Kazakkstan',1,6,NULL),
    (102,'KE','Kenya',1,4,NULL),
    (103,'KI','Kiribati',0,3,NULL),
    (104,'KP','Korea Democratic People',1,9,NULL),
    (105,'KW','Kuwait',1,8,NULL),
    (106,'KG','Kyrgyzstan',0,8,NULL),
    (107,'LA','Lao People',0,9,NULL),
    (108,'LV','Latvia',1,5,NULL),
    (109,'LB','Lebanon',0,8,NULL),
    (110,'LS','Lesotho',0,4,NULL),
    (111,'LR','Liberia',0,4,NULL),
    (112,'LY','Libya',1,4,NULL),
    (113,'LI','Liechtenstein',1,5,NULL),
    (114,'LT','Lithuania',1,5,NULL),
    (115,'LU','Luxembourg',1,5,NULL),
    (116,'MO','Macao',1,6,NULL),
    (117,'MK','Macedonia',1,5,NULL),
    (118,'MG','Madagascar',0,4,NULL),
    (119,'MW','Malawi',0,4,NULL),
    (120,'MY','Malaysia',0,6,NULL),
    (121,'MV','Maldives',0,6,NULL),
    (122,'ML','Mali',0,4,NULL),
    (123,'MT','Malta',1,5,NULL),
    (124,'MH','Marshall Islands',0,3,NULL),
    (125,'MQ','Martinique',0,7,NULL),
    (126,'MR','Mauritania',0,4,NULL),
    (127,'MU','Mauritius',1,9,NULL),
    (128,'YT','Mayotte',0,4,NULL),
    (129,'MX','Mexico',1,1,NULL),
    (130,'FM','Micronesia',0,3,NULL),
    (131,'MD','Moldova',1,5,NULL),
    (132,'MC','Monaco',0,5,NULL),
    (133,'MN','Mongolia',0,6,NULL),
    (134,'MS','Montserrat',0,7,NULL),
    (135,'MA','Morocco',0,4,NULL),
    (136,'MZ','Mozambique',0,4,NULL),
    (137,'MM','Myanmar',1,6,NULL),
    (138,'NA','Namibia',0,4,NULL),
    (139,'NR','Nauru',0,3,NULL),
    (140,'NP','Nepal',0,6,NULL),
    (141,'NL','Netherlands',1,5,NULL),
    (142,'AN','Netherlands Antilles',0,2,NULL),
    (143,'NC','New Caledonia',0,3,NULL),
    (144,'NZ','New Zealand',1,3,NULL),
    (145,'NI','Nicaragua',0,7,NULL),
    (146,'NE','Niger',0,4,NULL),
    (147,'NG','Nigeria',1,4,NULL),
    (148,'NU','Niue',0,3,NULL),
    (149,'MP','Northern Mariana Islands',0,3,NULL),
    (150,'NO','Norway',1,5,NULL),
    (151,'OM','Oman',1,8,NULL),
    (152,'PK','Pakistan',1,8,NULL),
    (153,'PW','Palau',0,3,NULL),
    (154,'PS','Palestinian Territory',0,8,NULL),
    (155,'PA','Panama',1,7,NULL),
    (156,'PG','Papua New Guinea',0,3,NULL),
    (157,'PY','Paraguay',1,2,NULL),
    (158,'PE','Peru',1,2,NULL),
    (159,'PH','Philippines',1,6,NULL),
    (160,'PL','Poland',1,5,NULL),
    (161,'PT','Portugal',1,5,NULL),
    (162,'PR','Puerto Rico',0,7,NULL),
    (163,'QA','Qatar',0,8,NULL),
    (164,'RE','Reunion',0,4,NULL),
    (165,'RO','Romania',1,5,NULL),
    (166,'RU','Russia',1,6,NULL),
    (167,'RW','Rwanda',0,4,NULL),
    (168,'WS','Samoa',0,9,NULL),
    (169,'SM','San Marino',0,5,NULL),
    (170,'ST','Sao Tome and Principe',0,4,NULL),
    (171,'SA','Saudi Arabia',1,8,NULL),
    (172,'SN','Senegal',0,4,NULL),
    (173,'RS','Serbia',1,5,NULL),
    (174,'ME','Montenegro',1,5,NULL),
    (175,'SC','Seychelles',0,4,NULL),
    (176,'SL','Sierra Leone',0,4,NULL),
    (177,'GT','Guatemala',1,7,NULL),
    (178,'SG','Singapore',1,6,NULL),
    (179,'SK','Slovakia',1,5,NULL),
    (180,'SI','Slovenia',1,5,NULL),
    (181,'SB','Solomon Islands',0,3,NULL),
    (182,'SO','Somalia',0,4,NULL),
    (183,'ZA','South Africa',0,4,NULL),
    (184,'KR','South Korea',0,6,NULL),
    (185,'ES','Spain',1,5,NULL),
    (186,'LK','Sri Lanka',1,6,NULL),
    (187,'SH','St. Helena',0,4,NULL),
    (188,'KN','St. Kitts And Nevis',0,9,NULL),
    (189,'LC','St. Lucia',0,7,NULL),
    (190,'PM','St. Pierre and Miquelon',0,7,NULL),
    (191,'VC','St. Vincent and the Grenadines',0,7,NULL),
    (192,'SD','Sudan',0,4,NULL),
    (193,'SR','Suriname',0,2,NULL),
    (194,'SZ','Swaziland',0,4,NULL),
    (195,'SE','Sweden',1,5,NULL),
    (196,'CH','Switzerland',1,5,NULL),
    (197,'SY','Syria',0,8,NULL),
    (198,'TW','Taiwan',0,6,NULL),
    (199,'TJ','Tajikistan',0,8,NULL),
    (200,'TZ','Tanzania',1,4,NULL),
    (201,'TH','Thailand',1,6,NULL),
    (202,'TL','Timor-Leste',0,6,NULL),
    (203,'TG','Togo',0,4,NULL),
    (204,'TK','Tokelau',0,3,NULL),
    (205,'TO','Tonga',0,3,NULL),
    (206,'TT','Trinidad and Tobago',0,7,NULL),
    (207,'TN','Tunisia',0,4,NULL),
    (208,'TR','Turkey',1,8,NULL),
    (209,'TM','Turkmenistan',0,8,NULL),
    (210,'TC','Turks and Caicos Islands',0,7,NULL),
    (211,'TV','Tuvalu',0,3,NULL),
    (212,'UG','Uganda',0,4,NULL),
    (213,'UA','Ukraine',1,5,NULL),
    (214,'AE','United Arab Emirates',1,8,NULL),
    (215,'GB','United Kingdom',1,5,NULL),
    (216,'UY','Uruguay',1,2,NULL),
    (217,'VI','US Virgin Islands',0,7,NULL),
    (218,'UZ','Uzbekistan',0,8,NULL),
    (219,'VU','Vanuatu',0,3,NULL),
    (220,'VE','Venezuela',1,2,NULL),
    (221,'VN','Viet Nam',1,6,NULL),
    (222,'WF','Wallis and Futuna Islands',0,3,NULL),
    (223,'YE','Yemen',1,8,NULL),
    (224,'ZM','Zambia',0,4,NULL),
    (225,'ZW','Zimbabwe',0,4,NULL),
    (226,'EH','Western Sahara',0,4,NULL),
    (227,'?1','Unknown Asia',0,9,NULL),
    (228,'?2','Unknown Europe',0,9,NULL),
    (229,'?3','Unknown Latin America',0,9,NULL),
    (230,'?4','Unknown Middle East',0,9,NULL),
    (231,'?5','Unknown Korea',0,9,NULL),
    (232,'?6','Unknown Serbia and Montenegro',0,9,NULL),
    (233,'AX','Aland Islands',0,9,NULL),
    (234,'AQ','Antarctica',0,9,NULL),
    (235,'BV','Bouvet Island',0,9,NULL),
    (236,'IO','British Indian Ocean Territory',0,9,NULL),
    (237,'GF','French Guiana',0,9,NULL),
    (238,'GG','Guernsey',0,9,NULL),
    (239,'HM','Heard Island and McDonald Islands',0,9,NULL),
    (240,'IM','Isle of Man',0,9,NULL),
    (241,'JE','Jersey',0,9,NULL),
    (242,'NF','Norfolk Island',0,9,NULL),
    (243,'PN','Pitcairn',0,9,NULL),
    (244,'GS','South Georgia and the South Sandwich Islands',0,9,NULL),
    (245,'UM','United States Minor Outlying Islands',0,9,NULL),
    (246,'CG','Congo',0,9,NULL),
    (247,'--','ALL',0,9,NULL),
    (248,'CW','Curacao',0,9,NULL),
    (249,'ZZ','Unknown or Invalid Territory',0,9,NULL),
    (250,'UN','UN001',0,9,NULL),
    (251,'CX','St. Maarten',0,9,NULL),
    (252,'MF','Saint Martin',0,9,NULL),
    (253,'SX','Sint Maarten',0,9,NULL),
    (254,'SS','South Sudan',0,4,NULL),
    (255,'BQ','British Antarctic Territory',0,9,NULL);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_alias`
--

DROP TABLE IF EXISTS `country_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_alias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(50) COLLATE utf8_bin NOT NULL,
  `country_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`),
  KEY `FKB086C867230CDCF` (`country_id`),
  CONSTRAINT `FKB086C867230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country_alias`
--

LOCK TABLES `country_alias` WRITE;
/*!40000 ALTER TABLE `country_alias` DISABLE KEYS */;
INSERT INTO `country_alias` (`id`, `alias`, `country_id`) VALUES
    (2,'United States',1),
    (4,'Afghanistan',2),
    (6,'Albania',3),
    (8,'Algeria',4),
    (10,'American Samoa',5),
    (12,'Andorra',6),
    (14,'Angola',7),
    (16,'Anguilla',8),
    (18,'Antigua and Barbuda',9),
    (20,'Argentina',10),
    (22,'Armenia',11),
    (24,'Aruba',12),
    (26,'Australia',13),
    (28,'Austria',14),
    (30,'Azerbaijan',15),
    (32,'Bahamas',16),
    (34,'Bahrain',17),
    (36,'Bangladesh',18),
    (38,'Barbados',19),
    (40,'Belarus',20),
    (42,'Belgium',21),
    (44,'Belize',22),
    (46,'Benin',23),
    (48,'Bermuda',24),
    (50,'Bhutan',25),
    (52,'Bolivia',26),
    (54,'Bosnia and Herzegovina',27),
    (56,'Botswana',28),
    (58,'Brazil',29),
    (60,'British Virgin Islands',30),
    (62,'Brunei Darussalam',31),
    (63,'Brunei',31),
    (65,'Bulgaria',32),
    (67,'Burkina Faso',33),
    (69,'Burundi',34),
    (71,'Cambodia',35),
    (73,'Cameroon',36),
    (75,'Canada',37),
    (77,'Cape Verde',38),
    (79,'Cayman Islands',39),
    (81,'Central African Republic',40),
    (83,'Chad',41),
    (85,'Chile',42),
    (87,'China',43),
    (89,'Colombia',44),
    (91,'Comoros',45),
    (93,'Congo, Democratic Republic',46),
    (94,'Republic of the Congo',246),
    (96,'Cook Islands',47),
    (98,'Costa Rica',48),
    (100,'Cote d`Ivoire',49),
    (101,'Côte dIvoire',49),
    (103,'Croatia',50),
    (105,'Cuba',51),
    (107,'Cyprus',52),
    (109,'Czech Republic',53),
    (111,'Denmark',54),
    (113,'Djibouti',55),
    (114,'Dijbouti',55),
    (116,'Dominica',56),
    (118,'Dominican Republic',57),
    (120,'Ecuador',58),
    (122,'Egypt',59),
    (124,'El Salvador',60),
    (126,'Equatorial Guinea',61),
    (128,'Eritrea',62),
    (130,'Estonia',63),
    (132,'Ethiopia',64),
    (134,'Faeroe Islands',65),
    (135,'Faroe Islands',65),
    (137,'Falkland Islands',66),
    (139,'Fiji',67),
    (141,'Finland',68),
    (143,'France',69),
    (145,'French Polynesia',70),
    (147,'Gabon',71),
    (149,'Gambia',72),
    (151,'Georgia',73),
    (153,'Germany',74),
    (155,'Ghana',75),
    (157,'Gibraltar',76),
    (159,'Greece',77),
    (161,'Greenland',78),
    (163,'Grenada',79),
    (165,'Guadaloupe',80),
    (166,'Guadeloupe',80),
    (168,'Guam',81),
    (170,'Guinea',82),
    (172,'Guinea-Bissau',83),
    (174,'Guyana',84),
    (176,'Haiti',85),
    (178,'Vatican City',86),
    (180,'Honduras',87),
    (182,'Hong Kong',88),
    (184,'Hungary',89),
    (186,'Iceland',90),
    (188,'India',91),
    (190,'Indonesia',92),
    (192,'Iran',93),
    (194,'Iraq',94),
    (196,'Ireland',95),
    (198,'Israel',96),
    (200,'Italy',97),
    (202,'Jamaica',98),
    (204,'Japan',99),
    (206,'Jordan',100),
    (208,'Kazakkstan',101),
    (209,'Kazakhstan',101),
    (211,'Kenya',102),
    (213,'Kiribati',103),
    (215,'Korea Democratic People',104),
    (217,'Kuwait',105),
    (219,'Kyrgyzstan',106),
    (221,'Lao People',107),
    (222,'Laos',107),
    (224,'Latvia',108),
    (226,'Lebanon',109),
    (228,'Lesotho',110),
    (230,'Liberia',111),
    (232,'Libya',112),
    (234,'Liechtenstein',113),
    (236,'Lithuania',114),
    (238,'Luxembourg',115),
    (240,'Macao',116),
    (241,'Macau',116),
    (243,'Macedonia',117),
    (245,'Madagascar',118),
    (247,'Malawi',119),
    (249,'Malaysia',120),
    (251,'Maldives',121),
    (253,'Mali',122),
    (255,'Malta',123),
    (256,'Republic of Malta',123),
    (258,'Marshall Islands',124),
    (260,'Martinique',125),
    (262,'Mauritania',126),
    (264,'Mauritius',127),
    (266,'Mayotte',128),
    (268,'Mexico',129),
    (270,'Micronesia',130),
    (272,'Moldova',131),
    (273,'Republic of Moldova',131),
    (275,'Monaco',132),
    (277,'Mongolia',133),
    (279,'Montserrat',134),
    (281,'Morocco',135),
    (283,'Mozambique',136),
    (285,'Myanmar',137),
    (287,'Namibia',138),
    (289,'Nauru',139),
    (291,'Nepal',140),
    (293,'Netherlands',141),
    (295,'Netherlands Antilles',142),
    (297,'New Caledonia',143),
    (299,'New Zealand',144),
    (301,'Nicaragua',145),
    (303,'Niger',146),
    (305,'Nigeria',147),
    (307,'Niue',148),
    (309,'Northern Mariana Islands',149),
    (311,'Norway',150),
    (313,'Oman',151),
    (315,'Pakistan',152),
    (317,'Palau',153),
    (319,'Palestinian Territory',154),
    (321,'Panama',155),
    (323,'Papua New Guinea',156),
    (325,'Paraguay',157),
    (327,'Peru',158),
    (329,'Philippines',159),
    (330,'Phillipines',159),
    (332,'Poland',160),
    (334,'Portugal',161),
    (336,'Puerto Rico',162),
    (338,'Qatar',163),
    (340,'Reunion',164),
    (341,'Réunion',164),
    (343,'Romania',165),
    (345,'Russia',166),
    (346,'Russian Federation',166),
    (348,'Rwanda',167),
    (350,'Samoa',168),
    (352,'San Marino',169),
    (354,'Sao Tome and Principe',170),
    (356,'Saudi Arabia',171),
    (358,'Senegal',172),
    (360,'Serbia',173),
    (362,'Montenegro',174),
    (364,'Seychelles',175),
    (366,'Sierra Leone',176),
    (368,'Guatemala',177),
    (370,'Singapore',178),
    (372,'Slovakia',179),
    (374,'Slovenia',180),
    (376,'Solomon Islands',181),
    (378,'Somalia',182),
    (380,'South Africa',183),
    (382,'South Korea',184),
    (384,'Spain',185),
    (386,'Sri Lanka',186),
    (388,'St. Helena',187),
    (390,'St. Kitts And Nevis',188),
    (391,'Saint Kitts and Nevis',188),
    (393,'St. Lucia',189),
    (394,'Saint Lucia',189),
    (396,'St. Pierre and Miquelon',190),
    (397,'Saint Pierre and Miquelon',190),
    (399,'St. Vincent and the Grenadines',191),
    (400,'Saint Vincent and the Grenadines',191),
    (402,'Sudan',192),
    (404,'Suriname',193),
    (406,'Swaziland',194),
    (408,'Sweden',195),
    (410,'Switzerland',196),
    (412,'Syria',197),
    (414,'Taiwan',198),
    (416,'Tajikistan',199),
    (418,'Tanzania',200),
    (420,'Thailand',201),
    (422,'Timor-Leste',202),
    (424,'Togo',203),
    (426,'Tokelau',204),
    (428,'Tonga',205),
    (430,'Trinidad and Tobago',206),
    (432,'Tunisia',207),
    (433,'Tunesia',207),
    (435,'Turkey',208),
    (437,'Turkmenistan',209),
    (439,'Turks and Caicos Islands',210),
    (441,'Tuvalu',211),
    (443,'Uganda',212),
    (445,'Ukraine',213),
    (447,'United Arab Emirates',214),
    (449,'United Kingdom',215),
    (451,'Uruguay',216),
    (453,'US Virgin Islands',217),
    (455,'Uzbekistan',218),
    (457,'Vanuatu',219),
    (459,'Venezuela',220),
    (461,'Viet Nam',221),
    (462,'Vietnam',221),
    (464,'Wallis and Futuna Islands',222),
    (466,'Yemen',223),
    (468,'Zambia',224),
    (470,'Zimbabwe',225),
    (472,'Western Sahara',226),
    (473,'UNITEDSTATES',1),
    (474,'NEWZEALAND',144),
    (475,'HONGKONG',88),
    (476,'SOUTHAFRICA',183),
    (477,'CZECHREPUBLIC',53),
    (479,'OTHERASIA',227),
    (480,'OTHEREUROPE',228),
    (481,'OTHERLATINAMERICA',229),
    (482,'MIDDLEEAST',230),
    (483,'KOREA',231),
    (485,'United States of America',1),
    (486,'Lao Peoples Democratic Rep.',107),
    (487,'Aland Islands',233),
    (488,'Antarctica',234),
    (489,'Bouvet Island',235),
    (490,'British Indian Ocean Territory',236),
    (491,'British Virgin Island',30),
    (492,'The Democratic Republic Of The Congo',46),
    (493,'Congo',246),
    (494,'Falkland Islands (malvinas)',66),
    (495,'French Guiana',237),
    (496,'Guernsey',238),
    (497,'Heard Island And Mcdonald Islands',239),
    (498,'Holy See (vatican City State)',86),
    (499,'Isle Of Man',240),
    (500,'Jersey',241),
    (501,'Democratic People`s Republic Of Korea',104),
    (502,'Republic of Korea',184),
    (503,'Lao People`s Democratic Republic',107),
    (504,'Libyan Arab Jamahiriya',112),
    (505,'Macedonia (FYROM)',117),
    (506,'Norfolk Island',242),
    (507,'Pitcairn',243),
    (508,'South Georgia And The South Sandwich Islands',244),
    (509,'Syrian Arab Republic',197),
    (510,'US virgin Island',217),
    (511,'United States Minor Outlying Islands',245),
    (512,'Wallis And Futuna',222),
    (513,'Serbia And Montenegro',232),
    (514,'ALL',247),
    (515,'Curacao',248),
    (523,'UN001',250),
    (526,'ivory coast / côte d`ivoire',49),
    (527,'ivory coast',49),
    (528,'unknown',249),
    (529,'hong kong sar',88),
    (530,'Amazon.com',1),
    (531,'Amazon.co.uk',215),
    (532,'Amazon.de',74),
    (533,'Amazon.fr',69),
    (534,'Amazon.it',97),
    (535,'Amazon.es',185),
    (536,'Virgin Islands, British',30),
    (537,'Virgin Islands, U.S.',217),
    (538,'Macedonia, Former Yugoslav Republic of',117),
    (540,'St. Maarten',251),
    (541,'Macao SAR',116),
    (542,'Amazon.co.jp',99),
    (543,'UK',215),
    (544,'CS',232),
    (545,'Saint Martin',252),
    (546,'Saint Vincent and the Grenadine',191),
    (547,'St. Vincent and the Grenadine',191);
/*!40000 ALTER TABLE `country_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `sign` varchar(5) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `name`, `sign`, `is_default`) VALUES
    (1,'USD','USD',1),
    (2,'GBP','GBP',0),
    (3,'CAD','CAD',0);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance`
--

DROP TABLE IF EXISTS `finance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` bigint(20) NOT NULL,
  `revenue` double NOT NULL,
  `start_date` bigint(20) NOT NULL,
  `country_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCD244FDA37770F28` (`game_id`),
  KEY `FKCD244FDADE6C1897` (`report_id`),
  KEY `FKCD244FDA230CDCF` (`country_id`),
  CONSTRAINT `FKCD244FDA230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `FKCD244FDA37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FKCD244FDADE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance`
--

LOCK TABLES `finance` WRITE;
/*!40000 ALTER TABLE `finance` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extra` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `finance_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  `title` varchar(256) COLLATE utf8_bin NOT NULL,
  `unit_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  `parent_game_id` bigint(20) DEFAULT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `partner_id` bigint(20) DEFAULT NULL,
  `default_price` double DEFAULT NULL,
  `default_currency_id` bigint(20) DEFAULT NULL,
  `is_required_in_report` tinyint(1) NOT NULL DEFAULT '1',
  `ranking_sku` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK304BF2133D4251` (`report_ui_id`),
  KEY `FK304BF2469B7493` (`parent_game_id`),
  KEY `FK304BF2E9E03AED` (`game_type_id`),
  KEY `FK304BF23E50EF93` (`meta_game_id`),
  KEY `FK304BF287E4952C` (`store_id`),
  KEY `FK304BF235543B4C` (`partner_id`),
  KEY `FK304BF227A4E268` (`resolution_id`),
  KEY `FK304BF22AD7ACA` (`default_currency_id`),
  CONSTRAINT `FK304BF2133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`),
  CONSTRAINT `FK304BF227A4E268` FOREIGN KEY (`resolution_id`) REFERENCES `resolution` (`id`),
  CONSTRAINT `FK304BF22AD7ACA` FOREIGN KEY (`default_currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `FK304BF235543B4C` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`),
  CONSTRAINT `FK304BF23E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FK304BF2469B7493` FOREIGN KEY (`parent_game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK304BF287E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FK304BF2E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` (`id`, `extra`, `finance_sku`, `game_type_id`, `resolution_id`, `title`, `unit_sku`, `meta_game_id`, `parent_game_id`, `report_ui_id`, `store_id`, `partner_id`, `default_price`, `default_currency_id`, `is_required_in_report`, `ranking_sku`) VALUES
    (16,NULL,'CUT THE ROPE HD',2,1,'Cut the Rope HD','394610743',1,NULL,1,2,1,1.99,NULL,1,NULL),
    (17,NULL,'CUT THE ROPE',2,2,'Cut the Rope','380293530',1,NULL,1,2,1,0.99,NULL,1,NULL),
    (18,NULL,NULL,1,2,'Cut the Rope: Holiday Gift','406513121',5,NULL,1,2,1,0,NULL,1,NULL),
    (19,NULL,NULL,1,2,'Cut the Rope Free','394613472',1,NULL,1,2,1,NULL,NULL,1,NULL),
    (20,NULL,NULL,1,1,'Cut the Rope HD Free','394611607',1,NULL,1,2,1,NULL,NULL,1,NULL),
    (21,NULL,'com.chillingo.cuttherope.unlockalllevels',2,2,'Unlock all Boxes','447066599',6,17,1,2,1,0.99,NULL,1,NULL),
    (22,NULL,'com.chillingo.cuttheropehd.unlockallboxes',2,1,'Unlock all Boxes','447066909',6,16,1,2,1,0.99,NULL,1,NULL),
    (23,NULL,NULL,2,1,'Superpowers 20','545657227',7,17,1,2,1,NULL,NULL,1,NULL),
    (24,NULL,NULL,2,1,'Superpowers 50','545663504',8,17,1,2,1,NULL,NULL,1,NULL),
    (25,NULL,NULL,2,1,'Superpowers 150','545663506',9,17,1,2,1,NULL,NULL,1,NULL),
    (26,NULL,NULL,2,1,'Superpowers 500','545663953',10,17,1,2,1,NULL,NULL,1,NULL),
    (27,NULL,NULL,2,1,'Superpowers 20','545664545',7,16,1,2,1,NULL,NULL,1,NULL),
    (28,NULL,NULL,2,1,'Superpowers 50','545665041',8,16,1,2,1,NULL,NULL,1,NULL),
    (29,NULL,NULL,2,1,'Superpowers 150','545665043',9,16,1,2,1,NULL,NULL,1,NULL),
    (30,NULL,NULL,2,1,'Superpowers 500','545665045',10,16,1,2,1,NULL,NULL,1,NULL),
    (31,NULL,NULL,1,2,'Cut the Rope: Comic','com.zeptolab.ctrcomic.1',3,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (32,NULL,'com.zeptolab.ctrcomic.2',2,2,'Cut the Rope: Comic - #2','com.zeptolab.ctrcomic.2',11,31,2,2,NULL,NULL,NULL,1,NULL),
    (33,NULL,'com.zeptolab.ctrcomic.3',2,2,'Cut the Rope: Comic - #3','com.zeptolab.ctrcomic.3',12,31,2,2,NULL,NULL,NULL,1,NULL),
    (34,NULL,'com.zeptolab.ctrexperiments.1',2,2,'Cut the Rope: Experiments','com.zeptolab.ctrexperiments.1',4,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (35,NULL,'com.zeptolab.ctrbonus.superpower1',2,2,'Superpowers 20','com.zeptolab.ctrbonus.superpower1',13,34,2,2,NULL,NULL,NULL,1,NULL),
    (36,NULL,'com.zeptolab.ctrbonus.superpower2',2,2,'Superpowers 50','com.zeptolab.ctrbonus.superpower2',14,34,2,2,NULL,NULL,NULL,1,NULL),
    (37,NULL,'com.zeptolab.ctrbonus.superpower3',2,2,'Superpowers 150','com.zeptolab.ctrbonus.superpower3',15,34,2,2,NULL,NULL,NULL,1,NULL),
    (38,NULL,'com.zeptolab.ctrbonus.superpower4',2,2,'Superpowers 500','com.zeptolab.ctrbonus.superpower4',16,34,2,2,NULL,NULL,NULL,1,NULL),
    (39,NULL,'com.zeptolab.ctrexperimentshd.1',2,1,'Cut the Rope: Experiments HD','com.zeptolab.ctrexperimentshd.1',4,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (40,NULL,'com.zeptolab.ctrbonushd.superpower1',2,1,'Superpowers 20','com.zeptolab.ctrbonushd.superpower1',13,39,2,2,NULL,NULL,NULL,1,NULL),
    (41,NULL,'com.zeptolab.ctrbonushd.superpower2',2,1,'Superpowers 50','com.zeptolab.ctrbonushd.superpower2',14,39,2,2,NULL,NULL,NULL,1,NULL),
    (42,NULL,'com.zeptolab.ctrbonushd.superpower3',2,1,'Superpowers 150','com.zeptolab.ctrbonushd.superpower3',15,39,2,2,NULL,NULL,NULL,1,NULL),
    (43,NULL,'com.zeptolab.ctrbonushd.superpower4',2,1,'Superpowers 500','com.zeptolab.ctrbonushd.superpower4',16,39,2,2,NULL,NULL,NULL,1,NULL),
    (44,NULL,NULL,1,1,'Cut the Rope: Experiments Free HD','com.zeptolab.ctrexperimentshdfree.1',4,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (45,NULL,NULL,1,2,'Cut the Rope: Experiments Free','com.zeptolab.ctrexperimentsfree.1',4,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (61,NULL,'CTR_MAC',2,3,'Cut the Rope','CTR_MAC',1,NULL,2,3,NULL,NULL,NULL,1,NULL),
    (62,NULL,'com.zeptolab.ctr.paid',2,2,'Cut the Rope','com.zeptolab.ctr.paid',1,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (63,'8C',NULL,1,2,'Cut the Rope Free 8C','com.zeptolab.ctr.ads',1,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (64,NULL,'com.zeptolab.ctrexperiments.google.paid',2,2,'Cut the Rope Experiments','com.zeptolab.ctrexperiments.google.paid',4,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (65,NULL,'com.zeptolab.ctrexperiments.hd.google.paid',2,1,'Cut the Rope Experiments HD','com.zeptolab.ctrexperiments.hd.google.paid',4,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (66,NULL,'com.zeptolab.ctr.hd.google.paid',2,1,'Cut the Rope HD','com.zeptolab.ctr.hd.google.paid',1,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (67,'GP',NULL,1,2,'Cut the Rope Google Play Free','com.zeptolab.ctr.lite.google',1,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (68,NULL,NULL,1,1,'Cut the Rope HD Free','com.zeptolab.ctr.hd.lite.google',1,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (69,'8C',NULL,1,2,'Cut the Rope: Experiments Free 8C','com.zeptolab.ctrexperiments.ads',4,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (70,NULL,'com.zeptolab.ctr.paid_star_key',2,2,'Star Key','com.zeptolab.ctr.paid_star_key',17,62,3,4,NULL,NULL,NULL,1,NULL),
    (71,NULL,'com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp1',2,2,'Superpowers 20','com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp1',13,64,3,4,NULL,NULL,NULL,1,NULL),
    (72,NULL,'com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp2',2,2,'Superpowers 50','com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp2',14,64,3,4,NULL,NULL,NULL,1,NULL),
    (73,NULL,'com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp3',2,2,'Superpowers 150','com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp3',15,64,3,4,NULL,NULL,NULL,1,NULL),
    (74,NULL,'com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp4',2,2,'Superpowers 500','com.zeptolab.ctrexperiments.google.paid_ctr_exp_sp4',16,64,3,4,NULL,NULL,NULL,1,NULL),
    (79,NULL,'com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp1',2,1,'Superpowers 20','com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp1',13,65,3,4,NULL,NULL,NULL,1,NULL),
    (80,NULL,'com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp2',2,1,'Superpowers 50','com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp2',14,65,3,4,NULL,NULL,NULL,1,NULL),
    (81,NULL,'com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp3',2,1,'Superpowers 150','com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp3',15,65,3,4,NULL,NULL,NULL,1,NULL),
    (82,NULL,'com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp4',2,1,'Superpowers 500','com.zeptolab.ctrexperiments.hd.google.paid_ctr_exp_sp4',16,65,3,4,NULL,NULL,NULL,1,NULL),
    (83,NULL,'Cut the Rope',2,2,'Cut the Rope','Cut the Rope',1,NULL,4,5,NULL,NULL,NULL,1,NULL),
    (84,NULL,'CTR_HD',2,1,'Cut the Rope HD','Cut the Rope HD (Kindle Tablet Edition)',1,NULL,4,5,NULL,NULL,NULL,1,NULL),
    (85,NULL,'Cut the Rope: Experiments',2,2,'Cut the Rope Experiments','Cut the Rope: Experiments',4,NULL,4,5,NULL,NULL,NULL,1,NULL),
    (86,NULL,'Cut the Rope: Experiments HD (Kindle Tablet Edition)',2,1,'Cut the Rope Experiments HD','Cut the Rope: Experiments HD (Kindle Tablet Edition)',4,NULL,4,5,NULL,NULL,NULL,1,NULL),
    (87,NULL,'star-key',2,2,'Star Key','star-key',17,83,4,5,NULL,NULL,NULL,1,NULL),
    (88,NULL,'star-key-hd',2,1,'Star Key','star-key-hd',17,84,4,5,NULL,NULL,NULL,1,NULL),
    (89,NULL,'ctre_superpower1',2,2,'Superpowers 20','ctre_superpower1',13,85,4,5,NULL,NULL,NULL,1,NULL),
    (90,NULL,'ctre_superpower2',2,2,'Superpowers 50','ctre_superpower2',14,85,4,5,NULL,NULL,NULL,1,NULL),
    (91,NULL,'ctre_superpower3',2,2,'Superpowers 150','ctre_superpower3',15,85,4,5,NULL,NULL,NULL,1,NULL),
    (92,NULL,'ctre_superpower4',2,2,'Superpowers 500','ctre_superpower4',16,85,4,5,NULL,NULL,NULL,1,NULL),
    (93,NULL,'2940043884855',2,1,'Cut the Rope HD','2940043884855',1,NULL,5,6,NULL,NULL,NULL,1,NULL),
    (94,NULL,'2940043890474',2,1,'Cut the Rope: Experiments HD','2940043890474',4,NULL,5,6,NULL,NULL,NULL,1,NULL),
    (95,NULL,'258167',2,2,'Cut the Rope','258167',1,NULL,6,7,NULL,NULL,NULL,1,NULL),
    (96,NULL,'Cut the Rope HD',2,1,'Cut the Rope HD','Cut the Rope HD',1,NULL,7,8,NULL,NULL,NULL,1,NULL),
    (97,NULL,NULL,1,2,'Cut the Rope Free','413435',1,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (98,NULL,NULL,1,2,'Cut the Rope: Experiments Free','412534',4,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (99,NULL,NULL,2,2,'Cut the Rope','75206',1,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (100,NULL,NULL,2,2,'Cut the Rope HD','518668',1,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (101,NULL,NULL,1,2,'Cut the Rope HD Lite','518667',1,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (102,NULL,NULL,1,2,'Cut the Rope Lite','518666',1,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (103,NULL,NULL,2,2,'Cut the Rope Experiments','518665',4,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (104,NULL,NULL,2,2,'Cut the Rope Experiments HD','518669',4,NULL,8,9,NULL,NULL,NULL,1,NULL),
    (105,NULL,'ctrehd_superpower1',2,1,'Superpowers 20','ctrehd_superpower1',13,86,4,5,NULL,NULL,NULL,1,NULL),
    (106,NULL,'ctrehd_superpower2',2,1,'Superpowers 50','ctrehd_superpower2',14,86,4,5,NULL,NULL,NULL,1,NULL),
    (107,NULL,'ctrehd_superpower3',2,1,'Superpowers 150','ctrehd_superpower3',15,86,4,5,NULL,NULL,NULL,1,NULL),
    (108,NULL,'ctrehd_superpower4',2,1,'Superpowers 500','ctrehd_superpower4',16,86,4,5,NULL,NULL,NULL,1,NULL),
    (109,NULL,'com.zeptolab.ctr.paid_ctr_sp1',2,2,'Superpowers 20','com.zeptolab.ctr.paid_ctr_sp1',7,62,3,4,NULL,NULL,NULL,1,NULL),
    (110,NULL,'com.zeptolab.ctr.paid_ctr_sp2',2,2,'Superpowers 50','com.zeptolab.ctr.paid_ctr_sp2',8,62,3,4,NULL,NULL,NULL,1,NULL),
    (111,NULL,'com.zeptolab.ctr.paid_ctr_sp3',2,2,'Superpowers 150','com.zeptolab.ctr.paid_ctr_sp3',9,62,3,4,NULL,NULL,NULL,1,NULL),
    (112,NULL,'com.zeptolab.ctr.paid_ctr_sp4',2,2,'Superpowers 500','com.zeptolab.ctr.paid_ctr_sp4',10,62,3,4,NULL,NULL,NULL,1,NULL),
    (113,NULL,'com.zeptolab.ctr.ads_ctr_sp1',2,2,'Superpowers 20','com.zeptolab.ctr.ads_ctr_sp1',7,63,3,4,NULL,NULL,NULL,1,NULL),
    (114,NULL,'com.zeptolab.ctr.ads_ctr_sp2',2,2,'Superpowers 50','com.zeptolab.ctr.ads_ctr_sp2',8,63,3,4,NULL,NULL,NULL,1,NULL),
    (115,NULL,'com.zeptolab.ctr.ads_ctr_sp3',2,2,'Superpowers 150','com.zeptolab.ctr.ads_ctr_sp3',9,63,3,4,NULL,NULL,NULL,1,NULL),
    (116,NULL,'com.zeptolab.ctr.ads_ctr_sp4',2,2,'Superpowers 500','com.zeptolab.ctr.ads_ctr_sp4',10,63,3,4,NULL,NULL,NULL,1,NULL),
    (117,NULL,'com.zeptolab.ctr.hd.google.paid_star_key',2,1,'Star Key','com.zeptolab.ctr.hd.google.paid_star_key',17,66,3,4,NULL,NULL,NULL,1,NULL),
    (118,NULL,'com.zeptolab.ctr.hd.google.paid_ctr_sp1',2,1,'Superpowers 20','com.zeptolab.ctr.hd.google.paid_ctr_sp1',7,66,3,4,NULL,NULL,NULL,1,NULL),
    (119,NULL,'com.zeptolab.ctr.hd.google.paid_ctr_sp2',2,1,'Superpowers 50','com.zeptolab.ctr.hd.google.paid_ctr_sp2',8,66,3,4,NULL,NULL,NULL,1,NULL),
    (120,NULL,'com.zeptolab.ctr.hd.google.paid_ctr_sp3',2,1,'Superpowers 150','com.zeptolab.ctr.hd.google.paid_ctr_sp3',9,66,3,4,NULL,NULL,NULL,1,NULL),
    (121,NULL,'com.zeptolab.ctr.hd.google.paid_ctr_sp4',2,1,'Superpowers 500','com.zeptolab.ctr.hd.google.paid_ctr_sp4',10,66,3,4,NULL,NULL,NULL,1,NULL),
    (122,NULL,'com.zeptolab.ctrexperiments.ads_ctr_exp_sp1',2,2,'Superpowers 20','com.zeptolab.ctrexperiments.ads_ctr_exp_sp1',13,69,3,4,NULL,NULL,NULL,1,NULL),
    (123,NULL,'com.zeptolab.ctrexperiments.ads_ctr_exp_sp2',2,2,'Superpowers 50','com.zeptolab.ctrexperiments.ads_ctr_exp_sp2',14,69,3,4,NULL,NULL,NULL,1,NULL),
    (124,NULL,'com.zeptolab.ctrexperiments.ads_ctr_exp_sp3',2,2,'Superpowers 150','com.zeptolab.ctrexperiments.ads_ctr_exp_sp3',15,69,3,4,NULL,NULL,NULL,1,NULL),
    (125,NULL,'com.zeptolab.ctrexperiments.ads_ctr_exp_sp4',2,2,'Superpowers 500','com.zeptolab.ctrexperiments.ads_ctr_exp_sp4',16,69,3,4,NULL,NULL,NULL,1,NULL),
    (126,NULL,'ctre-star-key',2,2,'Star Key','ctre-star-key',18,85,4,5,NULL,NULL,NULL,1,NULL),
    (127,NULL,'ctre-star-key-hd',2,1,'Star Key','ctre-star-key-hd',18,86,4,5,NULL,NULL,NULL,1,NULL),
    (128,NULL,'ctrhd_superpower1',2,1,'Superpowers 20','ctrhd_superpower1',7,84,4,5,NULL,NULL,NULL,1,NULL),
    (129,NULL,'ctrhd_superpower2',2,1,'Superpowers 50','ctrhd_superpower2',8,84,4,5,NULL,NULL,NULL,1,NULL),
    (130,NULL,'ctrhd_superpower3',2,1,'Superpowers 150','ctrhd_superpower3',9,84,4,5,NULL,NULL,NULL,1,NULL),
    (131,NULL,'ctrhd_superpower4',2,1,'Superpowers 500','ctrhd_superpower4',10,84,4,5,NULL,NULL,NULL,1,NULL),
    (132,NULL,'_fake_finance_sku_',2,2,'Generic Chillingo Game For Refunds',NULL,19,NULL,1,2,1,NULL,NULL,0,NULL),
    (133,NULL,'com.chillingo.cut.the.rope.iap.all',2,2,'Generic Chillingo SD InApp',NULL,20,17,1,2,1,NULL,NULL,1,NULL),
    (134,NULL,'com.chillingo.cut.the.rope.hd.iap.all',2,1,'Generic Chillingo HD InApp',NULL,20,16,1,2,1,NULL,NULL,1,NULL),
    (135,NULL,'com.zeptolab.ctrexperiments.google.paid_ctrexp_star_key',2,2,'Star Key','com.zeptolab.ctrexperiments.google.paid_ctrexp_star_key',18,64,3,4,NULL,NULL,NULL,1,NULL),
    (136,NULL,'com.zeptolab.ctrexperiments.hd.google.paid_ctrexp_star_key',2,1,'Star Key','com.zeptolab.ctrexperiments.hd.google.paid_ctrexp_star_key',18,65,3,4,NULL,NULL,NULL,1,NULL),
    (137,NULL,'com.zeptolab.ctr.ads_star_key',2,2,'Star Key','com.zeptolab.ctr.ads_star_key',17,63,3,4,NULL,NULL,NULL,1,NULL),
    (138,NULL,'com.zeptolab.ctrexperiments.ads_ctrexpfree_star_key',2,2,'Star Key','com.zeptolab.ctrexperiments.ads_ctrexpfree_star_key',18,69,3,4,NULL,NULL,NULL,1,NULL),
    (139,NULL,'DEFAULT_PACKAGE',2,2,'DEFAULT_TITLE','DEFAULT_PACKAGE',21,NULL,3,4,NULL,NULL,NULL,0,NULL),
    (140,NULL,'ctr_superpower1',2,2,'Superpower 20','ctr_superpower1',7,83,4,5,NULL,1.99,1,1,NULL),
    (141,NULL,'ctr_superpower2',2,2,'Superpower 50','ctr_superpower2',8,83,4,5,NULL,3.99,1,1,NULL),
    (142,NULL,'ctr_superpower3',2,2,'Superpower 150','ctr_superpower3',9,83,4,5,NULL,7.99,1,1,NULL),
    (143,NULL,'ctr_superpower4',2,2,'Superpower 500','ctr_superpower4',10,83,4,5,NULL,17.99,1,1,NULL),
    (144,NULL,'com.zeptolab.monsters.free.google',1,2,'Pudding Monsters','com.zeptolab.monsters.free.google',22,NULL,3,4,NULL,0,NULL,1,NULL),
    (145,NULL,'com.zeptolab.monsters.paid.google',2,1,'Pudding Monsters HD','com.zeptolab.monsters.paid.google',22,NULL,3,4,NULL,NULL,NULL,1,NULL),
    (146,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.full_version',2,2,'Pudding Monsters: Disable Ads','com.zeptolab.monsters.free.google_com.zeptolab.monsters.full_version',23,144,3,4,NULL,NULL,NULL,1,NULL),
    (147,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp1',2,2,'Mushroom Boost 6','com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp1',24,144,3,4,NULL,NULL,NULL,1,NULL),
    (148,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp2',2,2,'Mushroom Boost 12','com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp2',25,144,3,4,NULL,NULL,NULL,1,NULL),
    (149,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp3',2,2,'Mushroom Boost 25','com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp3',26,144,3,4,NULL,NULL,NULL,1,NULL),
    (150,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp4',2,2,'Mushroom Boost 50','com.zeptolab.monsters.free.google_com.zeptolab.monsters.sp4',27,144,3,4,NULL,NULL,NULL,1,NULL),
    (151,NULL,'com.zeptolab.monsters.free.google_com.zeptolab.monsters.key',2,2,'Pudding Monsters: Unlock all Episodes','com.zeptolab.monsters.free.google_com.zeptolab.monsters.key',28,144,3,4,NULL,NULL,NULL,1,NULL),
    (152,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.full_version',2,1,'Pudding Monsters: Disable Ads','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.full_version',23,145,3,4,NULL,NULL,NULL,1,NULL),
    (153,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp1',2,1,'Mushroom Boost 6','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp1',24,145,3,4,NULL,NULL,NULL,1,NULL),
    (154,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp2',2,1,'Mushroom Boost 12','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp2',25,145,3,4,NULL,NULL,NULL,1,NULL),
    (155,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp3',2,1,'Mushroom Boost 25','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp3',26,145,3,4,NULL,NULL,NULL,1,NULL),
    (156,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp4',2,1,'Mushroom Boost 50','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.sp4',27,145,3,4,NULL,NULL,NULL,1,NULL),
    (157,NULL,'com.zeptolab.monsters.paid.google_com.zeptolab.monsters.key',2,1,'Pudding Monsters: Unlock all Episodes','com.zeptolab.monsters.paid.google_com.zeptolab.monsters.key',28,145,3,4,NULL,NULL,NULL,1,NULL),
    (158,NULL,'com.zeptolab.monsters.1',2,2,'Pudding Monsters','com.zeptolab.monsters.1',22,NULL,2,2,NULL,0.99,1,1,NULL),
    (159,NULL,'com.zeptolab.monstershd.1',2,1,'Pudding Monsters HD','com.zeptolab.monstershd.1',22,NULL,2,2,NULL,0.99,1,1,NULL),
    (160,NULL,'com.zeptolab.ctrbonus.starkey',2,2,'Star Key','com.zeptolab.ctrbonus.starkey',18,34,2,2,NULL,3.99,1,1,NULL),
    (161,NULL,'com.zeptolab.ctrbonushd.starkey',2,1,'Star Key','com.zeptolab.ctrbonushd.starkey',18,39,2,2,NULL,3.99,1,1,NULL),
    (162,NULL,'com.zeptolab.monsters.sp1',2,2,'Mushroom Boost 6','com.zeptolab.monsters.sp1',24,158,2,2,NULL,NULL,NULL,1,NULL),
    (163,NULL,'com.zeptolab.monsters.sp2',2,2,'Mushroom Boost 12','com.zeptolab.monsters.sp2',25,158,2,2,NULL,NULL,NULL,1,NULL),
    (164,NULL,'com.zeptolab.monsters.sp3',2,2,'Mushroom Boost 25','com.zeptolab.monsters.sp3',26,158,2,2,NULL,NULL,NULL,1,NULL),
    (165,NULL,'com.zeptolab.monsters.sp4',2,2,'Mushroom Boost 50','com.zeptolab.monsters.sp4',27,158,2,2,NULL,NULL,NULL,1,NULL),
    (166,NULL,'2940043936226',2,1,'Pudding Monsters HD','2940043936226',22,NULL,5,6,NULL,1.99,1,1,NULL);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_period`
--

DROP TABLE IF EXISTS `game_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_period` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `start_date` bigint(20) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `game_id` (`game_id`,`start_date`,`end_date`),
  KEY `FKFB2532AE39799FC8` (`currency_id`),
  KEY `FKFB2532AE37770F28` (`game_id`),
  CONSTRAINT `FKFB2532AE37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FKFB2532AE39799FC8` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_period`
--

LOCK TABLES `game_period` WRITE;
/*!40000 ALTER TABLE `game_period` DISABLE KEYS */;
INSERT INTO `game_period` (`id`, `is_active`, `end_date`, `price`, `start_date`, `currency_id`, `game_id`) VALUES
    (16,1,NULL,0,NULL,1,16),
    (17,1,NULL,0,NULL,1,17),
    (18,1,1317945600000,NULL,1291939200000,1,18),
    (19,1,1325808000000,NULL,1321401600000,1,18),
    (20,1,NULL,NULL,NULL,1,19),
    (21,1,NULL,NULL,NULL,1,20),
    (22,1,NULL,NULL,NULL,1,21),
    (23,1,NULL,1.99,NULL,1,22),
    (24,1,NULL,1.99,1345680000001,1,23),
    (25,1,NULL,0,1345680000001,1,24),
    (26,1,NULL,0,1345680000001,1,25),
    (27,1,NULL,0,1345680000001,1,26),
    (28,1,NULL,0.99,1345680000001,1,27),
    (29,1,NULL,0.99,1345680000001,1,28),
    (30,1,NULL,4.99,1345680000001,1,29),
    (31,1,NULL,0.99,1345680000001,1,30),
    (32,1,NULL,1.99,NULL,1,31),
    (33,1,NULL,0.99,NULL,1,32),
    (34,1,NULL,1.99,NULL,1,33),
    (35,1,NULL,0.99,NULL,1,34),
    (36,1,NULL,0.99,NULL,1,35),
    (37,1,NULL,0.99,NULL,1,36),
    (38,1,NULL,0,NULL,1,37),
    (39,1,NULL,0.99,NULL,1,38),
    (40,1,NULL,1.99,NULL,1,39),
    (41,1,NULL,1.99,NULL,1,40),
    (42,1,NULL,0,NULL,1,41),
    (43,1,NULL,0,NULL,1,42),
    (44,1,NULL,0,NULL,1,43),
    (45,1,NULL,0.99,NULL,1,44),
    (46,1,NULL,2.99,NULL,1,45),
    (62,1,NULL,NULL,NULL,1,61),
    (63,1,NULL,NULL,NULL,1,62),
    (64,1,NULL,NULL,NULL,1,63),
    (65,1,NULL,NULL,NULL,1,64),
    (66,1,NULL,NULL,NULL,1,65),
    (67,1,NULL,NULL,NULL,1,66),
    (68,1,NULL,NULL,NULL,1,67),
    (69,1,NULL,NULL,NULL,1,68),
    (70,1,NULL,NULL,NULL,1,69),
    (71,1,NULL,NULL,NULL,1,70),
    (72,1,NULL,NULL,1345248000000,1,71),
    (73,1,NULL,NULL,1345248000000,1,72),
    (74,1,NULL,NULL,1345248000000,1,73),
    (75,1,NULL,NULL,1345248000000,1,74),
    (80,1,NULL,NULL,1345248000000,1,79),
    (81,1,NULL,NULL,1345248000000,1,80),
    (82,1,NULL,NULL,1345248000000,1,81),
    (83,1,NULL,NULL,1345248000000,1,82),
    (84,1,NULL,NULL,1335398400000,1,83),
    (85,1,NULL,NULL,1335398400000,1,84),
    (86,1,NULL,NULL,1337817600000,1,85),
    (87,1,NULL,NULL,1337817600000,1,86),
    (88,1,NULL,NULL,1319500800000,1,87),
    (89,1,NULL,NULL,NULL,1,88),
    (90,1,NULL,NULL,1344988800000,1,89),
    (91,1,NULL,NULL,1344988800000,1,90),
    (92,1,NULL,NULL,1344988800000,1,91),
    (93,1,NULL,NULL,1344988800000,1,92),
    (94,1,NULL,NULL,NULL,1,93),
    (95,1,NULL,NULL,NULL,1,94),
    (96,1,NULL,NULL,NULL,1,95),
    (97,1,NULL,NULL,NULL,1,96),
    (98,1,NULL,NULL,1312156800000,1,97),
    (99,1,NULL,NULL,1312156800000,1,98),
    (100,1,1334707200000,NULL,1308700800000,1,99),
    (101,1,NULL,NULL,1312156800000,1,100),
    (102,1,NULL,NULL,1345852800000,1,101),
    (103,1,NULL,NULL,1345852800000,1,102),
    (104,1,NULL,NULL,1312156800000,1,103),
    (105,1,NULL,NULL,1312156800000,1,104),
    (106,1,NULL,NULL,1344988800000,1,105),
    (107,1,NULL,NULL,1344988800000,1,106),
    (108,1,NULL,NULL,1344988800000,1,107),
    (109,1,NULL,NULL,1344988800000,1,108),
    (110,1,NULL,NULL,NULL,1,109),
    (111,1,NULL,NULL,1347494400000,1,110),
    (112,1,NULL,NULL,NULL,1,111),
    (113,1,NULL,NULL,NULL,1,112),
    (114,1,NULL,NULL,NULL,1,113),
    (115,1,NULL,NULL,NULL,1,114),
    (116,1,NULL,NULL,NULL,1,115),
    (117,1,NULL,NULL,NULL,1,116),
    (118,1,NULL,NULL,NULL,1,117),
    (119,1,NULL,NULL,NULL,1,118),
    (120,1,NULL,NULL,1347494400000,1,119),
    (121,1,NULL,NULL,NULL,1,120),
    (122,1,NULL,NULL,NULL,1,121),
    (123,1,NULL,NULL,1345248000000,1,122),
    (124,1,NULL,NULL,1345248000000,1,123),
    (125,1,NULL,NULL,1345248000000,1,124),
    (126,1,NULL,NULL,1345248000000,1,125),
    (127,1,NULL,NULL,1341792000000,1,99),
    (128,0,1345680000000,1.99,NULL,1,23),
    (129,0,1345680000000,0,NULL,1,24),
    (130,0,1345680000000,0,NULL,1,25),
    (131,0,1345680000000,0,NULL,1,26),
    (132,0,1345680000000,0.99,NULL,1,27),
    (133,0,1345680000000,0.99,NULL,1,28),
    (134,0,1345680000000,4.99,NULL,1,29),
    (135,0,1345680000000,0.99,NULL,1,30),
    (139,1,NULL,NULL,NULL,1,132),
    (140,1,NULL,NULL,1343779200000,1,133),
    (141,1,NULL,NULL,1343779200000,1,134),
    (142,1,NULL,NULL,1349049600000,1,135),
    (143,1,NULL,NULL,1349049600000,1,136),
    (144,1,NULL,NULL,1349049600000,1,137),
    (145,1,NULL,NULL,1349049600000,1,138),
    (146,1,NULL,NULL,NULL,1,139),
    (147,1,NULL,1.99,1351728000000,1,140),
    (148,1,NULL,3.99,1351728000000,1,141),
    (149,1,NULL,7.99,1351728000000,1,142),
    (150,1,NULL,17.99,1351728000000,1,143),
    (151,1,NULL,0,1355961600000,1,144),
    (152,1,NULL,0.99,1355961600000,1,145),
    (153,1,NULL,1.99,1355961600000,1,146),
    (154,1,NULL,1.99,1355961600000,1,147),
    (155,1,NULL,3.99,1355961600000,1,148),
    (156,1,NULL,5.99,1355961600000,1,149),
    (157,1,NULL,9.99,1355961600000,1,150),
    (158,1,NULL,4.99,1355961600000,1,151),
    (159,1,NULL,0.99,1355961600000,1,152),
    (160,1,NULL,1.99,1355961600000,1,153),
    (161,1,NULL,3.99,1355961600000,1,154),
    (162,1,NULL,5.99,1355961600000,1,155),
    (163,1,NULL,9.99,1355961600000,1,156),
    (164,1,NULL,4.99,1355961600000,1,157),
    (165,1,NULL,0.99,1355875200000,1,158),
    (166,1,NULL,0.99,1355875200000,1,159),
    (167,1,NULL,3.99,NULL,1,160),
    (168,1,NULL,3.99,NULL,1,161),
    (169,1,NULL,1.99,1355875200000,1,162),
    (170,1,NULL,3.99,1355875200000,1,163),
    (171,1,NULL,5.99,1355875200000,1,164),
    (172,1,NULL,9.99,1355875200000,1,165),
    (173,1,NULL,1.99,1355961600000,1,166);
/*!40000 ALTER TABLE `game_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_price`
--

DROP TABLE IF EXISTS `game_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_price` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` bigint(20) DEFAULT NULL,
  `price` double NOT NULL,
  `start_date` bigint(20) DEFAULT NULL,
  `currency_id` bigint(20) NOT NULL,
  `game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `game_id` (`game_id`,`currency_id`,`start_date`,`end_date`),
  KEY `FK39AC22BC39799FC8` (`currency_id`),
  KEY `FK39AC22BC37770F28` (`game_id`),
  CONSTRAINT `FK39AC22BC37770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK39AC22BC39799FC8` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_price`
--

LOCK TABLES `game_price` WRITE;
/*!40000 ALTER TABLE `game_price` DISABLE KEYS */;
INSERT INTO `game_price` (`id`, `end_date`, `price`, `start_date`, `currency_id`, `game_id`) VALUES
    (1,NULL,0,NULL,1,1),
    (2,NULL,0,NULL,1,2),
    (3,NULL,0,NULL,1,3),
    (4,NULL,1.99,NULL,1,4),
    (5,NULL,1.99,NULL,1,5),
    (6,NULL,0.99,NULL,1,6),
    (7,NULL,0.99,NULL,1,7),
    (8,NULL,1.99,NULL,1,8),
    (9,NULL,3.99,NULL,1,9),
    (10,NULL,7.99,NULL,1,10),
    (11,NULL,1.99,NULL,1,11),
    (12,NULL,0.99,NULL,1,12),
    (13,NULL,1.99,NULL,1,13),
    (14,NULL,3.99,NULL,1,14),
    (15,NULL,7.99,NULL,1,15),
    (16,NULL,0,NULL,1,16),
    (17,NULL,0,NULL,1,17),
    (18,NULL,1.99,NULL,1,22),
    (19,NULL,0.99,NULL,1,23),
    (20,NULL,0,NULL,1,24),
    (21,NULL,0,NULL,1,25),
    (22,NULL,0,NULL,1,26),
    (23,NULL,0.99,NULL,1,27),
    (24,NULL,0.99,NULL,1,28),
    (25,NULL,4.99,NULL,1,29),
    (26,NULL,0.99,NULL,1,30),
    (27,NULL,1.99,NULL,1,31),
    (28,NULL,0.99,NULL,1,32),
    (29,NULL,1.99,NULL,1,33),
    (30,NULL,0.99,NULL,1,34),
    (31,NULL,0.99,NULL,1,35),
    (32,NULL,0.99,NULL,1,36),
    (33,NULL,0,NULL,1,37),
    (34,NULL,0.99,NULL,1,38),
    (35,NULL,1.99,NULL,1,39),
    (36,NULL,1.99,NULL,1,40),
    (37,NULL,0,NULL,1,41),
    (38,NULL,0,NULL,1,42),
    (39,NULL,0,NULL,1,43),
    (40,NULL,0.99,NULL,1,44),
    (41,NULL,2.99,NULL,1,45),
    (42,NULL,2.99,NULL,1,46),
    (43,NULL,2.99,NULL,1,47),
    (44,NULL,2.99,NULL,1,48);
/*!40000 ALTER TABLE `game_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_type`
--

DROP TABLE IF EXISTS `game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_type`
--

LOCK TABLES `game_type` WRITE;
/*!40000 ALTER TABLE `game_type` DISABLE KEYS */;
INSERT INTO `game_type` (`id`, `name`, `is_default`) VALUES
    (1,'FREE',0),
    (2,'PAID',1),
    (3,'LITE',0);
/*!40000 ALTER TABLE `game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invalid_finance`
--

DROP TABLE IF EXISTS `invalid_finance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invalid_finance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cause` longtext COLLATE utf8_bin NOT NULL,
  `end_date` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `revenue` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `start_date` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `report_id` bigint(20) NOT NULL,
  `game` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA0A0192DE6C1897` (`report_id`),
  CONSTRAINT `FKA0A0192DE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invalid_finance`
--

LOCK TABLES `invalid_finance` WRITE;
/*!40000 ALTER TABLE `invalid_finance` DISABLE KEYS */;
/*!40000 ALTER TABLE `invalid_finance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invalid_unit`
--

DROP TABLE IF EXISTS `invalid_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invalid_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cause` longtext COLLATE utf8_bin NOT NULL,
  `date` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `downloads` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `refunds` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `updates` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `report_id` bigint(20) NOT NULL,
  `game` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1F6D42CCDE6C1897` (`report_id`),
  CONSTRAINT `FK1F6D42CCDE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invalid_unit`
--

LOCK TABLES `invalid_unit` WRITE;
/*!40000 ALTER TABLE `invalid_unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `invalid_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_info`
--

DROP TABLE IF EXISTS `mail_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_info`
--

LOCK TABLES `mail_info` WRITE;
/*!40000 ALTER TABLE `mail_info` DISABLE KEYS */;
INSERT INTO `mail_info` (`id`, `email`, `name`) VALUES
    (1,'ivan.ivashchenko@7bits.it','Ivan');
/*!40000 ALTER TABLE `mail_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_info`
--

DROP TABLE IF EXISTS `manager_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `report_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `required_product_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `cron` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `lag` bigint(20) DEFAULT NULL,
  `is_full_active` tinyint(1) NOT NULL,
  `comment` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `commentForSku` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `report_ui_id` (`report_ui_id`,`report_type`,`required_product_type`),
  KEY `FK492AD200133D4251` (`report_ui_id`),
  CONSTRAINT `FK492AD200133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_info`
--

LOCK TABLES `manager_info` WRITE;
/*!40000 ALTER TABLE `manager_info` DISABLE KEYS */;
INSERT INTO `manager_info` (`id`, `is_active`, `name`, `report_type`, `report_ui_id`, `required_product_type`, `cron`, `is_deleted`, `lag`, `is_full_active`, `comment`, `commentForSku`) VALUES
    (1,0,'itunes_finance_manager','FINANCE',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (2,0,'itunes_unit_manager','UNIT',2,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (3,0,'google_finance_manager','FINANCE',3,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (4,0,'google_app_unit_manager','UNIT',3,'APP',NULL,0,NULL,1,NULL,NULL),
    (5,0,'amazon_finance_manager','FINANCE',4,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (6,0,'amazon_app_unit_manager','UNIT',4,'APP',NULL,0,NULL,1,NULL,NULL),
    (7,0,'chillingo_finance_manager','FINANCE',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (8,0,'chillingo_unit_manager','UNIT',1,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (9,0,'ovi_finance_manager','FINANCE',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (10,0,'ovi_unit_manager','UNIT',6,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (11,0,'nook_finance_manager','FINANCE',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (12,0,'nook_unit_manager','UNIT',5,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (13,0,'blackberry_finance_manager','FINANCE',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (14,0,'blackberry_unit_manager','UNIT',7,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (15,0,'getjar_unit_manager','UNIT',8,'BOTH',NULL,0,NULL,1,NULL,NULL),
    (16,0,'google_in_app_unit_manager','UNIT',3,'IN_APP',NULL,0,NULL,1,NULL,NULL),
    (17,0,'amazon_in_app_unit_manager','UNIT',4,'IN_APP',NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `manager_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager_info_period_sign`
--

DROP TABLE IF EXISTS `manager_info_period_sign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager_info_period_sign` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_date` bigint(20) NOT NULL,
  `sign` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `start_date` bigint(20) NOT NULL,
  `manager_info_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF731C1BC4B312E68` (`manager_info_id`),
  CONSTRAINT `FKF731C1BC4B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager_info_period_sign`
--

LOCK TABLES `manager_info_period_sign` WRITE;
/*!40000 ALTER TABLE `manager_info_period_sign` DISABLE KEYS */;
/*!40000 ALTER TABLE `manager_info_period_sign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game`
--

DROP TABLE IF EXISTS `meta_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_inapp` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game`
--

LOCK TABLES `meta_game` WRITE;
/*!40000 ALTER TABLE `meta_game` DISABLE KEYS */;
INSERT INTO `meta_game` (`id`, `name`, `is_inapp`) VALUES
    (1,'Cut the Rope',0),
    (3,'Cut the Rope: Comic',0),
    (4,'Cut the Rope: Experiments',0),
    (5,'Cut the Rope: Holiday Gift',0),
    (6,'Cut the Rope - Unlock all Boxes',1),
    (7,'Cut the Rope - SP/20',1),
    (8,'Cut the Rope - SP/50',1),
    (9,'Cut the Rope - SP/150',1),
    (10,'Cut the Rope - SP/500',1),
    (11,'Cut the Rope: Comic - #2',1),
    (12,'Cut the Rope: Comic - #3',1),
    (13,'Cut the Rope: Experiments - SP/20',1),
    (14,'Cut the Rope: Experiments - SP/50',1),
    (15,'Cut the Rope: Experiments - SP/150',1),
    (16,'Cut the Rope: Experiments - SP/500',1),
    (17,'Cut the Rope - Star Key',1),
    (18,'Cut the Rope: Experiments - Star Key',1),
    (19,'Generic App',0),
    (20,'Generic InApp',1),
    (21,'DEFAULT_META_GAME',0),
    (22,'Pudding Monsters',0),
    (23,'Pudding Monsters: Disable Ads',1),
    (24,'Mushroom Boost 6',1),
    (25,'Mushroom Boost 12',1),
    (26,'Mushroom Boost 25',1),
    (27,'Mushroom Boost 50',1),
    (28,'Pudding Monsters: Unlock all Episodes',1);
/*!40000 ALTER TABLE `meta_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game_game_type`
--

DROP TABLE IF EXISTS `meta_game_game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game_game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `icon_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `icon_path` varchar(255) COLLATE utf8_bin NOT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_game_id` (`meta_game_id`,`game_type_id`),
  KEY `FKA47110D43E50EF93` (`meta_game_id`),
  KEY `FKA47110D4E9E03AED` (`game_type_id`),
  CONSTRAINT `FKA47110D43E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FKA47110D4E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game_game_type`
--

LOCK TABLES `meta_game_game_type` WRITE;
/*!40000 ALTER TABLE `meta_game_game_type` DISABLE KEYS */;
INSERT INTO `meta_game_game_type` (`id`, `icon_name`, `icon_path`, `game_type_id`, `meta_game_id`) VALUES
    (1,'cut-the-rope.png','/uploads/images/cut-the-rope/',2,1),
    (2,'cut-the-rope-free.png','/uploads/images/cut-the-rope-free/',1,1),
    (3,'the-comic.png','/uploads/images/comic/',1,3),
    (4,'experiments.png','/uploads/images/experimentas/',2,4),
    (5,'holiday-gift.png','/uploads/images/holiday-gift/',1,5),
    (6,'experiments-free.png','/uploads/images/experimentas-free/',1,4);
/*!40000 ALTER TABLE `meta_game_game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `generation_time` bigint(20) NOT NULL,
  `is_sent` tinyint(1) NOT NULL,
  `message` text COLLATE utf8_bin NOT NULL,
  `subject` varchar(256) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `royalty` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `name`, `royalty`) VALUES
    (1,'Chillingo',0.3);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` (`id`, `name`) VALUES
    (1,'Android'),
    (2,'BlackBerry OS (RIM)'),
    (3,'iOS'),
    (4,'Mac OS'),
    (5,'Symbian OS');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` (`id`, `name`) VALUES
    (1,'North America'),
    (2,'South America'),
    (3,'Oceania'),
    (4,'Africa'),
    (5,'Europe'),
    (6,'Asia'),
    (7,'Central America'),
    (8,'Middle East'),
    (9,'Others');
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reparsing_rule`
--

DROP TABLE IF EXISTS `reparsing_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reparsing_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `repeat_count` int(11) DEFAULT NULL,
  `manager_info_id` bigint(20) NOT NULL,
  `email_only` tinyint(1) NOT NULL,
  `parsing_status` varchar(100) COLLATE utf8_bin NOT NULL,
  `repeat_interval` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parsing_status` (`parsing_status`,`manager_info_id`),
  KEY `FKE5C400DE4B312E68` (`manager_info_id`),
  CONSTRAINT `FKE5C400DE4B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reparsing_rule`
--

LOCK TABLES `reparsing_rule` WRITE;
/*!40000 ALTER TABLE `reparsing_rule` DISABLE KEYS */;
INSERT INTO `reparsing_rule` (`id`, `repeat_count`, `manager_info_id`, `email_only`, `parsing_status`, `repeat_interval`) VALUES
    (1,1,1,0,'INTERNAL_FATAL',120000),
    (2,7,2,0,'DATA_SOURCE_AUTH_FAILED',300000),
    (3,2,3,0,'DATA_SOURCE_AUTH_FAILED',390000),
    (4,NULL,1,1,'ABORTED',NULL),
    (5,NULL,1,1,'EMPTY_REPORT',NULL),
    (6,NULL,1,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (7,NULL,1,1,'EXTRA_GAMES',NULL),
    (8,NULL,1,1,'NOT_ENOUGH_GAMES',NULL),
    (9,NULL,1,1,'INVALID_DATA',NULL),
    (10,NULL,1,1,'INTERNAL_WARNING',NULL),
    (11,NULL,1,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (12,NULL,1,1,'DATA_SOURCE_FATAL',NULL),
    (13,NULL,1,1,'DATA_SOURCE_REJECTED',NULL),
    (14,NULL,1,1,'INVALID_REPORT_FORMAT',NULL),
    (16,NULL,1,1,'UNKNOWN_TYPE',NULL),
    (17,NULL,2,1,'ABORTED',NULL),
    (18,NULL,2,1,'EMPTY_REPORT',NULL),
    (19,NULL,2,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (20,NULL,2,1,'EXTRA_GAMES',NULL),
    (21,NULL,2,1,'NOT_ENOUGH_GAMES',NULL),
    (22,NULL,2,1,'INVALID_DATA',NULL),
    (23,NULL,2,1,'INTERNAL_WARNING',NULL),
    (24,NULL,2,1,'DATA_SOURCE_FATAL',NULL),
    (25,NULL,2,1,'DATA_SOURCE_REJECTED',NULL),
    (26,NULL,2,1,'INVALID_REPORT_FORMAT',NULL),
    (27,NULL,2,1,'INTERNAL_FATAL',NULL),
    (29,NULL,2,1,'UNKNOWN_TYPE',NULL),
    (30,NULL,3,1,'ABORTED',NULL),
    (31,NULL,3,1,'EMPTY_REPORT',NULL),
    (32,NULL,3,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (33,NULL,3,1,'EXTRA_GAMES',NULL),
    (34,NULL,3,1,'NOT_ENOUGH_GAMES',NULL),
    (35,NULL,3,1,'INVALID_DATA',NULL),
    (36,NULL,3,1,'INTERNAL_WARNING',NULL),
    (37,NULL,3,1,'DATA_SOURCE_FATAL',NULL),
    (38,NULL,3,1,'DATA_SOURCE_REJECTED',NULL),
    (39,NULL,3,1,'INVALID_REPORT_FORMAT',NULL),
    (40,NULL,3,1,'INTERNAL_FATAL',NULL),
    (42,NULL,3,1,'UNKNOWN_TYPE',NULL),
    (43,NULL,4,1,'ABORTED',NULL),
    (44,NULL,4,1,'EMPTY_REPORT',NULL),
    (45,NULL,4,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (46,NULL,4,1,'EXTRA_GAMES',NULL),
    (47,NULL,4,1,'NOT_ENOUGH_GAMES',NULL),
    (48,NULL,4,1,'INVALID_DATA',NULL),
    (49,NULL,4,1,'INTERNAL_WARNING',NULL),
    (50,NULL,4,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (51,NULL,4,1,'DATA_SOURCE_FATAL',NULL),
    (52,NULL,4,1,'DATA_SOURCE_REJECTED',NULL),
    (53,NULL,4,1,'INVALID_REPORT_FORMAT',NULL),
    (54,NULL,4,1,'INTERNAL_FATAL',NULL),
    (56,NULL,4,1,'UNKNOWN_TYPE',NULL),
    (57,NULL,5,1,'ABORTED',NULL),
    (58,NULL,5,1,'EMPTY_REPORT',NULL),
    (59,NULL,5,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (60,NULL,5,1,'EXTRA_GAMES',NULL),
    (61,NULL,5,1,'NOT_ENOUGH_GAMES',NULL),
    (62,NULL,5,1,'INVALID_DATA',NULL),
    (63,NULL,5,1,'INTERNAL_WARNING',NULL),
    (64,NULL,5,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (65,NULL,5,1,'DATA_SOURCE_FATAL',NULL),
    (66,NULL,5,1,'DATA_SOURCE_REJECTED',NULL),
    (67,NULL,5,1,'INVALID_REPORT_FORMAT',NULL),
    (68,NULL,5,1,'INTERNAL_FATAL',NULL),
    (70,NULL,5,1,'UNKNOWN_TYPE',NULL),
    (71,NULL,6,1,'ABORTED',NULL),
    (72,NULL,6,1,'EMPTY_REPORT',NULL),
    (73,NULL,6,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (74,NULL,6,1,'EXTRA_GAMES',NULL),
    (75,NULL,6,1,'NOT_ENOUGH_GAMES',NULL),
    (76,NULL,6,1,'INVALID_DATA',NULL),
    (77,NULL,6,1,'INTERNAL_WARNING',NULL),
    (78,NULL,6,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (79,NULL,6,1,'DATA_SOURCE_FATAL',NULL),
    (80,NULL,6,1,'DATA_SOURCE_REJECTED',NULL),
    (81,NULL,6,1,'INVALID_REPORT_FORMAT',NULL),
    (82,NULL,6,1,'INTERNAL_FATAL',NULL),
    (84,NULL,6,1,'UNKNOWN_TYPE',NULL),
    (85,NULL,7,1,'ABORTED',NULL),
    (86,NULL,7,1,'EMPTY_REPORT',NULL),
    (87,NULL,7,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (88,NULL,7,1,'EXTRA_GAMES',NULL),
    (89,NULL,7,1,'NOT_ENOUGH_GAMES',NULL),
    (90,NULL,7,1,'INVALID_DATA',NULL),
    (91,NULL,7,1,'INTERNAL_WARNING',NULL),
    (92,NULL,7,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (93,NULL,7,1,'DATA_SOURCE_FATAL',NULL),
    (94,NULL,7,1,'DATA_SOURCE_REJECTED',NULL),
    (95,NULL,7,1,'INVALID_REPORT_FORMAT',NULL),
    (96,NULL,7,1,'INTERNAL_FATAL',NULL),
    (98,NULL,7,1,'UNKNOWN_TYPE',NULL),
    (99,NULL,8,1,'ABORTED',NULL),
    (100,NULL,8,1,'EMPTY_REPORT',NULL),
    (101,NULL,8,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (102,NULL,8,1,'EXTRA_GAMES',NULL),
    (103,NULL,8,1,'NOT_ENOUGH_GAMES',NULL),
    (104,NULL,8,1,'INVALID_DATA',NULL),
    (105,NULL,8,1,'INTERNAL_WARNING',NULL),
    (106,NULL,8,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (107,NULL,8,1,'DATA_SOURCE_FATAL',NULL),
    (108,NULL,8,1,'DATA_SOURCE_REJECTED',NULL),
    (109,NULL,8,1,'INVALID_REPORT_FORMAT',NULL),
    (110,NULL,8,1,'INTERNAL_FATAL',NULL),
    (112,NULL,8,1,'UNKNOWN_TYPE',NULL),
    (113,NULL,9,1,'ABORTED',NULL),
    (114,NULL,9,1,'EMPTY_REPORT',NULL),
    (115,NULL,9,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (116,NULL,9,1,'EXTRA_GAMES',NULL),
    (117,NULL,9,1,'NOT_ENOUGH_GAMES',NULL),
    (118,NULL,9,1,'INVALID_DATA',NULL),
    (119,NULL,9,1,'INTERNAL_WARNING',NULL),
    (120,NULL,9,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (121,NULL,9,1,'DATA_SOURCE_FATAL',NULL),
    (122,NULL,9,1,'DATA_SOURCE_REJECTED',NULL),
    (123,NULL,9,1,'INVALID_REPORT_FORMAT',NULL),
    (124,NULL,9,1,'INTERNAL_FATAL',NULL),
    (126,NULL,9,1,'UNKNOWN_TYPE',NULL),
    (127,NULL,10,1,'ABORTED',NULL),
    (128,NULL,10,1,'EMPTY_REPORT',NULL),
    (129,NULL,10,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (130,NULL,10,1,'EXTRA_GAMES',NULL),
    (131,NULL,10,1,'NOT_ENOUGH_GAMES',NULL),
    (132,NULL,10,1,'INVALID_DATA',NULL),
    (133,NULL,10,1,'INTERNAL_WARNING',NULL),
    (134,NULL,10,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (135,NULL,10,1,'DATA_SOURCE_FATAL',NULL),
    (136,NULL,10,1,'DATA_SOURCE_REJECTED',NULL),
    (137,NULL,10,1,'INVALID_REPORT_FORMAT',NULL),
    (138,NULL,10,1,'INTERNAL_FATAL',NULL),
    (140,NULL,10,1,'UNKNOWN_TYPE',NULL),
    (141,NULL,11,1,'ABORTED',NULL),
    (142,NULL,11,1,'EMPTY_REPORT',NULL),
    (143,NULL,11,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (144,NULL,11,1,'EXTRA_GAMES',NULL),
    (145,NULL,11,1,'NOT_ENOUGH_GAMES',NULL),
    (146,NULL,11,1,'INVALID_DATA',NULL),
    (147,NULL,11,1,'INTERNAL_WARNING',NULL),
    (148,NULL,11,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (149,NULL,11,1,'DATA_SOURCE_FATAL',NULL),
    (150,NULL,11,1,'DATA_SOURCE_REJECTED',NULL),
    (151,NULL,11,1,'INVALID_REPORT_FORMAT',NULL),
    (152,NULL,11,1,'INTERNAL_FATAL',NULL),
    (154,NULL,11,1,'UNKNOWN_TYPE',NULL),
    (155,NULL,12,1,'ABORTED',NULL),
    (156,NULL,12,1,'EMPTY_REPORT',NULL),
    (157,NULL,12,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (158,NULL,12,1,'EXTRA_GAMES',NULL),
    (159,NULL,12,1,'NOT_ENOUGH_GAMES',NULL),
    (160,NULL,12,1,'INVALID_DATA',NULL),
    (161,NULL,12,1,'INTERNAL_WARNING',NULL),
    (162,NULL,12,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (163,NULL,12,1,'DATA_SOURCE_FATAL',NULL),
    (164,NULL,12,1,'DATA_SOURCE_REJECTED',NULL),
    (165,NULL,12,1,'INVALID_REPORT_FORMAT',NULL),
    (166,NULL,12,1,'INTERNAL_FATAL',NULL),
    (168,NULL,12,1,'UNKNOWN_TYPE',NULL),
    (169,NULL,13,1,'ABORTED',NULL),
    (170,NULL,13,1,'EMPTY_REPORT',NULL),
    (171,NULL,13,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (172,NULL,13,1,'EXTRA_GAMES',NULL),
    (173,NULL,13,1,'NOT_ENOUGH_GAMES',NULL),
    (174,NULL,13,1,'INVALID_DATA',NULL),
    (175,NULL,13,1,'INTERNAL_WARNING',NULL),
    (176,NULL,13,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (177,NULL,13,1,'DATA_SOURCE_FATAL',NULL),
    (178,NULL,13,1,'DATA_SOURCE_REJECTED',NULL),
    (179,NULL,13,1,'INVALID_REPORT_FORMAT',NULL),
    (180,NULL,13,1,'INTERNAL_FATAL',NULL),
    (182,NULL,13,1,'UNKNOWN_TYPE',NULL),
    (183,NULL,14,1,'ABORTED',NULL),
    (184,NULL,14,1,'EMPTY_REPORT',NULL),
    (185,NULL,14,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (186,NULL,14,1,'EXTRA_GAMES',NULL),
    (187,NULL,14,1,'NOT_ENOUGH_GAMES',NULL),
    (188,NULL,14,1,'INVALID_DATA',NULL),
    (189,NULL,14,1,'INTERNAL_WARNING',NULL),
    (190,NULL,14,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (191,NULL,14,1,'DATA_SOURCE_FATAL',NULL),
    (192,NULL,14,1,'DATA_SOURCE_REJECTED',NULL),
    (193,NULL,14,1,'INVALID_REPORT_FORMAT',NULL),
    (194,NULL,14,1,'INTERNAL_FATAL',NULL),
    (196,NULL,14,1,'UNKNOWN_TYPE',NULL),
    (197,NULL,15,1,'ABORTED',NULL),
    (198,NULL,15,1,'EMPTY_REPORT',NULL),
    (199,NULL,15,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (200,NULL,15,1,'EXTRA_GAMES',NULL),
    (201,NULL,15,1,'NOT_ENOUGH_GAMES',NULL),
    (202,NULL,15,1,'INVALID_DATA',NULL),
    (203,NULL,15,1,'INTERNAL_WARNING',NULL),
    (204,NULL,15,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (205,NULL,15,1,'DATA_SOURCE_FATAL',NULL),
    (206,NULL,15,1,'DATA_SOURCE_REJECTED',NULL),
    (207,NULL,15,1,'INVALID_REPORT_FORMAT',NULL),
    (208,NULL,15,1,'INTERNAL_FATAL',NULL),
    (210,NULL,15,1,'UNKNOWN_TYPE',NULL),
    (211,NULL,16,1,'ABORTED',NULL),
    (212,NULL,16,1,'EMPTY_REPORT',NULL),
    (213,NULL,16,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (214,NULL,16,1,'EXTRA_GAMES',NULL),
    (215,NULL,16,1,'NOT_ENOUGH_GAMES',NULL),
    (216,NULL,16,1,'INVALID_DATA',NULL),
    (217,NULL,16,1,'INTERNAL_WARNING',NULL),
    (218,NULL,16,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (219,NULL,16,1,'DATA_SOURCE_FATAL',NULL),
    (220,NULL,16,1,'DATA_SOURCE_REJECTED',NULL),
    (221,NULL,16,1,'INVALID_REPORT_FORMAT',NULL),
    (222,NULL,16,1,'INTERNAL_FATAL',NULL),
    (224,NULL,16,1,'UNKNOWN_TYPE',NULL),
    (225,NULL,17,1,'ABORTED',NULL),
    (226,NULL,17,1,'EMPTY_REPORT',NULL),
    (227,NULL,17,1,'EMPTY_LIST_FOR_VALIDATION',NULL),
    (228,NULL,17,1,'EXTRA_GAMES',NULL),
    (229,NULL,17,1,'NOT_ENOUGH_GAMES',NULL),
    (230,NULL,17,1,'INVALID_DATA',NULL),
    (231,NULL,17,1,'INTERNAL_WARNING',NULL),
    (232,NULL,17,1,'DATA_SOURCE_AUTH_FAILED',NULL),
    (233,NULL,17,1,'DATA_SOURCE_FATAL',NULL),
    (234,NULL,17,1,'DATA_SOURCE_REJECTED',NULL),
    (235,NULL,17,1,'INVALID_REPORT_FORMAT',NULL),
    (236,NULL,17,1,'INTERNAL_FATAL',NULL),
    (238,NULL,17,1,'UNKNOWN_TYPE',NULL);
/*!40000 ALTER TABLE `reparsing_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cause` longtext COLLATE utf8_bin,
  `requested_date` bigint(20) NOT NULL,
  `manager_info_id` bigint(20) NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `start_date` bigint(20) NOT NULL,
  `end_date` bigint(20) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL,
  `games_lack_check` tinyint(1) NOT NULL DEFAULT '0',
  `games_excess_check` tinyint(1) NOT NULL DEFAULT '0',
  `zero_check` tinyint(1) NOT NULL DEFAULT '0',
  `custom_validation` text COLLATE utf8_bin,
  `launch_type` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'RANGE',
  `parent_report_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC84C55344B312E68` (`manager_info_id`),
  KEY `FKC84C5534B60CCF42` (`parent_report_id`),
  CONSTRAINT `FKC84C5534B60CCF42` FOREIGN KEY (`parent_report_id`) REFERENCES `report` (`id`),
  CONSTRAINT `FKC84C55344B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` (`id`, `cause`, `requested_date`, `manager_info_id`, `is_visible`, `start_date`, `end_date`, `status`, `games_lack_check`, `games_excess_check`, `zero_check`, `custom_validation`, `launch_type`, `parent_report_id`) VALUES
    (1,'cause',1350459310000,1,1,1350459410000,1350459510000,'SUCCESS',0,0,1,NULL,'USUAL',NULL);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_ui`
--

DROP TABLE IF EXISTS `report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_ui` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `login_page_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `unit_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `finance_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `code` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'fake',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_ui`
--

LOCK TABLES `report_ui` WRITE;
/*!40000 ALTER TABLE `report_ui` DISABLE KEYS */;
INSERT INTO `report_ui` (`id`, `login`, `name`, `password`, `login_page_domain`, `unit_domain`, `finance_domain`, `code`) VALUES
    (1,'ZeptoLab','Chillingo','OmNom123','http://www.clickgamer.com','http://www.clickgamer.com','http://www.clickgamer.com','chillingo'),
    (2,'konstantin.kolotyuk@7bits.it','ITunes','7bitsitunes','https://itunesconnect.apple.com','https://itunesconnect.apple.com','https://itunesconnect.apple.com','itunes'),
    (3,'zepto.parser@gmail.com','Google play','ZLParserPass','https://market.android.com','https://play.google.com','https://play.google.com','google'),
    (4,'konstantin.kolotyuk@7bits.it','Amazon','7bitsamazon','https://developer.amazon.com','https://developer.amazon.com','https://developer.amazon.com','amazon'),
    (5,'accounts@zeptolab.com','Nook','NookZLPass','https://nookdeveloper.barnesandnoble.com','https://nookdeveloper.barnesandnoble.com','https://nookdeveloper.barnesandnoble.com','nook'),
    (6,'accounts@zeptolab.com','Ovi','ZLNokiaPass1','https://publish.nokia.com','https://reports.nokia.com','https://publish.nokia.com','ovi'),
    (7,'konstantin.kolotyuk@7bits.it','BlackBerry','zeptolab','https://appworld.blackberry.com','https://appworld.blackberry.com','https://appworld.blackberry.com','blackberry'),
    (8,'accounts@zeptolab.com','GetJar','ZLGetJarPass','https://developer.getjar.com','https://developer.getjar.com','https://developer.getjar.com','getjar');
/*!40000 ALTER TABLE `report_ui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolution`
--

LOCK TABLES `resolution` WRITE;
/*!40000 ALTER TABLE `resolution` DISABLE KEYS */;
INSERT INTO `resolution` (`id`, `name`) VALUES
    (1,'HD'),
    (2,'SD'),
    (3,'DESKTOP');
/*!40000 ALTER TABLE `resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commission` double NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `platform_id` bigint(20) NOT NULL,
  `store_logo_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68AF8E1AE932C88` (`platform_id`),
  KEY `FK68AF8E110A0530F` (`store_logo_id`),
  CONSTRAINT `FK68AF8E110A0530F` FOREIGN KEY (`store_logo_id`) REFERENCES `store_logo` (`id`),
  CONSTRAINT `FK68AF8E1AE932C88` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`id`, `commission`, `name`, `platform_id`, `store_logo_id`) VALUES
    (2,0.3,'App Store',3,1),
    (3,0.3,'Mac App Store',4,2),
    (4,0.3,'Google play',1,3),
    (5,0.3,'Amazon',1,4),
    (6,0.3,'Nook',1,5),
    (7,0.4,'Ovi',5,6),
    (8,0.3,'BlackBerry App World',2,7),
    (9,0.3,'GetJar',1,8);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_logo`
--

DROP TABLE IF EXISTS `store_logo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_logo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_logo`
--

LOCK TABLES `store_logo` WRITE;
/*!40000 ALTER TABLE `store_logo` DISABLE KEYS */;
INSERT INTO `store_logo` (`id`, `name`, `path`) VALUES
    (1,'app-store.png','/uploads/images/appstore/'),
    (2,'mac-app-store.png','/uploads/images/macappstore/'),
    (3,'google-play.png','/uploads/images/gp/'),
    (4,'amazon-appstore.png','/uploads/images/amazon/'),
    (5,'nook.png','/uploads/images/nook/'),
    (6,'ovi.png','/uploads/images/ovi/'),
    (7,'blackberry-app-world.png','/uploads/images/blackberry/'),
    (8,'getjar.png','/uploads/images/getjar/');
/*!40000 ALTER TABLE `store_logo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_report_ui`
--

DROP TABLE IF EXISTS `store_report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_report_ui` (
  `store_id` bigint(20) NOT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  PRIMARY KEY (`store_id`,`report_ui_id`),
  KEY `FKDE67AFA1133D4251` (`report_ui_id`),
  KEY `FKDE67AFA187E4952C` (`store_id`),
  CONSTRAINT `FKDE67AFA187E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FKDE67AFA1133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_report_ui`
--

LOCK TABLES `store_report_ui` WRITE;
/*!40000 ALTER TABLE `store_report_ui` DISABLE KEYS */;
INSERT INTO `store_report_ui` (`store_id`, `report_ui_id`) VALUES
    (2,1),
    (2,2),
    (3,2),
    (4,3),
    (5,4),
    (6,5),
    (7,6),
    (8,7),
    (9,8);
/*!40000 ALTER TABLE `store_report_ui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `date` bigint(20) NOT NULL,
  `downloads` int(11) NOT NULL,
  `refunds` int(11) NOT NULL,
  `updates` int(11) NOT NULL,
  `country_id` bigint(20) NOT NULL DEFAULT '0',
  `game_id` bigint(20) NOT NULL,
  `report_id` bigint(20) NOT NULL,
  PRIMARY KEY (`date`,`game_id`,`country_id`),
  KEY `FK36D98437770F28` (`game_id`),
  KEY `FK36D984DE6C1897` (`report_id`),
  KEY `FK36D984230CDCF` (`country_id`),
  CONSTRAINT `FK36D984230CDCF` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`),
  CONSTRAINT `FK36D98437770F28` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK36D984DE6C1897` FOREIGN KEY (`report_id`) REFERENCES `report` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validation_info`
--

DROP TABLE IF EXISTS `validation_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validation_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `games_excess_check` tinyint(1) NOT NULL,
  `games_lack_check` tinyint(1) NOT NULL,
  `zero_check` tinyint(1) NOT NULL,
  `custom_validation` text COLLATE utf8_bin,
  `manager_info_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `manager_info_id` (`manager_info_id`),
  KEY `FKFBCC38B44B312E68` (`manager_info_id`),
  CONSTRAINT `FKFBCC38B44B312E68` FOREIGN KEY (`manager_info_id`) REFERENCES `manager_info` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validation_info`
--

LOCK TABLES `validation_info` WRITE;
/*!40000 ALTER TABLE `validation_info` DISABLE KEYS */;
INSERT INTO `validation_info` (`id`, `games_excess_check`, `games_lack_check`, `zero_check`, `custom_validation`, `manager_info_id`) VALUES
    (1,1,1,0,NULL,7),
    (2,1,1,0,NULL,1),
    (4,1,1,0,NULL,3),
    (5,1,1,0,NULL,5),
    (6,1,1,0,'{\"items\":[{\"code\":\"eGift\",\"title\":\"eGift Validation\",\"description\":\"Some reports contain info about gifts. Gift is sale by zero price even game has non-zero price.\",\"options\":[{\"code\":\"Ignore\",\"title\":\"Ignore Gifts\",\"description\":\"Do not consider gifts neither valid nor invalid raws. Report will not contain info about them at all.\",\"isSelected\":\"false\"},{\"code\":\"Valid\",\"title\":\"Consider Valid\",\"description\":\"Consider gifts as valid raws. So this info will be added to report as valid.\",\"isSelected\":\"true\"},{\"code\":\"Invalid\",\"title\":\"Consider Invalid\",\"description\":\"Consider gifts as invalid raws. So this info will be added to report as invalid.\",\"isSelected\":\"false\"}]}]}',11),
    (7,1,1,0,NULL,9),
    (8,1,1,0,NULL,13),
    (9,1,1,0,NULL,8),
    (10,1,1,0,NULL,2),
    (12,1,1,0,NULL,4),
    (13,1,1,0,NULL,6),
    (14,1,1,0,'{\"items\":[{\"code\":\"eGift\",\"title\":\"eGift Validation\",\"description\":\"Some reports contain info about gifts. Gift is sale by zero price even game has non-zero price.\",\"options\":[{\"code\":\"Ignore\",\"title\":\"Ignore Gifts\",\"description\":\"Do not consider gifts neither valid nor invalid raws. Report will not contain info about them at all.\",\"isSelected\":\"false\"},{\"code\":\"Valid\",\"title\":\"Consider Valid\",\"description\":\"Consider gifts as valid raws. So this info will be added to report as valid.\",\"isSelected\":\"true\"},{\"code\":\"Invalid\",\"title\":\"Consider Invalid\",\"description\":\"Consider gifts as invalid raws. So this info will be added to report as invalid.\",\"isSelected\":\"false\"}]}]}',12),
    (15,1,1,0,NULL,10),
    (16,1,1,0,NULL,14),
    (17,1,1,0,NULL,15),
    (18,1,1,0,NULL,16),
    (19,1,1,0,NULL,17);
/*!40000 ALTER TABLE `validation_info` ENABLE KEYS */;
UNLOCK TABLES;


/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

SET FOREIGN_KEY_CHECKS = 1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-14 12:13:26
