-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: zeptostats
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

SET FOREIGN_KEY_CHECKS = 0;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `sign` varchar(5) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` (`id`, `name`, `sign`, `is_default`) VALUES
    (1,'USD','USD',1);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extra` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `finance_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  `title` varchar(256) COLLATE utf8_bin NOT NULL,
  `unit_sku` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  `parent_game_id` bigint(20) DEFAULT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  `partner_id` bigint(20) DEFAULT NULL,
  `default_price` double DEFAULT NULL,
  `default_currency_id` bigint(20) DEFAULT NULL,
  `is_required_in_report` tinyint(1) NOT NULL DEFAULT '1',
  `ranking_sku` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK304BF2133D4251` (`report_ui_id`),
  KEY `FK304BF2469B7493` (`parent_game_id`),
  KEY `FK304BF2E9E03AED` (`game_type_id`),
  KEY `FK304BF23E50EF93` (`meta_game_id`),
  KEY `FK304BF287E4952C` (`store_id`),
  KEY `FK304BF235543B4C` (`partner_id`),
  KEY `FK304BF227A4E268` (`resolution_id`),
  KEY `FK304BF22AD7ACA` (`default_currency_id`),
  CONSTRAINT `FK304BF2133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`),
  CONSTRAINT `FK304BF227A4E268` FOREIGN KEY (`resolution_id`) REFERENCES `resolution` (`id`),
  CONSTRAINT `FK304BF22AD7ACA` FOREIGN KEY (`default_currency_id`) REFERENCES `currency` (`id`),
  CONSTRAINT `FK304BF235543B4C` FOREIGN KEY (`partner_id`) REFERENCES `partner` (`id`),
  CONSTRAINT `FK304BF23E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FK304BF2469B7493` FOREIGN KEY (`parent_game_id`) REFERENCES `game` (`id`),
  CONSTRAINT `FK304BF287E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FK304BF2E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
INSERT INTO `game` (`id`, `extra`, `finance_sku`, `game_type_id`, `resolution_id`, `title`, `unit_sku`, `meta_game_id`, `parent_game_id`, `report_ui_id`, `store_id`, `partner_id`, `default_price`, `default_currency_id`, `is_required_in_report`, `ranking_sku`) VALUES
    (1,NULL,'CUT THE ROPE HD',1,1,'Cut the Rope HD','394610743',1,NULL,1,2,1,1.99,NULL,1,NULL),
    (2,NULL,'CUT THE ROPE',2,2,'Cut the Rope','380293530',1,NULL,1,2,1,0.99,NULL,1,NULL),
    (3,NULL,'CTR_MAC',3,3,'Cut the Rope','CTR_MAC',1,NULL,2,2,NULL,NULL,NULL,1,NULL),
    (4,NULL,NULL,1,1,'Cut the Rope HD Free','394611607',1,NULL,1,3,1,NULL,NULL,1,NULL),
    (5,NULL,NULL,2,2,'Cut the Rope: Holiday Gift','406513121',3,NULL,1,3,1,0,NULL,1,NULL),
    (6,NULL,NULL,1,1,'Cut the Rope Free','394613472',1,NULL,1,4,1,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_type`
--

DROP TABLE IF EXISTS `game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_type`
--

LOCK TABLES `game_type` WRITE;
/*!40000 ALTER TABLE `game_type` DISABLE KEYS */;
INSERT INTO `game_type` (`id`, `name`, `is_default`) VALUES
    (1,'FREE',0),
    (2,'PAID',1),
    (3,'LITE',0);
/*!40000 ALTER TABLE `game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game`
--

DROP TABLE IF EXISTS `meta_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `is_inapp` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game`
--

LOCK TABLES `meta_game` WRITE;
/*!40000 ALTER TABLE `meta_game` DISABLE KEYS */;
INSERT INTO `meta_game` (`id`, `name`, `is_inapp`) VALUES
    (1,'Cut the Rope',0),
    (3,'Cut the Rope: Comic',0),
    (4,'Cut the Rope: Experiments',0),
    (5,'Cut the Rope: Holiday Gift',0),
    (6,'Cut the Rope - Unlock all Boxes',1),
    (7,'Cut the Rope - SP/20',1),
    (8,'Cut the Rope - SP/50',1),
    (9,'Cut the Rope - SP/150',1),
    (10,'Cut the Rope - SP/500',1),
    (11,'Cut the Rope: Comic - #2',1),
    (12,'Cut the Rope: Comic - #3',1),
    (13,'Cut the Rope: Experiments - SP/20',1),
    (14,'Cut the Rope: Experiments - SP/50',1),
    (15,'Cut the Rope: Experiments - SP/150',1),
    (16,'Cut the Rope: Experiments - SP/500',1),
    (17,'Cut the Rope - Star Key',1),
    (18,'Cut the Rope: Experiments - Star Key',1),
    (19,'Generic App',0),
    (20,'Generic InApp',1),
    (21,'DEFAULT_META_GAME',0),
    (22,'Pudding Monsters',0),
    (23,'Pudding Monsters: Disable Ads',1),
    (24,'Mushroom Boost 6',1),
    (25,'Mushroom Boost 12',1),
    (26,'Mushroom Boost 25',1),
    (27,'Mushroom Boost 50',1),
    (28,'Pudding Monsters: Unlock all Episodes',1);
/*!40000 ALTER TABLE `meta_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_game_game_type`
--

DROP TABLE IF EXISTS `meta_game_game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_game_game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `icon_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `icon_path` varchar(255) COLLATE utf8_bin NOT NULL,
  `game_type_id` bigint(20) NOT NULL,
  `meta_game_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `meta_game_id` (`meta_game_id`,`game_type_id`),
  KEY `FKA47110D43E50EF93` (`meta_game_id`),
  KEY `FKA47110D4E9E03AED` (`game_type_id`),
  CONSTRAINT `FKA47110D43E50EF93` FOREIGN KEY (`meta_game_id`) REFERENCES `meta_game` (`id`),
  CONSTRAINT `FKA47110D4E9E03AED` FOREIGN KEY (`game_type_id`) REFERENCES `game_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_game_game_type`
--

LOCK TABLES `meta_game_game_type` WRITE;
/*!40000 ALTER TABLE `meta_game_game_type` DISABLE KEYS */;
INSERT INTO `meta_game_game_type` (`id`, `icon_name`, `icon_path`, `game_type_id`, `meta_game_id`) VALUES
    (1,'cut-the-rope.png','/uploads/images/cut-the-rope/',2,1),
    (2,'cut-the-rope-free.png','/uploads/images/cut-the-rope-free/',1,1),
    (3,'the-comic.png','/uploads/images/comic/',1,3),
    (4,'experiments.png','/uploads/images/experimentas/',2,4),
    (5,'holiday-gift.png','/uploads/images/holiday-gift/',1,5),
    (6,'experiments-free.png','/uploads/images/experimentas-free/',1,4);
/*!40000 ALTER TABLE `meta_game_game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner`
--

DROP TABLE IF EXISTS `partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `royalty` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner`
--

LOCK TABLES `partner` WRITE;
/*!40000 ALTER TABLE `partner` DISABLE KEYS */;
INSERT INTO `partner` (`id`, `name`, `royalty`) VALUES
    (1,'Chillingo',0.3);
/*!40000 ALTER TABLE `partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `platform` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `platform`
--

LOCK TABLES `platform` WRITE;
/*!40000 ALTER TABLE `platform` DISABLE KEYS */;
INSERT INTO `platform` (`id`, `name`) VALUES
    (1,'Android'),
    (2,'BlackBerry OS (RIM)'),
    (3,'iOS'),
    (4,'Mac OS'),
    (5,'Symbian OS');
/*!40000 ALTER TABLE `platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_ui`
--

DROP TABLE IF EXISTS `report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_ui` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(50) COLLATE utf8_bin NOT NULL,
  `login_page_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `unit_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `finance_domain` varchar(255) COLLATE utf8_bin NOT NULL,
  `code` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT 'fake',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_ui`
--

LOCK TABLES `report_ui` WRITE;
/*!40000 ALTER TABLE `report_ui` DISABLE KEYS */;
INSERT INTO `report_ui` (`id`, `login`, `name`, `password`, `login_page_domain`, `unit_domain`, `finance_domain`, `code`) VALUES
    (1,'ZeptoLab','Chillingo','OmNom123','http://www.clickgamer.com','http://www.clickgamer.com','http://www.clickgamer.com','chillingo'),
    (2,'konstantin.kolotyuk@7bits.it','ITunes','7bitsitunes','https://itunesconnect.apple.com','https://itunesconnect.apple.com','https://itunesconnect.apple.com','itunes'),
    (3,'zepto.parser@gmail.com','Google play','ZLParserPass','https://market.android.com','https://play.google.com','https://play.google.com','google'),
    (4,'konstantin.kolotyuk@7bits.it','Amazon','7bitsamazon','https://developer.amazon.com','https://developer.amazon.com','https://developer.amazon.com','amazon'),
    (5,'accounts@zeptolab.com','Nook','NookZLPass','https://nookdeveloper.barnesandnoble.com','https://nookdeveloper.barnesandnoble.com','https://nookdeveloper.barnesandnoble.com','nook'),
    (6,'accounts@zeptolab.com','Ovi','ZLNokiaPass1','https://publish.nokia.com','https://reports.nokia.com','https://publish.nokia.com','ovi'),
    (7,'konstantin.kolotyuk@7bits.it','BlackBerry','zeptolab','https://appworld.blackberry.com','https://appworld.blackberry.com','https://appworld.blackberry.com','blackberry'),
    (8,'accounts@zeptolab.com','GetJar','ZLGetJarPass','https://developer.getjar.com','https://developer.getjar.com','https://developer.getjar.com','getjar');
/*!40000 ALTER TABLE `report_ui` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resolution`
--

DROP TABLE IF EXISTS `resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(24) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resolution`
--

LOCK TABLES `resolution` WRITE;
/*!40000 ALTER TABLE `resolution` DISABLE KEYS */;
INSERT INTO `resolution` (`id`, `name`) VALUES
    (1,'HD'),
    (2,'SD'),
    (3,'DESKTOP');
/*!40000 ALTER TABLE `resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store`
--

DROP TABLE IF EXISTS `store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `commission` double NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `platform_id` bigint(20) NOT NULL,
  `store_logo_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK68AF8E1AE932C88` (`platform_id`),
  KEY `FK68AF8E110A0530F` (`store_logo_id`),
  CONSTRAINT `FK68AF8E110A0530F` FOREIGN KEY (`store_logo_id`) REFERENCES `store_logo` (`id`),
  CONSTRAINT `FK68AF8E1AE932C88` FOREIGN KEY (`platform_id`) REFERENCES `platform` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store`
--

LOCK TABLES `store` WRITE;
/*!40000 ALTER TABLE `store` DISABLE KEYS */;
INSERT INTO `store` (`id`, `commission`, `name`, `platform_id`, `store_logo_id`) VALUES
    (2,0.3,'App Store',1,1),
    (3,0.3,'Mac App Store',1,2),
    (4,0.3,'Google play',2,3),
    (5,0.3,'Amazon',3,4),
    (6,0.3,'Nook',3,5),
    (7,0.4,'Ovi',3,6),
    (8,0.3,'BlackBerry App World',3,7),
    (9,0.3,'GetJar',3,8),
    (10,0.3,'FAKE',3,8);
/*!40000 ALTER TABLE `store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_report_ui`
--

DROP TABLE IF EXISTS `store_report_ui`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `store_report_ui` (
  `store_id` bigint(20) NOT NULL,
  `report_ui_id` bigint(20) NOT NULL,
  PRIMARY KEY (`store_id`,`report_ui_id`),
  KEY `FKDE67AFA1133D4251` (`report_ui_id`),
  KEY `FKDE67AFA187E4952C` (`store_id`),
  CONSTRAINT `FKDE67AFA187E4952C` FOREIGN KEY (`store_id`) REFERENCES `store` (`id`),
  CONSTRAINT `FKDE67AFA1133D4251` FOREIGN KEY (`report_ui_id`) REFERENCES `report_ui` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_report_ui`
--

LOCK TABLES `store_report_ui` WRITE;
/*!40000 ALTER TABLE `store_report_ui` DISABLE KEYS */;
INSERT INTO `store_report_ui` (`store_id`, `report_ui_id`) VALUES
    (2,1),
    (2,2),
    (3,2),
    (4,3),
    (5,4),
    (6,5),
    (7,6),
    (8,7),
    (9,8),
    (10,8);
/*!40000 ALTER TABLE `store_report_ui` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

SET FOREIGN_KEY_CHECKS = 1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-14 11:38:38
