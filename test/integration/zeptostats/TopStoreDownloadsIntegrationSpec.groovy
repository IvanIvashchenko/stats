package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec;

class TopStoreDownloadsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql')
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql')
    }

    def "fetch downloads for games in top store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/downloads.sql')

        //Regular case
        when:
        def gameIds = [16,17];
        def countryIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 5, code: "AF", cname: "Afghanistan", game_id: 16, country_id: 2],
                [downloads: 3, code: "AF", cname: "Afghanistan", game_id: 17, country_id: 2]
            ],
            "United States": [
                [downloads: 8, code: "US", cname: "United States", game_id: 16, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 17, country_id: 1]
            ]
        ].equals(appsDownloads)

        //Empty countries list
        when:
        gameIds = [16,17];
        countryIds = [];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [:].equals(appsDownloads)

        //Empty games list
        when:
        gameIds = [];
        countryIds = [1,2];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [:].equals(appsDownloads)

        //Wrong time period
        when:
        gameIds = [16,17];
        countryIds = [1,2];
        storeId = 2;
        savedTime = [from_time: 1350455310000,to_time:1350456310000];
        appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 16, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 17, country_id: 2]
            ],
            "United States": [
                [downloads: 0, code: "US", cname: "United States", game_id: 16, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 17, country_id: 1]
            ]
        ].equals(appsDownloads)

    }

    def "fetch downloads for games in top store with empty unit"() {
        when:
        def gameIds = [16,17];
        def countryIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 16, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 17, country_id: 2]
            ],
            "United States": [
                [downloads: 0, code: "US", cname: "United States", game_id: 16, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 17, country_id: 1]
            ]
        ].equals(appsDownloads)
    }

    def "fetch downloads for games from another store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/downloads.sql')

        when:
        def gameIds = [16,17];
        def countryIds = [1,2];
        def storeId = 4;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [:].equals(appsDownloads)
    }

    def "fetch downloads for games in special order in sorted countries"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/ordered_games_downloads.sql')

        when:
        def gameIds = [45,44,16,17,31,18];
        def countryIds = [3,1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 16, country_id: 2],
                [downloads: 5, code: "AF", cname: "Afghanistan", game_id: 17, country_id: 2],
                [downloads: 2, code: "AF", cname: "Afghanistan", game_id: 31, country_id: 2],
                [downloads: 1, code: "AF", cname: "Afghanistan", game_id: 44, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 45, country_id: 2],
                [downloads: 1, code: "AF", cname: "Afghanistan", game_id: 18, country_id: 2]
            ],
            "Albania": [
                [downloads: 0, code: "AL", cname: "Albania", game_id: 16, country_id: 3],
                [downloads: 0, code: "AL", cname: "Albania", game_id: 17, country_id: 3],
                [downloads: 0, code: "AL", cname: "Albania", game_id: 31, country_id: 3],
                [downloads: 0, code: "AL", cname: "Albania", game_id: 44, country_id: 3],
                [downloads: 5, code: "AL", cname: "Albania", game_id: 45, country_id: 3],
                [downloads: 0, code: "AL", cname: "Albania", game_id: 18, country_id: 3]
            ],
            "United States": [
                [downloads: 8, code: "US", cname: "United States", game_id: 16, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 17, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 31, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 44, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 45, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 18, country_id: 1]
            ]
        ].equals(appsDownloads)

    }

    def "fetch downloads for games without inapps"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/downloads.sql')

        when:
        def gameIds = [16,22,17,21];
        def countryIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 5, code: "AF", cname: "Afghanistan", game_id: 16, country_id: 2],
                [downloads: 3, code: "AF", cname: "Afghanistan", game_id: 17, country_id: 2]
            ],
            "United States": [
                [downloads: 8, code: "US", cname: "United States", game_id: 16, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 17, country_id: 1]
            ]
        ].equals(appsDownloads)
    }

}