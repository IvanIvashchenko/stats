package zeptostats

import spock.lang.*
import zeptostats.Step
import zeptostats.TimeService
import grails.plugin.spock.IntegrationSpec
import java.text.SimpleDateFormat
import java.util.Calendar

class TimeServiceSpec extends IntegrationSpec {

    private static final int NEED_DAY_OF_WEEK = Calendar.MONDAY;
    private static final int NEED_DAY_OF_MONTH = 5;
    def TimeService timeService;

    /** --- getTimestampPreviousOrCurWeekDay --- */

    def "getTimestampPreviousOrCurWeekDay Sat of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1345852800000; //NOTE:: Sat, 25 Aug 2012 00:00:00 GMT
        def Long expected = 1345420800000;  //NOTE:: Mon, 20 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Sun of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1345939200000; //NOTE:: Sun, 26 Aug 2012 00:00:00 GMT
        def Long expected = 1345420800000;  //NOTE:: Mon, 20 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Mon of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346025600000; //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Tue of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346112000000; //NOTE:: Tue, 28 Aug 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Wed of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346198400000; //NOTE:: Wed, 29 Aug 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Thu of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346284800000; //NOTE:: Thu, 30 Aug 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Fri of same month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346371200000; //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Sat of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346457600000; //NOTE:: Sat, 01 Sep 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Sun of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1346544000000; //NOTE:: Sun, 02 Sep 2012 00:00:00 GMT
        def Long expected = 1346025600000;  //NOTE:: Mon, 27 Aug 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Tue of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1356998400000; //NOTE:: Tue, 01 Jan 2013 00:00:00 GMT
        def Long expected = 1356912000000;  //NOTE:: Mon, 31 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Wed of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1357084800000; //NOTE:: Wed, 02 Jan 2013 00:00:00 GMT
        def Long expected = 1356912000000;  //NOTE:: Mon, 31 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Thu of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1357171200000; //NOTE:: Thu, 03 Jan 2013 00:00:00 GMT
        def Long expected = 1356912000000;  //NOTE:: Mon, 31 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurWeekDay Fri of next month (Previous or Current Mon is need)" () {

        when:

        def Long timestamp = 1357257600000; //NOTE:: Fri, 04 Jan 2013 00:00:00 GMT
        def Long expected = 1356912000000;  //NOTE:: Mon, 31 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    /** --- getTimestampNextOrCurWeekDay --- */

    def "getTimestampNextOrCurWeekDay Mon of same month (Next Mon or Current is need)" () {

        when:

        def Long timestamp = 1358726400000; //NOTE:: Mon, 21 Jan 2013 00:00:00 GMT
        def Long expected = 1358726400000;  //NOTE:: Mon, 21 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Tue of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1358812800000; //NOTE:: Tue, 22 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Wed of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1358899200000; //NOTE:: Wed, 23 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Thu of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1358985600000; //NOTE:: Thu, 24 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Fri of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359072000000; //NOTE:: Fri, 25 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Sat of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359158400000; //NOTE:: Sat, 26 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Sun of same month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359244800000; //NOTE:: Sun, 27 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Mon of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359331200000; //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Tue of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359417600000; //NOTE:: Tue, 29 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Wed of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359504000000; //NOTE:: Wed, 30 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Thu of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1359590400000; //NOTE:: Thu, 31 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Fri of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1348790400000; //NOTE:: Fri, 28 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Sat of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1348876800000; //NOTE:: Sat, 29 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextOrCurWeekDay Sun of previous month (Next or Current Mon is need)" () {

        when:

        def Long timestamp = 1348963200000; //NOTE:: Sun, 30 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextOrCurWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    /** --- getTimestampNextWeekDay --- */

    def "getTimestampNextWeekDay Mon of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1358726400000; //NOTE:: Mon, 21 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Tue of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1358812800000; //NOTE:: Tue, 22 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Wed of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1358899200000; //NOTE:: Wed, 23 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Thu of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1358985600000; //NOTE:: Thu, 24 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Fri of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359072000000; //NOTE:: Fri, 25 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Sat of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359158400000; //NOTE:: Sat, 26 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Sun of same month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359244800000; //NOTE:: Sun, 27 Jan 2013 00:00:00 GMT
        def Long expected = 1359331200000;  //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Mon of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359331200000; //NOTE:: Mon, 28 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Tue of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359417600000; //NOTE:: Tue, 29 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Wed of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359504000000; //NOTE:: Wed, 30 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Thu of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1359590400000; //NOTE:: Thu, 31 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: Mon, 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Fri of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1348790400000; //NOTE:: Fri, 28 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Sat of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1348876800000; //NOTE:: Sat, 29 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextWeekDay Sun of previous month (Next Mon is need)" () {

        when:

        def Long timestamp = 1348963200000; //NOTE:: Sun, 30 Sep 2012 00:00:00 GMT
        def Long expected = 1349049600000;  //NOTE:: Mon, 01 Oct 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampNextWeekDay(timestamp, NEED_DAY_OF_WEEK);

        then:
        expected.equals(actual);
    }


    /** --- getTimestampPreviousOrCurMonthDay --- */

    def "getTimestampPreviousOrCurMonthDay 5 day of same month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1357344000000; //NOTE:: 05 Jan 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 6 day of same month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1357430400000; //NOTE:: 06 Jan 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 7 day of same month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1357516800000; //NOTE:: 07 Jan 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 29 day of same month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1359417600000; //NOTE:: 29 Jan 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 3 day of next month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1359849600000; //NOTE:: 03 Feb 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 4 day of next month of same year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1359936000000; //NOTE:: 04 Feb 2013 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 3 day of next month of next year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1357171200000; //NOTE:: 03 Jan 2013 00:00:00 GMT
        def Long expected = 1354665600000;  //NOTE:: 05 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampPreviousOrCurMonthDay 4 day of next month of next year (Previous or Current 5 day of month is need)" () {

        when:

        def Long timestamp = 1357257600000; //NOTE:: 04 Jan 2013 00:00:00 GMT
        def Long expected = 1354665600000;  //NOTE:: 05 Dec 2012 00:00:00 GMT

        def Long actual = timeService.getTimestampPreviousOrCurMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }


    /** --- getTimestampNextMonthDay --- */

    def "getTimestampNextMonthDay 5 day of previous month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1357344000000; //NOTE:: 05 Jan 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 6 day of previous month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1357430400000; //NOTE:: 06 Jan 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 7 day of previous month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1357516800000; //NOTE:: 07 Jan 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 29 day of previous month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1359417600000; //NOTE:: 29 Jan 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 3 day of same month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1359849600000; //NOTE:: 03 Feb 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 4 day of same month of same year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1359936000000; //NOTE:: 04 Feb 2013 00:00:00 GMT
        def Long expected = 1360022400000;  //NOTE:: 05 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 5 day of previous month of previous year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1354665600000; //NOTE:: 05 Dec 2012 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 6 day of previous month of previous year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1354752000000; //NOTE:: 06 Dec 2012 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 7 day of previous month of previous year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1354838400000; //NOTE:: 07 Dec 2012 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthDay 29 day of previous month of previous year (Next 5 day of month is need)" () {

        when:

        def Long timestamp = 1356739200000; //NOTE:: 29 Dec 2012 00:00:00 GMT
        def Long expected = 1357344000000;  //NOTE:: 05 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }


    /** --- getTimestampNextMonthPrevDay --- */

    def "getTimestampNextMonthPrevDay 5 day of previous month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1357344000000; //NOTE:: 05 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 6 day of previous month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1357430400000; //NOTE:: 06 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 7 day of previous month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1357516800000; //NOTE:: 07 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 29 day of previous month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1359417600000; //NOTE:: 29 Jan 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 3 day of same month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1359849600000; //NOTE:: 03 Feb 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 4 day of same month of same year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1359936000000; //NOTE:: 04 Feb 2013 00:00:00 GMT
        def Long expected = 1359936000000;  //NOTE:: 04 Feb 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 5 day of previous month of previous year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1354665600000; //NOTE:: 05 Dec 2012 00:00:00 GMT
        def Long expected = 1357257600000;  //NOTE:: 04 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 6 day of previous month of previous year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1354752000000; //NOTE:: 06 Dec 2012 00:00:00 GMT
        def Long expected = 1357257600000;  //NOTE:: 04 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 7 day of previous month of previous year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1354838400000; //NOTE:: 07 Dec 2012 00:00:00 GMT
        def Long expected = 1357257600000;  //NOTE:: 04 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

    def "getTimestampNextMonthPrevDay 29 day of previous month of previous year (Next or Current 4 day of month is need)" () {

        when:

        def Long timestamp = 1356739200000; //NOTE:: 29 Dec 2012 00:00:00 GMT
        def Long expected = 1357257600000;  //NOTE:: 04 Jan 2013 00:00:00 GMT

        def Long actual = timeService.getTimestampNextMonthPrevDay(timestamp, NEED_DAY_OF_MONTH);

        then:
        expected.equals(actual);
    }

}
