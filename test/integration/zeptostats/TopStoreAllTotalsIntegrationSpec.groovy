package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class TopStoreAllTotalsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def String ALL_TOTAL_FILTER = "all";

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql');
    }

    def "fetch total downloads for ordered games without inapps in top store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        //Regular case
        when:
        def gameIds = [16,17,45,39,18,22];
        def countryIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchTopStoreAppsTotals(gameIds, countryIds, ALL_TOTAL_FILTER, storeId, savedTime);

        then:
        [
            [downloads: 13],
            [downloads: 3],
            [downloads: 1],
            [downloads: 2],
            [downloads: 5]
        ].equals(downloads);

        //Empty games list
        when:
        gameIds = [];
        countryIds = [1,2];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchTopStoreAppsTotals(gameIds, countryIds, ALL_TOTAL_FILTER, storeId, savedTime);

        then:
        [].equals(downloads);

        //Empty countries list
        when:
        gameIds = [16,17,45,39,18,22];
        countryIds = [];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchTopStoreAppsTotals(gameIds, countryIds, ALL_TOTAL_FILTER, storeId, savedTime);

        then:
        [
            [downloads: 13],
            [downloads: 3],
            [downloads: 1],
            [downloads: 2],
            [downloads: 5]
        ].equals(downloads);
    }

    def "fetch total downloads for games from another store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def countryIds = [1,2];
        def storeId = 4;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchTopStoreAppsTotals(gameIds, countryIds, ALL_TOTAL_FILTER, storeId, savedTime);

        then:
        [].equals(downloads);
    }

    def "fetch total downloads for games from top store by wrong period"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def countryIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459410000,to_time:1350459510000];
        def downloads = countriesService.fetchTopStoreAppsTotals(gameIds, countryIds, ALL_TOTAL_FILTER, storeId, savedTime);

        then:
        [
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0]
        ].equals(downloads);
    }

}