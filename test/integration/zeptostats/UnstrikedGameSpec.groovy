package zeptostats

import spock.lang.*
import grails.plugin.spock.IntegrationSpec

class UnstrikedGameSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    def CompareDependentColumnService compareDependentColumnService

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/compareDependentColumnService/strike_game.sql');
    }

    //--------------------------------------------------------------------
    // Game1 GameType1
    // Game2 GameType1
    // Game3 GameType2

    def "Games (1) are on. Dependent GameTypes are (1) and GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (2) are on. Dependent GameTypes are (1) and GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (3) are on. Dependent GameTypes are (2)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Games (1,2) are on. Dependent GameTypes are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (1,3) are on. Dependent GameTypes are (1,2) and GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Games (2,3) are on. Dependent GameTypes are (1,2) and GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Games (1,2,3) are on. Dependent GameTypes are (1,2)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameTypesByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 Resolution1
    // Game2 Resolution1
    // Game3 Resolution2

    def "Games (1) are on. Dependent Resolutions are (1) and Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (2) are on. Dependent Resolutions are (1) and Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (3) are on. Dependent Resolutions are (2)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Games (1,2) are on. Dependent Resolutions are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (1,3) are on. Dependent Resolutions are (1,2) and Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Games (2,3) are on. Dependent Resolutions are (1,2) and Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Games (1,2,3) are on. Dependent Resolutions are (1,2)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedResolutionsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 Store2
    // Game2 Store2
    // Game3 Store3

    def "Games (1) are on. Dependent Stores are (2) and Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Games (2) are on. Dependent Stores are (2) and Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Games (3) are on. Dependent Stores are (3)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentUnstrikedIds);
    }

    def "Games (1,2) are on. Dependent Stores are (2)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Games (1,3) are on. Dependent Stores are (2,3) and Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2, 3].equals(dependentUnstrikedIds);
    }

    def "Games (2,3) are on. Dependent Stores are (2,3) and Stores (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2, 3].equals(dependentUnstrikedIds);
    }

    def "Games (1,2,3) are on. Dependent Stores are (2,3)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedStoreIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [2, 3].equals(dependentUnstrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 MetaGame1
    // Game2 MetaGame1
    // Game3 MetaGame2

    def "Games (1) are on. Dependent MetaGames are (1) and MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (2) are on. Dependent MetaGames are (1) and MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (3) are on. Dependent MetaGames are (3)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentUnstrikedIds);
    }

    def "Games (1,2) are on. Dependent MetaGames are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Games (1,3) are on. Dependent MetaGames are (1,3) and MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 3].equals(dependentUnstrikedIds);
    }

    def "Games (2,3) are on. Dependent MetaGames are (1,3) and MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 3].equals(dependentUnstrikedIds);
    }

    def "Games (1,2,3) are on. Dependent MetaGames are (1,3)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allStrikedIdsStr);

        then:
        [1, 3].equals(dependentUnstrikedIds);
    }

}