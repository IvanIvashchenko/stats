package zeptostats

import spock.lang.*
import grails.plugin.spock.IntegrationSpec

class StrikedResolutionSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    def CompareDependentColumnService compareDependentColumnService

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/compareDependentColumnService/strike_resolution.sql');
    }

    //--------------------------------------------------------------------
    // Resolution1 Game1
    // Resolution1 Game2
    // Resolution1 Game3
    // Resolution2 Game4
    // Resolution2 Game5
    // Resolution3 Game6
    // Resolution4 -

    def "Resolutions (1) are striked. Dependent Games are (1,2,3)." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [1, 2, 3].equals(dependentStrikedIds);
    }

    def "Resolutions (2) are striked. Dependent Games are (4,5)." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [4, 5].equals(dependentStrikedIds);
    }

    def "Resolutions (3) are striked. Dependent Games are (6)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [6].equals(dependentStrikedIds);
    }

    def "Resolutions (1,2) are striked. Dependent Games are (1,2,3,4,5)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5].equals(dependentStrikedIds);
    }

    def "Resolutions (1,3) are striked. Dependent Games are (1,2,3,6)." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [1, 2, 3,6].equals(dependentStrikedIds);
    }

    def "Resolutions (2,3) are striked. Dependent Games are (4,5,6)." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [4, 5, 6].equals(dependentStrikedIds);
    }

    def "Resolutions (1,2,3) are striked. Dependent Games are (1,2,3,4,5,6)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5, 6].equals(dependentStrikedIds);
    }

    def "Resolutions (4) are striked. There is no Dependent Game." () {

        when:
        def allStrikedIdsStr = "4";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedResolutions(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }
}