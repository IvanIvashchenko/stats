package zeptostats

import spock.lang.*
import zeptostats.Step
import zeptostats.TimelineService
import grails.plugin.spock.IntegrationSpec
import java.text.SimpleDateFormat

class TimelineUpdatesIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def TimelineService timelineService;

    private static final def MILLISECONDS_IN_SECOND = 1000;
    private static final def SECONDS_IN_MINUTE = 60;
    private static final def MINUTES_IN_HOUR = 60;
    private static final def HOURS_IN_DAY = 24;

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/timeline/updates.sql');
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/timeline/calendar.sql');
    }

    /**
     * NOTE::
        1346371200000 (31.08.2012) - 1 updates - game 17
        1346457600000 (01.09.2012) - 5 updates - game 16
        1346544000000 (02.09.2012) - 3 updates - game 16
        1346630400000 (03.09.2012) - 5 updates - game 16
        1346716800000 (04.09.2012) - 5 updates - game 16
        1346803200000 (05.09.2012) - 2 updates - game 17
        1346889600000 (06.09.2012) - 1 updates - game 17

        1347235200000 (10.09.2012) - 2 updates - game 16
        1347321600000 (11.09.2012) - 1 updates - game 17
        1347408000000 (12.09.2012) - 2 updates - game 16
    */

    def "createBlankUpdateMap daily" () {
        //NOTE:: daily
        when:
        def timeStep = Step.DAILY;
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '2012-08-31':0,
            '2012-09-01':0,
            '2012-09-02':0,
            '2012-09-03':0,
            '2012-09-04':0,
            '2012-09-05':0,
            '2012-09-06':0,
            '2012-09-07':0,
            '2012-09-08':0,
            '2012-09-09':0,
            '2012-09-10':0,
            '2012-09-11':0,
            '2012-09-12':0
        ].equals(actual);
    }

    def "createBlankUpdateMap weekly fri - wed" () {
        //NOTE:: weekly

        when:
        def timeStep = Step.WEEKLY;
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012 Fri
        def toTime = 1347408000000;     //NOTE:: 12.09.2012 Wed
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '201235':0,
            '201236':0,
            '201237':0
        ].equals(actual);
    }

    def "createBlankUpdateMap weekly mon - wed" () {
        //NOTE:: weekly
        when:
        def timeStep = Step.WEEKLY;
        def fromTime = 1346025600000;   //NOTE:: 27.08.2012 Mon
        def toTime = 1347408000000;     //NOTE:: 12.09.2012 Wed
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '201235':0,
            '201236':0,
            '201237':0
        ].equals(actual);
    }

    def "createBlankUpdateMap weekly one week" () {
        //NOTE:: weekly
        when:
        def timeStep = Step.WEEKLY;
        def fromTime = 1346371200000;   //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1346544000000;     //NOTE:: Sun, 02 Sep 2012 00:00:00 GMT
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '201235':0
        ].equals(actual);
    }

    def "createBlankUpdateMap weekly one week" () {
        //NOTE:: weekly
        when:
        def timeStep = Step.WEEKLY;
        def fromTime = 1346371200000;   //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1346457600000;     //NOTE:: Sat, 01 Sep 2012 00:00:00 GMT
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '201235':0
        ].equals(actual);
    }

    def "createBlankUpdateMap weekly two week" () {
        //NOTE:: weekly
        when:
        def timeStep = Step.WEEKLY;
        def fromTime = 1346371200000;   //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1346630400000;     //NOTE:: Mon, 03 Sep 2012 00:00:00 GMT
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '201235':0,
            '201236':0
        ].equals(actual);
    }

    def "createBlankUpdateMap monthly" () {
        //NOTE:: monthly
        when:
        def timeStep = Step.MONTHLY;
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '2012-8':0,
            '2012-9':0
        ].equals(actual);
    }

    def "createBlankUpdateMap monthly one month" () {
        //NOTE:: monthly
        when:
        def timeStep = Step.MONTHLY;
        def fromTime = 1346457600000;   //NOTE:: 01.09.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def actual = timelineService.createBlankUpdateMap(fromTime, toTime, timeStep);

        then:
        [
            '2012-9':0
        ].equals(actual);
    }

    def "fetch daily updates after SQL query" () {
        //NOTE:: daily
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.DAILY;
        def actual = timelineService.fetchUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [updates:1, startStepTime:'2012-08-31'],
            [updates:5, startStepTime:'2012-09-01'],
            [updates:3, startStepTime:'2012-09-02'],
            [updates:5, startStepTime:'2012-09-03'],
            [updates:5, startStepTime:'2012-09-04'],
            [updates:2, startStepTime:'2012-09-05'],
            [updates:1, startStepTime:'2012-09-06'],
            [updates:2, startStepTime:'2012-09-10'],
            [updates:1, startStepTime:'2012-09-11'],
            [updates:2, startStepTime:'2012-09-12']
        ].equals(actual);
    }

    def "fetch daily updates merging maps" () {
        //NOTE:: daily
        when:
        LinkedHashMap<String, Integer> blankUpdateMap = [
            '2012-08-31':0,
            '2012-09-01':0,
            '2012-09-02':0,
            '2012-09-03':0,
            '2012-09-04':0,
            '2012-09-05':0,
            '2012-09-06':0,
            '2012-09-07':0,
            '2012-09-08':0,
            '2012-09-09':0,
            '2012-09-10':0,
            '2012-09-11':0,
            '2012-09-12':0
        ];

        def updates = [
            [updates:1, startStepTime:'2012-08-31'],
            [updates:5, startStepTime:'2012-09-01'],
            [updates:3, startStepTime:'2012-09-02'],
            [updates:5, startStepTime:'2012-09-03'],
            [updates:5, startStepTime:'2012-09-04'],
            [updates:2, startStepTime:'2012-09-05'],
            [updates:1, startStepTime:'2012-09-06'],
            [updates:2, startStepTime:'2012-09-10'],
            [updates:1, startStepTime:'2012-09-11'],
            [updates:2, startStepTime:'2012-09-12']
        ];

        def actual = timelineService.mergeUpdates(blankUpdateMap, updates);

        then:
        [
            '2012-08-31':1,
            '2012-09-01':5,
            '2012-09-02':3,
            '2012-09-03':5,
            '2012-09-04':5,
            '2012-09-05':2,
            '2012-09-06':1,
            '2012-09-07':0,
            '2012-09-08':0,
            '2012-09-09':0,
            '2012-09-10':2,
            '2012-09-11':1,
            '2012-09-12':2
        ].equals(actual);
    }

    def "fetch daily updates" () {
        //NOTE:: daily
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.DAILY;
        def actual = timelineService.fetchGamesUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            '2012-08-31':1,
            '2012-09-01':5,
            '2012-09-02':3,
            '2012-09-03':5,
            '2012-09-04':5,
            '2012-09-05':2,
            '2012-09-06':1,
            '2012-09-07':0,
            '2012-09-08':0,
            '2012-09-09':0,
            '2012-09-10':2,
            '2012-09-11':1,
            '2012-09-12':2
        ].equals(actual);
    }

    def "fetch weekly updates SQL query" () {
        //NOTE:: weekly
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.WEEKLY;
        def actual = timelineService.fetchUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [updates:9, startStepTime:'201235'],
            [updates:13, startStepTime:'201236'],
            [updates:5, startStepTime:'201237']
        ].equals(actual);
    }

    def "fetch weekly updates merging maps" () {
        //NOTE:: weekly
        when:
        LinkedHashMap<String,Integer> blankUpdateMap = [
            '201235':0,
            '201236':0,
            '201237':0
        ];

        def updates = [
            [updates:9, startStepTime:'201235'],
            [updates:13, startStepTime:'201236'],
            [updates:5, startStepTime:'201237']
        ];

        def actual = timelineService.mergeUpdates(blankUpdateMap, updates);

        then:
        [
            '201235':9,
            '201236':13,
            '201237':5
        ].equals(actual);
    }

    def "fetch weekly updates" () {
        //NOTE:: weekly
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.WEEKLY;
        def actual = timelineService.fetchGamesUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            '201235':9,
            '201236':13,
            '201237':5
        ].equals(actual);
    }

    def "fetch monthly updates SQL query" () {
        //NOTE:: monthly
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.MONTHLY;
        def actual = timelineService.fetchUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [updates:1, startStepTime:'2012-8'],
            [updates:26, startStepTime:'2012-9']
        ].equals(actual);
    }

    def "fetch monthly updates merging maps" () {
        //NOTE:: monthly
        when:
        LinkedHashMap<String,Integer> blankUpdateMap = [
            '2012-8':0,
            '2012-9':0
        ];

        def updates = [
            [updates:1, startStepTime:'2012-8'],
            [updates:26, startStepTime:'2012-9']
        ];

        def actual = timelineService.mergeUpdates(blankUpdateMap, updates);

        then:
        [
            '2012-8':1,
            '2012-9':26
        ].equals(actual);
    }

    def "fetch monthly updates" () {
        //NOTE:: monthly
        when:
        def metaGameIds = [-1];
        def storeIds = [-1];
        def resolutionIds = [-1];
        def gameTypeIds = [-1];
        def countryIds = [-1];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = Step.MONTHLY;
        def actual = timelineService.fetchGamesUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            '2012-8':1,
            '2012-9':26
        ].equals(actual);
    }

/*
    def "fetch updates for given apps (wrong cases)" () {

        //Empty games list
        when:
        def gameIds = [16,17,83];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;   //NOTE:: 31.08.2012
        def toTime = 1347408000000;     //NOTE:: 12.09.2012
        def timeStep = "month";
        def actual = timelineService.fetchGamesUpdates([], countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(actual);

        //Empty countries list
        when:
        actual = timelineService.fetchGamesUpdates(gameIds, [], fromTime, toTime, timeStep);

        then:
        [].equals(actual);

        //Wrong time period
        when:
        fromTime = this.futureTimestampInSec(1);
        toTime = this.futureTimestampInSec(2);
        actual = timelineService.fetchGamesUpdates(gameIds, countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(actual);
    }
    */

    private def futureTimestampInSec(final int dayCount) {
        System.currentTimeMillis() + dayCount * HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE * MILLISECONDS_IN_SECOND;
    }
}