package zeptostats

import java.util.List;

import spock.lang.*
import grails.plugin.spock.IntegrationSpec
import zeptostats.CompareColumn;
import zeptostats.DataExportCheckerService
import zeptostats.Step;

class DataExportCheckerServiceSpec extends IntegrationSpec {

    /**
     * checkCompareColumnsStepAndCurves & Rankings
     * checkCompareColumnsStepAndCurves & 2LevelColumns
     * checkCompareColumnsStepAndCurves & 1LevelColumn
     * checkCompareColumnsStepAndCurves & justAvailable
     * checkCompareColumnsStepAndCurves & multiple
     * 
     * configureNeedSortedExportColumns & Rankings
     * configureNeedSortedExportColumns & 2LevelColumns
     * configureNeedSortedExportColumns & justAvailable
     * checkCompareColumnsStepAndCurves & multiple
     */

    def DataExportCheckerService dataExportCheckerService;

    /** --- checkCompareColumnsStepAndCurves & Rankings */
    def "checkCompareColumnsStepAndCurves & Rankings {DAILY; <SKU>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {DAILY; <COUNTRY>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.COUNTRY]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {DAILY; <REGION>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.REGION]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {DAILY; <SKU, COUNTRY>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException);
    }

    def "checkCompareColumnsStepAndCurves & Rankings {WEEKLY; <SKU, COUNTRY>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Step needStepSafe = Step.WEEKLY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {WEEKLY; <VERSION>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.VERSION]
        Step needStepSafe = Step.WEEKLY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {MONTHLY; <SKU, COUNTRY>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Step needStepSafe = Step.MONTHLY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & Rankings {MONTHLY; <STYLE>; Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STYLE]
        Step needStepSafe = Step.MONTHLY
        Boolean isNeedRankings = true

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    /** --- checkCompareColumnsStepAndCurves & 2LevelColumns */
    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <STORE, OS>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STORE, CompareColumn.OS]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <REGION, COUNTRY>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.REGION, CompareColumn.COUNTRY]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <METASKU, SKU>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.METASKU, CompareColumn.SKU]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <STORE>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STORE]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <OS>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.OS]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <REGION>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.REGION]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <COUNTRY>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.COUNTRY]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <METASKU>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.METASKU]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 2LevelColumns {DAILY; <SKU>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    /** --- checkCompareColumnsStepAndCurves & 1LevelColumn */
    def "checkCompareColumnsStepAndCurves & 1LevelColumn {DAILY; <STYLE>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STYLE]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & 1LevelColumns {DAILY; <VERSION>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.VERSION]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    /** --- checkCompareColumnsStepAndCurves & justAvailable */
    def "checkCompareColumnsStepAndCurves & justAvailable {DAILY; <SKU, COUNTRY, VERSION>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY, CompareColumn.VERSION]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & justAvailable {DAILY; <SKU, STYLE>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.STYLE]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & justAvailable {DAILY; <SKU, COUNTRY>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & justAvailable {DAILY; <SKU, REGION>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.REGION]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    /** --- checkCompareColumnsStepAndCurves & multiple */
    def "checkCompareColumnsStepAndCurves & multiple {DAILY; <STYLE, VERSION, COUNTRY, STORE, METASKU>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [
            CompareColumn.STYLE, CompareColumn.VERSION, CompareColumn.COUNTRY, CompareColumn.STORE, CompareColumn.METASKU
        ]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & multiple {DAILY; <STYLE, VERSION, COUNTRY, STORE, OS>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [
            CompareColumn.STYLE, CompareColumn.VERSION, CompareColumn.COUNTRY, CompareColumn.STORE, CompareColumn.OS
        ]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        thrown(DataExportNeedColumnException)
    }

    def "checkCompareColumnsStepAndCurves & multiple {DAILY; <COUNTRY, STORE>; no Rankings}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.COUNTRY, CompareColumn.STORE]
        Step needStepSafe = Step.DAILY
        Boolean isNeedRankings = false

        dataExportCheckerService.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        then:

        notThrown(DataExportNeedColumnException)
    }


    /** --- configureNeedSortedExportColumns & Rankings */
    def "configureNeedSortedExportColumns & Rankings {DAILY; <SKU, COUNTRY>; <Downloads, Revenues, Updates, Refunds, RefundsMonetized, Rankings>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = true
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.META_SKU,
            ExportColumn.SKU,
            ExportColumn.PARENT,
            ExportColumn.VERSION,
            ExportColumn.STYLE,
            ExportColumn.EXTRA,
            ExportColumn.OS,
            ExportColumn.STORE,

            ExportColumn.REGION,
            ExportColumn.COUNTRY,

            ExportColumn.DOWNLOADS,
            ExportColumn.REVENUE,
            ExportColumn.REFUNDS,
            ExportColumn.REFUNDS_MONETIZED,
            ExportColumn.UPDATES,
            ExportColumn.RANKINGS
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & Rankings {WEEKLY; <SKU, COUNTRY>; <Downloads, Revenues, Updates, Refunds, RefundsMonetized, Rankings>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = true
        Step needStepSafe = Step.WEEKLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        thrown(DataExportNeedColumnException);
    }

    def "configureNeedSortedExportColumns & Rankings {MONTHLY; <SKU, COUNTRY>; <Downloads, Revenues, Updates, Refunds, RefundsMonetized, Rankings>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.COUNTRY]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = true
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        thrown(DataExportNeedColumnException);
    }

    /** --- configureNeedSortedExportColumns & 2LevelColumns */
    def "configureNeedSortedExportColumns & 2LevelColumns {DAILY; <STORE, OS>; <Revenues>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STORE, CompareColumn.OS]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:

        thrown(DataExportNeedColumnException);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {WEEKLY; <REGION, COUNTRY>; <Updates>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.REGION, CompareColumn.COUNTRY]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.WEEKLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:

        thrown(DataExportNeedColumnException);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {MONTHLY; <METASKU, SKU>; <Updates>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.METASKU, CompareColumn.SKU]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:

        thrown(DataExportNeedColumnException);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {DAILY; <OS>; <Refunds>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.OS]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.OS,

            ExportColumn.REFUNDS
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {MONTHLY; <STORE>; <RefundsMonetized>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STORE]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.OS,
            ExportColumn.STORE,

            ExportColumn.REFUNDS_MONETIZED
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {WEEKLY; <REGION>; <Downloads, Revenues>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.REGION]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.WEEKLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.REGION,

            ExportColumn.DOWNLOADS,
            ExportColumn.REVENUE
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {MONTHLY; <COUNTRY>; <Downloads, Updates>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.COUNTRY]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.REGION,
            ExportColumn.COUNTRY,

            ExportColumn.DOWNLOADS,
            ExportColumn.UPDATES
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {WEEKLY; <METASKU>; <Refunds, RefundsMonetized>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.METASKU]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = false
        Step needStepSafe = Step.WEEKLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.META_SKU,

            ExportColumn.REFUNDS,
            ExportColumn.REFUNDS_MONETIZED
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & 2LevelColumns {MONTHLY; <SKU>; <Downloads, Revenues, Updates>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.META_SKU,
            ExportColumn.SKU,
            ExportColumn.PARENT,
            ExportColumn.VERSION,
            ExportColumn.STYLE,
            ExportColumn.EXTRA,
            ExportColumn.OS,
            ExportColumn.STORE,

            ExportColumn.DOWNLOADS,
            ExportColumn.REVENUE,
            ExportColumn.UPDATES
        ].equals(actual);
    }

    /** --- configureNeedSortedExportColumns & justAvailable */
    def "configureNeedSortedExportColumns & justAvailable {MONTHLY; <SKU, VERSION>; <Downloads, Revenues, Updates, Refunds, RefundsMonetized>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.VERSION]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        thrown(DataExportNeedColumnException);
    }

    def "configureNeedSortedExportColumns & justAvailable {DAILY; <SKU, VERSION>; <Downloads>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.SKU, CompareColumn.VERSION]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        thrown(DataExportNeedColumnException);
    }

    /** --- checkCompareColumnsStepAndCurves & multiple */
    def "configureNeedSortedExportColumns & multiple {DAILY; <STYLE, VERSION, COUNTRY, OS>; <Downloads, Revenues, Updates>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STYLE, CompareColumn.VERSION, CompareColumn.COUNTRY, CompareColumn.OS]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.VERSION,
            ExportColumn.STYLE,
            ExportColumn.OS,

            ExportColumn.REGION,
            ExportColumn.COUNTRY,

            ExportColumn.DOWNLOADS,
            ExportColumn.REVENUE,
            ExportColumn.UPDATES
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & multiple {WEEKLY; <VERSION, COUNTRY, STORE>; <Revenues, RefundsMonetized>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.VERSION, CompareColumn.COUNTRY, CompareColumn.STORE]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = true
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = true
        Boolean isNeedRankings = false
        Step needStepSafe = Step.WEEKLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.VERSION,
            ExportColumn.OS,
            ExportColumn.STORE,

            ExportColumn.REGION,
            ExportColumn.COUNTRY,

            ExportColumn.REVENUE,
            ExportColumn.REFUNDS_MONETIZED
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & multiple {MONTHLY; <METASKU, COUNTRY>; <Downloads, Updates, Refunds>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.METASKU, CompareColumn.COUNTRY]
        Boolean isNeedDownloads = true
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = true
        Boolean isNeedRefunds = true
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.MONTHLY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:
        [
            ExportColumn.START_DATE,
            ExportColumn.END_DATE,

            ExportColumn.META_SKU,

            ExportColumn.REGION,
            ExportColumn.COUNTRY,

            ExportColumn.DOWNLOADS,
            ExportColumn.REFUNDS,
            ExportColumn.UPDATES
        ].equals(actual);
    }

    def "configureNeedSortedExportColumns & multiple {DAILY; <STYLE, METASKU, SKU>; <>}" () {

        when:

        List<CompareColumn> compareColumnsUnique = [CompareColumn.STYLE, CompareColumn.METASKU, CompareColumn.SKU]
        Boolean isNeedDownloads = false
        Boolean isNeedRevenues = false
        Boolean isNeedUpdates = false
        Boolean isNeedRefunds = false
        Boolean isNeedRefundsMonetized = false
        Boolean isNeedRankings = false
        Step needStepSafe = Step.DAILY

        def List<ExportColumn> actual = dataExportCheckerService.configureNeedSortedExportColumns(
            compareColumnsUnique,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            needStepSafe
        );

        then:

        thrown(DataExportNeedColumnException)
    }
}
