package zeptostats

import java.util.regex.Pattern
import groovy.sql.Sql

class IntegrationTestsHelper {

    static loadFixtures(testDataSource, sqlFilePath) {

        def db = new Sql(testDataSource);
        def sqlString = new File(sqlFilePath).text
        sqlString.split(";").each() {
            if (!it.matches( /\s*/ )) {
                db.execute(it)
            }
        }
        db.close()
    }
}