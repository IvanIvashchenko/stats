package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class OtherStoreAllTotalsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def ALL_TOTAL_FILTER = "all";
    def TOP_STORES_IDS = "2,4";

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql');
    }

    def "fetch total downloads for ordered games without inapps in top store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        //Regular case
        when:
        def gameIds = [16,17,85,84,61,93,83];
        def countryIds = [1];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            countryIds,
            ALL_TOTAL_FILTER, TOP_STORES_IDS,
            savedTime
        );

        then:
        [
            [downloads: 8],
            [downloads: 13],
            [downloads: 9],
            [downloads: 5],
            [downloads: 2]
        ].equals(appsDownloads);

        //Empty games list
        when:
        gameIds = [];
        countryIds = [1];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            countryIds,
            ALL_TOTAL_FILTER, TOP_STORES_IDS,
            savedTime
        );

        then:
        [].equals(appsDownloads);

        //Empty countries list
        when:
        gameIds = [16,17,85,84,61,93,83];
        countryIds = [];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            countryIds,
            ALL_TOTAL_FILTER, TOP_STORES_IDS,
            savedTime
        );

        then:
        [
            [downloads: 8],
            [downloads: 13],
            [downloads: 9],
            [downloads: 5],
            [downloads: 2]
        ].equals(appsDownloads);
    }

    def "fetch total downloads for games from another store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def countryIds = [1,2];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            countryIds,
            ALL_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [].equals(appsDownloads);
    }

    def "fetch total downloads for games from other stores by wrong period"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,85,84,61,93,83];
        def countryIds = [1];
        def savedTime = [from_time: 1350459410000,to_time:1350459510000];
        def appsDownloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            countryIds,
            ALL_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0]
        ].equals(appsDownloads);
    }

}