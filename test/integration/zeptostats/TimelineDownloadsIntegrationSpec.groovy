package zeptostats

import spock.lang.*
import zeptostats.Step
import zeptostats.TimelineService
import grails.plugin.spock.IntegrationSpec

class TimelineDownloadsIntegrationSpec extends IntegrationSpec {

    private static final def MILLISECONDS_IN_SECOND = 1000;
    private static final def SECONDS_IN_MINUTE = 60;
    private static final def MINUTES_IN_HOUR = 60;
    private static final def HOURS_IN_DAY = 24;

    def dataSource;
    @Shared def dataSource_zeptostats;
    def TimelineService timelineService;


    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/timeline/downloads.sql');
    }

    /**
     * NOTE:: all items:
        1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
        1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
        1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
        1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
        1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
        1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
        1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
        1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
        1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)

        1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
        1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
        1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
        1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
     */

    def "fetch weekly inapps downloads for given inapps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //weekly
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:9],
            [downloads:13],
            [downloads:3]
        ].equals(downloads);
    }

    def "fetch weekly inapps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //weekly
        when:
        def inappIds = [21,22,35,36, 16,83];
        def countryIds = [1,2,3, 4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:9],
            [downloads:13],
            [downloads:3]
        ].equals(downloads);
    }

    def "fetch weekly inapps downloads for given inapps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //weekly
        when:
        def inappIds = [21,22,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:9],
            [downloads:12],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch weekly inapps downloads for given inapps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //weekly
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:13],
            [downloads:3]
        ].equals(downloads);
    }

    def "fetch weekly inapps downloads for given inapps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //weekly
        when:
        def inappIds = [21,22,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:12],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch weekly apps downloads for given apps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //weekly
        when:
        def appIds = [16,83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:7],
            [downloads:8]
        ].equals(downloads);
    }

    def "fetch weekly apps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //weekly
        when:
        def appIds = [16,83, 21,22];
        def countryIds = [1,4, 2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:7],
            [downloads:8]
        ].equals(downloads);
    }

    def "fetch weekly apps downloads for given apps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //weekly
        when:
        def appIds = [83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch weekly apps downloads for given apps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //weekly
        when:
        def appIds = [16,83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:7],
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch weekly apps downloads for given apps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //weekly
        when:
        def appIds = [83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.WEEKLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch daily inapps downloads for given inapps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //daily
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:5],
            [downloads:3],
            [downloads:5],
            [downloads:5],
            [downloads:2],
            [downloads:1],
            [downloads:2],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch daily inapps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //daily
        when:
        def inappIds = [21,22,35,36, 16,83];
        def countryIds = [1,2,3, 4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:5],
            [downloads:3],
            [downloads:5],
            [downloads:5],
            [downloads:2],
            [downloads:1],
            [downloads:2],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch daily inapps downloads for given inapps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //daily
        when:
        def inappIds = [21,22,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:5],
            [downloads:3],
            [downloads:5],
            [downloads:5],
            [downloads:2],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch daily inapps downloads for given inapps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //daily
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:5],
            [downloads:5],
            [downloads:2],
            [downloads:1],
            [downloads:2],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch daily inapps downloads for given inapps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //daily
        when:
        def inappIds = [21,22,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:5],
            [downloads:5],
            [downloads:2],
            [downloads:1]
        ].equals(downloads);
    }

    def "fetch daily apps downloads for given apps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //daily
        when:
        def appIds = [16,83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:7],
            [downloads:8]
        ].equals(downloads);
    }

    def "fetch daily apps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //daily
        when:
        def appIds = [16,83, 21,22];
        def countryIds = [1,4, 2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:7],
            [downloads:8]
        ].equals(downloads);
    }

    def "fetch daily apps downloads for given apps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 1 country - 83 game (App)
         */

        //daily
        when:
        def appIds = [83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch daily apps downloads for given apps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //daily
        when:
        def appIds = [16,83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:7],
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch daily apps downloads for given apps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //daily
        when:
        def appIds = [83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.DAILY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch monthly inapps downloads for given inapps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //monthly
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:24]
        ].equals(downloads);
    }

    def "fetch monthly inapps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //monthly
        when:
        def inappIds = [21,22,35,36, 16,83];
        def countryIds = [1,2,3, 4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:24]
        ].equals(downloads);
    }

    def "fetch monthly inapps downloads for given inapps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346457600000 (Sat, 01 Sep 2012 00:00:00 GMT) - 5 downloads - 1 country - 21 game (InApp)
            1346544000000 (Sun, 02 Sep 2012 00:00:00 GMT) - 3 downloads - 1 country - 21 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //monthly
        when:
        def inappIds = [21,22,36];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:21]
        ].equals(downloads);
    }

    def "fetch monthly inapps downloads for given inapps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 35 game (InApp)
            1347235200000 (Mon, 10 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 35 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //monthly
        when:
        def inappIds = [21,22,35,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:16]
        ].equals(downloads);
    }

    def "fetch monthly inapps downloads for given inapps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 1 downloads - 3 country - 36 game (InApp)
            1346630400000 (Mon, 03 Sep 2012 00:00:00 GMT) - 5 downloads - 2 country - 22 game (InApp)
            1346716800000 (Tue, 04 Sep 2012 00:00:00 GMT) - 5 downloads - 3 country - 22 game (InApp)
            1346803200000 (Wed, 05 Sep 2012 00:00:00 GMT) - 2 downloads - 2 country - 22 game (InApp)
            1347321600000 (Tue, 11 Sep 2012 00:00:00 GMT) - 1 downloads - 2 country - 36 game (InApp)
         */

        //monthly
        when:
        def inappIds = [21,22,36];
        def countryIds = [2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:1],
            [downloads:13]
        ].equals(downloads);
    }

    def "fetch monthly apps downloads for given apps(exact) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //monthly
        when:
        def appIds = [16,83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:15]
        ].equals(downloads);
    }

    def "fetch monthly apps downloads for given games(more) and countries (more)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 2 downloads - 1 country - 16 game (App)
         */

        //monthly
        when:
        def appIds = [16,83, 21,22];
        def countryIds = [1,4, 2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:15]
        ].equals(downloads);
    }

    def "fetch monthly apps downloads for given apps(less) and countries (exact)" () {
        /**
         * NOTE:: suitable items:
            1346371200000 (Fri, 31 Aug 2012 00:00:00 GMT) - 3 downloads - 1 country - 83 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //monthly
        when:
        def appIds = [83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:3],
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch monthly apps downloads for given apps(exact) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1346889600000 (Thu, 06 Sep 2012 00:00:00 GMT) - 7 downloads - 4 country - 16 game (App)
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //monthly
        when:
        def appIds = [16,83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:13]
        ].equals(downloads);
    }

    def "fetch monthly apps downloads for given apps(less) and countries (less)" () {
        /**
         * NOTE:: suitable items:
            1347408000000 (Wed, 12 Sep 2012 00:00:00 GMT) - 6 downloads - 4 country - 83 game (App)
         */

        //monthly
        when:
        def appIds = [83];
        def countryIds = [4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [
            [downloads:6]
        ].equals(downloads);
    }

    def "fetch downloads inapps for given inapps (wrong cases)" () {

        when:
        def inappIds = [21,22,35,36];
        def appIds = [16, 83];
        def countryIds = [1,2,3];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchInAppsDownloads([], countryIds, fromTime, toTime, timeStep);

        //Empty inapps list
        then:
        [].equals(downloads);

        //Apps only
        when:
        downloads = timelineService.fetchInAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(downloads);

        //Empty countries list
        when:
        downloads = timelineService.fetchInAppsDownloads(inappIds, [], fromTime, toTime, timeStep);

        then:
        [].equals(downloads);

        //Wrong time period
        when:
        fromTime = this.futureTimestampInSec(1);
        toTime = this.futureTimestampInSec(2);
        downloads = timelineService.fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(downloads);
    }

    def "fetch downloads apps for given apps (wrong cases)" () {

        when:
        def inappIds = [21,22,35,36];
        def appIds = [16,83];
        def countryIds = [1,4];
        def fromTime = 1346371200000;  //NOTE:: Fri, 31 Aug 2012 00:00:00 GMT
        def toTime = 1347408000000;    //NOTE:: Wed, 12 Sep 2012 00:00:00 GMT
        def timeStep = Step.MONTHLY;
        def downloads = timelineService.fetchAppsDownloads([], countryIds, fromTime, toTime, timeStep);

        //Empty apps list
        then:
        [].equals(downloads);

        //Inapps only
        when:
        downloads = timelineService.fetchAppsDownloads(inappIds, countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(downloads);

        //Empty countries list
        when:
        downloads = timelineService.fetchAppsDownloads(appIds, [], fromTime, toTime, timeStep);

        then:
        [].equals(downloads);

        //Wrong time period
        when:
        fromTime = this.futureTimestampInSec(1);
        toTime = this.futureTimestampInSec(2);
        downloads = timelineService.fetchAppsDownloads(appIds, countryIds, fromTime, toTime, timeStep);

        then:
        [].equals(downloads);
    }

    private def futureTimestampInSec(final int dayCount) {
        System.currentTimeMillis() / MILLISECONDS_IN_SECOND + dayCount * HOURS_IN_DAY * MINUTES_IN_HOUR * SECONDS_IN_MINUTE;
    }
}