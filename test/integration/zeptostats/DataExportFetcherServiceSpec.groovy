package zeptostats

import spock.lang.*
import zeptostats.CompareColumn;
import zeptostats.DataExportCheckerService
import zeptostats.DataExportFetcherService
import grails.plugin.spock.IntegrationSpec
import java.io.FileReader
import groovy.json.JsonSlurper
import groovy.sql.GroovyRowResult

/**
 * Null parameters tests:
 *     nullIdListsTest
 *
 * Strike out tests:
 *     noFiltersTest
 *     noSingleGameTest
 *     noMultipleGamesTest
 *     noGamesTest
 *     noSingleCountryTest
 *     noMultipleCountriesTest
 *     noCountriesTest
 *     noMultipleGamesAndCountriesTest
 *
 * Chechboxes tests:
 *     checkBoxesTest
 *
 * Data summing tests:
 *     dailyDataSumTest
 *     weeklyDataSumTest
 *     monthlyDataSumTest
 *     zeroDownloadsTest
 *
 * Candies tests:
 *     noCandiesTest
 *     versionCandyTest
 *     styleCandyTest
 *     regionCandyTest
 *     countryCandyTest
 *     osCandyTest
 *     storeCandyTest
 *     metaSkuCandyTest
 *     skuCandyTest
 *     skuCandyNoMultipleGamesTest
 *     skuCandyNoGamesTest
 *     skuCountryCandiesTest
 *     skuCountryCandiesNoMultipleGamesCountriesTest
 *
 * Commission and royalty in revenue calculation tests:
 *     revenueCalculatingTest
 *
 * Inapp join tests:
 *     parentGameTest
 *
 * No game periods tests:
 *     noGamePeriodTest
 *
 * Sorting tests:
 *     sortGameTypeTest
 *     sortResolutionTest
 *     sortRegionTest
 *     sortCountryTest
 *     sortMetaSkuTest
 *     sortSkuTest
 *     sortSkuCountryTest
 *     sortOsTest
 *     sortStoreTest
 *
 * Totals tests:
 *     validTotalsTest
 *     noGamePeriodTotalsTest
 *     noUnitsTotalsTest
 *     zeroDownloadsTotalsTest
 *
 * Rankings tests:
 *     rankingTest
*/
class DataExportFetcherServiceSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def DataExportFetcherService dataExportFetcherService;
    def DataExportCheckerService dataExportCheckerService;

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/data_export.sql');
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def nullIdListsTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noFilters.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch(null,null,orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noFiltersTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noFilters.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noSingleGameTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noSingleGame.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([1],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noMultipleGamesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noMultipleGames.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([1..10],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noGamesTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([1..27],[],orderedExportColumns,0,0,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_VERSION): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STYLE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_REGION): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_COUNTRY): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_OS): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STORE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_META_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_PARENT): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_EXTRA): DataExportFetcherService.NONE
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noSingleCountryTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noSingleCountry.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[1],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noMultipleCountriesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noMultipleCountries.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[1,2],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noCountriesTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[1..3],orderedExportColumns,0,0,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_VERSION): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STYLE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_REGION): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_COUNTRY): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_OS): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STORE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_META_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_PARENT): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_EXTRA): DataExportFetcherService.NONE
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noMultipleGamesAndCountriesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/noMultipleGamesAndCountries.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([1..10],[1,2],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-2,1970-1-2]
     */
    def checkBoxesTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,86400000,86400000,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "02 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "02 Jan 1970",
                (DataExportFetcherService.ALIAS_DOWNLOADS): "3",
                (DataExportFetcherService.ALIAS_UPDATES): "7",
                (DataExportFetcherService.ALIAS_REVENUE): "2.97",
                (DataExportFetcherService.ALIAS_REFUNDS): "5",
                (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "4.95"
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-1-5,1970-1-8]
     */
    def dailyDataSumTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/dailySum.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,345600000,604800000,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-12,1970-2-1]
     */
    def weeklyDataSumTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/weeklySum.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.WEEKLY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,950400000,2678400000,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-3-1,1970-5-1]
     */
    def monthlyDataSumTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/monthlySum.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.MONTHLY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,5097600000,10368000000,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noCandiesTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "01 Jan 1970",
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def versionCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/versionCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.VERSION],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def styleCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/styleCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.STYLE],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def regionCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/regionCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.REGION],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def countryCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/countryCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def osCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/osCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.OS],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def storeCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/storeCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.STORE],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def metaSkuCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/metaSkuCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.METASKU],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def skuCandyTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/skuCandy.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def skuCandyNoMultipleGamesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/skuCandyNoMultipleGames.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([3..27],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def skuCandyNoGamesTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([1..27],[],orderedExportColumns,0,0,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "01 Jan 1970",
                (DataExportFetcherService.ALIAS_VERSION): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STYLE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_OS): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_STORE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_META_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_SKU): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_PARENT): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_EXTRA): DataExportFetcherService.NONE
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def skuCountryCandiesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/skuAndCountryCandies.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def skuCountryCandiesNoMultipleGamesCountriesTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/skuAndCountryCandiesNoMultipleGamesAndCountries.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([3..27],[1,3],orderedExportColumns,0,0,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-3,1970-1-3]
     * Checks if commission and royalty are proprly subtracted
     */
    def revenueCalculatingTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/revenueCalculating.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            false, true, false, false, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,172800000,172800000,needStep);

        then:

        expected.equals(actual)
    }

    /**
     * Time: [1970-1-4,1970-1-4]
     */
    def parentGameTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            false, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,259200000,259200000,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "04 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "04 Jan 1970",
                (DataExportFetcherService.ALIAS_VERSION): "LITE",
                (DataExportFetcherService.ALIAS_STYLE): "DESKTOP",
                (DataExportFetcherService.ALIAS_OS): "Android",
                (DataExportFetcherService.ALIAS_STORE): "Ovi",
                (DataExportFetcherService.ALIAS_META_SKU): "Cut the Rope",
                (DataExportFetcherService.ALIAS_SKU): "inapp",
                (DataExportFetcherService.ALIAS_PARENT): "Cut the Rope 1",
                (DataExportFetcherService.ALIAS_EXTRA):"8C"
             ]
         ].equals(actual)
    }

    /**
     * Time: [1970-1-9,1970-1-9]
     */
    def noGamePeriodTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            false, true, false, false, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,691200000,691200000,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "09 Jan 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "09 Jan 1970",
                (DataExportFetcherService.ALIAS_REVENUE): DataExportFetcherService.NONE,
                (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): DataExportFetcherService.NONE
             ]
         ].equals(actual)
    }

    def sortGameTypeTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingGameType.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.VERSION],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortResolutionTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingResolution.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.STYLE],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortRegionTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingRegion.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.REGION],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortCountryTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingCountry.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.COUNTRY],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortMetaSkuTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingMetasku.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.METASKU],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortSkuTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingSku.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortSkuCountryTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingSkuCountry.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315532800000,315619200000,needStep);

        then:

        expected.equals(actual)
    }

    def sortOsTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingOs.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.OS],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315705600000,315792000000,needStep);

        then:

        expected.equals(actual)
    }

    def sortStoreTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/sortingStore.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.STORE],
            true, false, false, false, false, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315705600000,315792000000,needStep);

        then:

        expected.equals(actual)
    }


    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noMultipleCountriesTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([],[1,2],0,0);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): "27",
            (DataExportFetcherService.ALIAS_UPDATES): "27",
            (DataExportFetcherService.ALIAS_REFUNDS): "27",
            (DataExportFetcherService.ALIAS_REVENUE): "26.73",
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "26.73"
         ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noMultipleGamesTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([1..18],[],0,0);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): "27",
            (DataExportFetcherService.ALIAS_UPDATES): "27",
            (DataExportFetcherService.ALIAS_REFUNDS): "27",
            (DataExportFetcherService.ALIAS_REVENUE): "26.73",
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "26.73"
         ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noGamesTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([1..27],[],0,0);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_UPDATES): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REVENUE): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): DataExportFetcherService.NONE
         ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def noCountriesTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([],[1..3],0,0);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_UPDATES): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REVENUE): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): DataExportFetcherService.NONE
         ].equals(actual)
    }

    /**
     * Time: [1970-1-10,1970-1-10]
     */
    def noGamePeriodTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([],[],777600000,777600000);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): "5",
            (DataExportFetcherService.ALIAS_UPDATES): "13",
            (DataExportFetcherService.ALIAS_REFUNDS): "9",
            (DataExportFetcherService.ALIAS_REVENUE): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): DataExportFetcherService.NONE
         ].equals(actual)
    }

    /**
     * Time: [1970-1-11,1970-1-11]
     */
    def noUnitsTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([],[],864000000,864000000);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_UPDATES): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REVENUE): DataExportFetcherService.NONE,
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): DataExportFetcherService.NONE
         ].equals(actual)
    }

    /**
     * Time: [1970-1-1,1970-1-1]
     */
    def validTotalsTest() {

        when:

        def actual = dataExportFetcherService.fetchTotals([],[],0,0);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): "81",
            (DataExportFetcherService.ALIAS_UPDATES): "81",
            (DataExportFetcherService.ALIAS_REFUNDS): "81",
            (DataExportFetcherService.ALIAS_REVENUE): "80.19",
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "80.19"
        ].equals(actual)
    }

    /**
     * Time: [1970-4-21,1970-4-21]
     */
    def zeroDownloadsTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,9504000000,9504000000,needStep);

        then:

        [
            [
                (DataExportFetcherService.ALIAS_START_DATE): "21 Apr 1970",
                (DataExportFetcherService.ALIAS_END_DATE): "21 Apr 1970",
                (DataExportFetcherService.ALIAS_DOWNLOADS): "0",
                (DataExportFetcherService.ALIAS_UPDATES): "0",
                (DataExportFetcherService.ALIAS_REVENUE): "0.00",
                (DataExportFetcherService.ALIAS_REFUNDS): "0",
                (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "0.00"
            ]
        ].equals(actual)
    }

    /**
     * Time: [1970-4-21,1970-4-21]
     */
    def zeroDownloadsTotalsTest() {

        when:

        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [],
            true, true, true, true, true, false, needStep
        );
        def actual = dataExportFetcherService.fetchTotals([],[],9504000000,9504000000);

        then:

        [
            (DataExportFetcherService.ALIAS_DOWNLOADS): "0",
            (DataExportFetcherService.ALIAS_UPDATES): "0",
            (DataExportFetcherService.ALIAS_REFUNDS): "0",
            (DataExportFetcherService.ALIAS_REVENUE): "0.00",
            (DataExportFetcherService.ALIAS_REFUNDS_MONETIZED): "0.00"
        ].equals(actual)
    }

    def rankingTest() {

        when:

        Reader fileReader = new FileReader("test/resources/export/ranking.json");
        JsonSlurper slurper = new JsonSlurper()
        def expected = slurper.parse(fileReader).expected
        def needStep = Step.DAILY
        def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
            [CompareColumn.SKU, CompareColumn.COUNTRY],
            false, false, false, false, false, true, needStep
        );
        def actual = dataExportFetcherService.fetch([],[],orderedExportColumns,315878400000,315878400000, needStep);

        then:

        expected.equals(actual)
    }
}