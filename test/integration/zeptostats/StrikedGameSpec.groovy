package zeptostats

import spock.lang.*
import grails.plugin.spock.IntegrationSpec

class StrikedGameSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    def CompareDependentColumnService compareDependentColumnService

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/compareDependentColumnService/strike_game.sql');
    }

    //--------------------------------------------------------------------
    // Game1 GameType1
    // Game2 GameType1
    // Game3 GameType2

    def "Games (1) are striked. Dependent GameTypes are (1). But GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (2) are striked. Dependent GameTypes are (1). But GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (3) are striked. Dependent GameTypes are (2)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (1,2) are striked. Dependent GameTypes are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentStrikedIds);
    }

    def "Games (1,3) are striked. Dependent GameTypes are (1,2). But GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (2,3) are striked. Dependent GameTypes are (1,2). But GameTypes (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (1,2,3) are striked. Dependent GameTypes are (1,2)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentStrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 Resolution1
    // Game2 Resolution1
    // Game3 Resolution2

    def "Games (1) are striked. Dependent Resolutions are (1). But Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (2) are striked. Dependent Resolutions are (1). But Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (3) are striked. Dependent Resolutions are (2)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (1,2) are striked. Dependent Resolutions are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentStrikedIds);
    }

    def "Games (1,3) are striked. Dependent Resolutions are (1,2). But Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (2,3) are striked. Dependent Resolutions are (1,2). But Resolutions (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (1,2,3) are striked. Dependent Resolutions are (1,2)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentStrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 Store2
    // Game2 Store2
    // Game3 Store3

    def "Games (1) are striked. Dependent Stores are (2). But Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (2) are striked. Dependent Stores are (2). But Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (3) are striked. Dependent Stores are (3)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (1,2) are striked. Dependent Stores are (2)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [2].equals(dependentStrikedIds);
    }

    def "Games (1,3) are striked. Dependent Stores are (2,3). But Stores (2) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (2,3) are striked. Dependent Stores are (2,3). But Stores (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (1,2,3) are striked. Dependent Stores are (2,3)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [2, 3].equals(dependentStrikedIds);
    }

    //--------------------------------------------------------------------
    // Game1 MetaGame1
    // Game2 MetaGame1
    // Game3 MetaGame2

    def "Games (1) are striked. Dependent MetaGames are (1). But MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (2) are striked. Dependent MetaGames are (1). But MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

    def "Games (3) are striked. Dependent MetaGames are (3)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (1,2) are striked. Dependent MetaGames are (1)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [1].equals(dependentStrikedIds);
    }

    def "Games (1,3) are striked. Dependent MetaGames are (1,3). But MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (2,3) are striked. Dependent MetaGames are (1,3). But MetaGames (1) depend on other Games." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [3].equals(dependentStrikedIds);
    }

    def "Games (1,2,3) are striked. Dependent MetaGames are (1,3)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr);

        then:
        [1, 3].equals(dependentStrikedIds);
    }

}