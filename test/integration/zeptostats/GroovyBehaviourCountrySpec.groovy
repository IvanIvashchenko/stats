package zeptostats

import spock.lang.*
import groovy.sql.Sql
import grails.plugin.spock.IntegrationSpec

class GroovyBehaviourCountrySpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    @Shared def longService;

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    protected def output(gameIds, storeId, ids, storeIdStr, sqlQuery, result) {

        System.out.println("gameIds:");
        System.out.println(gameIds);
        System.out.println(gameIds.class);
        System.out.println();
        System.out.println("ids:");
        System.out.println(ids);
        System.out.println(ids.class);
        System.out.println();
        System.out.println("storeId:");
        System.out.println(storeId);
        System.out.println(storeId.class);
        System.out.println();
        System.out.println("storeIdStr:");
        System.out.println(storeIdStr);
        System.out.println(storeIdStr.class);
        System.out.println();
        System.out.println("sqlQuery:");
        System.out.println(sqlQuery);
        System.out.println();
        System.out.println("result:");
        System.out.println(result);
        System.out.println();
        System.out.println();
    }

    protected def testingSqlQueryDollarStr(gameIds, storeId) {

        def db = new Sql(dataSource_zeptostats);
        def ids = longService.configureIdsString(gameIds);
        def storeIdStr = storeId.toString();
        def sqlQuery = """
            select
                resolution.name as resolution,
                game.id,
                game.extra,
                game.title
            from
                game
                inner join resolution resolution on resolution.id=game.resolution_id
            where
                game.parent_game_id is null
                and game.store_id=:store_id
                and game.id in ($ids)
            order by CONCAT(game.meta_game_id,' ',game_type_id), game.id
        """;
        def result = db.rows(sqlQuery, [store_id: storeId.toString()]);

        db.close();

        this.output(gameIds, storeId, ids, storeIdStr, sqlQuery, result);

        result;
    }

    //--------------------------------------------------------------------
    // testingSqlQueryDollarStr
    //
    // 16 App   - 2 Store
    // 17 App   - 2 Store
    // 18 App   - 2 Store
    // 21 InApp - 2 Store
    // 22 InApp - 2 Store
    // 23 InApp - 2 Store
    // 31 App   - 2 Store
    // 34 App   - 2 Store
    // 39 App   - 2 Store
    // 44 App   - 2 Store
    // 45 App   - 2 Store
    // 70 InApp - 4 Store
    // 93 App   - 5 Store
    // 83 App   - 6 Store

    def "testingSqlQueryDollarStr: need Ids are () because of empty game list." () {

        when:
        def gameIds = [];
        def storeId = 2;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are () because of game list contains app for other store." () {

        when:
        def gameIds = [16];
        def storeId = 3;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are () because of game list contains inapp for need store." () {

        when:
        def gameIds = [ 22];
        def storeId = 2;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are () because of game list contains inapp for other store." () {

        when:
        def gameIds = [ 70];
        def storeId = 2;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are (16)." () {

        when:
        def gameIds = [16];
        def storeId = 2;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"]
        ].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are (16,17)." () {

        when:
        def gameIds = [16,17, 22,23, 93,83];
        def storeId = 2;
        def result = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"],
            [resolution: "SD", id: 17, extra: null, title: "Cut the Rope"]
        ].equals(result);
    }

    def "testingSqlQueryDollarStr: need Ids are (16,17,31,44,45,34,39,18)." () {

        when:
        def gameIds = [16,17,31,44,45,34,39,18, 22,21];
        def storeId = 2;
        def resolutions = this.testingSqlQueryDollarStr(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"],
            [resolution: "SD", id: 17, extra: null, title: "Cut the Rope"],
            [resolution: "SD", id: 31, extra: null, title: "Cut the Rope: Comic"],
            [resolution: "HD", id: 44, extra: null, title: "Cut the Rope: Experiments Free HD"],
            [resolution: "SD", id: 45, extra: null, title: "Cut the Rope: Experiments Free"],
            [resolution: "SD", id: 34, extra: null, title: "Cut the Rope: Experiments"],
            [resolution: "HD", id: 39, extra: null, title: "Cut the Rope: Experiments HD"],
            [resolution: "SD", id: 18, extra: null, title: "Cut the Rope: Holiday Gift"]
        ].equals(resolutions);
    }

}