package zeptostats

import spock.lang.*
import grails.plugin.spock.IntegrationSpec

class StrikedGameTypeSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    def CompareDependentColumnService compareDependentColumnService

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/compareDependentColumnService/strike_game_type.sql');
    }

    //--------------------------------------------------------------------
    // GameType1 Game1
    // GameType1 Game2
    // GameType1 Game3
    // GameType2 Game4
    // GameType2 Game5
    // GameType3 Game6
    // GameType4 -

    def "GameTypes (1) are striked. Dependent Games are (1,2,3)." () {

        when:
        def allStrikedIdsStr = "1";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [1, 2, 3].equals(dependentStrikedIds);
    }

    def "GameTypes (2) are striked. Dependent Games are (4,5)." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [4, 5].equals(dependentStrikedIds);
    }

    def "GameTypes (3) are striked. Dependent Games are (6)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [6].equals(dependentStrikedIds);
    }

    def "GameTypes (1,2) are striked. Dependent Games are (1,2,3,4,5)." () {

        when:
        def allStrikedIdsStr = "1, 2";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5].equals(dependentStrikedIds);
    }

    def "GameTypes (1,3) are striked. Dependent Games are (1,2,3,6)." () {

        when:
        def allStrikedIdsStr = "1, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [1, 2, 3,6].equals(dependentStrikedIds);
    }

    def "GameTypes (2,3) are striked. Dependent Games are (4,5,6)." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [4, 5, 6].equals(dependentStrikedIds);
    }

    def "GameTypes (1,2,3) are striked. Dependent Games are (1,2,3,4,5,6)." () {

        when:
        def allStrikedIdsStr = "1, 2, 3";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5, 6].equals(dependentStrikedIds);
    }

    def "GameTypes (4) are striked. There is no Dependent Game." () {

        when:
        def allStrikedIdsStr = "4";
        def dependentStrikedIds = compareDependentColumnService.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIdsStr);

        then:
        [].equals(dependentStrikedIds);
    }

}