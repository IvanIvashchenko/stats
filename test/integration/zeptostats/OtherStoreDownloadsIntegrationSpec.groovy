package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class OtherStoreDownloadsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def TOP_STORES_IDS = "2,4"

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql');
    }

    def "fetch downloads for ordered games without inapps in other stores"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql')

        //Regular case
        when:
        def gameIds = [16,17,85,84,61,93,83];
        def countryIds = [1,2];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 7, code: "AF", cname: "Afghanistan", game_id: 61, country_id: 2],
                [downloads: 8, code: "AF", cname: "Afghanistan", game_id: 83, country_id: 2],
                [downloads: 7, code: "AF", cname: "Afghanistan", game_id: 84, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 85, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 93, country_id: 2]
            ],
            "United States": [
                [downloads: 1, code: "US", cname: "United States", game_id: 61, country_id: 1],
                [downloads: 5, code: "US", cname: "United States", game_id: 83, country_id: 1],
                [downloads: 2, code: "US", cname: "United States", game_id: 84, country_id: 1],
                [downloads: 5, code: "US", cname: "United States", game_id: 85, country_id: 1],
                [downloads: 2, code: "US", cname: "United States", game_id: 93, country_id: 1]
            ]
        ].equals(downloads)

        //Empty countries list
        when:
        gameIds = [16,17,85,84,61,93,83];
        countryIds = [];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [:].equals(downloads)

        //Empty games list
        when:
        gameIds = [];
        countryIds = [1,2];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [:].equals(downloads)

        //Wrong time period
        when:
        gameIds = [16,17,85,84,61,93,83];
        countryIds = [1,2];
        savedTime = [from_time: 1350455310000,to_time:1350456310000];
        downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 61, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 83, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 84, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 85, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 93, country_id: 2]
            ],
            "United States": [
                [downloads: 0, code: "US", cname: "United States", game_id: 61, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 83, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 84, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 85, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 93, country_id: 1]
            ]
        ].equals(downloads)
    }

    def "fetch downloads for games from other stores by wrong period"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        when:
        def gameIds = [16,17,85,84,61,93,83];
        def countryIds = [1,2];
        def savedTime = [from_time: 1350459410000,to_time:1350459510000];
        def downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 61, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 83, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 84, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 85, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 93, country_id: 2]
            ],
            "United States": [
                [downloads: 0, code: "US", cname: "United States", game_id: 61, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 83, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 84, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 85, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 93, country_id: 1]
            ]
        ].equals(downloads)
    }

    def "fetch downloads for games from other stores witch has no unit records"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        when:
        def gameIds = [104,102,103];
        def countryIds = [1,2];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchOtherStoreAppsDownloads(gameIds, countryIds, TOP_STORES_IDS, savedTime);

        then:
        [
            "Afghanistan": [
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 102, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 103, country_id: 2],
                [downloads: 0, code: "AF", cname: "Afghanistan", game_id: 104, country_id: 2]
            ],
            "United States": [
                [downloads: 0, code: "US", cname: "United States", game_id: 102, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 103, country_id: 1],
                [downloads: 0, code: "US", cname: "United States", game_id: 104, country_id: 1]
            ]
        ].equals(downloads)
    }

}