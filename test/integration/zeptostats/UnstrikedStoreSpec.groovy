package zeptostats

import spock.lang.*
import zeptostats.CompareDependentColumnService
import grails.plugin.spock.IntegrationSpec

class UnstrikedStoreSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    def CompareDependentColumnService compareDependentColumnService

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/compareDependentColumnService/strike_store.sql');
    }

    //--------------------------------------------------------------------
    // Store2 Game1
    // Store2 Game2
    // Store2 Game3
    // Store3 Game4
    // Store3 Game5
    // Store4 Game6
    // Store10 -

    def "Stores (2) are on. Dependent Games are (1,2,3)." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2, 3].equals(dependentUnstrikedIds);
    }

    def "Stores (3) are on. Dependent Games are (4,5)." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [4, 5].equals(dependentUnstrikedIds);
    }

    def "Stores (4) are on. Dependent Games are (6)." () {

        when:
        def allStrikedIdsStr = "4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [6].equals(dependentUnstrikedIds);
    }

    def "Stores (2,3) are on. Dependent Games are (1,2,3,4,5)." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5].equals(dependentUnstrikedIds);
    }

    def "Stores (2,4) are on. Dependent Games are (1,2,3,6)." () {

        when:
        def allStrikedIdsStr = "2, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2, 3,6].equals(dependentUnstrikedIds);
    }

    def "Stores (3,4) are on. Dependent Games are (4,5,6)." () {

        when:
        def allStrikedIdsStr = "3, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [4, 5, 6].equals(dependentUnstrikedIds);
    }

    def "Stores (2,3,4) are on. Dependent Games are (1,2,3,4,5,6)." () {

        when:
        def allStrikedIdsStr = "2, 3, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2, 3, 4, 5, 6].equals(dependentUnstrikedIds);
    }

    def "Stores (10) are on. There is no Dependent Game." () {

        when:
        def allStrikedIdsStr = "10";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedGameIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [].equals(dependentUnstrikedIds);
    }

    //--------------------------------------------------------------------
    // Store2 Platform1
    // Store3 Platform1
    // Store4 Platform2

    def "Stores (2) are on. Dependent Platforms are (1) and Platforms (1) depend on other Stores." () {

        when:
        def allStrikedIdsStr = "2";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Stores (3) are on. Dependent Platforms are (1) and Platforms (1) depend on other Stores." () {

        when:
        def allStrikedIdsStr = "3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Stores (4) are on. Dependent Platforms are (2)." () {

        when:
        def allStrikedIdsStr = "4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [2].equals(dependentUnstrikedIds);
    }

    def "Stores (2,3) are on. Dependent Platforms are (1)." () {

        when:
        def allStrikedIdsStr = "2, 3";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1].equals(dependentUnstrikedIds);
    }

    def "Stores (2,4) are on. Dependent Platforms are (1,2) and Platforms (1) depend on other Stores." () {

        when:
        def allStrikedIdsStr = "2, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Stores (3,4) are on. Dependent Platforms are (1,2) and Platforms (1) depend on other Stores." () {

        when:
        def allStrikedIdsStr = "3, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

    def "Stores (2,3,4) are on. Dependent Platforms are (1,2)." () {

        when:
        def allStrikedIdsStr = "2, 3, 4";
        def dependentUnstrikedIds = compareDependentColumnService.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allStrikedIdsStr);

        then:
        [1, 2].equals(dependentUnstrikedIds);
    }

}