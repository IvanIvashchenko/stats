package zeptostats

import spock.lang.*
import groovy.sql.Sql
import grails.plugin.spock.IntegrationSpec

/*
 * ATTENTION:
 *   commented tests are not passed.
 *   Uncomment and run test to see it
 */

class GroovyBehaviourSimpleSpec extends IntegrationSpec {

    @Shared def dataSource_zeptostats;
    @Shared def longService;

    def setupSpec() {
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/groovyBehaviour/simple_dump.sql');
    }

    protected def output(needIdsList, needIdsStr, sqlQuery, rows, resultIds) {

        System.out.println("NEED IDS LIST:");
        System.out.println(needIdsList);
        System.out.println(needIdsList.class);
        System.out.println();
        System.out.println("NEED IDS STRING:");
        System.out.println(needIdsStr);
        System.out.println(needIdsStr.class);
        System.out.println();
        System.out.println("SQL QUERY:");
        System.out.println(sqlQuery);
        System.out.println();
        System.out.println("ROWS:");
        System.out.println(rows);
        System.out.println();
        System.out.println("RESULT IDS:");
        System.out.println(resultIds);
        System.out.println();
        System.out.println();
    }

    protected def testingSqlQueryConcatStr(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                st.id in (""" + needIdsStr + """)
            order by st.id
        """;
        def rows = db.rows(sqlQuery);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

    protected def testingSqlQueryDollarStr(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                st.id in ($needIdsStr)
            order by st.id
        """;
        def rows = db.rows(sqlQuery);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

    protected def testingSqlQueryOrderParamStr(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                st.id in (?)
            order by st.id
        """;
        def rows = db.rows(sqlQuery, [needIdsStr]);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

//    //NOTE:: commented because does not work
//    protected def testingSqlQueryOrderParamList(needIdsList) {
//
//        def resultIds = [];
//
//        def db = new Sql(dataSource_zeptostats);
//        def sqlQuery = """
//            select
//                st.id as id,
//                st.name as name
//            from
//                simple_table st
//            where
//                st.id in (?)
//            order by st.id
//        """;
//        def rows = db.rows(sqlQuery, [needIdsList]);
//        rows?.each() {
//            resultIds.add(it.id);
//        }
//        db.close();
//
//        this.output(needIdsList, "", sqlQuery, rows, resultIds);
//
//        return resultIds;
//    }

    protected def testingSqlQueryNamedParamStr(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                st.id in (:ids)
            order by st.id
        """;
        def rows = db.rows(sqlQuery, [ids: needIdsStr]);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

//    //NOTE:: commented because does not work
//    protected def testingSqlQueryNamedParamList(needIdsList) {
//
//        def resultIds = [];
//
//        def db = new Sql(dataSource_zeptostats);
//        def sqlQuery = """
//            select
//                st.id as id,
//                st.name as name
//            from
//                simple_table st
//            where
//                st.id in (:ids)
//            order by st.id
//        """;
//        def rows = db.rows(sqlQuery, [ids: needIdsList]);
//        rows?.each() {
//            resultIds.add(it.id);
//        }
//        db.close();
//
//        this.output(needIdsList, "", sqlQuery, rows, resultIds);
//
//        return resultIds;
//    }

    protected def testingSqlQueryDollarStrAndOrderParamList(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                (?)
                and st.id in ($needIdsStr)
            order by st.id
        """;
        def rows = db.rows(sqlQuery, [true]);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

    protected def testingSqlQueryDollarStrAndNamedParamList(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                (:fake_parameter)
                and st.id in ($needIdsStr)
            order by st.id
        """;
        def rows = db.rows(sqlQuery, [fake_parameter: true]);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

    protected def testingSqlQueryDollarStrAndEmptyParamList(needIdsList) {

        def needIdsStr = longService.configureIdsString(needIdsList);

        def resultIds = [];

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                st.id as id,
                st.name as name
            from
                simple_table st
            where
                st.id in ($needIdsStr)
            order by st.id
        """;
        def rows = db.rows(sqlQuery, []);
        rows?.each() {
            resultIds.add(it.id);
        }
        db.close();

        this.output(needIdsList, needIdsStr, sqlQuery, rows, resultIds);

        return resultIds;
    }

    //--------------------------------------------------------------------
    // testingSqlQueryConcatStr
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryConcatStr: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [4].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (1,2)." () {

        when:
        def needIdsStr = [1, 2];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [1, 2].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (1,3)." () {

        when:
        def needIdsStr = [1, 3];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [1, 3].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (2,3)." () {

        when:
        def needIdsStr = [2, 3];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [2, 3].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are (1,2,3)." () {

        when:
        def needIdsStr = [1, 2, 3];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [1, 2, 3].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryConcatStr: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryConcatStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryDollarStr
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryDollarStr: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryDollarStr: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryDollarStr: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryDollarStr: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [4].equals(resultIds);
    }

//    //NOTE:: commented because does not work
//    def "testingSqlQueryDollarStr: need Ids are (1,2)." () {
//
//        when:
//        def needIdsStr = [1, 2];
//        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);
//
//        then:
//        [1, 2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryDollarStr: need Ids are (1,3)." () {
//
//        when:
//        def needIdsStr = [1, 3];
//        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);
//
//        then:
//        [1, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryDollarStr: need Ids are (2,3)." () {
//
//        when:
//        def needIdsStr = [2, 3];
//        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);
//
//        then:
//        [2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryDollarStr: need Ids are (1,2,3)." () {
//
//        when:
//        def needIdsStr = [1, 2, 3];
//        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);
//
//        then:
//        [1, 2, 3].equals(resultIds);
//    }

    def "testingSqlQueryDollarStr: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryDollarStr: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryDollarStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryOrderParamStr
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryOrderParamStr: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryOrderParamStr: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryOrderParamStr: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryOrderParamStr: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [4].equals(resultIds);
    }

//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamStr: need Ids are (1,2)." () {
//
//        when:
//        def needIdsStr = [1, 2];
//        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);
//
//        then:
//        [1, 2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamStr: need Ids are (1,3)." () {
//
//        when:
//        def needIdsStr = [1, 3];
//        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);
//
//        then:
//        [1, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamStr: need Ids are (2,3)." () {
//
//        when:
//        def needIdsStr = [2, 3];
//        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);
//
//        then:
//        [2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamStr: need Ids are (1,2,3)." () {
//
//        when:
//        def needIdsStr = [1, 2, 3];
//        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);
//
//        then:
//        [1, 2, 3].equals(resultIds);
//    }

    def "testingSqlQueryOrderParamStr: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryOrderParamStr: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryOrderParamStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryOrderParamList
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (1)." () {
//
//        when:
//        def needIdsStr = [1];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [1].equals(resultIds);
//    }
//
//    def "testingSqlQueryOrderParamList: need Ids are (2)." () {
//
//        when:
//        def needIdsStr = [2];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (3)." () {
//
//        when:
//        def needIdsStr = [3];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (4)." () {
//
//        when:
//        def needIdsStr = [4];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [4].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (1,2)." () {
//
//        when:
//        def needIdsStr = [1, 2];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [1, 2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (1,3)." () {
//
//        when:
//        def needIdsStr = [1, 3];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [1, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (2,3)." () {
//
//        when:
//        def needIdsStr = [2, 3];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are (1,2,3)." () {
//
//        when:
//        def needIdsStr = [1, 2, 3];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [1, 2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are unexistent (5)." () {
//
//        when:
//        def needIdsStr = [5];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryOrderParamList: need Ids are unexistent (5,6)." () {
//
//        when:
//        def needIdsStr = [5,6];
//        def resultIds = this.testingSqlQueryOrderParamList(needIdsStr);
//
//        then:
//        [].equals(resultIds);
//    }

    //--------------------------------------------------------------------
    // testingSqlQueryNamedParamStr
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryNamedParamStr: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryNamedParamStr: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryNamedParamStr: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryNamedParamStr: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [4].equals(resultIds);
    }

//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamStr: need Ids are (1,2)." () {
//
//        when:
//        def needIdsStr = [1, 2];
//        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);
//
//        then:
//        [1, 2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamStr: need Ids are (1,3)." () {
//
//        when:
//        def needIdsStr = [1, 3];
//        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);
//
//        then:
//        [1, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamStr: need Ids are (2,3)." () {
//
//        when:
//        def needIdsStr = [2, 3];
//        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);
//
//        then:
//        [2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamStr: need Ids are (1,2,3)." () {
//
//        when:
//        def needIdsStr = [1, 2, 3];
//        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);
//
//        then:
//        [1, 2, 3].equals(resultIds);
//    }

    def "testingSqlQueryNamedParamStr: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryNamedParamStr: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryNamedParamStr(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryNamedParamList
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (1)." () {
//
//        when:
//        def needIdsStr = [1];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [1].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (2)." () {
//
//        when:
//        def needIdsStr = [2];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (3)." () {
//
//        when:
//        def needIdsStr = [3];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (4)." () {
//
//        when:
//        def needIdsStr = [4];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [4].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (1,2)." () {
//
//        when:
//        def needIdsStr = [1, 2];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [1, 2].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (1,3)." () {
//
//        when:
//        def needIdsStr = [1, 3];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [1, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (2,3)." () {
//
//        when:
//        def needIdsStr = [2, 3];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are (1,2,3)." () {
//
//        when:
//        def needIdsStr = [1, 2, 3];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [1, 2, 3].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are unexistent (5)." () {
//
//        when:
//        def needIdsStr = [5];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [].equals(resultIds);
//    }
//
//    //NOTE:: commented because does not work
//    def "testingSqlQueryNamedParamList: need Ids are unexistent (5,6)." () {
//
//        when:
//        def needIdsStr = [5, 6];
//        def resultIds = this.testingSqlQueryNamedParamList(needIdsStr);
//
//        then:
//        [].equals(resultIds);
//    }

    //--------------------------------------------------------------------
    // testingSqlQueryDollarStrAndNamedParamList
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [4].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (1,2)." () {

        when:
        def needIdsStr = [1, 2];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [1, 2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (1,3)." () {

        when:
        def needIdsStr = [1, 3];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [1, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (2,3)." () {

        when:
        def needIdsStr = [2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are (1,2,3)." () {

        when:
        def needIdsStr = [1, 2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [1, 2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndNamedParamList: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryDollarStrAndNamedParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryDollarStrAndOrderParamList
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [4].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (1,2)." () {

        when:
        def needIdsStr = [1, 2];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [1, 2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (1,3)." () {

        when:
        def needIdsStr = [1, 3];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [1, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (2,3)." () {

        when:
        def needIdsStr = [2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are (1,2,3)." () {

        when:
        def needIdsStr = [1, 2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [1, 2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndOrderParamList: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryDollarStrAndOrderParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }

    //--------------------------------------------------------------------
    // testingSqlQueryDollarStrAndEmptyParamList
    //
    // 1 - Alpha
    // 2 - Beta - Default
    // 3 - Gamma
    // 4 - XYZ

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (1)." () {

        when:
        def needIdsStr = [1];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [1].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (2)." () {

        when:
        def needIdsStr = [2];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (3)." () {

        when:
        def needIdsStr = [3];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (4)." () {

        when:
        def needIdsStr = [4];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [4].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (1,2)." () {

        when:
        def needIdsStr = [1, 2];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [1, 2].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (1,3)." () {

        when:
        def needIdsStr = [1, 3];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [1, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (2,3)." () {

        when:
        def needIdsStr = [2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are (1,2,3)." () {

        when:
        def needIdsStr = [1, 2, 3];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [1, 2, 3].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are unexistent (5)." () {

        when:
        def needIdsStr = [5];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }

    def "testingSqlQueryDollarStrAndEmptyParamList: need Ids are unexistent (5,6)." () {

        when:
        def needIdsStr = [5, 6];
        def resultIds = this.testingSqlQueryDollarStrAndEmptyParamList(needIdsStr);

        then:
        [].equals(resultIds);
    }
}