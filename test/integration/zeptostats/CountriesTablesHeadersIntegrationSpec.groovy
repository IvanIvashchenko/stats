package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class CountriesTablesHeadersIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;


    def TOP_STORES_IDS = "2,4";

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def "fetch top store resolutions for given games without inapps" () {

        //Regular case
        when:
        def gameIds = [16,17,22,23,93,83];
        def storeId = 2;
        def appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"],
            [resolution: "SD", id: 17, extra: null, title: "Cut the Rope"]
        ].equals(appsCharacteristics);

        //Empty games list
        when:
        gameIds = [];
        storeId = 2;
        appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [].equals(appsCharacteristics);

        //Store without games
        when:
        gameIds = [];
        storeId = -1;
        appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [].equals(appsCharacteristics);
    }

    def "fetch top store ordered resolutions for given games without inapps" () {

        when:
        def gameIds = [16,17,22,21,31,34,18,39,45,44];
        def storeId = 2;
        def appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"],
            [resolution: "SD", id: 17, extra: null, title: "Cut the Rope"],
            [resolution: "SD", id: 31, extra: null, title: "Cut the Rope: Comic"],
            [resolution: "HD", id: 44, extra: null, title: "Cut the Rope: Experiments Free HD"],
            [resolution: "SD", id: 45, extra: null, title: "Cut the Rope: Experiments Free"],
            [resolution: "SD", id: 34, extra: null, title: "Cut the Rope: Experiments"],
            [resolution: "HD", id: 39, extra: null, title: "Cut the Rope: Experiments HD"],
            [resolution: "SD", id: 18, extra: null, title: "Cut the Rope: Holiday Gift"]
        ].equals(appsCharacteristics);
    }

    def "fetch top store resolutions for given games from another store" () {

        when:
        def gameIds = [16,17,22,23,93,83,61,62];
        def storeId = 2;
        def appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [
            [resolution: "HD", id: 16, extra: null, title: "Cut the Rope HD"],
            [resolution: "SD", id: 17, extra: null, title: "Cut the Rope"]
        ].equals(appsCharacteristics);

    }

    def "fetch extra info about given games" () {

        when:
        def gameIds = [16,17,22,23,67,69,63];
        def storeId = 4;
        def appsCharacteristics = countriesService.fetchTopStoreAppsCharacteristics(gameIds, storeId);

        then:
        [
            [resolution: "SD", id: 63, extra: "8C", title: "Cut the Rope Free 8C"],
            [resolution: "SD", id: 67, extra: "GP", title: "Cut the Rope Google Play Free"],
            [resolution: "SD", id: 69, extra: "8C", title: "Cut the Rope: Experiments Free 8C"]
        ].equals(appsCharacteristics);
    }

    def "fetch top store meta games for given games without inapps" () {

        //Regular case
        when:
        def gameIds = [62,66,64,70,71];
        def storeId = 4;
        def metaAppsCharacteristics = countriesService.fetchTopStoreMetaAppsCharacteristics(gameIds, storeId);

        then:
        [
            [path: "/uploads/images/cut-the-rope/", name: "cut-the-rope.png", count: 2],
            [path: "/uploads/images/experimentas/", name: "experiments.png", count: 1]
        ].equals(metaAppsCharacteristics);

        //Empty games list
        when:
        gameIds = [];
        storeId = 4;
        metaAppsCharacteristics = countriesService.fetchTopStoreMetaAppsCharacteristics(gameIds, storeId);

        then:
        [].equals(metaAppsCharacteristics);
    }

    def "fetch top store meta games for given games from another store"() {

        when:
        def gameIds = [62,66,64,70,71];
        def storeId = 2;
        def metaAppsCharacteristics = countriesService.fetchTopStoreMetaAppsCharacteristics(gameIds, storeId);

        then:
        [].equals(metaAppsCharacteristics);
    }

    def "fetch other ordered stores logos for given games"() {

        //Regular case
        when:
        def gameIds = [84,85,83,61,93];
        def storeCharacteristics = countriesService.fetchOtherStoreAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [
            [path: "/uploads/images/macappstore/", name: "mac-app-store.png", count: 1, store: "Mac App Store"],
            [path: "/uploads/images/amazon/", name: "amazon-appstore.png", count: 3, store: "Amazon"],
            [path: "/uploads/images/nook/", name: "nook.png", count: 1, store: "Nook"]
        ].equals(storeCharacteristics);

        //Empty games list
        when:
        gameIds = [];
        storeCharacteristics = countriesService.fetchOtherStoreAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [].equals(storeCharacteristics);

    }

    def "fetch other stores logos for given games from top stores"() {

        when:
        def gameIds = [16,17,22,21,31];
        def storeCharacteristics = countriesService.fetchOtherStoreAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [].equals(storeCharacteristics);
    }

    def "fetch for other stores ordered selected games"() {

        //Regular case
        when:
        def gameIds = [84,85,83,61,93];
        def appsCharacteristics = countriesService.fetchOtherAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [
            [path: "/uploads/images/cut-the-rope/", name: "cut-the-rope.png", resolution: "DESKTOP", title: "Cut the Rope"],
            [path: "/uploads/images/cut-the-rope/", name: "cut-the-rope.png", resolution: "SD", title: "Cut the Rope"],
            [path: "/uploads/images/cut-the-rope/", name: "cut-the-rope.png", resolution: "HD", title: "Cut the Rope HD"],
            [path: "/uploads/images/experimentas/", name: "experiments.png", resolution: "SD", title: "Cut the Rope Experiments"],
            [path: "/uploads/images/cut-the-rope/", name: "cut-the-rope.png", resolution: "HD", title: "Cut the Rope HD"]
        ].equals(appsCharacteristics);

        //Empty games list
        when:
        gameIds = [];
        appsCharacteristics = countriesService.fetchOtherAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [].equals(appsCharacteristics);
    }

    def "fetch other stores ordered selected games for given games from top stores"() {

        when:
        def gameIds = [16,17,22,21,31];
        def appsCharacteristics = countriesService.fetchOtherAppsCharacteristics(gameIds, TOP_STORES_IDS);

        then:
        [].equals(appsCharacteristics);
    }
}