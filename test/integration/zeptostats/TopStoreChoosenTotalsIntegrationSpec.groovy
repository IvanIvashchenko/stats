package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class TopStoreChoosenTotalsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def String CHOOSEN_TOTAL_FILTER = "choosen";

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql');
    }

    def "fetch total downloads for ordered games without inapps in top store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        //Regular case
        when:
        def gameIds = [16,17,45,39,18,22];
        def geoIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsTotals(gameIds, geoIds, CHOOSEN_TOTAL_FILTER, storeId, savedTime);

        then:
        [
            [downloads: 13],
            [downloads: 2],
            [downloads: 0],
            [downloads: 2],
            [downloads: 0]
        ].equals(appsDownloads);

        //Empty games list
        when:
        gameIds = [];
        geoIds = [1,2];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchTopStoreAppsTotals(gameIds, geoIds, CHOOSEN_TOTAL_FILTER, storeId, savedTime);

        then:
        [].equals(appsDownloads);

        //Empty countries list
        when:
        gameIds = [16,17,45,39,18,22];
        geoIds = [];
        storeId = 2;
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        appsDownloads = countriesService.fetchTopStoreAppsTotals(gameIds, geoIds, CHOOSEN_TOTAL_FILTER, storeId, savedTime);

        then:
        [].equals(appsDownloads);
    }

    def "fetch total downloads for games from another store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def geoIds = [1,2];
        def storeId = 4;
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def appsDownloads = countriesService.fetchTopStoreAppsTotals(gameIds, geoIds, CHOOSEN_TOTAL_FILTER, storeId, savedTime);

        then:
        [].equals(appsDownloads);
    }

    def "fetch total downloads for games from top store by wrong period"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/total_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def geoIds = [1,2];
        def storeId = 2;
        def savedTime = [from_time: 1350459410000,to_time:1350459510000];
        def appsDownloads = countriesService.fetchTopStoreAppsTotals(gameIds, geoIds, CHOOSEN_TOTAL_FILTER, storeId, savedTime);

        then:
        [
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0]
        ].equals(appsDownloads);
    }

}