package zeptostats

import spock.lang.*
import zeptostats.CountriesService
import grails.plugin.spock.IntegrationSpec

class OtherStoreChoosenTotalsIntegrationSpec extends IntegrationSpec {

    def dataSource;
    @Shared def dataSource_zeptostats;
    def CountriesService countriesService;

    def CHOOSEN_TOTAL_FILTER = "choosen";
    def TOP_STORES_IDS = "2,4";

    def setupSpec() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/zeptostats_test.sql');
    }

    def setup() {

        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/clear_unit.sql');
    }

    def "fetch total downloads for ordered games without inapps in top store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        //Regular case
        when:
        def gameIds = [16,17,85,84,61,93,83];
        def geoIds = [1];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            geoIds,
            CHOOSEN_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [
            [downloads: 1],
            [downloads: 5],
            [downloads: 2],
            [downloads: 5],
            [downloads: 2]
        ].equals(downloads);

        //Empty games list
        when:
        gameIds = [];
        geoIds = [1];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            geoIds,
            CHOOSEN_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [].equals(downloads);

        //Empty countries list
        when:
        gameIds = [16,17,85,84,61,93,83];
        geoIds = [];
        savedTime = [from_time: 1350459310000,to_time:1350459310000];
        downloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            geoIds,
            CHOOSEN_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [].equals(downloads);
    }

    def "fetch total downloads for games from another store"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        when:
        def gameIds = [16,17,45,39,18,22];
        def geoIds = [1];
        def savedTime = [from_time: 1350459310000,to_time:1350459310000];
        def downloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            geoIds,
            CHOOSEN_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [].equals(downloads);
    }

    def "fetch total downloads for games from other stores by wrong period"() {

        setup:
        IntegrationTestsHelper.loadFixtures(dataSource_zeptostats, 'test/data/countries/other_downloads.sql');

        when:
        def gameIds = [16,17,85,84,61,93,83];
        def geoIds = [2];
        def savedTime = [from_time: 1350459410000, to_time:1350459510000];
        def downloads = countriesService.fetchOtherStoreAppsTotals(
            gameIds,
            geoIds,
            CHOOSEN_TOTAL_FILTER,
            TOP_STORES_IDS,
            savedTime
        );

        then:
        [
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0],
            [downloads: 0]
        ].equals(downloads);
    }

}