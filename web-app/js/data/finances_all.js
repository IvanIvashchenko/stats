{
   "height": 450,
   "from":"Sun Jun 12 2011 08:00:17 GMT+0700 (ICT)",
   "to":"Wed Sep 12 2012 08:00:00 GMT+0700",
   "forceCount": 14,
   "axis":[
      {
         "max": 2500,
         "name":"Revenue, k$",
         "vLines": true,
         "linesColor": "white",
         "segments": 5,
         "step":"month",
         "expected": 7200500,
         "paid": 7001060,
         "delta": -190567
      },
      {
         "max": 2500,
         "name":"Revenue, k$",
         "segments": 5,
         "step":"month"
      }
   ],
   "data":[
      {
         "axis":0,
         "name":"Expected revenue",
         "fill": true,
         "stroke": false,
         "hover": true,
         "hoverType": "finances",
         "per":{
            "grow":-1,
            "value":500
         },
         "total":435000,
         "type": "graph",
         "colors": ["", "", "#d7edc1"],
         "values": [1087, 1243, 1160, 1102, 1047, 1070, 1229, 1054, 1180, 1194, 1025, 1155, 1212, 1401, 1536],
         "tips": [
           {
             "date": "05/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "06/26...06/30",
             "real": 52638,
             "expected": 56000
           },
           {
             "date": "07/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 5738,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 5738,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 576038,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "06/26...06/30",
             "real": 52638,
             "expected": 56000
           },
           {
             "date": "07/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 5738,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 5738,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 576038,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 57638,
             "expected": 56000
           },
           {
             "date": "05/26...06/30",
             "real": 57638,
             "expected": 56000
           }
         ]
      },
      {
         "axis": 1,
         "name":"store data",
         "fill": false,
         "per":{
            "grow":-1,
            "value":500
         },
         "total":435000,
         "type": "compare_graph",
         "compareWith": 0,
         "colors": ["#3d0483", "#3d0483", ""],
         "compareColors": ["#3d0483", "#831c04"],
         "values": [2000, 300, 1026, 1036, 1055, 1143, 1104, 1192, 1281, 1255, 1287, 1272, 1245, 1378, 600]
      }
   ]
}
