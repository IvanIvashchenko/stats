require('sugar')

DrawUtils  = require('../javascripts/modules/graphic/draw_utils.coffee')

chai      = require('chai')
sinon     = require('sinon')
sinonChai = require('sinon-chai')

chai.should()
chai.use(sinonChai)

describe 'Graphic.DrawUtils', ->

  describe '.moveTo()', ->

    it 'should build correct sequence from arguments in order to move cursor to x,y', ->
      sequence = DrawUtils.moveTo([], 10, 10)
      sequence.should.eql ['M10,10']
      DrawUtils.moveTo(sequence, 42, 13).should.eql ['M10,10', 'M42,13']

    it 'should round passes arguments', ->
      DrawUtils.moveTo([], 10.123, 10.823).should.eql ['M10,11']

  describe '.drawTo()', ->

    it 'should build correct sequence from arguments in order to draw lines', ->
      sequence = DrawUtils.drawTo([], 10, 15)
      sequence.should.eql ['L10,15']
      DrawUtils.drawTo(sequence, 42, 13).should.eql ['L10,15', 'L42,13']

    it 'should round passes arguments', ->
      DrawUtils.drawTo([], 10.123, 10.823).should.eql ['L10,11']

  describe '.addColor', ->

    it 'should build correct sequence from arguments', ->
      DrawUtils.addColor([], 'red').should.eql [['color', 'red']]
      DrawUtils.addColor(['test'], 'red').should.eql ['test', ['color', 'red']]

  describe '.addPath', ->

    it 'should build correct sequence from arguments', ->
      DrawUtils.addPath([], 0, 10, 19, 10).should.eql [['path',  ['M0,10', 'L19,10']]]
      DrawUtils.addPath(['test'], 0, 10, 19, 10).should.eql ['test', ['path',  ['M0,10', 'L19,10']]]

  describe '.drawGraphicChunk', ->

    it 'should build correct SVG sequence for graphic chunk', ->
      DrawUtils.drawGraphicChunk([], 10, 20, ['red', 'green', 'blue'], 20, 0)
        .should.eql [
          ['color', 'red']
          ['path',  ['M0,10', 'L19,10']]
          ['color', 'green']
          ['path',  ['M20,10', 'L20,19']]
        ]

      DrawUtils.drawGraphicChunk(['test'], 10, 20, ['red', 'green', 'blue'], 20, 0)
        .should.eql [
          'test'
          ['color', 'red']
          ['path',  ['M0,10', 'L19,10']]
          ['color', 'green']
          ['path',  ['M20,10', 'L20,19']]
        ]

      DrawUtils.drawGraphicChunk([], 10, null, ['red', 'green', 'blue'], 20, 0)
        .should.eql [
          ['color', 'red']
          ['path',  ['M0,10', 'L20,10']]
        ]

      DrawUtils.drawGraphicChunk([], 20, 15, ['red', 'green', 'blue'], 20, 20)
        .should.eql [
          ['color', 'red']
          ['path',  ['M20,20', 'L39,20']]
          ['color', 'green']
          ['path',  ['M40,20', 'L40,16']]
        ]

  describe '.sequence()', ->

    it 'should build correct SVG from array of values and graphic paremeters', ->
      DrawUtils.sequence([10, 20, 15], width: 60, step: 20, colors: ['red', 'green', 'blue']) \
        .should.eql [
          ['color', 'red']
          ['path',  ['M0,10', 'L19,10']]
          ['color', 'green']
          ['path',  ['M20,10', 'L20,19']]

          ['color', 'red']
          ['path',  ['M20,20', 'L39,20']]
          ['color', 'green']
          ['path',  ['M40,20', 'L40,16']]

          ['color', 'red']
          ['path',  ['M40,15', 'L60,15']]
        ]

  describe '.inverseY()', ->

    it 'should inverse y in svg path', ->
      DrawUtils.inverseY(['M40,15', 'L60,15'], 100)
        .should.eql ['M40,85', 'L60,85']

  describe '.columnSequence()', ->

    it 'should build proper column sequence', ->
      DrawUtils.columnSequence([10, 20, 7], ['green', 'blue', 'red'], 10, 5)
        .should.eql [
          ['rect', color: 'red',   rect: [5,  0, 15,  7]]
          ['rect', color: 'green', rect: [5,  8, 15, 10]]
          ['rect', color: 'blue',  rect: [5, 11, 15, 20]]
        ]

  describe '.fillColumnsSequence()', ->

    it 'should returns proper command sequence for fill columns', ->
      graphics = [
        [10, 15, 10]
        [20, 20, 30]
        [ 7, 10, 15]
      ]

      DrawUtils.fillColumnsSequence(graphics, ['green', 'blue', 'red'], 10)
        .should.eql [
          ['rect', color: 'red',   rect: [ 0,  0, 10,  7]]
          ['rect', color: 'green', rect: [ 0,  8, 10, 10]]
          ['rect', color: 'blue',  rect: [ 0, 11, 10, 20]]

          ['rect', color: 'red',   rect: [10,  0, 20, 10]]
          ['rect', color: 'green', rect: [10, 11, 20, 15]]
          ['rect', color: 'blue',  rect: [10, 16, 20, 20]]

          ['rect', color: 'green', rect: [20,  0, 30, 10]]
          ['rect', color: 'red',   rect: [20, 11, 30, 15]]
          ['rect', color: 'blue',  rect: [20, 16, 30, 30]]
        ]
