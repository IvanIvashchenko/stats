require('sugar')

_         = require('underscore')
Color     = require('color')

Utils     = require('../javascripts/modules/graphic/utils.coffee')

chai      = require('chai')
sinon     = require('sinon')
sinonChai = require('sinon-chai')

chai.should()
chai.use(sinonChai)

describe 'Graphic.Utils', ->

  describe '.getAxisMaxAndStep()', ->

    it 'should return rounded step close to max/count', ->
      Utils.getAxisMaxAndStep(100, 2).should.eql  [100,  50]
      Utils.getAxisMaxAndStep(400, 4).should.eql  [400,  100]
      Utils.getAxisMaxAndStep(950, 5).should.eql  [1000, 200]
      Utils.getAxisMaxAndStep(1246, 3).should.eql [1500, 500]
      Utils.getAxisMaxAndStep(1246, 7).should.eql [1400, 200]
      Utils.getAxisMaxAndStep(4000, 8).should.eql [4000, 500]
      Utils.getAxisMaxAndStep(4100, 8).should.eql [4500, 500]

  describe '.getAxisLine()', ->

    it 'should returns array of numbers', ->
      Utils.getAxisLine(100, 2).should.eql [[50, 100], 100]
      Utils.getAxisLine(100, 10).should.eql [[10, 20, 30, 40, 50, 60, 70, 80, 90, 100], 100]

  describe '.getDateString()', ->

    it 'should return day string if second argument false or not passed', ->
      Utils.getDateString(Date.create('Feb 11')).should.eql '11'
      Utils.getDateString(Date.create('Feb 11'), false).should.eql '11'

    it 'should return day with month if second argument is true', ->
      Utils.getDateString(Date.create('Feb 11'), true).should.eql '11 Feb'

    it 'should return day with month and short year if year of date is not same', ->
      Utils.getDateString(Date.create('Feb 11 1987'), true).should.eql "11 Feb '87"

  describe '.getMinimalStep()', ->

    it 'should return minimal step for all axis', ->
      Utils.getMinimalStep(axis: [{ step: 'day' }, { step: 'month' }])
        .should.eq 'day'
      Utils.getMinimalStep(axis: [{ step: '6 days' }, { step: '1 month' }])
        .should.eq '6 days'
      Utils.getMinimalStep(axis: [{ step: '60 days' }, { step: '3 weeks' }])
        .should.eq '3 weeks'

  describe '.isFirstOrLastDayOfMonth()', ->

    it 'should return true if date is last day of month', ->
      Utils.isFirstOrLastDayOfMonth(Date.create('Aug 31')).should.be.ok

    it 'should return true if date is first day of month', ->
      Utils.isFirstOrLastDayOfMonth(Date.create('Aug 1')).should.be.ok

    it 'should return falser if date is not last and not first day of month', ->
      Utils.isFirstOrLastDayOfMonth(Date.create('Aug 5')).should.be.not.ok

  describe '.randomColor()', ->

    it 'should return random HEX color', ->
      Utils.randomColor().should.match /#[0123456789ABCDEF]{6}/

  describe '.graphicColor()', ->

    it 'should return random HEX color', ->
      Utils.graphicColor().should.match /#[0123456789ABCDEF]{6}/

  describe '.formatDate()', ->

    it 'should be proxy to Utils.getDateString', ->
      stub = sinon.stub(Utils, 'getDateString')
      Utils.helpers.formatDate(Date.create('Feb 11'))
      stub.should.be.calledOnce
      stub.restore()

    it 'should add month if second argument is passed', ->
      Utils.helpers.formatDate(Date.create('Feb 11')).should.eql '11'
      Utils.helpers.formatDate(Date.create('Feb 11'), true).should.eql '11 Feb'

  describe '.growArrows()', ->

    it 'should return nothing if first argument is zero, null or undefined', ->
      Utils.helpers.growArrows(0).should.be.eql ''
      Utils.helpers.growArrows(null).should.be.eql ''
      Utils.helpers.growArrows(undefined).should.be.eql ''

    it 'should return down arrows if first argument is negative', ->
      Utils.helpers.growArrows(-1).should.be.eql '↓'
      Utils.helpers.growArrows(-2).should.be.eql '↓↓'

    it 'should return up arrows if first argument is positive', ->
      Utils.helpers.growArrows(1).should.be.eql '↑'
      Utils.helpers.growArrows(2).should.be.eql '↑↑'

  describe '.scaleValues', ->

    it 'should return new array with new values scaled to new max', ->
      Utils.scaleValues([1, 4, 5, 6, 8], 10, 100)
        .should.eql [10, 40, 50, 60, 80]

  describe '.inverseValue()', ->

    it 'should inverse value', ->
      Utils.inverseValue(5, 10).should.eql 5
      Utils.inverseValue(9, 10).should.eql 1
