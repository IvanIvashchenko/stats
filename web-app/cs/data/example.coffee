from: 'Tue, 10 Jul 2012 09:54:51 MSK +04:00'
to:   'Thu, 09 Aug 2012 09:55:32 MSK +04:00'

axis: [
    name:    'Downloads'
    max:     2500
    step:    'month'
    total:   7200000
    per:
      value: 1003
      grow:  +1
  ,
    name:    'Revenue'
    max:     1500
    measure: '$'
    step:    'day'
    total:   7200000
    per:
      value: 1003
      grow:  +1
  ]
data: [
    name:  'Russia'
    axis:  0
    type:  'graph'
    total: 435000
    per:
      value: 500
      grow:  -1
    values: [
      400
      5699
      500
      506060
      # ...
    ]
  ,
    name:  'UK'
    axis:  0
    type:  'graph'
    total: 435000
    per:
      value: 500
      grow:  -1
    values: [
      400
      5699
      500
      506060
      # ...
    ]
  ,
    name:  'Revenue'
    axis:  1
    type:  'curve'
    total: 5000000
    per:
      value: 5400
      grow:  +2
    values: [
      400
      5699
      500
      506060
      # ...
    ]
  ]
