@app =

  on: (selector, callback) ->
    $ ->
      content = $(selector)
      if content.length
        $$ = (selector) ->
          jQuery(selector, content)
        callback.call content, $$, content

  flash:
    error: (text) ->
      @_show @_div('error', text)

    notice: (text) ->
      @_show @_div('notice', text)

    autoHide: (el) ->
      return el unless el.length
      el.click ->
        app.flash._hide el
      setTimeout (->
        app.flash._hide el, 1000
      ), 2500
      el

    _show: (el) ->
      el.css(top: -el.height())
        .animate top: 0, 600
      @autoHide el

    _div: (cls, text) ->
      $(".flash.#{cls}").remove()
      text = $('<div>').html(text).prepend('<span>')
      $('<div>').addClass("flash #{cls}").append(text).prependTo 'body'

    _hide: (el, duration) ->
      el.animate top: -el.height(), 400, ->
        el.remove()
