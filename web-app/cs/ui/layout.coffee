###
  Layout UI code
###
app.on 'body', ($$, body) ->

  $.ajaxSetup({
    statusCode: {
      401: () ->
        window.location.reload();
    }
  });

  $(document).jkey 'esc', ->
    console.log 'esc pressed'
    $('body').trigger('esc-pressed')

  @on 'click', '@overlay', ->
    $('body').trigger('overlay-clicked')

  # Handle click on open settings
  @on 'click', '@open-settings', ->
    $(@).toggleClass('is-overlay-over')
    if $$('@settings').is(':visible')
      $('body').trigger('hide-overlay')
      $('body').unbind('.settings')
    else
      $$('@settings').css(top: $(@).offset().top)
      $('body').trigger('show-overlay')
      $('body').bind 'esc-pressed.settings, overlay-clicked.settings', =>
        $(@).click()

    $$('@settings').fadeToggle(300)
    false

  # Handle show-overlay trigger
  @on 'show-overlay', ->
    $('@overlay').fadeIn(300)

  # Handle hide-overlay trigger
  @on 'hide-overlay', ->
    $('@overlay').fadeOut(300)

  # Handle hover on logo
  @on 'mouseenter mouseleave', '@logo', ->
    $$('@header').toggleClass('is-animated')

  # Click on open-popup
  @on 'click', '@open-popup', ->
    popup = $(@).next('@popup')

    if popup.is(':visible')
      popup.fadeOut(300)
    else
      $('@popup').not(popup).fadeOut(300)
      if popup.is('@save-bookmark')
        popup
          .css
            top: $(@).position().top + $(@).outerHeight() + 13
            left: $(@).position().left - popup.outerWidth() + $(@).width() + 35
          .fadeIn(300)
      else
        popup
          .css
            top: $(@).position().top + $(@).outerHeight() + 13
            left: $(@).position().left - (popup.outerWidth() - $(@).width()) / 2
          .fadeIn(300)

    false

  # Configure disabled candies if it is need
  disableCandiesIfNeed = () ->
    candySome = $('@candy:first');
    candyGroups = $('@candy-group');
    checkboxBehaviour = candySome.is('.is-checkbox')
    radioBehaviour = not checkboxBehaviour and candySome.is('.is-radio')

    if checkboxBehaviour
      for candyGroup in candyGroups
        activeCandies = $(candyGroup).find('.is-active');
        inactiveCandies = $(candyGroup).find(':not(.is-active)');
        if (activeCandies?.length > 0 and inactiveCandies?.length > 0)
          inactiveCandies.addClass('is-disabled')
    true;

  @on 'click', '@candy-text', ->
    $(@).trigger('click-text');
    return false;

  # Click on candy control
  @on 'click', '@candy', ->

    if not $(@).is('.is-disabled')
      wasActive = $(@).is('.is-active')
      checkboxBehaviour = $(@).is('.is-checkbox')
      radioBehaviour = not checkboxBehaviour and $(@).is('.is-radio')
      shouldNotify = false

      if checkboxBehaviour
        candyId = $(@).data('id')
        candyGroup = $(@).parents('@candy-group:first')
        otherGroupCandies = candyGroup?.find('@candy[data-id != "' + candyId + '"]')

        if (wasActive)
          $(@)
            .toggleClass('is-active')
            .trigger('change', $(@).is('.is-active'))
          if otherGroupCandies?.length
            $(otherGroupCandies).removeClass('is-disabled')
        else
          $(@)
            .toggleClass('is-active')
          if otherGroupCandies?.length
            $(otherGroupCandies).addClass('is-disabled')
            $(otherGroupCandies).removeClass('is-active')
        shouldNotify = true

      else if radioBehaviour
        if not wasActive
          $(@)
            .toggleClass('is-active')
            .trigger('change', $(@).is('.is-active'))
          shouldNotify = true

      else
        $(@)
          .toggleClass('is-active')
          .trigger('change', $(@).is('.is-active'))

      if shouldNotify
        $('@candy-state-dependent').trigger('candy-state-change', [$(@)])

    false

  # Click on save button control
  @on 'click', '@button@save', ->
    $(@)
      .addClass('is-loading')
      .spin(app.spinOptions.small)
      .trigger('loading')
    false

  # Handle loaded event on save button
  @on 'loaded', '@button@save', ->
    $(@)
      .removeClass('is-loading')
      .spin(false)

  # Date helpers
  # TODO: Rewrite it useing Sugar.js

  months = 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'.split(/\s/g)
  currentYear = new Date().getFullYear()

  # Is passed dates is same day
  isSameDay = (dateOne, dateTwo) ->
    dateOne.setHours(0, 0, 0, 0) == dateTwo.setHours(0, 0, 0, 0)

  # Is passed date is current year
  isCurrentYear = (date) ->
    date.getFullYear() == currentYear

  # Returns short year string (’12)
  getShortYear = (date) ->
    '’' + date.getFullYear().toString().slice(2, 4)

  # Date formatter helper for date picker control
  getDateString = (date) ->
    values = [date.getDate(), months[date.getMonth()]]
    unless isCurrentYear(date)
      values.push(getShortYear(date))
    values.join(' ')

  # Disable
  checkDateSelectorArrows = (selector, dateTo) ->
    next = selector.next()

    # Only next arrow can be disabled
    if dateTo.clone().advance('1 day').isFuture()
      next.addClass('is-disabled')
    else
      next.removeClass('is-disabled')

  formatDates = (dates) ->
    datesFormatted = []
    datesFormatted.push(dates[0].format('{yyyy}-{MM}-{dd}'))
    datesFormatted.push(dates[1].format('{yyyy}-{MM}-{dd}'))
    return datesFormatted

  setDatesDataAndText = (selector, datesFormatted, dates) ->
    # Update data
    $(selector).data('from', datesFormatted[0])
    $(selector).data('to', datesFormatted[1])

    # Update text
    $(selector).find('@text').text if isSameDay.apply(null, dates)
      dates[0].format('{d} {Month} {yyyy}')
    else
      "from #{getDateString(dates[0])} to #{getDateString(dates[1])}"

  updateDatesSelector = (selector, dates) ->
    # Update data from and to
    datesFormatted = formatDates(dates)

    setDatesDataAndText(selector, datesFormatted, dates)

    # Disable or enable arrows
    checkDateSelectorArrows(selector, dates[1])

    $(selector).trigger('change', datesFormatted)

  @on 'setDatesSelector', '@period-selector', (e, datesFormatted) ->
    dates = []
    dates[0] = new Date(datesFormatted[0])
    dates[1] = new Date(datesFormatted[1])

    setDatesDataAndText($(@), datesFormatted, dates)

  @on 'updatePeriodByStep', '@period-selector', (e, callback = null, callbackParamsMap = null) ->
    step = $(".is-period-tabs").find("a.is-current").data("id")
    datesFormatted = [$(@).data('from'), $(@).data('to')]
    if (step)
      $.post(
        '/filter/ajaxRoundDatesByStep'
        {
          step: step,
          fromDate: datesFormatted[0],
          toDate: datesFormatted[1]
        }
        (data) =>
          if data?.success
            datesFormatted = []
            datesFormatted[0] = data.result.roundFromFormatted
            datesFormatted[1] = data.result.roundToFormatted
            $(@).trigger('setDatesSelector', [datesFormatted])

            if callback
              if callbackParamsMap
                callback(callbackParamsMap)
              else
                callback()
      )

  #  Date picker change callback
  onDateChange = (__, dates, popup) ->

    selector = $(popup).data('selector')

    # Why dates[1]? In fact DatePicker call this callback on every pick.
    # On first click `dates` will be look like:
    #   [Sun Apr 01 2012 00:00:00 GMT+0700 (ICT), Sun Apr 01 2012 23:59:59 GMT+0700 (ICT)]
    # So we can use this to allow pick singe day on double click.
    selector.data('dates').push(dates[1])

    if not isSameDay.apply(null, dates) or
       (selector.data('can-be-single') and selector.data('dates').length >= 2)
      updateDatesSelector(selector, dates)
      $(popup).fadeOut(300)

  onDatePickerUpdated = (e, dates) ->
    updateDatesSelector($(@), dates)

  # DatePicker dates process callback
  onDatePickerRender = (date) ->
    # Disable date if is future
    disabled: date.isFuture()

  @on('updated', '@period-selector', onDatePickerUpdated)

  # Handle calendar picker control
  @on 'click', '@period-selector', ->
    selector = $(@)
    popup    = $(@).data('popup')

    # TODO: Refactor this crappy code
    if popup? and popup.is(':visible')
      popup.fadeOut 300, ->
        popup.remove()
      $(@).data('popup', null)
    else
      popup.remove() if popup?
      $('@popup').not(popup).fadeOut(300)
      popup = $('<div role="popup" class="popup"><div class="popup-tri"></div></div>')
      popup
        .appendTo('body')
        .css
          width: 328
          height: 138
        .css
          top: $(@).offset().top + $(@).outerHeight() + 13
          left: $(@).offset().left - (popup.outerWidth() - $(@).width()) / 2
        .DatePicker
          flat: true,
          date: [selector.data('from'), selector.data('to')]
          current: selector.data('from')
          calendars: 2
          mode: 'range'
          starts: 1
          onChange: onDateChange
          onRender: onDatePickerRender
        .fadeIn(300)

        selector.data('popup', popup)
        selector.data('dates', [])
        popup.data('selector', selector)

  # Change date from click on prev/next
  changeDayFromArrows = (arrow) ->
    # Build modify params
    [find, origin, fun] = if arrow.data('change') == 'prev'
      ['next', 'from', 'rewind']
    else # next
      ['prev', 'to', 'advance']

    selector = arrow[find]()

    # Modify date
    date = new Date(selector.data(origin)).clone()[fun]('1 day')

    # And pass to update dates function
    updateDatesSelector(selector, [date, date])

    false

  # Handle click on prev/next date
  @on 'click', '@period-selector-change-day', ->
    unless $(@).is('.is-disabled')
      changeDayFromArrows($(@))
    false

  ## Popup selectors

  @on 'change', '@countries-selector', ->
    $(@)
      .parents('@popup')
      .prev('@open-popup:first')
        .find('@text')
        .text if $(@).find('@item').is(':not(.is-active)')
                'for selected countries'
              else
                'for all countries'

  @on 'change', '@apps-selector', ->
    $(@)
      .parents('@popup')
      .prev('@open-popup:first')
        .find('@text')
        .text if $(@).find('@item').is(':not(.is-active)')
                'for selected apps'
              else
                'for all apps'

  @on 'change', '@stores-selector', ->
    $(@)
      .parents('@popup')
      .prev('@open-popup:first')
        .find('@text')
        .text if $(@).find('@item').is(':not(.is-active)')
                'for selected stores'
              else
                'for all stores'

  @on 'click', '@popup-selector @item', ->
    $(@)
      .toggleClass('is-active')
      .parents('@popup-selector')
        .trigger('change')

  disableCandiesIfNeed()

