###
  Tabs UI
###
app.on '@tabs', ($$, tabs) ->

  # Handle click on tab
  @on 'click', '@tab', ->
    oldTab = $$('@tab.is-current')
    oldTab.removeClass('is-current')
    $(@).addClass('is-current')
    tabs.trigger('change', [$(@).data('id'), oldTab.data('id')])
    false

  @on 'click', '@save-button', ->

    bookmarkName = $("@bookmark-new-name").val()
    unless bookmarkName
      $("@save-bookmark-error").css "display", "block"
      return false

    if $("@bookmark-item:contains(" + bookmarkName + ")").length isnt 0
      $("@save-bookmark-error").css "display", "block"
      return false


    $("@save-bookmark-error").css "display", "none"
    tdEmpty = "<td></td>"
    if $("@bookmark-list tr@bookmark-row").length is 0
      $("@message-row").remove()
      newRow = $("<tr><td>" + bookmarkName + "</td>" + tdEmpty + "</tr>").appendTo($("@bookmark-list"))
    else
      newRow = $("<tr><td>" + bookmarkName + "</td>" + tdEmpty + "</tr>").insertAfter($("@bookmark-row").last())

    newRow.addClass("popup-table-row").attr("role", "bookmark-row")
    newRow.find("td:not(:empty)").addClass("popup-table-cell").attr("role", "bookmark-item")
    newRow.find("td:empty").addClass("delete-bookmark-img").attr("role", "bookmark-delete")

    $("@popup@save-bookmark").fadeOut(300)
   	tabsRole = $("@bookmarks").data("role")
   	postUrl = ""
   	if tabsRole is "export-tabs"
   		postUrl = '/export/saveSettings'
   	else postUrl = '/timeline/updateGraphs' if tabsRole is "timeline-tabs"
    settings = collectSettings(true)
    exportUrl = $(@).attr('href')
    $.post(
      postUrl
      settings
      (data) =>
        $("@bookmark-new-name").val("")
        $(@).trigger('loaded')
        if data?.bookmarkId
          $("@bookmark-row").last().attr("data-id", data.bookmarkId)
        if data?.success
          # window.location.href = exportUrl
          $.post(
            exportUrl
            {}
            (exportdata) =>
              console?.log exportdata.success
          )
    )

  @on 'click', '@bookmark-item', ->

    tabsRole = $("@bookmarks").data("role")
    postUrl = ""
    if tabsRole is "export-tabs"
      postUrl = '/export/loadBookmark'
    # TODO: change url for timeline
    else postUrl = '/timeline/index' if tabsRole is "timeline-tabs"
    bookmarkId = $(@).parent().data('id')
    $.post(
      postUrl
      {bookmarkId: bookmarkId}
      (data) =>
        location.reload()
        console?.log data
    )

  @on 'click', '@bookmark-delete', ->

    tabsRole = $("@bookmarks").data("role")
    postUrl = ""
    if tabsRole is "export-tabs"
      postUrl = '/export/deleteBookmark'
    # TODO: change url for timeline
    else postUrl = '/timeline/index' if tabsRole is "timeline-tabs"
    bookmarkId = $(@).parent().data('id')
    $(@).parent().remove()
    if $("@bookmark-list tr@bookmark-row").length is 0
      $("@popup@open-bookmark-list").fadeOut(10)
    $.post(
      postUrl
      {bookmarkId: bookmarkId}
      (data) =>
        console?.log data
    )

  @on 'click', '@open-bookmarks-popup', ->

    if $("@bookmark-list tr").length is 0 and not $("@open-bookmark-list").is(':visible') 
      newRow = $("<tr><td>There are no bookmarks yet</td></tr>").appendTo($("@bookmark-list"))
      newRow.attr("role", "message-row")
