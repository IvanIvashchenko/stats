###
  Graphics UI code
###
app.on 'body', ($$, body) ->

  # Pass templates to Templates module
  Templates.init($$('@templates @template'))

  # Handler for process-graphics-container trigger
  $('body').on 'process-graphics-container', ->
    $$('@graphic-container').each ->
      Graphic.Builder.fetchAndBuild($(@), $(@).data('id'))

  triggerProcess = ->
    $('body').trigger('process-graphics-container')

  onWindowResize = ->
    $('@graphic-container').empty()
    triggerProcess()

  $(window).resize(onWindowResize.debounce(100))

  # Run processGraphicContainers on DOM ready
  triggerProcess()
