###
  Countries (страны для Лены) page code
###
app.on '@countries', ($$, countries) ->

  settings = countries.find('@settings')

  # Handle tabs switch
  @on 'change', '@tabs', (e, id, oldId) ->
    $('@settings-page', settings).hide()
    $("@settings-page[data-id='#{id}']", settings).show()
    false

  # NOTE: This is example callback to update tables
  updateCountriesTables = (html) ->
    $('@countries-tables').html(html.html)

  # Handle options change
  @on 'change', '@countries-settings', (
    ->
      appsSettings = []
      geoSettings = []
      # NOTE: Example of settings collect
      $(@).find(".settings-columns[data-id='apps']").find('@option').map ->
        if $(@).is('.is-active') && $(@).data('id') != undefined
          appsSettings.push($(@).data('id'))
      $(@).find(".settings-columns[data-id='countries']").find('@option').map ->
        if $(@).is('.is-active') && $(@).data('id') != undefined
          geoSettings.push($(@).data('id'))

      total = $('.settings-option.total-option.is-active').data("total");

      # NOTE: Example of post settings and update html
      $.post('/countries/updateFilter', {appsSettings: appsSettings, geoSettings: geoSettings, total: total}, updateCountriesTables)

  ).debounce(500)

  # Handle date period change
  @on 'change', '@countries-period-selector', (e, from, to) ->
    # NOTE: Example of post and update html
    $.post('/countries/updateDateRange', {from: from, to: to}, updateCountriesTables)
    false

  # Handle click on option
  @on 'click', '@countries-settings @option', ->
    active = $(@).is('.is-active')

    # Process radio group
    if $(@).data('type') == 'radio'

      # If option is not active
      unless active
        activated = true

        $(@)
          .addClass('is-active')
          .parents('@radio-group')
            .find('@option.is-active')
            .not(@)
              .removeClass('is-active')

    else # Regular option

      $(@).toggleClass('is-active')
      activated = not active

      tree = $(@).parents('@options-tree')

      # If option is in subtree
      if tree.length > 0
        treeOption  = tree.find('@option:first')
        restOptions = tree.find('@option').not(treeOption)

        [who, what] = if $(@).is(treeOption)
          # If changed option is tree option
          [restOptions, if activated then 'add' else 'remove']
        else # changed option is subtree option
          # If tree has actived options keep tree option actived
          [treeOption, if restOptions.is('.is-active') then 'add' else 'remove']

        who[what + 'Class']('is-active')

    unless active == activated
      settings.trigger('change', $(@).data('id'), activated)

    false

  @on 'click', '@settings-tilte', ->
    # TODO: Replace line when bug will be fixed:
    #   https://github.com/kossnocorp/role/issues/17
    # nextContainers = $(@).nextUntil('@settings-title')
    nextContainers = $(@).nextUntil('.settings-column-title')
    allOptions     = nextContainers.find('@option')
    options        = allOptions.not \
                      $(@).parents('@settings-column').find('@radio-group @option')

    if options.is(':not(.is-active)')
      options.addClass('is-active')
    else
      options.removeClass('is-active')
    settings.trigger('change')

  $("@options-tree").map ->
    #TODO::add trigger
    if $(@).find(".game-item.is-active").length == $(@).find(".game-item").length
      $(@).find("a:first").addClass("is-active")


