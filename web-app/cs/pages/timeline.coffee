###
  Timeline page code
###

# Spin options
app.spinOptions =
  small:
    lines:     11
    length:    3
    width:     2
    radius:    5
    rotate:    0
    color:     '#000'
    speed:     1
    trail:     50
    shadow:    false
    hwaccel:   false
    className: 'spinner'
    zIndex:    2e9
    top:       'auto'
    left:      'auto'

app.on '@timeline', ($$, timeline) ->

  collectSettings = ->
    typeIds = []
    resolutionIds = []
    countryIds = []
    storeIds = []
    requiredProductTypeIds = []
    skuIds = []

    $('.is-versions').find("@setting.is-striked").map ->
      typeIds.push($(@).data("id"))
    $('.is-style').find("@setting.is-striked").map ->
      resolutionIds.push($(@).data("id"))
    $('.is-regions-and-countries').find("@setting.is-striked").map ->
      countryIds.push($(@).data("id"))
    $('.is-os-and-stores').find("@setting.is-striked").map ->
      storeIds.push($(@).data("id"))
    $('.is-required-product-types').find("@setting.is-striked").map ->
      requiredProductTypeIds.push($(@).data("id"))
    $('.is-metaskus-and-skus').find("@setting.is-striked").map ->
      skuIds.push($(@).data("id"))

    compareColumn = $("@candy-group").find(".candy.is-active").data("id")
    ifNeedAppsDownloads = $("@apps-downloads").is(":checked")
    ifNeedAppsRevenue = $("@apps-revenue").is(":checked")
    ifNeedAppsCompare = $("@apps-candy").is(".is-active")
    ifNeedInappsDownloads = $("@inapps-downloads").is(":checked")
    ifNeedInappsRevenue = $("@inapps-revenue").is(":checked")
    ifNeedInappsCompare = $("@inapps-candy").is(".is-active")
    ifNeedUpdates = $("@updates").is(":checked")
    ifNeedUpdatesCompare = $("@updates-candy").is(".is-active")

    settings = {
      typeIds: typeIds,
      resolutionIds: resolutionIds,
      countryIds: countryIds,
      storeIds: storeIds,
      requiredProductTypeIds: requiredProductTypeIds,
      skuIds: skuIds,
      compareColumn: compareColumn,
      ifNeedAppsDownloads: ifNeedAppsDownloads,
      ifNeedAppsRevenue: ifNeedAppsRevenue,
      ifNeedAppsCompare: ifNeedAppsCompare,
      ifNeedInappsDownloads: ifNeedInappsDownloads,
      ifNeedInappsRevenue: ifNeedInappsRevenue,
      ifNeedInappsCompare: ifNeedInappsCompare,
      ifNeedUpdates: ifNeedUpdates,
      ifNeedUpdatesCompare: ifNeedUpdatesCompare,
      fromDate: $("@timeline-period-selector").data("from"),
      toDate: $("@timeline-period-selector").data("to"),
      step: $(".is-period-tabs").find("a.is-current").data("id")
    }

  ## Update timeline from settings

  updateTimeline = (html) ->
    $(".graphs").html(html.html)
    $('body').trigger('hide-overlay')
    $(".settings-window").hide()
    $("@open-settings").removeClass("is-overlay-over")
    $('@tabs').trigger('reload')
    $('body').trigger('process-graphics-container')

  saveSettingsAndUpdateHtml = (buttonSelector = null) ->
    settings = collectSettings()
    # NOTE: Example of post and update html
    $.post(
      '/timeline/updateGraphs'
      settings
      (html) =>
        if buttonSelector
          $(buttonSelector).trigger('loaded')
        updateTimeline(html)
    )

  @on 'change', '@timeline-period-selector', (e, from, to) ->
    $(@).trigger('updatePeriodByStep', [saveSettingsAndUpdateHtml])
    false

  @on 'change', '@timeline-popup-selector', ->
    saveSettingsAndUpdateHtml()

  @on 'change', '@timeline-tabs', (e, id, oldId) ->
    $('@timeline-period-selector').trigger('updatePeriodByStep', [saveSettingsAndUpdateHtml])

  ## Timeline settings code

  # Axis selector container (timeline settings)
  axisChoose = $$('@axis-choose')

  # Handle candy change
  axisChoose.on 'change', '@candy', (e, activated) ->
    if activated
      axisChoose.find('@candy').not(@).removeClass('is-active')
      axisChoose.trigger('change', $(@).data('id'))

      $('@compared-by-candy').text($(@).data('title'))

  # Hide all subtrees
  $$('@tree:not(.is-open) @subtree').hide()

  # Handle click on subtree open/close link
  axisChoose.on 'click', '@open-subtree', ->
    tree = $(@).parent('@tree')

    tree.toggleClass('is-open')
    tree.find('@subtree').slideToggle(200)

    false

  # Strikes or unstrikes dependent ids to elements and their trees
  strikeAndUnstrikeDependentObjects = (result) ->
    if result?
      for filterColumn of result
        strikedIds = result[filterColumn].allStrikedIds
        unstrikedIds = result[filterColumn].allUnstrikedIds
        if filterColumn? and (strikedIds?.length > 0 or unstrikedIds?.length > 0)
          filterColumnElem = $('@filter[data-filter="' + filterColumn + '"]')
          if filterColumnElem?.length > 0
            if strikedIds?
              for strikedId in strikedIds
                elem = $(filterColumnElem).find('@setting[data-id="' + strikedId + '"]')
                if elem?.length > 0
                  applyStrikeToElement(elem, false, true, true)
            if unstrikedIds?
              for unstrikedId in unstrikedIds
                elem = $(filterColumnElem).find('@setting[data-id="' + unstrikedId + '"]')
                if elem?.length > 0
                  applyStrikeToElement(elem, false, false, true)
    true

  # Collects strikes or unstrikes elements for column with need filterColumn
  collectFilterStrikedUnstrikedIds = (filterColumn = "", shouldAllBeStriked = null) ->
    actionStrikedIds = []
    actionUnstrikedIds = []
    filterElem = $('@filter[data-filter="' + filterColumn + '"]')
    treeElem = filterElem?.find('@whole-tree:first')
    if treeElem?.length > 0
      settings = $(treeElem).find('@setting')
      if shouldAllBeStriked != null
        settings.map ->
          id = $(@).data("id")
          if shouldAllBeStriked
            actionStrikedIds.push(id)
          else
            actionUnstrikedIds.push(id)
      else
        topParent = $(treeElem).data("top-parent-filter");
        isTop = !topParent
        if isTop
          settings.map ->
            id = $(@).data("id")
            if $(@).is('.is-striked')
              actionStrikedIds.push(id)
            else
              actionUnstrikedIds.push(id)
        else
          settings.map ->
            id = $(@).data("id")
            wasStriked = $(@).data('strike-state') == "is-striked"
            if $(@).is('.is-striked')
              if (!wasStriked)
                actionStrikedIds.push(id)
            else
              if (wasStriked)
                actionUnstrikedIds.push(id)
    return {
      strikedIds: actionStrikedIds
      unstrikedIds: actionUnstrikedIds
    }

  # Fetches dependent ids to all striked and all unstriked elements of column, found by someElements,
  # and strikes or unstrikes elements and their trees in dependent columns
  fetchAndStrikeUnstrikeDependentObjects = (someElements, shouldAllBeStriked = null) ->
    if someElements?.length > 0
      someElem = someElements.first()
      filterColumnElem = someElem?.parents('@filter:first')
      treeElem = someElem?.parents('@whole-tree:first')

      if filterColumnElem?.length > 0 and treeElem?.length > 0
        filterColumn = $(filterColumnElem).data("filter")
        topParentFilterColumn = $(treeElem).data("top-parent-filter")
        existsDependentFilterColumns = $(treeElem).data("dependent-filters-exists")

        if filterColumn?.length > 0 and (existsDependentFilterColumns or topParentFilterColumn)
          actionStrikedUnstrikeIds = collectFilterStrikedUnstrikedIds(filterColumn, shouldAllBeStriked)

          if actionStrikedUnstrikeIds.strikedIds.length > 0 or actionStrikedUnstrikeIds.unstrikedIds.length > 0

            params = {
              actionFilterColumn: filterColumn
            }
            params[filterColumn] = actionStrikedUnstrikeIds
            params[topParentFilterColumn] = collectFilterStrikedUnstrikedIds(topParentFilterColumn)

            $.post(
              '/filter/ajaxFetchDependentStrikedIds'
              params
              (data) =>
                if data?.success
                  strikeAndUnstrikeDependentObjects(data.result)
            )
    true

  # Click on candy-text control
  @on 'click-text', '@candy-text', ->
    filter = $(@).parents('@filter')
    treeSwitchers = $(filter).find('@tree-switcher')
    settings = $(filter).find('@setting')
    candies = $(filter).find('@candy')

    strikedSettingExist = settings.is('.is-striked')
    shouldBeStriked = not strikedSettingExist

    if shouldBeStriked
      $(candies).removeClass('is-striked-little')
      $(treeSwitchers).removeClass('is-striked-little')
      $(candies).addClass('is-striked')
      $(treeSwitchers).addClass('is-striked')
      $(settings).addClass('is-striked')
    else
      $(candies).removeClass('is-striked-little')
      $(candies).removeClass('is-striked')
      $(treeSwitchers).removeClass('is-striked-little')
      $(treeSwitchers).removeClass('is-striked')
      $(settings).removeClass('is-striked')

    fetchAndStrikeUnstrikeDependentObjects(settings, shouldBeStriked)

    return false;

  # Modifies parent tree because of modified child
  applyStrikeToTree = (tree, wholeTree) ->
    if tree?.length > 0
      settings = tree.find('@setting')
      title    = tree.find("@tree-switcher")

      strikedSettingExist = settings.is('.is-striked')
      unstrikedSettingExist = settings.is(':not(.is-striked)')

      if strikedSettingExist and not unstrikedSettingExist
        title.removeClass('is-striked-little')
        title.addClass('is-striked')
      else if strikedSettingExist and unstrikedSettingExist
        title.removeClass('is-striked')
        title.addClass('is-striked-little')
      else
        title.removeClass('is-striked')
        title.removeClass('is-striked-little')

    if wholeTree?.length > 0
      wholeTreeSettings = $(wholeTree).find('@setting')
      filter            = $(wholeTree).parents('@filter')
      candies           = $(filter).find('@candy')

      wholeTreeStrikedSettingExist = wholeTreeSettings.is('.is-striked')
      wholeTreeUstrikedSettingExist = wholeTreeSettings.is(':not(.is-striked)')

      if wholeTreeStrikedSettingExist and not wholeTreeUstrikedSettingExist
        $(candies).removeClass('is-striked-little')
        $(candies).addClass('is-striked')
      else if wholeTreeStrikedSettingExist and wholeTreeUstrikedSettingExist
        $(candies).removeClass('is-striked')
        $(candies).addClass('is-striked-little')
      else
        $(candies).removeClass('is-striked')
        $(candies).removeClass('is-striked-little')

  # Modifies tree's element
  applyStrikeToElement = (elem, shouldStrikeDependent, shouldBeStriked = null, shouldModifyState = null) ->
    setting = if $(elem).is('@setting')
      $(elem)
    else
      $(elem).parent('@setting')

    settingId = setting.data("id");
    shouldBeStriked ?= not setting.hasClass("is-striked");

    settingsDuplicate = []
    wholeTree = $(elem).parents('@whole-tree:first')
    if wholeTree?.length > 0
      wholeTree.find('@setting[data-id="' + settingId + '"]').map ->
        settingsDuplicate.push($(@))

    for settingDuplicate in settingsDuplicate
      if shouldBeStriked
        settingDuplicate.addClass('is-striked')
        if shouldModifyState
          settingDuplicate.data('strike-state', 'is-striked')
      else
        settingDuplicate.removeClass('is-striked')
        if shouldModifyState
          settingDuplicate.data('strike-state', '')
      tree = settingDuplicate.parents('@tree:first')
      applyStrikeToTree(tree, wholeTree)

    if shouldStrikeDependent
      fetchAndStrikeUnstrikeDependentObjects(setting)

    true

  # Click on axis switcher
  axisChoose.on 'click', '@switcher', ->
    return applyStrikeToElement(@, true, null, false)

  # Click on tree switcher (enable or disable whole tree)
  axisChoose.on 'click', '@tree-switcher', ->
    tree        = $(@).parents('@tree:first')
    allSettings = tree.find('@setting')
    shouldBeStriked = not allSettings.is('.is-striked');

    for setting in allSettings
      applyStrikeToElement(setting, false, shouldBeStriked, false)

    fetchAndStrikeUnstrikeDependentObjects(allSettings)

    true

  # Display settings (in timeline settings footer)
  displayChoose = $$('@display')

  # Check if el enabled to check
  isEnableToCheck = (el) ->
    not el.parents('@option:first').is('.is-disabled')

  # Handle graphics click on candy in display settings
  displayChoose.on 'click', '@candy', ->
    return false unless isEnableToCheck($(@))

  # Handle graphics display candies change
  displayChoose.on 'change', '@candy', (e, activated) ->
    $(@).parents('@option:first').toggleClass('is-active')

  disableComparedByCandyIfNeed = (option) ->
    optionGroup = option.parents('@optionGroup')
    optionsCheckboxes = optionGroup.find('@option @checkbox')
    checkboxChild = optionGroup.find('@compared-by-candy:first')
    optionChild = checkboxChild.parents('@option:first')

    checkedSettingExist = $(optionsCheckboxes).is(':checked')

    if checkedSettingExist
      checkboxChild.removeAttr('disabled')
      checkboxChild.removeClass('is-disabled')
      optionChild.removeClass('is-disabled')
    else
      checkboxChild.attr('disabled', 'disabled')
      checkboxChild.removeAttr('checked')
      checkboxChild.addClass('is-disabled')
      optionChild.addClass('is-disabled')

  # Graphics checkbox (in timeline settings footer)
  displayChoose.on 'change', '@checkbox', ->
    option = $(@).parents('@option:first')
    option.toggleClass('is-active')
    disableComparedByCandyIfNeed(option)


  # Handle loading trigger (save button)
  displayChoose.on 'loading', '@save@timeline-settings-save', ->
    saveSettingsAndUpdateHtml($(@))

  wholeTrees = $("@whole-tree")
  if wholeTrees?.length > 0
    for wholeTree in wholeTrees
      trees = $(@).find('@tree')
      if trees?.length > 0
        for tree in trees
          applyStrikeToTree($(tree), $(wholeTree))
      else
        applyStrikeToTree(null, $(wholeTree))

  displayChoose.find("@option").map ->
    disableComparedByCandyIfNeed($(@))
