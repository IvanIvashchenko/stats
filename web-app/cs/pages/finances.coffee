###
  Finances page code
###
app.on '@finances', ($$, finances) ->

  # NOTE: This is example callback to update graphics
  updateGraphics = (html) ->
    $('.graphs-container').html(html.html)
    $('body').trigger('process-graphics-container')

  @on 'change', '@finances-period-selector', (e, from, to) ->
    # NOTE: Example of post and update html
    $.post('/finances/updateTime', {from: from, to: to}, updateGraphics)
    false

  @on 'change', '@finances-stores-selector', ->
    # NOTE: Example of post and update html
    settings = []
    $(@).find('@item').not('.is-active').map ->
      settings.push($(@).data('id'))
    $.post('/finances/updateStores', {storeIds: settings}, updateGraphics)
