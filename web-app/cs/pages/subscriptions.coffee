###
  Subscriptions code page
###
app.on '@subscriptions', ->

  # Handle loading trigger (save button state)
  @on 'loading', '@save', ->
    saveButton = $(@);
    params =  {weekly: $('@weekly').is(':checked'),everydayRating: $('@everydayRating').is(':checked'),everydayDownloads: $('@everydayDownloads').is(':checked')}; 	
    $.post('/reports/saveSubscriptions', params , saveButton.trigger('loaded'));

