app.on '@login', ($$, login) ->
  # NOTE: I can't change layout because piano can't do it,
  #       so I use this hack: remove everything.
  $('@header, @overlay').remove()

  $.backstretch('/images/ui/login_background.png');
