###
  Export page code
###
# checked = []
# activeCheckboxes = []

# TODO:: change this
$(document).ready ->
  if $("@monthlyFinancial").is(":checked")
    $("@display-settings").find("span.timeline-row-setting:not(.export-modifiers)").removeClass("is-active").addClass("is-disabled")
    activeCheckboxes = $("@display-settings input:checkbox:not([role='checkbox monthlyFinancial'])")
    activeCheckboxes.attr "disabled", true

app.on '@export', ($$, exportEl) ->

  # TODO:: use classes
  window.collectSettings = (isBookmark) ->
    typeIds = []
    resolutionIds = []
    countryIds = []
    storeIds = []
    skuIds = []
    requiredProductTypeIds = []
    compareColumns = []

    $('.is-versions').find("@setting.is-striked").map ->
      typeIds.push($(@).data("id"))
    $('.is-style').find("@setting.is-striked").map ->
      resolutionIds.push($(@).data("id"))
    $('.is-regions-and-countries').find("@setting.is-striked").map ->
      countryIds.push($(@).data("id"))
    $('.is-os-and-stores').find("@setting.is-striked").map ->
      storeIds.push($(@).data("id"))
    $('.is-metaskus-and-skus').find("@setting.is-striked").map ->
      skuIds.push($(@).data("id"))
    $('.is-required-product-types').find("@setting.is-striked").map ->
      requiredProductTypeIds.push($(@).data("id"))
    $('@candy-group').find(".candy.is-active").map ->
      compareColumns.push($(@).data("id"))

    ifNeedDownloads = $("@downloads").is(":enabled") && $("@downloads").is(":checked")
    ifNeedRevenues = $("@revenue").is(":enabled") && $("@revenue").is(":checked")
    ifNeedUpdates = $("@updates").is(":enabled") && $("@updates").is(":checked")
    ifNeedRefunds = $("@refunds").is(":enabled") && $("@refunds").is(":checked")
    ifNeedRefundsMonetized = $("@refundsMonetized").is(":enabled") && $("@refundsMonetized").is(":checked")
    ifNeedRankings = $("@rankings").is(":enabled") && $("@rankings").is(":checked")
    ifNeedTotal = $("@total").is(":checked")
    ifNeedMonthlyFinancial = $("@monthlyFinancial").is(":checked")

    bookmarkName = $("@bookmark-new-name").val() if isBookmark


    settings = {
      typeIds: typeIds,
      resolutionIds: resolutionIds,
      countryIds: countryIds,
      storeIds: storeIds,
      skuIds: skuIds,
      requiredProductTypeIds: requiredProductTypeIds,
      compareColumns: compareColumns,
      ifNeedDownloads: ifNeedDownloads,
      ifNeedRevenues: ifNeedRevenues,
      ifNeedUpdates: ifNeedUpdates,
      ifNeedRefunds: ifNeedRefunds,
      ifNeedRefundsMonetized: ifNeedRefundsMonetized,
      ifNeedRankings: ifNeedRankings,
      ifNeedTotal: ifNeedTotal,
      ifNeedMonthlyFinancial: ifNeedMonthlyFinancial,
      fromDate: $("@export-period-selector").data("from"),
      toDate: $("@export-period-selector").data("to"),
      step: $(".is-period-tabs").find("a.is-current").data("id"),
      bookmarkName: bookmarkName
    }

  # Handle loading trigger (save button)
  @on 'loading', '@save@export-save', ->
    settings = collectSettings(false)
    exportUrl = $(@).attr('href')
    $.post(
      '/export/saveSettings'
      settings
      (data) =>
        $(@).trigger('loaded')
        if data?.success
          # window.location.href = exportUrl
          $.post(
            exportUrl
            {}
            (exportdata) =>
              console?.log exportdata.success
          )
    )

  updateOptionStateIfNeed = (option) ->
    checkbox = option?.find ('@checkbox:first')
    if option?.length > 0 && option?.length > 0
      needStep = option.data('need-step')
      needComparedByColumns = option.data('need-comparissons')

      step = $(".is-period-tabs").find("a.is-current").data("id")

      needStep ?= ''
      isAvailable = '' != needStep and needStep == step
      if isAvailable && needComparedByColumns?.length > 0
        for needComparedByColumn in needComparedByColumns
          elem = $('@candy-group').find('.candy.is-active[data-id="' + needComparedByColumn + '"]')
          if elem?.length <= 0
            isAvailable = false;
            break;

      if isAvailable
        $(checkbox).removeAttr('disabled')
        $(option).removeClass('is-disabled')
      else
        $(checkbox).attr('disabled', 'disabled')
        $(checkbox).removeAttr('checked')
        $(option).addClass('is-disabled')

    false

  # If candy is empty then noone clicked on it.
  updateAvailableCandiesIfNeed = (dependentCandy, candy) ->

    someOneClicked = candy?
    candy = candy ? dependentCandy

    dependentCandyId = dependentCandy?.data('id')
    candyId = candy?.data('id')
    if dependentCandyId? and candyId? and dependentCandyId == candyId
      isActive = candy.is('.is-active')
      availableComparedByColumns = candy.data('available-comparissons')
      candies = $('@axis-choose').find('.candy')
      if candies?.length > 0
        if isActive or (not isActive and someOneClicked)
          availableComparedByColumns ?= []
          for curCandy in candies
            curCandyId = $(curCandy).data('id');
            curCandyId ?= '';
            if curCandyId != candyId
              isAvailable = false;
              for availableComparedByColumn in availableComparedByColumns
                if curCandyId == availableComparedByColumn
                  isAvailable = true;
                  break;
              if not isAvailable
                if isActive
                  $(curCandy).addClass('is-disabled')
                  $(curCandy).removeClass('is-active')
                else
                  $(curCandy).removeClass('is-disabled')

    false

  # Handle change trigger on exportTabs
  @on 'change', '@export-tabs', (e, id, oldId) ->
    $('@step-state-dependent').trigger('step-state-change')
    false

  # Handle change trigger on step state
  @on 'step-state-change', '@step-state-dependent', ->
    if $(@).is('@option')
      updateOptionStateIfNeed($(@))
    if $(@).is('@export-period-selector')
      $(@).trigger('updatePeriodByStep')
    false

  # Handle change trigger on candy state
  @on 'candy-state-change', '@candy-state-dependent', (event, candy) ->

    if $(@).is('@option')
      updateOptionStateIfNeed($(@))
    else if $(@).is('@candy')
      updateAvailableCandiesIfNeed($(@), candy)
    false

  @on 'change', '@export-period-selector', (e, from, to) ->
    $(@).trigger('updatePeriodByStep')
    false

  # Handle change checkbox for monthly financial reports
  @on 'change', '@monthlyFinancial', ->
    activeCheckboxes = $("@display-settings input:checkbox:not([role='checkbox monthlyFinancial'])")
    if $(this).is(":checked")
      $("@display-settings").find("span.timeline-row-setting:not(.export-modifiers)").removeClass("is-active").addClass("is-disabled")
      # checked = $("@display-settings input:checkbox:not([role='checkbox monthlyFinancial']):checked");
      # checked.removeAttr "checked"
      activeCheckboxes.attr "disabled", true
      $(".is-period-tabs").find("a.is-current").removeClass("is-current")
      $(".is-period-tabs").find("a[data-id='MONTHLY']").addClass("is-current")
      $('@step-state-dependent').trigger('step-state-change')
    else
      $("@display-settings").find("span.timeline-row-setting.is-disabled").removeClass("is-disabled").addClass("is-active")
      activeCheckboxes.removeAttr "disabled" if activeCheckboxes.length isnt 0
      # checked.attr "checked", true if checked.length isnt 0

  # Call change trigger on exportTabs and candies
  $('@step-state-dependent@option').trigger('step-state-change')
  $('@candy-state-dependent').trigger('candy-state-change', [null])
