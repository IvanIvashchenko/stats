GraphicUtils = @?.Graphic?.Utils or require('./graphic/utils')

# Private

# Templates list
templates = {}

# Template function extracted from Underscore.js
# Annotated source code: http://underscorejs.org/docs/underscore.html#section-126
_template = do ->

  settings =
    evaluate:    /{{([\s\S]+?)}}/g
    interpolate: /{{=([\s\S]+?)}}/g
    escape:      /{{-([\s\S]+?)}}/g

  noMatch = /.^/

  escapes =
    '\\':    '\\'
    "'":     "'"
    'r':     '\r'
    'n':     '\n'
    't':     '\t'
    'u2028': '\u2028'
    'u2029': '\u2029'

  escapes[escapes[p]] = p for p of escapes

  escaper = /\\|'|\r|\n|\t|\u2028|\u2029/g
  unescaper = /\\(\\|'|r|n|t|u2028|u2029)/g

  unescape = (code) ->
    code.replace(unescaper, (match, escape) -> escapes[escape])

  template = (text, data) ->

    processedText = text
      .replace(escaper, (match) -> '\\' + escapes[match])
      .replace(settings.escape || noMatch, (match, code) -> "'+\n_.escape(#{unescape(code)})+\n'")
      .replace(settings.interpolate || noMatch, (match, code) -> "'+\n(#{unescape(code)})+\n'")
      .replace(settings.evaluate || noMatch, (match, code) ->
        "';\n#{unescape(code)}\n;__p+='")

    source = "__p+='#{processedText}';\n"

    unless settings.variable
      source = "with(obj||{}){\n#{source}}\n"

    source =
      "var __p='';" +
      "var print=function(){__p+=Array.prototype.join.call(arguments, '')};\n" +
      source + "return __p;\n";

    render = new Function(settings.variable || 'obj', '_', source)
    return render(data, _) if data

    template = (data) -> render.call(this, data, _)
    template.source = "function(#{settings.variable || 'obj'}){\n#{source}}"

    template

  template

###
  Templates

  Helpers for render templates
###
Templates =

  ###
    Public: initialize templates from els
  ###
  init: (els) ->
    els.each ->
      templates[$(@).data('id')] = _template($(@).html())

  ###
    Public: returns jQuery object with interpolated values
  ###
  template: (name, data = {}) ->
    $(templates[name](_.extend({}, data, GraphicUtils.helpers)))

# Export to global scope
if window?
  window.Templates = Templates
else
  module.exports = Templates
