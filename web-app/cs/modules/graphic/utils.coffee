_ = @?._ or require('underscore')
Color = @?.Color or require('color')

###
  Internal: get dinstance between two numbers
###
getDistance = (a, b) -> (a - b).abs()

###
  Graphics.Utils

  Helpers for build and format graphic
###
GraphicUtils =

  ###
    Public: return rounded axis max and axis step
  ###
  getAxisMaxAndStep: (max, count) ->
    # TODO: Function is too big, refactor to private functions
    roughlyStep = (max / count).floor()

    roundedStep = if roughlyStep >= 500
      500
    else if roughlyStep >= 100
      100
    else
      10

    if roughlyStep % roundedStep == 0
      step = roughlyStep
    else

      step = roughlyStep.ceil(-(roughlyStep.toString().length - 1))

      if step > 500

        kStart  = step.floor(-3)
        kMiddle = kStart + 500
        kEnd    = step.ceil(-3)

        # Between k and k + 500
        step = if kStart < step < kMiddle

          if getDistance(step, kStart) <
             getDistance(step, kMiddle)
            kStart
          else
            kMiddle

        # Between k + 500 and k + 1000
        else

          if getDistance(step, kMiddle) <
             getDistance(step, kEnd)
            kMiddle
          else
            kEnd

    [(max / step).ceil() * step, step]

  ###
    Public: returns arrays of axis values
  ###
  getAxisLine: (max, count) ->
    [realMax, step] = @getAxisMaxAndStep(max, count)
    [step.upto(realMax, null, step), realMax]

  ###
    Public: returns formatted date string
  ###
  getDateString: (date, addMonth = false) ->
    values = [date.getDate()]
    values.push(date.format('{Mon}')) if addMonth
    # TODO: Find symbol for short year (not ' actualy)
    values.push("'" + date.format('{yy}')) unless date.isThisYear()
    values.join(' ')

  ###
    Public: returns minimal step for axis
  ###
  getMinimalStep: (data) ->
    data.axis
      .map('step')
      .min((step) -> Date.create().daysUntil(Date.create().advance(step)))

  ###
    Public: check if passed date is last or first day of year
  ###
  isFirstOrLastDayOfMonth: (date) ->
    date.getDate() == 1 or
    date.getTime() == new Date(date.getFullYear(), date.getMonth() + 1, 0).getTime()

  ###
    Public: generate random hex color
  ###
  randomColor: ->
    color = []
    chars = '0123456789ABCDEF'.split('')
    (6).times -> color.push(chars[Math.round(Math.random() * 15)])
    '#' + color.join('')

  ###
    Public: returns random hex color with luminosity greater than 0.6
  ###
  graphicColor: ->
    color = Color(@randomColor())
    contrast = Color(color.hexString()).contrast(Color('white'))
    if contrast > 2 and contrast < 6
      color.hexString()
    else
      @graphicColor()

  ###
    Public: generate second color from origin
  ###
  secondColor: (color) ->
    @opaquerColor(color, 0.5)

  ###
    Public: generate third color from origin
  ###
  thirdColor: (color) ->
    @opaquerColor(color, 0.1)

  ###
    Public: generate hover color (third color + 10%)
  ###
  hoverColor: (color) ->
    @opaquerColor(color, 0.2)

  opaquerColor: (color, opacity) ->
    Color()
      .rgb(@rgba2rgb(Color(color).rgbArray().add(opacity)))
      .hexString()

  ###
    Public: generate rgb from rgba
  ###
  rgba2rgb: (rgba) ->
    opacity = rgba.last()
    rgba.first(3).map((c) -> 255 * (1 - opacity) + c * opacity)

  ###
    Public: scale passed array from one coordinate system to another
  ###
  scaleValues: (values, valuesMax, max) ->
    scale = max / valuesMax
    values.map (value) -> value * scale

  ###
    Public: inverse passed value
  ###
  inverseValue: (value, max) -> max - value

  ###
    Public: templates helpers
  ###
  helpers:
    ###
      Public: format date to render in graphic timeline
    ###
    formatDate: (date, forceAddMonth = false) ->
      GraphicUtils.getDateString(date, forceAddMonth or GraphicUtils.isFirstOrLastDayOfMonth(date))

    ###
      Public: format passed number (add spaces)
    ###
    formatNumber: (num) ->
      num.format(0, ' ')

    ###
      Public: returns string with respective count of arrows
    ###
    growArrows: (val) ->
      arrow = if val < 0
        '↓'
      else
        '↑'

      arrow.repeat(if val? then val.abs() else 0)

# Export to global scope
if window?
  window.Graphic ?= {}
  window.Graphic.Utils = GraphicUtils
else
  module.exports = GraphicUtils
