Templates        = @?.Templates         or require('./../templates')
GraphicUtils     = @?.Graphic?.Utils    or require('./utils')
GraphicDrawUtils = @?.Graphic?.DrawUtils or require('./draw_utils')

# Private module functions

# Offset for axis lines (on left and right)
OFFSET_X = 10

###
  Internal: returns array of axis header jQuery objects
###
getHeaderEls = (data) ->
  headerEls = []

  for axis in data.axis
    headerEls.push \
      Templates.template('graphic_header_axis', axis)

  headerEls

###
  Internal: returns array of axis value and axis max
###
getAxis = (data) ->
  _(data.axis).map (axis) ->
    [axisLine, axisMax] = GraphicUtils.getAxisLine(axis.max, axis.segments)
    line: axisLine, max: axisMax, obj: axis

###
  Internal: returns array of axis jQ objects
###
getAxisEls = (data, axis, timeline) ->
  axisElArray = []

  _(axis).each (axis) ->
    axisElArray.add \
      Templates.template('graphic_axis', line: axis.line)

  # Time axis
  axisElArray.add \
    Templates.template('graphic_axis_time', dates: timeline)

  axisElArray

###
  Internal: append axis headers to el
###
appendHeaders = (el, headers) ->
  headerEl = el.find('@header')
  headerEl.append.apply(headerEl, headers)

###
  Internal: append axis to el
###
appendAxis = (el, axis) ->
  axisEl = el.find('@axis')
  axisEl.append.apply(axisEl, axis)

###
  Internal: append graphic (DOM part) to container
###
appendGraphic = (el, graphicEl, height) ->
  graphicEl
    .filter('@graphic')
      .css(height: height)
      .end()
    .appendTo(el)

###
  Internal: render path
###
runDrawPathCommand = (paper, command, strokeColor) ->
  line = paper.path \
    GraphicDrawUtils.inverseY(command, paper.height).join('')
  line.attr('stroke', strokeColor)
  # 1px line width
  line.translate(0.5, 0.5)

###
  Internal: render graphic
###
renderGraphic = (paper, graphic) ->
  sequence = GraphicDrawUtils.sequence \
    graphic.sys,
    shift:  OFFSET_X
    colors: graphic.colors
    width:  (paper.width - 10)
    step:   (paper.width - 20) / graphic.sys.length
    fuzzyValues: graphic.data.fuzzyValues

  for command in sequence
    if command[0] == 'color'
      strokeColor = command[1]

    else if command[0] == 'path'
      runDrawPathCommand(paper, command[1], strokeColor)

  # TODO: Add envent listener

###
  Internal: render curve graphic
###
renderCurve = (paper, graphic) ->
  height = paper.height * graphic.scale
  y      = paper.height - height

  paper.linechart \
    0, y, paper.width, height,
    (0).upto(graphic.sys.length - 1), [graphic.sys],
    nostroke: false, smooth: true, width: 1, colors: graphic.colors

###
  Internal: render rough graphic
###
renderRough = (paper, graphic, offsetX) ->
  # TODO: Move this code to GraphicDrawUtils

  path = []

  graphic.sys.each (sy, i) ->
    x = graphic.step * i + (if offsetX? then offsetX else OFFSET_X)
    y = GraphicUtils.inverseValue(sy, paper.height).round()

    path.push("M#{x},#{y}") if i == 0
    path.push("L#{x},#{y}")

  line = paper.path(path.join(''))
  line.attr(stroke: graphic.colors[0])
  line.translate(0.5, 0.5)

###
  Internal: create Raphael paper for el
###
getPaper = (el) ->
  gEl = el.find('@graphic')
  new Raphael \
    el.find('@paper').get(0), gEl.width() - 80, gEl.height() - 50

###
  Internal: get scaled values for graphics
###
getScaledGraphics = (graphics, axis, paper) ->
  graphics.map (graphic) ->
    GraphicUtils.scaleValues \
      graphic.values, axis[graphic.axis].max, paper.height

###
  Internal: get colors for graphic
###
getColorsForGraphic = (graphic) ->
  return graphic.colors if graphic.colors?

  baseColor   = graphic.color || GraphicUtils.graphicColor()
  secondColor = GraphicUtils.secondColor(baseColor)
  fillColor   = GraphicUtils.thirdColor(baseColor)
  fillHoverColor = GraphicUtils.hoverColor(baseColor)

  [baseColor, secondColor, fillColor, fillHoverColor]

###
  Internal:
    each scaled values and render rect for all graphics from
    min to max
###
renderFillColumns = (paper, axisData) ->
  filteredGraphics = axisData.graphs.findAll (g, i) ->
    not (g.data.type == 'curve') and not (g.data.fill == false)

  return [] if filteredGraphics.isEmpty()

  rects = []

  # NOTO: currenly only one step supported
  sequence = GraphicDrawUtils.fillColumnsSequence \
    filteredGraphics.map('sys'),
    filteredGraphics.map('colors').map(2),
    filteredGraphics[0].step,
    filteredGraphics.map((g) -> g.data.fuzzyValues)[0],
    OFFSET_X

  for command in sequence
    x      = command[1].rect[0]
    y      = GraphicUtils.inverseValue(command[1].rect[3], paper.height)
    width  = (command[1].rect[2] - command[1].rect[0]) + 1
    height = (command[1].rect[3] - command[1].rect[1]) + 1
    rect   = paper.rect(x, y, width, height)
    rect.attr(fill: command[1].color, stroke: 'none')
    rects.push([rect, command[1].id])

  rects

###
  Internal: render compare graph (graph for finances page)
###
renderCompareGraph = (paper, graphic, graphics) ->
  # TODO: Refactor this function

  renderGraphic(paper, graphic)

  gToCompare = graphics[graphic.data.compareWith]
  [greaterColor, lessColor] = graphic.data.compareColors
  gradientDown = "90-#{lessColor}-#{greaterColor}"
  gradientUp   = "90-#{greaterColor}-#{lessColor}"

  graphic.sys.each (sy, i) ->

    # y to compare
    cy = gToCompare.sys[i]
    # current y
    y = GraphicUtils.inverseValue(sy, paper.height).round()
    # next y
    ny = graphic.sys[i + 1]
    # next y (scaled)
    nsy = GraphicUtils.inverseValue(ny, paper.height).round()
    # next y to compare
    ncy = gToCompare.sys[i + 1]

    if graphic.data.fuzzyValues?
      step = graphic.data.fuzzyValues[i] * graphic.step
      x1 = graphic.data.fuzzyValues.first(i).sum() * graphic.step + OFFSET_X
      x2 = (x1 + step).round()
    else
      step = graphic.step
      x1 = (i * step + OFFSET_X).round()
      x2 = (i * step + OFFSET_X + step).round()

    if cy > sy
      # Draw red line over lower values
      line = paper.path("M#{x1},#{y}L#{x2},#{y}")
      line.attr(stroke: lessColor)
      line.translate(0.5, 0.5)

      if ny
        if ny > ncy
          # Less -> greater
          rect = paper.rect(x2, nsy, 1, (y - nsy).abs())
          rect.attr(fill: gradientDown, stroke: 'none')

        else if ny < ncy
          # Less -> less
          line = paper.path("M#{x2},#{y}L#{x2},#{nsy}")
          line.attr(stroke: lessColor)
          line.translate(0.5, 0.5)

    else if ny and ny < ncy
      # Draw greater -> less
      down = y < nsy
      rect = paper.rect(x2, (if down then y else nsy), 1, (y - nsy).abs())
      rect.attr(fill: (if down then gradientDown else gradientUp), stroke: 'none')

###
  Internal:
    each scaled values and render lines for each graphic
###
renderGraphicLines = (paper, axis, axisData) ->
  #for graphicData, i in graphics
  axis.graphs.each (g, i) ->
    unless g.data.stroke == false
      switch g.data.type
        when 'curve'
          renderCurve(paper, g)
        when 'rough'
          renderRough(paper, g)
        when 'compare_graph'
          renderCompareGraph(paper, g, axisData.map('graphs').flatten())
        else
          renderGraphic(paper, g)

###
  Internal:
    render axis lines for all axis chunks
###
renderAxisLines = (paper, axis) ->
  if axis.data.obj.lines
    axis.svs.each (y) ->
      sy = GraphicUtils.inverseValue(y, paper.height).round()

      # TODO: Move this code to GraphicDrawUtils
      line = paper.path("M0,#{sy}L#{paper.width},#{sy}")
      line.attr(stroke: axis.data.obj.linesColor || 'white', opacity: 0.6)
      line.translate(0.5, 0.5)

  else if axis.data.obj.vLines

    step = axis.graphs[0].step

    axis.graphs[0].ys.each (v, i) ->
      return if i == 0
      x = (step * i + OFFSET_X).round()
      line = paper.path("M#{x},0L#{x},#{paper.height}")
      line.attr(stroke: axis.data.obj.linesColor || 'white')
      line.translate(0.5, 0.5)

###
  Internal: render underlines
###
renderUnderlines = (paper, allAxis) ->
  # NOTE: currently only first axis supported
  graphics = allAxis[0].graphs.map('sys')
  step     = allAxis[0].graphs[0].step

  # TODO: Move this code to GraphicDrawUtils
  allAxis.each (a) ->
    a.svs.each (y) ->
      iy = GraphicUtils.inverseValue(y, paper.height)
      graphics[0]
        .map((g, i) -> if y > graphics.map(i).max() then i else null)
        .compact()
        .reduce(
          (memo, x, i, xs) ->
            memoCopy = memo.clone()
            unless xs[i - 1] == x - 1
              memoCopy.push([x]) # Start x
            unless xs[i + 1] == x + 1
              memoCopy.last().push(x) # End x
            memoCopy
          []
        )
        .map((coord) -> [coord[0] * step, (coord[1] + 1) * step])
        .map((coord) -> coord.map((v) -> v + OFFSET_X))
        .add([[0, 10]], 0)
        .each (coord) ->
          line = paper.path("M#{coord[0]},#{iy}L#{coord[1]},#{iy}")

          colorIndex = null
          average = a.graphs.map('sys').map('average').map('round')
          average.each((v, i) -> colorIndex = i if y <= v)
          unless colorIndex?
            colorIndex = average.indexOf(average.max())

          color = a.graphs[colorIndex].colors[2]

          line.attr(stroke: color, opacity: 0.6)
          line.translate(0.5, 0.5)

###
  Internal: highlight columns
###
highlightColumns = (graph) ->
  graph.columns.each (c) ->
    c.attr(fill: graph.colors[3])

###
  Internal: restore highlight columns
###
restoreColumns = (graph) ->
  graph.columns.each (c) ->
    c.attr(fill: graph.colors[2])

###
  Internal: mouse enter graphic
###
mouseenterGraph = (graph, el) ->
  if graph?

    # Graphic is hovered
    if graph.data.hover

      offset = el.offset()

      xs = if graph.data.fuzzyValues
        graph.data.fuzzyValues
          .map((v) -> v * graph.step )
          .reduce(
            (memo, v) -> memo.add(memo.last() + v)
            [0]
          )
      else
        (0).upto(el.width(), null, graph.step)

      if graph.data.hoverFill
        if g = el.data('hovered-graph')
          restoreColumns(g)
        el.data('hovered-graph', graph)

        highlightColumns(graph)

      el.bind 'mousemove.graphic-hover', (e) ->
        x = e.pageX - offset.left - OFFSET_X

        if x > 0
          i = xs.findIndex((v) -> x < v) - 1

          if el.data('hovered-tip')

            el.data('hovered-tip-el')?.remove()
            el.data('hovered-tip', i)

            if graph.data.hoverType == 'finances'
              tip = graph.data.tips[i]

              tipEl = Templates
                .template('graphic_finances_tip', tip)
                .css(position: 'fixed')
                .appendTo('body')

              tipEl
                .addClass(if tip.real >= tip.expected then 'is-up' else 'is-down')
            else
              tipEl = Templates
                .template(
                  'graphic_value_tip',
                  date:  graph.axis.dates[i].format('{dd} {Mon} ’{yy}'),
                  value: graph.ys[i]
                )
                .css(position: 'fixed')
                .appendTo('body')

            el.data('hovered-tip-el', tipEl)
          else
            tipEl = el.data('hovered-tip-el')

          tipEl?.css(left: e.pageX + 20, top: e.pageY)

      if graph.data.hoverHeader

        gEl = el.parents('@graphic')

        graphNumbers = Templates.template('graphic_header_axis', graph.data).find('@numbers')
        curHeader    = $(gEl.find('@axis-header').get(graph.data.axis))
        curNumbers   = curHeader.find('@numbers')

        unless el.data('hover-origin-numbers')
          el.data('hover-origin-header', curHeader)
          el.data('hover-origin-numbers', curNumbers.clone().wrap('<div>').parent().html())

        curNumbers.replaceWith(graphNumbers.css(color: graph.colors[0]))

  else
    if graph = el.data('hovered-graph')
      restoreColumns(graph)

    el
      .removeData('hovered-graph')
      .unbind('.graphic-hover')
      .data('hovered', -1)
      .data('hovered-tip', -1)
      .data('hovered-tip-el')?.remove()

    if el.data('hover-origin-numbers')
        el.data('hover-origin-header')
          .find('@numbers')
            .replaceWith(el.data('hover-origin-numbers'))
            .end()
          .removeData('hover-origin-header')
          .removeData('hover-origin-numbers')

###
  Internal: mouse leave graphic
###
mouseleavePaper = ->
  mouseenterGraph(undefined, $(@))

###
  Internal: mousemove handler
###
mousemoveOverPaper = (axis, height, offset, e) ->
  x = e.pageX - offset.left - OFFSET_X
  y = e.pageY - offset.top

  return if x < 0

  indexObj = axis.graphs
            .map (g, i) ->
              # TODO: Maybe it should be inversed by default?
              ly = GraphicUtils.inverseValue \
                g.sys[(x / g.step).floor()],
                height
              [ly, i]
            .sortBy(0, true)
            .find((gy) -> y > gy[0])

  index = if indexObj? then indexObj[1] else null
  graph = axis.graphs[index]
  el    = $(e.currentTarget)

  if el.data('hovered') != index
    el.data('hovered', index)
    mouseenterGraph(graph, el)

###
  Internal: bind hover events
###
bindHover = (paper, axis) ->
  el = $(paper.canvas)
  el
    .on(
      'mousemove',
      mousemoveOverPaper
        .bind(null, axis, paper.height, el.offset())
        .throttle(10)
    )
    .on('mouseleave', mouseleavePaper)

###
  Internal:
    build graphics data (scaled values, step by x etc) in format:

      [
        {
          id:     0,
          data:   {...},  // original axis data
          graphs: [
            {
              ys: [...],  // array of values sys: [...], // array of scaled values
              data: {...} // original data from server
            },
            ... // more graphs
          ]
        },
        ... // more axis
      ]
###
buildGraphicsData = (paper, data, axis) ->
  axis.map (a, i) ->
    range = Date.range(data.from, data.to)

    obj =
      id:     i
      data:   a # TODO: Is this really need?
      # Scaled values
      svs:  GraphicUtils.scaleValues(a.line, a.max, paper.height)
      dates: range.every(range.duration() / (data.data[0].values.length - 1))

    obj.graphs =
      data.data.findAll((g) -> g.axis == i).map (g) ->
        # Link to axis
        axis: obj
        # Values from db
        ys: g.values
        # Scale (used in curve graphics)
        scale: a.max / a.line.max()
        # Scaled values
        sys: GraphicUtils.scaleValues(g.values, a.max, paper.height)
        # Step by x
        step: (paper.width - 20) / g.values.length
        # Colors for graphic
        colors: getColorsForGraphic(g)
        # Link to original data from JSON
        data: g # TODO: Is this really need?

    obj

###
  Internal: colorify single axis by index
###
colorifyAxisByIndex = (el, index, color) ->
  $(el.find('@axis-column').get(index))
    .add(el.find('@axis-header @name').get(index))
    .css(color: color)

###
  Internal: set colors to axis
###
colorifyAxis = (axis, el) ->
  if axis.graphs.length == 1
    colorifyAxisByIndex(el, axis.id, axis.graphs[0].colors[0])

  else if axis.graphs.length == 2
    average     = axis.graphs.map('sys').map('average').map('round')
    sum         = average.sum()
    biggerIndex = average.indexOf(average.max())
    lowerIndex  = if biggerIndex == 1 then 0 else 1
    lowerColor  = axis.graphs[lowerIndex].colors[0]

    colorifyAxisByIndex(el, axis.id, axis.graphs[biggerIndex].colors[0])

    scale = (average[lowerIndex] / average.sum()).round(2)

    vs = $(el.find('@axis-column').get(axis.id)).find('@val')
    vs
      .slice(vs.length - (vs.length * scale).ceil())
      .css(color: lowerColor)

    name = $(el.find('@axis-header @name').get(axis.id))
    nameStr    = name.text()
    charsCount = (nameStr.length * scale).ceil()
    nameFirst  = nameStr.first(nameStr.length - charsCount)
    nameLast   = nameStr.last(charsCount)

    name
      .empty()
      .append($('<span>').text(nameFirst))
      .append($('<span>').text(nameLast).css(color: lowerColor))

###
  Internal: render legend
###
renderLegend = (el, axis) ->
  legend = Templates.template \
    'graphic_legend',
    name:   axis.data.obj.legendName,
    graphs: axis.graphs.map (g) ->
              name:  g.data.name
              color: g.colors[0]

  el.append(legend)

###
  Internal:
    render graphics for current screen width.

    What's going on here:
      * Get all scaled values of graphics;
      * render fill columns for all graphics;
      * render graphic lines;
      * render axis lines for all graphics.
###
renderGraphics = (el, data, axis) ->
  paper    = getPaper(el)
  axisData = buildGraphicsData(paper, data, axis)

  axisData.each (a, i) ->
    columns = renderFillColumns(paper, a)

    a.graphs.each (g, i) ->
      g.columns = columns
        .findAll((c) -> c[1] == i)
        .map(0)

    renderGraphicLines(paper, a, axisData)
    renderAxisLines(paper, a)

    colorifyAxis(a, el) if a.data.obj.colorify

    renderLegend(el, a) if a.data.obj.legend

    # NOTE: currently only first axis supported
    if i == 0 and data.underlines == true
      renderUnderlines(paper, axisData)

  if data.map == true
    el.find('@graphic').addClass('is-with-map')
    xhr = $.getJSON(el.data('map-source'))
    xhr.done(buildMap.bind(null, el, el.find('@map'), data, axisData))

  # NOTE: currently supported hover on left axis
  bindHover(paper, axisData[0])

###
  Internal: get timeline
###
getTimeline = (from, to, forceCount) ->
  range = Date.range(from, to)
  # Try to keep 16 values
  range.every(range.duration() / (forceCount || 16))

###
  Internal: get formatted timeline
###
getFormattedTimeline = (from, to, forceCount) ->
  timeline = if from.constructor == Array
    from
  else
    getTimeline(from, to, forceCount)

  timeline.map (date, i) ->
    format = []
    last   = timeline[i - 1]
    next   = timeline[i + 1]

    unless last and date.format('{M}') != last.format('{M}') and
           next and date.format('{M}') != next.format('{M}')
      format.push('{dd}')

    if i == 0 or i == timeline.length - 1 or
       date.format('{M}') != last.format('{M}') or
       (next and date.format('{M}') != next.format('{M}'))
      format.push('{Mon}')

    if i == 0 or i == timeline.length - 1 or
       date.format('{yy}') != last.format('{yy}') or
       (next and date.format('{yy}') != next.format('{yy}'))
      format.push('’{yy}')

    date.format(format.join(' '))

###
  Internal: map position changed
###
mapChanged = (el, e, data) ->
  el
    .prev()
    .find('@period-selector')
      .trigger('updated', [[data.values.min, data.values.max]])

###
  Internal: build graphic map (trends)
###
buildMap = (el, mapEl, data, axisData, mapData) ->
  from  = Date.create(mapData.from)
  to    = Date.create(mapData.to)

  timeline = getFormattedTimeline(from, to)

  Templates
    .template('graphic_axis_time', dates: timeline)
    .find('.graphic-axis-time-table')
    .addClass('is-outside')
    .insertAfter(mapEl)

  mapEl
    .find('@map-container')
    .dateRangeSlider(
      arrows: false
      bounds: min: from, max: to
      defaultValues: min: Date.create(data.from), max: Date.create(data.to)
    )
    .bind('valuesChanged', mapChanged.bind(null, el))

  mapEl
    .find('.ui-rangeSlider-bar')
    .append('<div class="graphic-map-shade is-left"></div><div class="graphic-map-shade is-right"></div>')

  paperEl = mapEl.find('@map-paper')
  paper = new Raphael \
    paperEl.get(0), paperEl.width(), 60

  max  = mapData.values.map('max').max()
  step = paper.width / mapData.values[0].length

  mapData.values.each (vs, i) ->
    sys = GraphicUtils.scaleValues(vs, max, paper.height)
    renderRough(paper, sys: sys, step: step, colors: axisData[0].graphs[i].colors, 0)

###
  Internal: Build graphics element and insert it to DOM

  Example:
    buildGraphics(containerEl, graphicsJSON)
###
buildGraphics = (el, data) ->
  # Build base el
  graphicEl = Templates.template('graphic', map: data.map == true)

  # Get timeline (array of graphic dates)
  timeline = getFormattedTimeline(data.from, data.to, data.forceCount)

  # Get axis
  axis = getAxis(data)

  # Build legend if graphics for left axis more than 2
  # TODO: Build legend

  # Insert table to DOM
  appendHeaders(graphicEl, getHeaderEls(data))
  appendAxis(graphicEl, getAxisEls(data, axis, timeline))
  appendGraphic(el, graphicEl, data.height)

  # Render each graphic
  renderGraphics(el, data, axis)

###
  Graphic.Builder

  Graphic builder: from axis to SVG
###
GraphicBuilder =

  ###
    Public: get JSON for graphic el and start build it on xhr.done
  ###
  fetchAndBuild: (el, id) ->
    xhr = $.getJSON(el.data('source'))
    xhr.done(buildGraphics.bind(null, el))

# Export to global scope
if window?
  window.Graphic ?= {}
  window.Graphic.Builder = GraphicBuilder
else
  module.exports = GraphicBuilder
