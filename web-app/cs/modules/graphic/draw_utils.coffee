GraphicUtils = @?.Graphic?.Utils or require('./utils')

###
  Graphics.DrawUtils

  Helpers for build data for render
###
GraphicDrawUtils =

  ###
    Public: add to array SVG command "move"
  ###
  moveTo: (array, x, y) ->
    array.concat ["M#{x.round()},#{y.round()}"]

  ###
    Public: add to array SVG command "draw line"
  ###
  drawTo: (array, x, y) ->
    array.concat ["L#{x.round()},#{y.round()}"]

  ###
    Public: add color command to array (used by our render)
  ###
  addColor: (array, color) ->
    array.concat [['color', color]]

  ###
    Public: add path command from x1,y1 to x2,y2
  ###
  addPath: (array, x1, y1, x2, y2) ->
    path = @moveTo([], x1, y1)
    path = @drawTo(path, x2, y2)
    array.concat [['path', path]]

  ###
    Public: draw graphic chunk (like inversed russian "г")
  ###
  drawGraphicChunk: (array, y1, y2, colors, step, x) ->
    result = @addColor([], colors[0])
    result = @addPath(result, x, y1, x + step - (if y2? then 1 else 0), y1)

    if y2?
      result = @addColor(result, colors[1])
      result = @addPath(result, x + step, y1, x + step, y2 + (if y1 > y2 then 1 else -1))

    array.concat result

  ###
    Public: build graphic sequence
  ###
  sequence: (ys, options) ->
    result = []

    ys.length.times (index) =>
      if options.fuzzyValues?
        x    = options.fuzzyValues.first(index).sum() * options.step +
               (options.shift || 0)
        step = options.fuzzyValues[index] * options.step
      else
        x    = index * options.step + (options.shift || 0)
        step = options.step

      result = @drawGraphicChunk \
        result,
        ys[index],
        ys[index + 1],
        options.colors,
        step,
        x

    result

  ###
    Public: inverse values in passed SVG paths
  ###
  inverseY: (paths, max) ->
    paths.map (path) ->
      path.replace \
        /(\d+)$/,
        (y) -> GraphicUtils.inverseValue(y, max)

  ###
    Public: build sequence for build fill columns
  ###
  columnSequence: (ys, colors, step, x, id) ->
    sortedYs =
      ys
        .map((y, i) -> [y, colors[i], i])
        .sortBy(0)

    sortedYs
      .map (yc, i) ->
        [
          'rect'
          color: yc[1]
          rect:  [x, (sortedYs[i - 1]?[0] || -1) + 1, x + step, yc[0]].map((v) -> v.round())
          id:    yc[2]
        ]

  ###
    Public: build fill sequence for all graphics
  ###
  fillColumnsSequence: (graphics, colors, axisStep, fuzzyValues, offsetX = 0) ->
    graphics[0]
      .map (g, i) ->
        if fuzzyValues?
          step = axisStep * fuzzyValues[i]
          x = fuzzyValues.first(i).sum() * axisStep + offsetX
        else
          step = axisStep
          x = step * i + offsetX

        GraphicDrawUtils.columnSequence(graphics.map(i), colors, step, x)
      .flatten(1)

# Export to global scope
if window?
  window.Graphic ?= {}
  window.Graphic.DrawUtils = GraphicDrawUtils
else
  module.exports = GraphicDrawUtils
