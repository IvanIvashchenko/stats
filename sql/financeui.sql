-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: financeui
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries_apps_filter`
--

DROP TABLE IF EXISTS `countries_apps_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries_apps_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE36F44DA32C05FA9` (`member_id`),
  CONSTRAINT `FKE36F44DA32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries_apps_filter`
--

LOCK TABLES `countries_apps_filter` WRITE;
/*!40000 ALTER TABLE `countries_apps_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries_apps_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries_geo_filter`
--

DROP TABLE IF EXISTS `countries_geo_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries_geo_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK15EA4E9132C05FA9` (`member_id`),
  CONSTRAINT `FK15EA4E9132C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries_geo_filter`
--

LOCK TABLES `countries_geo_filter` WRITE;
/*!40000 ALTER TABLE `countries_geo_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries_geo_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries_time_filter`
--

DROP TABLE IF EXISTS `countries_time_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries_time_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_time` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `to_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD748B01F32C05FA9` (`member_id`),
  CONSTRAINT `FKD748B01F32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries_time_filter`
--

LOCK TABLES `countries_time_filter` WRITE;
/*!40000 ALTER TABLE `countries_time_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries_time_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries_total_filter`
--

DROP TABLE IF EXISTS `countries_total_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries_total_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `total_filter` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKBAF14C7E32C05FA9` (`member_id`),
  CONSTRAINT `FKBAF14C7E32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries_total_filter`
--

LOCK TABLES `countries_total_filter` WRITE;
/*!40000 ALTER TABLE `countries_total_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries_total_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_compare_column`
--

DROP TABLE IF EXISTS `export_compare_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_compare_column` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compare_column` varchar(255) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4D4433DB32C05FA9` (`member_id`),
  CONSTRAINT `FK4D4433DB32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_compare_column`
--

LOCK TABLES `export_compare_column` WRITE;
/*!40000 ALTER TABLE `export_compare_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_compare_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_filter_country`
--

DROP TABLE IF EXISTS `export_filter_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_filter_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF1F3CDBA32C05FA9` (`member_id`),
  CONSTRAINT `FKF1F3CDBA32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_filter_country`
--

LOCK TABLES `export_filter_country` WRITE;
/*!40000 ALTER TABLE `export_filter_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_filter_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_filter_game`
--

DROP TABLE IF EXISTS `export_filter_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_filter_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5120284E32C05FA9` (`member_id`),
  CONSTRAINT `FK5120284E32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_filter_game`
--

LOCK TABLES `export_filter_game` WRITE;
/*!40000 ALTER TABLE `export_filter_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_filter_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_filter_game_type`
--

DROP TABLE IF EXISTS `export_filter_game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_filter_game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_type_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2F44208B32C05FA9` (`member_id`),
  CONSTRAINT `FK2F44208B32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_filter_game_type`
--

LOCK TABLES `export_filter_game_type` WRITE;
/*!40000 ALTER TABLE `export_filter_game_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_filter_game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_filter_resolution`
--

DROP TABLE IF EXISTS `export_filter_resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_filter_resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1FF9A00832C05FA9` (`member_id`),
  CONSTRAINT `FK1FF9A00832C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_filter_resolution`
--

LOCK TABLES `export_filter_resolution` WRITE;
/*!40000 ALTER TABLE `export_filter_resolution` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_filter_resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_filter_store`
--

DROP TABLE IF EXISTS `export_filter_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_filter_store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD396A80532C05FA9` (`member_id`),
  CONSTRAINT `FKD396A80532C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_filter_store`
--

LOCK TABLES `export_filter_store` WRITE;
/*!40000 ALTER TABLE `export_filter_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_filter_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_setting`
--

DROP TABLE IF EXISTS `export_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_timestamp` bigint(20) DEFAULT NULL,
  `if_need_downloads` bit(1) DEFAULT NULL,
  `if_need_rankings` bit(1) DEFAULT NULL,
  `if_need_refunds` bit(1) DEFAULT NULL,
  `if_need_refunds_monetized` bit(1) DEFAULT NULL,
  `if_need_revenues` bit(1) DEFAULT NULL,
  `if_need_total` bit(1) DEFAULT NULL,
  `if_need_updates` bit(1) DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  `step` varchar(255) DEFAULT NULL,
  `to_timestamp` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKD172D4C532C05FA9` (`member_id`),
  CONSTRAINT `FKD172D4C532C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_setting`
--

LOCK TABLES `export_setting` WRITE;
/*!40000 ALTER TABLE `export_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `export_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_setting`
--

DROP TABLE IF EXISTS `finance_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_time` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `to_time` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKCC9A8AEB32C05FA9` (`member_id`),
  CONSTRAINT `FKCC9A8AEB32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_setting`
--

LOCK TABLES `finance_setting` WRITE;
/*!40000 ALTER TABLE `finance_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_stores_filter`
--

DROP TABLE IF EXISTS `finance_stores_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_stores_filter` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK81ECD50032C05FA9` (`member_id`),
  CONSTRAINT `FK81ECD50032C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_stores_filter`
--

LOCK TABLES `finance_stores_filter` WRITE;
/*!40000 ALTER TABLE `finance_stores_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_stores_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_subscription`
--

DROP TABLE IF EXISTS `mail_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_subscription` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `everyday_downloads` bit(1) NOT NULL,
  `everyday_rating` bit(1) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  `weekly` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8C1DFEA532C05FA9` (`member_id`),
  CONSTRAINT `FK8C1DFEA532C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_subscription`
--

LOCK TABLES `mail_subscription` WRITE;
/*!40000 ALTER TABLE `mail_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `account_expired` bit(1) NOT NULL,
  `account_locked` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `is_password_encoded` bit(1) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_expired` bit(1) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` (`id`, `version`, `account_expired`, `account_locked`, `email`, `enabled`, `is_password_encoded`, `password`, `password_expired`, `username`) VALUES (1,0,'\0','\0','kirilld@zeptolab.com','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','kirill'),(2,0,'\0','\0','annie.tarasenko@7bits.it','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','annie'),(3,0,'\0','\0','konstantin.kolotyuk@7bits.it','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','konstantin'),(4,0,'\0','\0','ilya.zakharov@7bits.it','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','ilya'),(5,0,'\0','\0','ivan.ivashchenko@7bits.it','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','ivan'),(6,0,'\0','\0','olga.martyn@7bits.it','','','5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8','\0','olga');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_role`
--

DROP TABLE IF EXISTS `member_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_role` (
  `role_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`role_id`,`member_id`),
  KEY `FK527E3EFB32C05FA9` (`member_id`),
  KEY `FK527E3EFBADAF81E9` (`role_id`),
  CONSTRAINT `FK527E3EFBADAF81E9` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK527E3EFB32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_role`
--

LOCK TABLES `member_role` WRITE;
/*!40000 ALTER TABLE `member_role` DISABLE KEYS */;
INSERT INTO `member_role` (`role_id`, `member_id`) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6);
/*!40000 ALTER TABLE `member_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `authority` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `authority` (`authority`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `version`, `authority`) VALUES (1,0,'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_filter_country`
--

DROP TABLE IF EXISTS `timeline_filter_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_filter_country` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA4C3418D32C05FA9` (`member_id`),
  CONSTRAINT `FKA4C3418D32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_filter_country`
--

LOCK TABLES `timeline_filter_country` WRITE;
/*!40000 ALTER TABLE `timeline_filter_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_filter_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_filter_game`
--

DROP TABLE IF EXISTS `timeline_filter_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_filter_game` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA6BA7D5B32C05FA9` (`member_id`),
  CONSTRAINT `FKA6BA7D5B32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_filter_game`
--

LOCK TABLES `timeline_filter_game` WRITE;
/*!40000 ALTER TABLE `timeline_filter_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_filter_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_filter_game_type`
--

DROP TABLE IF EXISTS `timeline_filter_game_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_filter_game_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_type_id` bigint(20) NOT NULL,
  `member_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6C05EB9E32C05FA9` (`member_id`),
  CONSTRAINT `FK6C05EB9E32C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_filter_game_type`
--

LOCK TABLES `timeline_filter_game_type` WRITE;
/*!40000 ALTER TABLE `timeline_filter_game_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_filter_game_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_filter_resolution`
--

DROP TABLE IF EXISTS `timeline_filter_resolution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_filter_resolution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `resolution_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7B71375532C05FA9` (`member_id`),
  CONSTRAINT `FK7B71375532C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_filter_resolution`
--

LOCK TABLES `timeline_filter_resolution` WRITE;
/*!40000 ALTER TABLE `timeline_filter_resolution` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_filter_resolution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_filter_store`
--

DROP TABLE IF EXISTS `timeline_filter_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_filter_store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) NOT NULL,
  `store_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3146F49832C05FA9` (`member_id`),
  CONSTRAINT `FK3146F49832C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_filter_store`
--

LOCK TABLES `timeline_filter_store` WRITE;
/*!40000 ALTER TABLE `timeline_filter_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_filter_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeline_setting`
--

DROP TABLE IF EXISTS `timeline_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timeline_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compare_column` varchar(255) DEFAULT NULL,
  `from_timestamp` bigint(20) DEFAULT NULL,
  `if_need_apps_compare` bit(1) DEFAULT NULL,
  `if_need_apps_downloads` bit(1) DEFAULT NULL,
  `if_need_apps_revenue` bit(1) DEFAULT NULL,
  `if_need_inapps_compare` bit(1) DEFAULT NULL,
  `if_need_inapps_downloads` bit(1) DEFAULT NULL,
  `if_need_inapps_revenue` bit(1) DEFAULT NULL,
  `if_need_updates` bit(1) DEFAULT NULL,
  `if_need_updates_compare` bit(1) DEFAULT NULL,
  `member_id` bigint(20) NOT NULL,
  `step` varchar(255) DEFAULT NULL,
  `to_timestamp` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK2073B85232C05FA9` (`member_id`),
  CONSTRAINT `FK2073B85232C05FA9` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeline_setting`
--

LOCK TABLES `timeline_setting` WRITE;
/*!40000 ALTER TABLE `timeline_setting` DISABLE KEYS */;
/*!40000 ALTER TABLE `timeline_setting` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-02-12 11:37:11
