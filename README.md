
**********************************************
Пошаговая инструкция для поднятия приложения *
**********************************************

1. Установка Grails

Для того,что бы установить Grails необходимо скачать с официального сайта и распакавать архив с требуемой версией http://grails.org/Download. (2.0.4)
Затем нужно настроить соответствующую переменную окружения. Для этого в файл .profile добавить следующие строки:

export GRAILS_HOME=/путь-к-распакованному-архиву/
export PATH=$PATH:$GRAILS_HOME/bin

Перед установкой необходимо убедиться, что установлена JDK и определена переменная окружения JAVA_HOME! (Sun SE JDK >1.6)

Подробную инструкцию можно найти на официальном сайте http://grails.org/doc/latest/guide/gettingStarted.html#requirements


2. Установка jRuby (для поддержки Compass Framework)

    1. нужно удалить системные ruby, jruby (могут быть какие нибудь ruby1.8 или ruby1.9.1 - удалить лучше все через apt)
    2. установка rvm:
    curl -L https://get.rvm.io | bash

    (Ubuntu) Terminal:
    Edit -> ProfilePreferences -> Tab "Title and Command" -> set "On" in checkbox "Run command as login shell"

    3. rvm install jruby
    4. перелогиниться в шелл, выполнить rvm --default jruby
    5. перейти в папку с проектом
    6. rvm --rvmrc --create jruby@myproject
    7. перейти на папку выше
    8. заново перейти в папку с проектом, на предложение использовать rvm конфиг ответить yes

После этого в консоли можно проверить правильность установки: jruby -v


3. Установка Compass

gem install compass --pre (нужно чтобы установилась версия не ниже 0.13.alpha.0)

Более подробная информация http://compass-style.org/install/


4. Создание и настройка MySql (Версия MySql - 5.5) базы данных

в файле /etc/mysql/my.cnf:
[mysqld]
#Table size
tmp_table_size=2048M
max_heap_table_size=2048M

из командной строки:
mysql -u username -p password
mysql> CREATE DATABASE financeui
            DEFAULT CHARACTER SET utf8
            COLLATE utf8_general_ci
        ;
mysql> CREATE DATABASE financeui_test
            DEFAULT CHARACTER SET utf8
            COLLATE utf8_general_ci
        ;
mysql> CREATE DATABASE zeptostats_financeui_test
            DEFAULT CHARACTER SET utf8
            COLLATE utf8_general_ci
        ;



Основные конфигурационный файлы:
grails-app/conf/BuildConfig.groovy
grails-app/conf/DataSource.groovy

Подробнее в официальной документации http://grails.org/doc/latest/guide/conf.html#3.3 The DataSource


5. log-файлы:
5.1. В файле grails-app/conf/Config.groovy в разделе log4j перечислены используемые log-файлы.
Они должны быть созданы.

Например, можно их создать:
> touch /var/log/zeptostats-financeUI.log
> touch /var/log/zeptostats-financeUI-debug.log
> sudo chmod 777 /var/log/zeptostats-financeUI.log
> sudo chmod 777 /var/log/zeptostats-financeUI-debug.log

5.2. Должен быть создан файл stacktrace.log

> touch /var/lib/tomcat7/stacktrace.log

5.3. Должны быть права на папку tomcat7:

> sudo chown tomcat7:tomcat7 /var/lib/tomcat7 -R



************************************
 6 Приложение                      *
************************************

************************************
 6.1 Обычный запуск PRODUCTION     *
************************************
Генерируем war файл и выкладываем
> rm target/ROOT.war
> jruby -S compass watch --sass-dir web-app/sass --css-dir web-app/css --output-style compact --images-dir web-app/images --relative-assets
> grails war
> cp target/zeptostats-0.1.war target/ROOT.war

> sudo /etc/init.d/tomcat7 stop
> sudo rm -R /var/lib/tomcat7/webapps/ROOT/ /var/lib/tomcat7/webapps/ROOT.war
> sudo cp target/ROOT.war /var/lib/tomcat7/webapps/
> sudo /etc/init.d/tomcat7 start


************************************
 6.2 Обычный запуск DEVELOPMENT    *
************************************

> grails run-app

или, например, можно поменять порт:
> grails run-app -Dserver.port=8090

или запустить в нужном окружении:
> grails [env]* run-app

Чтобы остановить Ctrl-C. Если не помогло, то:

> touch .kill-run-app

Если не помогло можно использовать также (Ubuntu):
> ps aux | grep grails

Ответ будет примерно таким:

1000 4002 10.7 10.4 1332704 845384 pts/1 Sl+ 11:43 2:11 /usr/local/java/jdk1.6.0_35/bin/java -server -Xmx768M -Xms768M -XX:PermSize=256m -XX:MaxPermSize=256m -Dfile.encoding=UTF-8 -javaagent:/opt/grails-2.0.4/lib/com.springsource.springloaded/springloaded-core/jars/springloaded-core-1.0.5.jar -noverify -Dspringloaded=profile=grails -classpath /opt/grails-2.0.4/lib/org.codehaus.groovy/groovy-all/jars/groovy-all-1.8.6.jar:/opt/grails-2.0.4/dist/grails-bootstrap-2.0.4.jar -Dgrails.home=/opt/grails-2.0.4 -Dtools.jar=/usr/local/java/jdk1.6.0_35/lib/tools.jar org.codehaus.groovy.grails.cli.support.GrailsStarter --main org.codehaus.groovy.grails.cli.GrailsScriptRunner --conf /opt/grails-2.0.4/conf/groovy-starter.conf --classpath run-app
1000 5662 0.0 0.0 5632 824 pts/2 S+ 12:03 0:00 grep grails

далее, в зависимости отответа, используйте:
> kill -9 4002


************************************
 6.3 TESTING                       *
************************************

Простой запуск
> grails test-app

Запуск только подвида тестов
> grails test-app unit:
> grails test-app integration:
> grails test-app :spock

Запуск некоторых тестов с именами
> grails test-app Foo Bar

Если вы хотите перезапустить только упавшие тесты, то добавьте -rerun флаг:
> grails test-app -rerun



************************************
 7 Основные использованные плагины *
************************************

1. Compass Framework - SASS/SCSS support (
   http://grails.org/plugin/compass-sass,
   https://github.com/stefankendall/compass-sass
   http://compass-style.org/help/tutorials/configuration-reference/
)
2. Haml plugin for Grails, based on JHaml (http://grails.org/plugin/haml)
3. CoffeeScript Resources (http://grails.org/plugin/coffeescript-resources)
4. Spring Security Core Plugin (http://grails.org/plugin/spring-security-core)
5. Resources (http://grails.org/plugin/resources)
6. (Testing) Spock Plugin (
    http://grails.org/plugin/spock
    http://code.google.com/p/spock/
)
7. Routing (http://grails.org/plugin/routing)

************************************
Справочная информация
************************************
Как настроить удаленный доступ к mysql базе данных

1) ssh на сервер, где лежит база.

2) sudo vim /etc/mysql/my.cnf
Закомментировать:
skip-external-locking
bind-address 127.0.0.1
3) mysql -uroot -p
GRANT ALL PRIVILEGES ON *.* TO 'имя'@'%' IDENTIFIED BY 'пароль' WITH GRANT OPTION;

4) sudo service mysql restart

5) Now that our user has been granted access from any host, all thats left is to make sure our OS will allow connections to the default MySQL port
/sbin/iptables -A INPUT -i eth0 -p tcp --destination-port 3306 -j ACCEPT

6) Теперь mysql -h сервер -uимя -pпароль должно сработать