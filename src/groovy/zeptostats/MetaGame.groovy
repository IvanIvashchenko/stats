package zeptostats

class MetaGame {

    private String iconPath;
    private Long id;
    private String name;
    private List<Game> apps;

    public MetaGame() {}

    public MetaGame(String iconPath, Long id, String name, List<Game> apps) {

        this.iconPath = iconPath;
        this.id= id;
        this.name = name;
        this.apps = apps;
    }
}