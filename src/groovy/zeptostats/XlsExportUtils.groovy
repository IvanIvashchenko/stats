package zeptostats;

import jxl.*;
import jxl.write.*;

class XlsExportUtils {

    static def createWorkbook(OutputStream out) {

        def workbook = Workbook.createWorkbook(out);
    }

    static def addSheetToWorkbook(workbook, sheetName, index) {
        workbook.createSheet(sheetName, index);
    }

    static def addLastRowToSheet(sheet, columns) {

        def rowNumber = sheet.getRows();
        columns.eachWithIndex() { col, i ->

            if (col instanceof java.lang.Number) {
                sheet.addCell(new Number(i, rowNumber, col))
            } else {
                sheet.addCell(new Label(i, rowNumber, col.toString()))
            }
        };
        sheet;
    }

    static def addBoldColumnToSheetRow(sheet, column, columnIndex, rowNumber, width) {

        sheet.setColumnView(columnIndex, width);
        def cell = new Label(columnIndex, rowNumber, column);

        WritableFont bold = new WritableFont(WritableFont.ARIAL, WritableFont.DEFAULT_POINT_SIZE, WritableFont.BOLD);
        WritableCellFormat cf = new WritableCellFormat(bold);
        cell.setCellFormat(cf);

        sheet.addCell(cell);
        sheet;
    }

    static def addColumnToSheetRow(sheet, column, columnIndex, rowNumber, width) {

        sheet.setColumnView(columnIndex, width);
        if (column instanceof java.lang.Number) {
            def cell = new Number(columnIndex, rowNumber, column);
            sheet.addCell(cell);
        } else {
            def cell = new Label(columnIndex, rowNumber, column.toString());
            sheet.addCell(cell);
        }
        sheet;
    }

    static def addEmtyCellsToSheetRow(sheet, rowNumber, firstEmptyColIndex, cellsCount) {

        def columnIndex = firstEmptyColIndex;

         while(columnIndex < cellsCount) {
            def cell = new Label(columnIndex, rowNumber, "");
            columnIndex++;
            sheet.addCell(cell);
        }

        sheet;
    }
}