package zeptostats

class Store {

    String logo;
    String name;
    List<Game> apps;

    public Store() {}

    public Store(String logo, String name, List<Game> apps) {
        this.logo = logo;
        this.name = name;
        this.apps = apps; 
    }
}