package zeptostats

class Game {

    private Map<String, Integer> downloads;
    private String resolution;
    private Long id;
    private Integer total;
    private String icon;


    public Game() {}

    public Game(Map<String, Integer> downloads, String resolution, Long id, Integer total, String icon) {
        this.id = id;
        this.downloads = downloads;
        this.resolution = resolution;
        this.total = total;
        this.icon = icon;
    }
}