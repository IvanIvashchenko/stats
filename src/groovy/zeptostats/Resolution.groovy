package zeptostats;

/**
 * Enum for game's Resolution.
 * 
 * Resolution can be 'HD' or 'SD' or 'DESKTOP'.
 */
public enum Resolution {

    /**
     * 'HD' Resolution.
     */
    HD('HD'),

    /**
     * 'SD' Resolution.
     */
    SD('SD'),

    /**
     * 'DESKTOP' Resolution.
     */
    DESKTOP('Desktop')


    final String value;

    Resolution(final String value) {
        this.value = value
    }

    String toHumanString() {
        this.value
    }
}
