package zeptostats;

/**
 * Enum for game's type.
 * 
 * GameType can be 'FREE' or 'PAID'.
 */
public enum GameType {

    /**
     * FREE Game type.
     */
    FREE('Free'),

    /**
     * PAID Game type.
     */
    PAID('Paid')


    final String value;

    GameType(final String value) {
        this.value = value
    }

    String toHumanString() {
        this.value
    }
}
