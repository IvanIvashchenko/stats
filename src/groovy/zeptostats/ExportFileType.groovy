package zeptostats

enum ExportFileType {

    CSV("csv", "csv", "csv"),
    XLS("xls", "xls", "excel"),
    TSV("tsv", "tsv", "tsv"),
    GZIP("gzip", "gz", "gzip")

    String code;
    String extension;
    String mimeType;

    ExportFileType(final String code, final String extension, final String mimeType) {

        this.code = code;
        this.extension = extension;
        this.mimeType = mimeType;
    }
}