<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Welcome to Grails</title>
    <meta content='main' name='layout' />
  </head>
  <body>
    <div class='tabs-holder'>
      <div class='tabs is-period-tabs' role='tabs'>
        <a class='tab' data-id='daily' href='#' role='tab'>
          Daily
        </a>
        <a class='tab is-current' data-id='weekly' href='#' role='tab'>
          Weekly
        </a>
        <a class='tab' data-id='monthly' href='#' role='tab'>
          Monthly
        </a>
      </div>
    </div>
    <div class='content-header is-graphic-header is-first is-with-tabs'>
      <h2 class='content-title'>
        Downloads for in different countries
      </h2>
      <div class='content-header-selectors'>
        <a class='content-header-selector' href='#' role='open-popup'>
          <span class='content-header-selector-label inner-link' role='text'>for all apps</span>
          <span class='content-header-selector-icon is-list'>
            <span class='content-header-selector-img'></span>
          </span>
        </a>
        <div class='popup' role='popup'>
          <div class='popup-tri'></div>
          <ul class='popup-list is-striked' role='popup-selector apps-selector'>
            <li class='popup-item is-active' role='item'>Cut The Rope SD</li>
            <li class='popup-item is-active' role='item'>Cut The Rope HD</li>
            <li class='popup-item is-active' role='item'>CTR: Experiments SD</li>
            <li class='popup-item' role='item'>CTR: Experiments HD</li>
            <li class='popup-item is-active' role='item'>CTR: Comics SD</li>
            <li class='popup-item' role='item'>CTR: Comics HD</li>
            <li class='popup-item' role='item'>CTR: Holiday Gift SD</li>
            <li class='popup-item is-active' role='item'>CTR: Holiday Gift HD</li>
          </ul>
        </div>
        <a class='content-header-selector' data-from='2012-04-10' data-to='2012-06-25' href='#' role='period-selector'>
          <span class='content-header-selector-label inner-link' role='text'>from 10 Apr to 25 Jun</span>
          <span class='content-header-selector-icon is-cal'>
            <span class='content-header-selector-img'></span>
          </span>
        </a>
      </div>
      <div class='content-header-options'>
        <a class='inner-link settings-link' href='#' role='open-settings '>
          Settings
        </a>
        <div class='export-links'>
          <span class='export-links-title'>Export</span>
          <a class='export-link' href='#'>XLS</a>
          <a class='export-link' href='#'>CSV</a>
        </div>
      </div>
    </div>
    <div class='graphic-container' data-id='downloads_for_apps' data-map-source='/js/data/trends_map.js' data-source='/js/data/trends.js' role='graphic-container'></div>
    <div class='graphic-container' data-id='downloads_for_apps' data-map-source='/js/data/trends_map.js' data-source='/js/data/rough_trends.js' role='graphic-container'></div>
    <div class='settings-window is-overlay-over' role='settings' style='display:none;'>
      <div class='settings-tri'></div>
      <div class='settings-content'>
        <div class='timeline-settings-tip'>
          <span class='timeline-settings-tip-wrapper'>
            Mark candy to see comparasion by this parameter
          </span>
        </div>
        <ul class='timeline-settings' role='axis-choose'>
          <li class='timeline-setting-column is-versions'>
            <div class='timeline-setting-column-title'>
              <a class='candy is-with-text is-radio is-active' data-id='versions' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Versions
                </div>
              </a>
            </div>
            <ul class='timeline-setting-tree'>
              <li class='timeline-setting is-switcher' role='setting switcher'>Free</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Paid</li>
            </ul>
          </li>
          <li class='timeline-setting-column is-style'>
            <div class='timeline-setting-column-title'>
              <a class='candy is-with-text is-radio' data-id='style' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Style
                </div>
              </a>
            </div>
            <ul class='timeline-setting-tree'>
              <li class='timeline-setting is-switcher' role='setting switcher'>SD</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>HD</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Desktop</li>
            </ul>
          </li>
          <li class='timeline-setting-column is-regions-and-countries'>
            <div class='timeline-setting-column-title'>
              <a class='candy is-with-text is-radio' data-id='regions' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Regions &
                </div>
              </a>
              <a class='candy is-with-text is-radio' data-id='countries' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Countries
                </div>
              </a>
            </div>
            <ul class='timeline-setting-tree'>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>Europa</div>
              </li>
              <li class='timeline-setting is-subtree is-open' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>Asia</div>
                <ul class='timeline-setting-subtree' role='subtree'>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>China</span>
                  </li>
                  <li class='timeline-setting is-switcher is-striked' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Thailand</span>
                  </li>
                  <li class='timeline-setting is-switcher is-striked' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Indonesia</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>India</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Phillipines</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>China</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Thailand</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Indonesia</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>India</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Phillipines</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>China</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Thailand</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Indonesia</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>India</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Phillipines</span>
                  </li>
                </ul>
              </li>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>USA</div>
              </li>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>Canada</div>
              </li>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>South America</div>
              </li>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>Africa</div>
              </li>
            </ul>
          </li>
          <li class='timeline-setting-column is-os-and-stores'>
            <div class='timeline-setting-column-title'>
              <a class='candy is-with-text is-radio' data-id='OS' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  OS &
                </div>
              </a>
              <a class='candy is-with-text is-radio' data-id='stores' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Stores
                </div>
              </a>
            </div>
            <ul class='timeline-setting-tree'>
              <li class='timeline-setting is-subtree is-open' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>iOS</div>
                <ul class='timeline-setting-subtree' role='subtree'>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Apple</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Chillingo</span>
                  </li>
                </ul>
              </li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Mac</li>
              <li class='timeline-setting is-subtree' role='tree'>
                <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
                <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>Android</div>
                <ul class='timeline-setting-subtree' role='subtree'>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Google Play</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Amazon</span>
                  </li>
                  <li class='timeline-setting is-switcher' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>Nook</span>
                  </li>
                </ul>
              </li>
              <li class='timeline-setting is-switcher' role='setting'>
                Blackberry
              </li>
            </ul>
          </li>
          <li class='timeline-setting-column is-apps-and-inapps'>
            <div class='timeline-setting-column-title'>
              <a class='candy is-with-text is-radio' data-id='apps &amp; inapps' href='#' role='candy'>
                <div class='candy-text' role='candy-text'>
                  Apps & Inapps
                </div>
              </a>
            </div>
            <ul class='timeline-setting-tree'>
              <li class='timeline-setting is-switcher' role='setting switcher'>Cut The Rope</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>CTR: Experiments</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>CTR: Comics</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>CTR: Holiday Gift</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Inapp 1</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Inapp 2</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Inapp 3</li>
              <li class='timeline-setting is-switcher' role='setting switcher'>Inapp 4</li>
            </ul>
          </li>
        </ul>
        <ul class='timeline-settings-footer' role='display'>
          <li class='timeline-settings-row'>
            <span class='timeline-row-setting is-disable' role='option'>
              <label>
                <input name='' role='checkbox' type='checkbox' value='' />
                Downloads and
              </label>
              <span class='timeline-row-setting is-disable' role='option'>
                <label>
                  <input disabled='disabled' name='' role='checkbox' type='checkbox' value='' />
                  revenues for all apps (except Getjar, Nook and Ovi Store):
                </label>
                <span class='timeline-row-setting is-disable' role='option'>
                  compared by
                  <a class='candy is-with-text' href='#' role='candy compared-by-candy'>
                    versions.
                  </a>
                </span>
              </span>
            </span>
          </li>
          <li class='timeline-settings-row'>
            <span class='timeline-row-setting' role='option'>
              <label>
                <input checked='checked' name='' role='checkbox' type='checkbox' value='' />
                Downloads and
              </label>
              <span class='timeline-row-setting' role='option'>
                <label>
                  <input checked='checked' name='' role='checkbox' type='checkbox' value='' />
                  revenues for all inapps
                </label>
                <span class='timeline-row-setting is-disable' role='option'>
                  compared by
                  <a class='candy is-with-text' href='#' role='candy compared-by-candy'>
                    versions.
                  </a>
                </span>
              </span>
            </span>
          </li>
          <li class='timeline-settings-row'>
            <span class='timeline-row-setting' role='option'>
              <label>
                <input checked='checked' name='' role='checkbox' type='checkbox' value='' />
                Download for update:
              </label>
              <span class='timeline-row-setting is-disable' role='option'>
                compared by
                <a class='candy is-with-text' href='#' role='candy compared-by-candy'>
                  versions.
                </a>
              </span>
            </span>
          </li>
          <li class='timeline-row-buttons'>
            <button class='button is-white' role='save button'>Save</button>
          </li>
        </ul>
      </div>
    </div>
  </body>
</html>