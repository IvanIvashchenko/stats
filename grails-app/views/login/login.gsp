<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY login.haml --%>
<html>
  <head>
    <title>Login</title>
    <meta content='main' name='layout' />
  </head>
  <body>
    <div class='login' role='login'>
      <div class='login-logo'></div>
      <form class='login-form'>
        <fieldset class='login-form-fields'>
          <label class='login-form-label'>
            <span class='login-form-label-name'>
              Login
            </span>
            <input class='login-form-input' />
          </label>
          <label class='login-form-label'>
            <span class='login-form-label-name'>
              Password
            </span>
            <input class='login-form-input' type='password' />
          </label>
          <input class='login-form-submit' type='submit' value='Login' />
        </fieldset>
      </form>
    </div>
  </body>
</html>