<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY auth.haml --%>
<html>
  <head>
    <title>Login</title>
    <meta content='main' name='layout' />
  </head>
  <body>
    <div class='login' role='login'>
      <div class='login-logo'></div>
      <g:if test='${flash.message}'>
        <div class='login_message'>${flash.message}</div>
      </g:if>
      <form action='${postUrl}' class='login-form' method='POST'>
        <fieldset class='login-form-fields'>
          <label class='login-form-label'>
            <span class='login-form-label-name'>
              Login
            </span>
            <input class='login-form-input' name='j_username' />
          </label>
          <label class='login-form-label'>
            <span class='login-form-label-name'>
              Password
            </span>
            <input class='login-form-input' name='j_password' type='password' />
          </label>
          <input class='login-form-submit' type='submit' value='Login' />
        </fieldset>
      </form>
    </div>
  </body>
</html>