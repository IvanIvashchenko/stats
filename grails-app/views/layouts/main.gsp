<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY main.haml --%>
<!DOCTYPE html>
<html>
  <head>
    <r:require module='css'></r:require>
    <g:javascript src='vendor/jquery-1.8.1.js'></g:javascript>
    <g:javascript src='vendor/jquery-ui-1.8.23.custom.min.js'></g:javascript>
    <g:javascript src='vendor/jQRangeSlider-min-4.2.js'></g:javascript>
    <g:javascript src='vendor/jQDateRangeSlider-min-4.2.js'></g:javascript>
    <g:javascript src='vendor/jquery.backstretch.min.js'></g:javascript>
    <g:javascript src='vendor/jquery.jkey.js'></g:javascript>
    <g:javascript src='vendor/sugar.js'></g:javascript>
    <g:javascript src='vendor/underscore.js'></g:javascript>
    <g:javascript src='vendor/jquery.role.js'></g:javascript>
    <g:javascript src='vendor/raphael.js'></g:javascript>
    <g:javascript src='vendor/g.raphael-min-0.51.js'></g:javascript>
    <g:javascript src='vendor/g.line-min-0.51.js'></g:javascript>
    <g:javascript src='vendor/datepicker.js'></g:javascript>
    <g:javascript src='vendor/spin.js'></g:javascript>
    <g:javascript src='vendor/jquery.spin.js'></g:javascript>
    <g:javascript src='vendor/color-0.4.1.js'></g:javascript>
    <r:layoutResources></r:layoutResources>
  </head>
  <body>
    <div class='header' role='header'>
      <g:render model="${[current: pageProperty(name: 'page.currentMenuItem')]}" template='/layouts/menu'></g:render>
    </div>
    <div class='content' role='countries timeline'>
      <g:layoutBody />
    </div>
    <div class='overlay' role='overlay' style='display:none;'></div>
    <g:render model='${[:]}' template='/layouts/templates'></g:render>
    <r:require module='coffee'></r:require>
    <r:layoutResources></r:layoutResources>
  </body>
</html>