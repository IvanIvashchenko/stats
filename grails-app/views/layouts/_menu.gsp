<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _menu.haml --%>
<div class='header-container'>
  <a class='header-logo' href='/export' role='logo'>
    <img alt='zepto' height='31px' src='/images/ui/header-logo.png' width='66px' />
  </a>
  <div class='header-links'>
    <g:if test='${current == "finances"}'>
      <a class='header-tab is-current' href='/finances'>finances</a>
    </g:if>
    <g:else>
      <a class='header-tab' href='/finances'>finances</a>
    </g:else>
  </div>
  <div class='header-links is-right'>
    <g:if test='${current == "reports"}'>
      <a class='header-tab is-current' href='/reports'>mail reports</a>
    </g:if>
    <g:else>
      <a class='header-tab' href='/reports'>mail reports</a>
    </g:else>
    <g:if test='${current == "export"}'>
      <a class='header-tab is-current' href='/export'>data export</a>
    </g:if>
    <g:else>
      <a class='header-tab' href='/export'>data export</a>
    </g:else>
    <a class='header-logout' href='/logout'></a>
  </div>
</div>