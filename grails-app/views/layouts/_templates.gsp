<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _templates.haml --%>
<div class='templates' role='templates'>
  <script data-id='graphic' role='template' type='text/x-js-template'>
    <div class='graphic' role='graphic'>
      <div class='graphic-header' role='header'></div>
      <div class='graphic-axis-container' role='axis'></div>
      <div class='graphic-paper' role='paper'></div>
    </div>
    {{ if (map) { }}
    <div class='graphic-map' role='map'>
      <div class='graphic-map-paper-container' role='map-container'>
        <div class='graphic-map-paper' role='map-paper'></div>
      </div>
    </div>
    {{ }; }}
  </script>
  <script data-id='graphic_header_axis' role='template' type='text/x-js-template'>
    <div class='graphic-header-axis' role='axis-header'>
      <div class='graphic-axis-label' role='name'>{{= name }}</div>
      <ul class='graphic-axis-numbers' role='numbers'>
        {{ if (typeof total != 'undefined') { }}
        <li class='graphic-axis-number' role='number'>
          <span class='graphic-axis-number-big'>
            {{ if (typeof measure != 'undefined') { }}{{= measure }}{{ }; }}{{= formatNumber(total) }}
          </span>
          <span class='graphic-axis-number-small'>total</span>
        </li>
        {{ }; }}
        {{ if (typeof per != 'undefined') { }}
        <li class='graphic-axis-number' role='number'>
          <span class='graphic-axis-number-big'>
            {{ if (typeof measure != 'undefined') { }}{{= measure }}{{ }; }}{{= formatNumber(per.value) }} {{- growArrows(per.grow) }}
          </span>
          <span class='graphic-axis-number-small'>per day</span>
        </li>
        {{ }; }}
        {{ if (typeof expected != 'undefined') { }}
        <li class='graphic-axis-number' role='number'>
          {{ if (expected > paid) { }}
          <span class='graphic-axis-number-big-outer is-up-green'>
            <span class='graphic-axis-number-big'>{{= formatNumber(expected) }}</span>
            <span class='graphic-axis-number-small'>expected, k$</span>
          </span>
          {{ } else { }}
          <span class='graphic-axis-number-big'>{{= formatNumber(expected) }}</span>
          <span class='graphic-axis-number-small'>expected, k$</span>
          {{ }; }}
        </li>
        {{ }; }}
        {{ if (typeof paid != 'undefined') { }}
        <li class='graphic-axis-number' role='number'>
          <span class='graphic-axis-number-big'>{{= formatNumber(paid) }}</span>
          <span class='graphic-axis-number-small'>paid, k$</span>
        </li>
        {{ }; }}
        {{ if (typeof delta != 'undefined') { }}
        <li class='graphic-axis-number' role='number'>
          {{ if (delta > 0) { }}
          <span class='graphic-axis-number-big-outer is-up'>
            <span class='graphic-axis-number-big'>
              {{= formatNumber(delta) }}
            </span>
            <span class='graphic-axis-number-small'>Δ, k$</span>
          </span>
          {{ } else { }}
          <span class='graphic-axis-number-big-outer is-down'>
            <span class='graphic-axis-number-big'>
              {{= formatNumber(delta) }}
            </span>
            <span class='graphic-axis-number-small'>Δ, k$</span>
          </span>
          {{ }; }}
        </li>
        {{ }; }}
      </ul>
    </div>
  </script>
  <script data-id='graphic_axis' role='template' type='text/x-js-template'>
    <table class='graphic-axis-side' role='axis-column'>
      <tbody>
        {{ line.reverse().each(function(num) { }}
        <tr>
          <td class='graphic-axis-side-val'>
            <span class='graphic-axis-side-val-container' role='val'>
              {{= num }}
            </span>
          </td>
        </tr>
        {{ }); }}
        <tr class='graphic-axis-side-val-zero-row'>
          <td class='graphic-axis-side-val'>
            <div class='graphic-axis-side-val-zero' role='val'>0</div>
          </td>
        </tr>
      </tbody>
    </table>
  </script>
  <script data-id='graphic_axis_time' role='template' type='text/x-js-template'>
    <div class='graphic-axis-time'>
      <table class='graphic-axis-time-table'>
        <tbody>
          <tr>
            {{ _(dates).each(function(date) { }}
            <td class='graphic-axis-time-val'>
              {{= date }}
            </td>
            {{ }); }}
          </tr>
        </tbody>
      </table>
    </div>
  </script>
  <% def dollar = '$' %>
  <script data-id='graphic_value_tip' role='template' type='text/x-js-template'>
    <ul class='graphic-tip'>
      <li class='graphic-tip-row is-header'>{{= date }}</li>
      <li class='graphic-tip-row'>${dollar}{{= formatNumber(value) }}</li>
    </ul>
  </script>
  <script data-id='graphic_finances_tip' role='template' type='text/x-js-template'>
    <ul class='graphic-tip'>
      <li class='graphic-tip-row is-header'>{{= date }}</li>
      <li class='graphic-tip-row'>${dollar}{{= formatNumber(expected) }}</li>
      <li class='graphic-tip-row is-real'>${dollar}{{= formatNumber(real) }}</li>
    </ul>
  </script>
  <script data-id='graphic_legend' role='template' type='text/x-js-template'>
    <ul class='graphic-legend'>
      <li class='graphic-legend-row is-header'>{{= name }}</li>
      {{ graphs.each(function (g) { }}
      <li class='graphic-legend-row'>
        <span class="graphic-legend-icon" style="background: {{= g.color }};">
          <span class="graphic-legend-icon-overlay"></span>
        </span>
        <span class='graphic-legend-name'>{{= g.name }}</span>
      </li>
      {{ }); }}
    </ul>
  </script>
</div>