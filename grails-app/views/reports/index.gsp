<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Mail Subscriptions</title>
    <meta content='main' name='layout' />
    <parameter name='currentMenuItem' value='reports'>
      <div class='content-header is-first is-subscriptions'>
        <h2 class='content-title'>
          Mail subscriptions
        </h2>
      </div>
      <form class='subscribtions' role='subscriptions'>
        <ul class='subscriptions-inputs'>
          <li class='subscriptions-row'>
            <label>
              <g:if test='${subscritions.weekly}'>
                <input checked='checked' name='subscriptions' role='weekly' type='checkbox' />
              </g:if>
              <g:else>
                <input name='subscriptions' role='weekly' type='checkbox' />
              </g:else>
              Weekly overal report (еженедельная рассылка по общему положению дел)
            </label>
          </li>
          <li class='subscriptions-row'>
            <label>
              <g:if test='${subscritions.everyday_rating}'>
                <input checked='checked' name='subscriptions' role='everydayRating' type='checkbox' />
              </g:if>
              <g:else>
                <input name='subscriptions' role='everydayRating' type='checkbox' />
              </g:else>
              Everyday country rating report (ежедневная рассылка по странам для Лены)
            </label>
          </li>
          <li class='subscriptions-row'>
            <label>
              <g:if test='${subscritions.everyday_downloads}'>
                <input checked='checked' name='subscriptions' role='everydayDownloads' type='checkbox' />
              </g:if>
              <g:else>
                <input name='subscriptions' role='everydayDownloads' type='checkbox' />
              </g:else>
              Everyday downloads report (ежедневная рассылка по загрузкам)
            </label>
          </li>
          <li class='subscriptions-row is-submit'>
            <button class='button' role='save button' type='submit'>Save</button>
          </li>
        </ul>
      </form>
    </parameter>
  </head>
</html>