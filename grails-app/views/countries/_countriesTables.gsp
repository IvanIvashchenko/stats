<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _countriesTables.haml --%>
<div class='store-tables-right-fade'></div>
<g:if test='${renderAppStore}'>
  <g:render model='${[logo: appstoreLogo, topAppsCharacteristics: appstoreAppsCharacteristics, topAppsDownloads: appstoreAppsDownloads,metaAppsCharacteristics: appstoreMetaAppsCharacteristics, lastPositions: appstoreLastPositions, appsTotal: appstoreAppsTotal,zeptostatsHost: zeptostatsHost]}' template='/countries/storeTable'></g:render>
</g:if>
<g:if test='${renderGooglePlay}'>
  <g:render model='${[metaAppsCharacteristics: googleMetaAppsCharacteristics, logo: googleStoreLogo, topAppsCharacteristics: googleAppsCharacteristics, topAppsDownloads: googleAppsDownloads, lastPositions: googleLastPositions, appsTotal: googleAppsTotal,zeptostatsHost: zeptostatsHost]}' template='/countries/storeTable'></g:render>
</g:if>
<g:if test='${renderOthers}'>
  <g:render model='${[otherAppsCharacteristics: otherAppsCharacteristics,otherAppsStoreCharacteristics: otherAppsStoreCharacteristics,otherLastPositions: otherLastPositions,otherAppsDownloads: otherAppsDownloads,otherAppsTotal: otherAppsTotals,zeptostatsHost: zeptostatsHost]}' template='/countries/otherStores'></g:render>
</g:if>
<g:if test='${!renderData}'>
  There is no avaliable data to display.
</g:if>