<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _filter.haml --%>
<div class='settings-window is-overlay-over' role='settings countries-settings' style='display:none;'>
  <div class='settings-tri'></div>
  <div class='settings-content'>
    <div class='tabs-holder'>
      <div class='tabs' role='tabs'>
        <a class='tab is-current' data-id='countries' href='#' role='tab'>
          Countries
        </a>
        <a class='tab' data-id='apps' href='#' role='tab'>
          Apps
        </a>
      </div>
    </div>
    <div class='settings-columns' data-id='countries' role='settings-page'>
      <g:each in='${countriesFilter}'>
        <div class='settings-column' role='settings-column'>
          <h3 class='settings-column-title' role='settings-tilte'>${it.key}</h3>
          <ul class='settings-list'>
            <g:each in='${it.value}' var='country'>
              <li class='settings-options'>
                <g:if test='${savedCountries.contains(country.id)}'>
                  <a class='settings-option is-active' data-id='${country.id}' href='#' role='option'>${country.name}</a>
                </g:if>
                <g:else>
                  <a class='settings-option' data-id='${country.id}' href='#' role='option'>${country.name}</a>
                </g:else>
              </li>
            </g:each>
          </ul>
        </div>
      </g:each>
      <div class='settings-column' role='settings-column'>
        <h3 class='settings-column-title' role='settings-tilte'>Total</h3>
        <ul class='settings-list' role='radio-group'>
          <li class='settings-options'>
            <g:if test="${total=='choosen'}">
              <a class='settings-option total-option is-active' data-total='choosen' data-type='radio' href='#' role='option'>Choosen Countries</a>
            </g:if>
            <g:else>
              <a class='settings-option total-option' data-total='choosen' data-type='radio' href='#' role='option'>Choosen Countries</a>
            </g:else>
          </li>
          <li class='settings-options'>
            <g:if test="${total=='all'}">
              <a class='settings-option total-option is-active' data-total='all' data-type='radio' href='#' role='option'>All Countries</a>
            </g:if>
            <g:else>
              <a class='settings-option total-option' data-total='all' data-type='radio' href='#' role='option'>All Countries</a>
            </g:else>
          </li>
        </ul>
      </div>
    </div>
    <div class='settings-columns is-apps' data-id='apps' role='settings-page' style='display:none;'>
      <g:each in='${appsFilter}'>
        <div class='settings-column' role='settings-column'>
          <h3 class='settings-column-title' role='settings-tilte'>${it.key}</h3>
          <ul class='settings-list'>
            <g:each in='${it.value}' var='storeApps'>
              <li class='settings-options' role='options-tree'>
                <a class='settings-option' href='#' role='option'>${storeApps.key.replaceAll("PAID", "")}</a>
                <ul class='settings-list'>
                  <g:each in='${storeApps.value}' var='app'>
                    <li class='settings-options'>
                      <g:if test='${savedApps.contains(app.id)}'>
                        <a class='settings-option is-active game-item' data-id='${app.id}' href='#' role='option'>${app.resolution} ${app.extra}</a>
                      </g:if>
                      <g:else>
                        <a class='settings-option game-item' data-id='${app.id}' href='#' role='option'>${app.resolution} ${app.extra}</a>
                      </g:else>
                    </li>
                  </g:each>
                </ul>
              </li>
            </g:each>
          </ul>
        </div>
      </g:each>
    </div>
  </div>
</div>