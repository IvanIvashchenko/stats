<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _storeTable.haml --%>
<table class='store-table'>
  <thead class='store-table-head'>
    <tr class='store-table-games'>
      <th class='store-table-logo' rowspan='2'>
        <div class='store-table-logo-container'>
          <div class='store-table-logo-inner-container'>
            <img class='store-table-logo-img' src='${logo}' />
          </div>
        </div>
      </th>
      <g:each in='${metaAppsCharacteristics}'>
        <th class='store-table-game' colspan='${it.count}'>
          <div class='store-table-game-logo'>
            <img alt='${it.name}' class='store-table-game-img' src='${zeptostatsHost}${it.path}${it.name}' />
          </div>
          <div class='store-table-game-underline'></div>
        </th>
      </g:each>
    </tr>
    <tr class='store-table-devices'>
      <g:each in='${topAppsCharacteristics}' status='i' var='game'>
        <g:if test='${lastPositions.contains(i.toLong())}'>
          <th class='store-table-device is-last'>
            ${game.resolution}
            <span class='store-table-upper-text'>${game.extra}</span>
          </th>
        </g:if>
        <g:else>
          <th class='store-table-device'>
            ${game.resolution}
            <span class='store-table-upper-text'>${game.extra}</span>
          </th>
        </g:else>
      </g:each>
    </tr>
  </thead>
  <tbody class='store-table-body'>
    <tr class='store-table-country-delimeter'>
      <td colspan='${topAppsCharacteristics.size()}'>
        <tr class='store-table-country'></tr>
      </td>
    </tr>
    <g:each in='${topAppsDownloads}' var='country'>
      <tr class='store-table-country flag-background-${country.value.first().code.toLowerCase()}'>
        <td class='store-table-country-name'>
          <div class='store-table-country-flag flag-${country.value.first().code.toLowerCase()}'></div>
          ${country.value.first().cname}
        </td>
        <g:each in='${country.value}' status='i' var='item'>
          <g:if test='${lastPositions.contains(i.toLong())}'>
            <td class='store-table-stat with-flag-background is-last'>
              <span class='store-table-stat-downloads'>${item.downloads ? item.downloads : 0}</span>
              <span class='store-table-stat-position'>19</span>
            </td>
          </g:if>
          <g:else>
            <td class='store-table-stat with-flag-background'>
              <span class='store-table-stat-downloads'>${item.downloads ? item.downloads : 0}</span>
              <span class='store-table-stat-position'>19</span>
            </td>
          </g:else>
        </g:each>
      </tr>
    </g:each>
  </tbody>
  <tfoot class='store-table-total'>
    <td class='store-table-total-title'>Total</td>
    <g:each in='${appsTotal}' status='i' var='item'>
      <g:if test='${lastPositions.contains(i.toLong())}'>
        <td class='store-table-total-col is-last'>
          <div class='store-table-total-num'>${item.downloads ? item.downloads : 0}</div>
        </td>
      </g:if>
      <g:else>
        <td class='store-table-total-col'>
          <div class='store-table-total-num'>${item.downloads ? item.downloads : 0}</div>
        </td>
      </g:else>
    </g:each>
  </tfoot>
</table>