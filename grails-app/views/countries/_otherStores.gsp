<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _otherStores.haml --%>
<table class='store-table'>
  <thead class='store-table-head'>
    <tr class='store-table-other-stores'>
      <th class='store-table-logo' rowspan='2'>
        <div class='store-table-logo-container'>
          <div class='store-table-logo-text'>
            Other
            <br />
            stores
          </div>
        </div>
      </th>
      <g:each in='${otherAppsStoreCharacteristics}'>
        <th class='store-table-other-store' colspan='${it.count}'>
          <div class='store-table-other-store-logo'>
            <img alt='${it.name}' class='store-table-other-store-img' src='${zeptostatsHost}${it.path}${it.name}' />
          </div>
        </th>
      </g:each>
    </tr>
    <tr class='store-table-other-games'>
      <g:each in='${otherAppsCharacteristics}' status='i' var='game'>
        <g:if test='${otherLastPositions.contains(i.toLong())}'>
          <th class='store-table-other-game is-last'>
            <img src='${zeptostatsHost}${game.path}${game.name}' />
            <span class='store-table-upper-text'>${game.resolution}</span>
          </th>
        </g:if>
        <g:else>
          <th class='store-table-other-game'>
            <img src='${zeptostatsHost}${game.path}${game.name}' />
            <span class='store-table-upper-text'>${game.resolution}</span>
          </th>
        </g:else>
      </g:each>
    </tr>
  </thead>
  <tbody class='store-table-body'>
    <tr class='store-table-country-delimeter'>
      <td colspan='${otherAppsCharacteristics.size()}'>
        <tr class='store-table-country'></tr>
      </td>
    </tr>
    <g:each in='${otherAppsDownloads}' status='i' var='item'>
      <tr class='store-table-country flag-background-${item.value.first().code.toLowerCase()}'>
        <td class='store-table-country-name'>
          <div class='store-table-country-flag flag-${item.value.first().code.toLowerCase()}'></div>
          ${item.value.first().cname}
        </td>
        <g:each in='${item.value}' status='j' var='country'>
          <g:if test='${otherLastPositions.contains(j.toLong())}'>
            <td class='store-table-stat with-flag-background is-last'>
              <span class='store-table-stat-downloads'>${country.downloads  ? country.downloads : 0}</span>
            </td>
          </g:if>
          <g:else>
            <td class='store-table-stat with-flag-background'>
              <span class='store-table-stat-downloads'>${country.downloads ? country.downloads : 0}</span>
            </td>
          </g:else>
        </g:each>
      </tr>
    </g:each>
  </tbody>
  <tfoot class='store-table-total'>
    <td class='store-table-total-title'>Total</td>
    <g:each in='${otherAppsTotal}' status='i' var='item'>
      <g:if test='${otherLastPositions.contains(i.toLong())}'>
        <td class='store-table-total-col is-last'>
          <div class='store-table-total-num'>${item.downloads ? item.downloads : 0}</div>
        </td>
      </g:if>
      <g:else>
        <td class='store-table-total-col'>
          <div class='store-table-total-num'>${item.downloads ? item.downloads : 0}</div>
        </td>
      </g:else>
    </g:each>
  </tfoot>
</table>