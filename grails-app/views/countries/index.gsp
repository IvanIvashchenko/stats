<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Countries</title>
    <meta content='main' name='layout' />
    <parameter name='currentMenuItem' value='countries'></parameter>
  </head>
  <body>
    <div class='content-header is-first'>
      <h2 class='content-title'>
        Downloads and ratings by country
      </h2>
      <div class='content-header-selectors'>
        <div class='content-header-selector'>
          <a class='content-header-selector-link is-calendar-link is-prev' data-change='prev' href='#' role='period-selector-change-day'></a>
          <a class='content-header-selector-link' data-can-be-single='true' data-from='${formattedDateFrom}' data-to='${formattedDateTo}' href='#' role='period-selector countries-period-selector'>
            <span class='content-header-selector-label inner-link' role='text'>${formattedFromToString}</span>
            <span class='content-header-selector-icon is-cal'>
              <span class='content-header-selector-img'></span>
            </span>
          </a>
          <a class='content-header-selector-link is-calendar-link is-next' data-change='next' href='#' role='period-selector-change-day'></a>
        </div>
      </div>
      <div class='content-header-options'>
        <!-- / Remove .is-overlay-over to regular state -->
        <a class='inner-link settings-link' href='#' role='open-settings'>
          Settings
        </a>
        <div class='export-links'>
          <span class='export-links-title'>Export</span>
          <a class='export-link' href='${createLink(action: "exportXls", controller: "countries")}'>XLS</a>
          <a class='export-link' href='${createLink(action: "exportCsv", controller: "countries")}'>CSV</a>
        </div>
      </div>
    </div>
    <div role='countries-tables'>
      <div class='store-tables-right-fade'></div>
      <g:render model='${tablesData}' template='/countries/countriesTables'></g:render>
    </div>
    <g:render model='${[countriesFilter: countriesFilter, appsFilter: appsFilter, savedApps: savedApps, savedCountries: savedCountries, total: total]}' template='/countries/filter'></g:render>
  </body>
</html>