<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Finances</title>
    <meta content='main' name='layout' />
    <parameter name='currentMenuItem' value='finances'></parameter>
  </head>
  <body>
    <div class='content-header is-graphic-header is-first' role='finances'>
      <h2 class='content-title'>
        <span class='content-title-part is-expected-revenue'>Expected revenue</span>
        and
        <span class='content-title-part is-data'>store data</span>
        <a class='content-title-selector' href='#' role='open-popup finances-selected-stores'>
          <span class='content-title-selector-label' role='text'>
            <g:if test='${strikedStores}'>
              for selected stores
            </g:if>
            <g:else>
              for all stores
            </g:else>
          </span>
          <span class='content-title-selector-icon is-list'>
            <span class='content-title-selector-img'></span>
          </span>
        </a>
        <div class='popup' role='popup'>
          <div class='popup-tri'></div>
          <table class='popup-table'>
            <thead class='popup-table-head'>
              <tr>
                <td class='popup-table-cell'>Δ by store, k$</td>
                <td class='popup-table-cell'>Apr '12</td>
              </tr>
            </thead>
            <tbody class='popup-table-body' role='popup-selector stores-selector finances-stores-selector'>
              <g:each in='${storesFilter}'>
                <tr class='${strikedStores?.contains(it.id) ? "" : "is-active"} popup-table-row' data-id='${it.id}' role='item'>
                  <td class='popup-table-cell'>${it.name}</td>
                  <td>+0.01</td>
                </tr>
              </g:each>
            </tbody>
          </table>
        </div>
      </h2>
      <div class='content-header-selectors is-finances-selectors'>
        <a class='content-header-selector' data-from='${formattedDateFrom}' data-to='${formattedDateTo}' href='#' role='period-selector finances-period-selector'>
          <span class='content-header-selector-label inner-link' role='text'>${formattedFromToString}</span>
          <span class='content-header-selector-icon is-cal'>
            <span class='content-header-selector-img'></span>
          </span>
        </a>
        <div class='popup'></div>
      </div>
      <div class='content-header-options'>
        <div class='export-links'>
          <span class='export-links-title'>Export</span>
          <a class='export-link' href='#'>XLS</a>
          <a class='export-link' href='#'>CSV</a>
          <a class='export-link is-statement' href='#'>Incoming statemnet</a>
        </div>
      </div>
    </div>
    <div class='graphs-container'>
      <g:render template='/finances/graphs'></g:render>
    </div>
  </body>
</html>