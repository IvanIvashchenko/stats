<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Timeline</title>
    <meta content='main' name='layout' />
    <parameter name='currentMenuItem' value='timeline'></parameter>
  </head>
  <body>
    <div class='graphs'>
      <g:render model='${[filterData: filterData, savedSettings: savedSettings]}' template='/timeline/graphs'></g:render>
    </div>
    <g:render model='${[filterData: filterData, savedSettings: savedSettings]}' template='/timeline/settings'></g:render>
  </body>
</html>