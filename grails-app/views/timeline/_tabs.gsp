<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _tabs.haml --%>
<%@page import="zeptostats.Step" %>
<div class='tabs-holder'>
  <div class='tabs is-period-tabs' role='tabs ${tabsRole}'>
    <g:if test='${Step.DAILY.equals(needStep)}'>
      <a class='tab is-current' data-id='${Step.DAILY}' href='#' role='tab'>
        ${Step.DAILY.title}
      </a>
    </g:if>
    <g:else>
      <a class='tab' data-id='${Step.DAILY}' href='#' role='tab'>
        ${Step.DAILY.title}
      </a>
    </g:else>
    <g:if test='${Step.WEEKLY.equals(needStep)}'>
      <a class='tab is-current' data-id='${Step.WEEKLY}' href='#' role='tab'>
        ${Step.WEEKLY.title}
      </a>
    </g:if>
    <g:else>
      <a class='tab' data-id='${Step.WEEKLY}' href='#' role='tab'>
        ${Step.WEEKLY.title}
      </a>
    </g:else>
    <g:if test='${Step.MONTHLY.equals(needStep)}'>
      <a class='tab is-current' data-id='${Step.MONTHLY}' href='#' role='tab'>
        ${Step.MONTHLY.title}
      </a>
    </g:if>
    <g:else>
      <a class='tab' data-id='${Step.MONTHLY}' href='#' role='tab'>
        ${Step.MONTHLY.title}
      </a>
    </g:else>
  </div>
  <div class='tabs bookmarks' data-role='${tabsRole}' role='tabs ${tabsRole} bookmarks'>
    <a class='tab' href='#' role='open-popup open-bookmarks-popup'>
      Bookmarks
    </a>
    <div class='popup' role='popup open-bookmark-list'>
      <div class='popup-tri'></div>
      <table class='popup-table'>
        <tbody class='popup-table-body' role='popup-selector bookmark-list'>
          <g:each in='${bookmarks}'>
            <tr class='popup-table-row' data-id='${it.id}' role='bookmark-row'>
              <td class='popup-table-cell' role='bookmark-item'>${it.name}</td>
              <td class='delete-bookmark-img' role='bookmark-delete'></td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </div>
    <a class='tab' href='#' role='open-popup'>
      +
    </a>
    <div class='popup' role='popup save-bookmark'>
      <div class='popup-tri save-bookmark'></div>
      <table class='popup-table'>
        <tbody class='popup-table-body' role='popup-selector'>
          <tr class='popup-table-row' role='item'>
            <td class='popup-table-cell'>
              <input placeholder='Save new bookmark as...' role='bookmark-new-name' size='20' type='text' />
              <div class='save-bookmark-error' role='save-bookmark-error'>Name of bookmark should not be empty and should be unique</div>
            </td>
            <td>
              <input role='save-button' type='button' value='Save' />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>