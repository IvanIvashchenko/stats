<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _graphs.haml --%>
<g:render model='${[needStep: savedSettings.step, tabsRole: "timeline-tabs"]}' template='/timeline/tabs'></g:render>
<div class='content-header is-graphic-header is-first is-with-tabs'>
  <h2 class='content-title'>
    Downloads for
    <a class='content-title-part is-free' href='#' role='open-popup'>free</a>
    <div class='popup' role='popup'>
      <div class='popup-tri'></div>
      <ul class='popup-list is-striked' role='popup-selector timeline-popup-selector'>
        <li class='popup-item is-active' role='item'>Cut The Rope SD</li>
        <li class='popup-item is-active' role='item'>Cut The Rope HD</li>
        <li class='popup-item is-active' role='item'>CTR: Experiments SD</li>
        <li class='popup-item' role='item'>CTR: Experiments HD</li>
        <li class='popup-item is-active' role='item'>CTR: Comics SD</li>
        <li class='popup-item' role='item'>CTR: Comics HD</li>
        <li class='popup-item' role='item'>CTR: Holiday Gift SD</li>
        <li class='popup-item is-active' role='item'>CTR: Holiday Gift HD</li>
      </ul>
    </div>
    and
    <a class='content-title-part is-paid' href='#' role='open-popup'>paid</a>
    <div class='popup' role='popup'>
      <div class='popup-tri'></div>
      <ul class='popup-list is-striked' role='popup-selector timeline-popup-selector'>
        <li class='popup-item is-active' role='item'>Cut The Rope SD</li>
        <li class='popup-item is-active' role='item'>Cut The Rope HD</li>
        <li class='popup-item is-active' role='item'>CTR: Experiments SD</li>
        <li class='popup-item' role='item'>CTR: Experiments HD</li>
        <li class='popup-item is-active' role='item'>CTR: Comics SD</li>
        <li class='popup-item' role='item'>CTR: Comics HD</li>
        <li class='popup-item' role='item'>CTR: Holiday Gift SD</li>
        <li class='popup-item is-active' role='item'>CTR: Holiday Gift HD</li>
      </ul>
    </div>
    apps
    and
    <span class='content-title-part is-revenue'>total revenue</span>
  </h2>
  <div class='content-header-selectors'>
    <a class='content-header-selector' href='#' role='open-popup'>
      <span class='content-header-selector-label inner-link' role='text'>for all countries</span>
      <span class='content-header-selector-icon is-list'>
        <span class='content-header-selector-img'></span>
      </span>
    </a>
    <div class='popup' role='popup'>
      <div class='popup-tri'></div>
      <ul class='popup-list is-striked' role='popup-selector countries-selector timeline-popup-selector'>
        <li class='popup-item is-active' role='item'>UK</li>
        <li class='popup-item is-active' role='item'>Germany</li>
        <li class='popup-item is-active' role='item'>USA</li>
        <li class='popup-item' role='item'>Russia</li>
        <li class='popup-item is-active' role='item'>Czech</li>
        <li class='popup-item' role='item'>Austria</li>
        <li class='popup-item' role='item'>China</li>
        <li class='popup-item is-active' role='item'>Indonesia</li>
      </ul>
    </div>
    <a class='content-header-selector' data-from='${filterData.formattedDateFrom}' data-to='${filterData.formattedDateTo}' href='#' role='period-selector timeline-period-selector'>
      <span class='content-header-selector-label inner-link' role='text'>${filterData.formattedFromToString}</span>
      <span class='content-header-selector-icon is-cal'>
        <span class='content-header-selector-img'></span>
      </span>
    </a>
  </div>
  <div class='content-header-options'>
    <a class='inner-link settings-link' href='#' role='open-settings '>
      Settings
    </a>
    <div class='export-links'>
      <span class='export-links-title'>Export</span>
      <a class='export-link' href='#'>XLS</a>
      <a class='export-link' href='#'>CSV</a>
    </div>
  </div>
</div>
<div class='graphic-container' data-id='downloads_for_apps' data-source='/js/data/downloads_for_apps.js' role='graphic-container'></div>
<div class='content-header is-graphic-header'>
  <h2 class='content-title'>Inapps downloads and total revenue</h2>
</div>
<div class='graphic-container' data-id='downloads_for_apps' data-source='/js/data/inapps_downloads.js' role='graphic-container'></div>
<div class='content-header is-graphic-header'>
  <h2 class='content-title'>Updates</h2>
</div>
<div class='graphic-container' data-id='downloads_for_apps' data-source='/js/data/updates.js' role='graphic-container'></div>