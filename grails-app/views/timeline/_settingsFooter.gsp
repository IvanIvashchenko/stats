<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _settingsFooter.haml --%>
<ul class='timeline-settings-footer' role='display'>
  <li class='timeline-settings-row' role='optionGroup'>
    <span class='${savedSettings.ifNeedAppsDownloads ? "is-active" : ""} timeline-row-setting' role='option'>
      <label>
        <g:if test='${savedSettings.ifNeedAppsDownloads}'>
          <input checked='checked' role='checkbox apps-downloads' type='checkbox' />
        </g:if>
        <g:else>
          <input role='checkbox apps-downloads' type='checkbox' />
        </g:else>
        Downloads and
      </label>
    </span>
    <span class='${savedSettings.ifNeedAppsRevenue ? "is-active" : ""} timeline-row-setting' role='option'>
      <label>
        <g:if test='${savedSettings.ifNeedAppsRevenue}'>
          <input checked='checked' name='' role='checkbox apps-revenue' type='checkbox' value='' />
        </g:if>
        <g:else>
          <input name='' role='checkbox apps-revenue' type='checkbox' value='' />
        </g:else>
        revenues for all apps:
      </label>
    </span>
    <span class='${savedSettings.ifNeedAppsCompare ? "is-active" : ""} timeline-row-setting' role='option'>
      compared by
      <a class='${savedSettings.ifNeedAppsCompare ? "is-active" : ""} candy is-with-text' href='#' role='candy compared-by-candy apps-candy'>
        ${savedSettings.compareColumn?.title}
      </a>
    </span>
  </li>
  <li class='timeline-settings-row' role='optionGroup'>
    <span class='${savedSettings.ifNeedInappsDownloads ? "is-active" : ""} timeline-row-setting' role='option'>
      <label>
        <g:if test='${savedSettings.ifNeedInappsDownloads}'>
          <input checked='checked' name='' role='checkbox inapps-downloads' type='checkbox' value='' />
        </g:if>
        <g:else>
          <input name='' role='checkbox inapps-downloads' type='checkbox' value='' />
        </g:else>
        Downloads and
      </label>
    </span>
    <span class='${savedSettings.ifNeedInappsRevenue ? "is-active" : ""} timeline-row-setting' role='option'>
      <label>
        <g:if test='${savedSettings.ifNeedInappsRevenue}'>
          <input checked='checked' name='' role='checkbox inapps-revenue' type='checkbox' value='' />
        </g:if>
        <g:else>
          <input name='' role='checkbox inapps-revenue' type='checkbox' value='' />
        </g:else>
        revenues for all inapps
      </label>
    </span>
    <span class='${savedSettings.ifNeedInappsCompare ? "is-active" : ""} timeline-row-setting' role='option'>
      compared by
      <a class='${savedSettings.ifNeedInappsCompare ? "is-active" : ""} candy is-with-text' href='#' role='candy compared-by-candy inapps-candy'>
        ${savedSettings.compareColumn?.title}
      </a>
    </span>
  </li>
  <li class='timeline-settings-row' role='optionGroup'>
    <span class='${savedSettings.ifNeedUpdates ? "is-active" : ""} timeline-row-setting' role='option'>
      <label>
        <g:if test='${savedSettings.ifNeedUpdates}'>
          <input checked='checked' name='' role='checkbox updates' type='checkbox' value='' />
        </g:if>
        <g:else>
          <input name='' role='checkbox updates' type='checkbox' value='' />
        </g:else>
        Download for update:
      </label>
    </span>
    <span class='${savedSettings.ifNeedUpdatesCompare ? "is-active" : ""} timeline-row-setting' role='option'>
      compared by
      <a class='${savedSettings.ifNeedUpdatesCompare ? "is-active" : ""} candy is-with-text' href='#' role='candy compared-by-candy updates-candy'>
        ${savedSettings.compareColumn?.title}
      </a>
    </span>
  </li>
  <li class='timeline-row-buttons'>
    <button class='button is-white' role='save button timeline-settings-save'>Save</button>
  </li>
</ul>