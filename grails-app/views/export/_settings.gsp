<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY _settings.haml --%>
<%@page import="zeptostats.CompareColumn" %>
<%@page import="zeptostats.CompareColumnDependence" %>
<%@page import="zeptostats.Step" %>
<div class='export-settings'>
  <div class='export-settings-container'>
    <div class='timeline-settings-tip'>
      <span class='timeline-settings-tip-wrapper'>
        Mark noone or one or several candies to see comparison by this parameters
      </span>
    </div>
    <ul class='timeline-settings' role='axis-choose'>
      <li class='timeline-setting-column is-versions' data-filter='${CompareColumnDependence.VERSION.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.VERSION.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.VERSION}' data-title='${CompareColumn.VERSION.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.VERSION.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.VERSION.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.VERSION.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.gameTypes}' var='gameType'>
            <% def isStriked = savedSettings.unnecessaryGameTypeIds?.contains(gameType) %>
            <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${gameType}' data-strike-state='${isStriked ? "is-striked" : ""}' role='setting switcher'>${gameType}</li>
          </g:each>
        </ul>
      </li>
      <li class='timeline-setting-column is-style' data-filter='${CompareColumnDependence.STYLE.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.STYLE.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.STYLE}' data-title='${CompareColumn.STYLE.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.STYLE.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.STYLE.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.STYLE.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.resolutions}' var='resolution'>
            <% isStriked = savedSettings.unnecessaryResolutionIds?.contains(resolution) %>
            <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${resolution}' data-strike-state='${isStriked ? "is-striked" : ""}' role='setting switcher'>${resolution}</li>
          </g:each>
        </ul>
      </li>
      <li class='timeline-setting-column is-regions-and-countries' data-filter='${CompareColumnDependence.COUNTRY.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.REGION.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.REGION}' data-title='${CompareColumn.REGION.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.REGION.title}
            </div>
          </a>
          &
          <a class='${CompareColumn.COUNTRY.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.COUNTRY}' data-title='${CompareColumn.COUNTRY.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.COUNTRY.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.COUNTRY.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.COUNTRY.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.countries}' var='regionCountries'>
            <li class='timeline-setting is-subtree' role='tree'>
              <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
              <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>${regionCountries.value.first().regionName}</div>
              <ul class='timeline-setting-subtree' role='subtree'>
                <g:each in='${regionCountries.value}' var='country'>
                  <% isStriked = savedSettings.unnecessaryCountryIds?.contains(country.id) %>
                  <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${country.id}' data-strike-state='${isStriked ? "is-striked" : ""}' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>${country.name}</span>
                  </li>
                </g:each>
              </ul>
            </li>
          </g:each>
        </ul>
      </li>
      <li class='timeline-setting-column is-os-and-stores' data-filter='${CompareColumnDependence.STORE.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.OS.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.OS}' data-title='${CompareColumn.OS.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.OS.title}
            </div>
          </a>
          &
          <a class='${CompareColumn.STORE.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.STORE}' data-title='${CompareColumn.STORE.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.STORE.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.STORE.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.STORE.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.stores}' var='osStores'>
            <li class='timeline-setting is-subtree is-open' role='tree'>
              <a class='timeline-setting-subtree-link' href='#' role='open-subtree'></a>
              <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>${osStores.value.first().platformName}</div>
              <ul class='timeline-setting-subtree' role='subtree'>
                <g:each in='${osStores.value}' var='store'>
                  <% isStriked = savedSettings.unnecessaryStoreIds?.contains(store.id) %>
                  <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${store.id}' data-strike-state='${isStriked ? "is-striked" : ""}' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher'>${store.name}</span>
                  </li>
                </g:each>
              </ul>
            </li>
          </g:each>
        </ul>
      </li>
      <li class='timeline-setting-column is-required-product-types' data-filter='${CompareColumnDependence.REQUIRED_PRODUCT_TYPE.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.REQUIRED_PRODUCT_TYPE.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.REQUIRED_PRODUCT_TYPE}' data-title='${CompareColumn.REQUIRED_PRODUCT_TYPE.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.REQUIRED_PRODUCT_TYPE.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.REQUIRED_PRODUCT_TYPE.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.REQUIRED_PRODUCT_TYPE.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.requiredProductTypes}' var='requiredProductType'>
            <% isStriked = savedSettings.unnecessaryRequiredProductTypeIds?.contains(requiredProductType) %>
            <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${requiredProductType}' data-strike-state='${isStriked ? "is-striked" : ""}' role='setting switcher'>${requiredProductType}</li>
          </g:each>
        </ul>
      </li>
      <li class='timeline-setting-column is-metaskus-and-skus' data-filter='${CompareColumnDependence.SKU.filterColumn}' role='filter'>
        <div class='timeline-setting-column-title' role='candy-group'>
          <a class='${CompareColumn.METASKU.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-id='${CompareColumn.METASKU}' data-title='${CompareColumn.METASKU.title}' href='#' role='candy'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.METASKU.title}
              &
            </div>
          </a>
          <a class='${CompareColumn.SKU.isIn(savedSettings.compareColumns) ? "is-active" : ""} candy is-with-text is-checkbox' data-available-comparissons='${CompareColumn.SKU.getJustAvailableCompareColumnsJsonStr()}' data-id='${CompareColumn.SKU}' data-title='${CompareColumn.SKU.title}' href='#' role='candy candy-state-dependent'>
            <div class='candy-text' role='candy-text'>
              ${CompareColumn.SKU.title}
            </div>
          </a>
        </div>
        <ul class='timeline-setting-tree' data-dependent-filters-exists='${CompareColumnDependence.SKU.hasDependentFilter()}' data-top-parent-filter='${CompareColumnDependence.SKU.getTopParentFilterColumn()}' role='whole-tree'>
          <g:each in='${filterData.games}' var='gameGroup'>
            <li class='timeline-setting is-switcher' role='tree'>
              <div class='timeline-setting-subtree-title-wrapper' role='tree-switcher'>${gameGroup.value.first().metaSkuName}</div>
              <ul class='timeline-setting-subtree' role='subtree'>
                <g:each in='${gameGroup.value}' var='game'>
                  <% isStriked = savedSettings.unnecessaryGameIds?.contains(game.id) %>
                  <li class='${isStriked ? "is-striked" : ""} timeline-setting is-switcher' data-id='${game.id}' data-strike-state='${isStriked ? "is-striked" : ""}' hidden='true' role='setting'>
                    <span class='timeline-setting-subtree-wrapper' role='switcher' title='${game.fullName}'>${game.cutName}</span>
                  </li>
                </g:each>
              </ul>
            </li>
          </g:each>
        </ul>
      </li>
    </ul>
  </div>
  <ul class='timeline-settings-footer' role='display display-settings'>
    <li class='timeline-settings-row' role='optionGroup'>
      <span class='${savedSettings.ifNeedDownloads ? "is-active" : ""} timeline-row-setting' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedDownloads}'>
            <input checked='checked' name='' role='checkbox downloads' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox downloads' type='checkbox' value='' />
          </g:else>
          Downloads and
        </label>
      </span>
      <span class='${savedSettings.ifNeedRevenues ? "is-active" : ""} timeline-row-setting' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedRevenues}'>
            <input checked='checked' name='' role='checkbox revenue' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox revenue' type='checkbox' value='' />
          </g:else>
          revenues for all apps and inapps
        </label>
      </span>
      <span class='timeline-row-setting export-modifiers' role='option'>
        <label>
          Modifiers:
        </label>
      </span>
    </li>
    <li class='timeline-settings-row' role='optionGroup'>
      <span class='${!savedSettings.ifNeedUpdates ? "is-active" : ""} timeline-row-setting' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedUpdates}'>
            <input checked='checked' name='' role='checkbox updates' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox updates' type='checkbox' value='' />
          </g:else>
          Download for update
        </label>
      </span>
      <span class='${savedSettings.ifNeedMonthlyFinancial ? "is-active" : ""} timeline-row-setting export-modifiers' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedMonthlyFinancial}'>
            <input checked='checked' name='' role='checkbox monthlyFinancial' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox monthlyFinancial' type='checkbox' value='' />
          </g:else>
          Generate monthly financial report for selected app stores
        </label>
      </span>
    </li>
    <li class='timeline-settings-row' role='optionGroup'>
      <span class='${savedSettings.ifNeedDownloads ? "is-active" : ""} timeline-row-setting' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedRefunds}'>
            <input checked='checked' name='' role='checkbox refunds' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox refunds' type='checkbox' value='' />
          </g:else>
          Refunds and
        </label>
      </span>
      <span class='${savedSettings.ifNeedRevenues ? "is-active" : ""} timeline-row-setting' role='option'>
        <label>
          <g:if test='${savedSettings.ifNeedRefundsMonetized}'>
            <input checked='checked' name='' role='checkbox refundsMonetized' type='checkbox' value='' />
          </g:if>
          <g:else>
            <input name='' role='checkbox refundsMonetized' type='checkbox' value='' />
          </g:else>
          refunds in $ for all apps and inapps
        </label>
      </span>
    </li>
    <li class='timeline-row-buttons'>
      <button class='button is-white' href='${createLink(action: "export", controller: "export")}' role='save button export-save'>Export TSV</button>
    </li>
  </ul>
</div>