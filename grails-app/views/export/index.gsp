<%-- DO NOT MODIFY THIS FILE, IT IS AUTOMATICALLY GENERATED. INSTEAD MODIFY index.haml --%>
<html>
  <head>
    <title>Export</title>
    <meta content='main' name='layout' />
    <parameter name='currentMenuItem' value='export'></parameter>
  </head>
  <body>
    <div class='wrapper' role='export'>
      <g:render model='${[needStep: savedSettings.step, tabsRole: "export-tabs", bookmarks: filterData.bookmarks]}' template='/timeline/tabs'></g:render>
      <div class='content-header is-graphic-header is-first'>
        <h2 class='content-title'>
          Data export
        </h2>
        <div class='content-header-selectors'>
          <a class='content-header-selector' data-from='${filterData.formattedDateFrom}' data-to='${filterData.formattedDateTo}' href='#' role='period-selector export-period-selector step-state-dependent'>
            <span class='content-header-selector-label inner-link' role='text'>${filterData.formattedFromToString}</span>
            <span class='content-header-selector-icon is-cal'>
              <span class='content-header-selector-img'></span>
            </span>
          </a>
          <div class='content-header-selector export-title-checkboxes' role='display display-data-settings'>
            <span class='export-title-setting is-disable' role='option'>
              <label>
                <g:if test='${savedSettings.ifNeedTotal}'>
                  <input checked='checked' name='' role='total' type='checkbox' value='' />
                </g:if>
                <g:else>
                  <input name='' role='total' type='checkbox' value='' />
                </g:else>
                overtime total
              </label>
            </span>
          </div>
        </div>
      </div>
      <g:render model='${[filterData: filterData, savedSettings: savedSettings]}' template='/export/settings'></g:render>
    </div>
  </body>
</html>