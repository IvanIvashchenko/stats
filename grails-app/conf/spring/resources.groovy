// Place your Spring DSL code here
beans = {
    baseResource = "file:."
    pluginSettings = new grails.util.PluginBuildSettings(grails.util.BuildSettingsHolder.settings)
    localeResolver(org.springframework.web.servlet.i18n.FixedLocaleResolver) {
        defaultLocale = "en"
    }
}
