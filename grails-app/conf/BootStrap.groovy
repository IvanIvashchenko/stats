import zeptostats.Role
import zeptostats.Member
import zeptostats.MemberRole

class BootStrap {

    def init = { servletContext ->

        def userRole = new Role(authority: 'ROLE_USER').save(flush: true);

        def testUserKirill = new Member(
            username: 'kirill',
            enabled: true,
            password: 'password',
            email: 'kirilld@zeptolab.com'
        );
        testUserKirill.save(flush: true);
        MemberRole.create(testUserKirill, userRole, true);

        def testUserAnnie = new Member(
            username: 'annie',
            enabled: true,
            password: 'password',
            email: 'annie.tarasenko@7bits.it'
        );
        testUserAnnie.save(flush: true);
        MemberRole.create(testUserAnnie, userRole, true);

        def testUserKonstantin = new Member(
            username: 'konstantin',
            enabled: true,
            password: 'password',
            email: 'konstantin.kolotyuk@7bits.it'
        );
        testUserKonstantin.save(flush: true);
        MemberRole.create(testUserKonstantin, userRole, true);

        def testUserIlya = new Member(
            username: 'ilya',
            enabled: true,
            password: 'password',
            email: 'ilya.zakharov@7bits.it'
        );
        testUserIlya.save(flush: true);
        MemberRole.create(testUserIlya, userRole, true);

        def testUserIvan = new Member(
            username: 'ivan',
            enabled: true,
            password: 'password',
            email: 'ivan.ivashchenko@7bits.it'
        );
        testUserIvan.save(flush: true);
        MemberRole.create(testUserIvan, userRole, true);

        def testUserOlga = new Member(
            username: 'olga',
            enabled: true,
            password: 'password',
            email: 'olga.martyn@7bits.it'
        );
        testUserOlga.save(flush: true);
        MemberRole.create(testUserOlga, userRole, true);
    }

    def destroy = {
    }
}
