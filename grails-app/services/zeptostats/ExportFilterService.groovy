package zeptostats
import groovy.sql.Sql

class ExportFilterService {

    def dataSource;
    def timeService;
    def compareDependentColumnService;

    def fetchAllUserSettings(final Long memberId, final Long bookmarkId) {

        def unnecessaryGameTypesAsIs = this.fetchSavedUserUnnecessaryGameTypes(memberId, bookmarkId);
        def unnecessaryResolutionsAsIs = this.fetchSavedUserUnnecessaryResolutions(memberId, bookmarkId);
        def unnecessaryGameIdsAsIs = this.fetchSavedUserUnnecessaryGameIds(memberId, bookmarkId);
        def unnecessaryCountryIdsAsIs = this.fetchSavedUserUnnecessaryCountryIds(memberId, bookmarkId);
        def unnecessaryStoreIdsAsIs = this.fetchSavedUserUnnecessaryStoreIds(memberId, bookmarkId);
        def unnecessaryRequiredProductTypeIdsAsIs = this.fetchSavedUserUnnecessaryRequiredProductTypeIds(memberId, bookmarkId);

        def Map<CompareColumn, Map<String, List<Long>>> idsInfo = compareDependentColumnService.correctIds(
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypesAsIs,
            unnecessaryResolutionsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductTypeIdsAsIs
        );

        List<Long> unnecessaryGameIds = idsInfo?.get(CompareColumn.SKU)?.allStrikedIds;
        List<Long> unnecessaryCountryIds = idsInfo?.get(CompareColumn.COUNTRY)?.allStrikedIds;
        List<Long> unnecessaryGameTypes = idsInfo?.get(CompareColumn.VERSION)?.allStrikedIds;
        List<Long> unnecessaryResolutions = idsInfo?.get(CompareColumn.STYLE)?.allStrikedIds;
        List<Long> unnecessaryStoreIds = idsInfo?.get(CompareColumn.STORE)?.allStrikedIds;

        def compareColumns = this.fetchSavedUserCompareColumns(memberId, bookmarkId);
        def settings = this.fetchSavedUserSettings(memberId, bookmarkId);
        def bookmarks = this.fetchBookmarks();

        if (!settings) {
            settings = this.fetchDefaultUserSettings();
        }

        def Step validStep = Step.parse(settings.step);
        def roundTimestamps = timeService.roundTimestampsByStep(validStep, settings.from_timestamp, settings.to_timestamp);

        [
            idsInfo: idsInfo,
            unnecessaryGameTypeIds: unnecessaryGameTypes,
            unnecessaryResolutionIds: unnecessaryResolutions,
            unnecessaryGameIds: unnecessaryGameIds,
            unnecessaryCountryIds: unnecessaryCountryIds,
            unnecessaryStoreIds: unnecessaryStoreIds,
            unnecessaryRequiredProductTypeIds: unnecessaryRequiredProductTypeIdsAsIs,
            compareColumns: compareColumns,
            fromTimestamp: roundTimestamps.fromTimestamp,
            toTimestamp: roundTimestamps.toTimestamp,
            ifNeedDownloads: settings.if_need_downloads,
            ifNeedRevenues: settings.if_need_revenues,
            ifNeedUpdates: settings.if_need_updates,
            ifNeedRefunds: settings.if_need_refunds,
            ifNeedRefundsMonetized: settings.if_need_refunds_monetized,
            ifNeedRankings: settings.if_need_rankings,
            ifNeedTotal: settings.if_need_total,
            ifNeedMonthlyFinancial: settings.if_need_monthly_financial,
            step: validStep,
            bookmarks: bookmarks
        ];
    }

    public boolean saveAllUserSettings(
        final Long memberId,
        final List<Long> unnecessaryGameIdsAsIs,
        final List<Long> unnecessaryCountryIdsAsIs,
        final List<String> unnecessaryGameTypeIdsAsIs,
        final List<String> unnecessaryResolutionIdsAsIs,
        final List<Long> unnecessaryStoreIdsAsIs,
        final List<String> unnecessaryRequiredProductTypeIdsAsIs,
        final List<CompareColumn> compareColumns,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean ifNeedDownloads,
        final Boolean ifNeedRevenue,
        final Boolean ifNeedUpdates,
        final Boolean ifNeedRefunds,
        final Boolean ifNeedRefundsMonetized,
        final Boolean ifNeedRankings,
        final Boolean ifNeedTotal,
        final Boolean ifNeedMonthlyFinancial,
        final Step needStep,
        final String bookmarkName
    ) {

        boolean success = false;

        def Map<CompareColumn, Map<String, List<Long>>> idsInfo = compareDependentColumnService.correctIds(
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypeIdsAsIs,
            unnecessaryResolutionIdsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductTypeIdsAsIs
        );

        List<Long> unnecessaryGameIds = idsInfo?.get(CompareColumn.SKU)?.allStrikedIds;
        List<Long> unnecessaryCountryIds = idsInfo?.get(CompareColumn.COUNTRY)?.allStrikedIds;
        List<Long> unnecessaryStoreIds = idsInfo?.get(CompareColumn.STORE)?.allStrikedIds;

        try {
            def bookmarkId;
            if (bookmarkName != null) {
                bookmarkId = this.saveBookmarks(bookmarkName);
            }
            this.saveUserUnnecessaryGameTypeIds(memberId, unnecessaryGameTypeIdsAsIs, bookmarkId);
            this.saveUserUnnecessaryResolutionIds(memberId, unnecessaryResolutionIdsAsIs, bookmarkId);
            this.saveUserUnnecessaryCountryIds(memberId, unnecessaryCountryIds, bookmarkId);
            this.saveUserUnnecessaryGameIds(memberId, unnecessaryGameIds, bookmarkId);
            this.saveUserUnnecessaryStoreIds(memberId, unnecessaryStoreIds, bookmarkId);
            this.saveUserUnnecessaryRequiredProductTypeIds(memberId, unnecessaryRequiredProductTypeIdsAsIs, bookmarkId);
            this.saveUserCompareColumns(memberId, compareColumns, bookmarkId);
            this.saveUserSettings(
                memberId,
                ifNeedDownloads,
                ifNeedRevenue,
                ifNeedUpdates,
                ifNeedRefunds,
                ifNeedRefundsMonetized,
                ifNeedRankings,
                ifNeedTotal,
                ifNeedMonthlyFinancial,
                needStep,
                fromTimestamp,
                toTimestamp,
                bookmarkId
            );
            success = true;
        } catch (Exception exc) {
        }

        return success;
    }

    public def removeBookmark(final Long bookmarkId) {

        def db = new Sql(dataSource);
        //TODO:: use GORM
        this.deleteSavedUserSettings(bookmarkId)
        this.deleteSavedUserCompareColumns(bookmarkId)
        this.deleteSavedUserUnnecessaryGameIds(bookmarkId)
        this.deleteSavedUserUnnecessaryCountryIds(bookmarkId)
        this.deleteSavedUserUnnecessaryResolutionIds(bookmarkId)
        this.deleteSavedUserUnnecessaryGameTypeIds(bookmarkId)
        this.deleteSavedUserUnnecessaryStoreIds(bookmarkId)
        this.deleteSavedUserUnnecessaryRequiredProductTypeIds(bookmarkId)
        def sqlRemoveBookmarkQuery = """delete from export_bookmark where id=?""";
        db.executeUpdate(sqlRemoveBookmarkQuery, [bookmarkId]);
        db.close();
    }

    public def fetchBookmarkId(final String bookmarkName) {

        def db = new Sql(dataSource);
        def sqlQuery = """select id from export_bookmark where name=?""";
        def resultId = db.firstRow(sqlQuery, [bookmarkName]);
        resultId?.id
    }

    private def fetchDefaultUserSettings() {

        def defaults = [:];
        defaults.put("from_timestamp", timeService.getMonthAgoUtcTimestamp());
        defaults.put("to_timestamp", timeService.getTodayUtcTimestamp());
        defaults.put("step", Step.DAILY.toString());
        defaults.put("if_need_total", false);
        defaults.put("if_need_updates", true);
        defaults.put("if_need_revenues", true);
        defaults.put("if_need_downloads", true);
        defaults.put("if_need_refunds", false);
        defaults.put("if_need_refunds_monetized", false);
        defaults.put("if_need_rankings", false);
        defaults.put("if_need_monthly_financial", false);

        defaults
    }

    private def fetchSavedUserSettings(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def settings;
        if (bookmarkId == null) {
            def sqlSelectedItemsQuery = """select * from export_setting where member_id=?""";
            settings = db.firstRow(sqlSelectedItemsQuery, [memberId]);
        } else {
            def sqlSelectedItemsQuery = """select * from export_setting where export_bookmark_id=?""";
            settings = db.firstRow(sqlSelectedItemsQuery, [bookmarkId]);
        }
        db.close();
        settings;
    }

    private def saveBookmarks(final String bookmarkName) {
        
        def db = new Sql(dataSource);
        def sqlSaveBookmarkQuery = """insert into export_bookmark (name) values (?)""";
        def result = db.executeInsert(sqlSaveBookmarkQuery, [bookmarkName]);
        db.close();
        //TODO:: change executeInsert to function, which returned ResultRow
        result.get(0).get(0);
    }

    private def fetchBookmarks() {

        def db = new Sql(dataSource);
        def sqlSaveBookmarkQuery = """select * from export_bookmark""";
        def rows = db.rows(sqlSaveBookmarkQuery);
        db.close();
        rows;
    }

    private def saveUserSettings(
        final Long memberId,
        final Boolean ifNeedDownloads,
        final Boolean ifNeedRevenues,
        final Boolean ifNeedUpdates,
        final Boolean ifNeedRefunds,
        final Boolean ifNeedRefundsMonetized,
        final Boolean ifNeedRankings,
        final Boolean ifNeedTotal,
        final Boolean ifNeedMonthlyFinancial,
        final Step needStep,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Long bookmarkId
    ) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_setting where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery, [memberId]);
        } 
        def sqlAddNewQuery = """
            insert into export_setting
                (member_id, if_need_downloads, if_need_revenues, if_need_updates, if_need_refunds, if_need_refunds_monetized,
                    if_need_rankings, if_need_total, if_need_monthly_financial, from_timestamp, to_timestamp, step, export_bookmark_id)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """;
        db.executeUpdate(sqlAddNewQuery,[
            bookmarkId == null ? memberId : null,
            ifNeedDownloads,
            ifNeedRevenues,
            ifNeedUpdates,
            ifNeedRefunds,
            ifNeedRefundsMonetized,
            ifNeedRankings,
            ifNeedTotal,
            ifNeedMonthlyFinancial,
            fromTimestamp,
            toTimestamp,
            needStep?.toString(),
            bookmarkId
        ]);
        db.close();
    }

    private def saveUserUnnecessaryGameTypeIds(final Long memberId, final List<String> unnecessaryVersionIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_game_type where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        } 
        def sqlAddNewQuery = """insert into export_filter_game_type (member_id, game_type, export_bookmark_id) values (?, ?, ?)""";
        unnecessaryVersionIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryGameTypes(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select game_type from export_filter_game_type where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select game_type from export_filter_game_type where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.game_type };
        db.close();
        result;
    }

    private def saveUserUnnecessaryResolutionIds(final Long memberId, final List<String> unnecessaryResolutionIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_resolution where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_filter_resolution (member_id, resolution, export_bookmark_id) values (?, ?, ?)""";
        unnecessaryResolutionIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryResolutions(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select resolution from export_filter_resolution where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select resolution from export_filter_resolution where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.resolution };
        db.close();
        result;
    }

    private def saveUserUnnecessaryCountryIds(final Long memberId, final List<Long> unnecessaryCountryIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_country where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_filter_country (member_id, country_id, export_bookmark_id) values (?, ?, ?)""";
        unnecessaryCountryIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryCountryIds(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select country_id from export_filter_country where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select country_id from export_filter_country where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.country_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryGameIds(final Long memberId, final List<Long> unnecessaryGameIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_game where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_filter_game (member_id, game_id, export_bookmark_id) values (?, ?, ?)""";
        unnecessaryGameIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryGameIds(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select game_id from export_filter_game where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select game_id from export_filter_game where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.game_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryStoreIds(final Long memberId, final List<Long> unnecessaryStoreIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_store where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_filter_store (member_id, store_id, export_bookmark_id) values (?, ?, ?)""";
        unnecessaryStoreIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryStoreIds(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select store_id from export_filter_store where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select store_id from export_filter_store where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.store_id };
        db.close();
        result;
    }

    private def saveUserCompareColumns(final Long memberId, final List<CompareColumn> compareColumns, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_compare_column where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_compare_column (member_id, compare_column, export_bookmark_id) values (?, ?, ?)""";
        compareColumns.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it?.toString(), bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserCompareColumns(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select compare_column from export_compare_column where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select compare_column from export_compare_column where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ CompareColumn.parse(it.compare_column) };
        db.close();
        result;
    }

    private def saveUserUnnecessaryRequiredProductTypeIds(final Long memberId, final List<String> unnecessaryRequiredProductTypeIds, final Long bookmarkId) {

        def db = new Sql(dataSource);
        if (bookmarkId == null) {
            def sqlDeleteOldQuery = """delete from export_filter_required_product_type where member_id=?""";
            db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        }
        def sqlAddNewQuery = """insert into export_filter_required_product_type (member_id, required_product_type, export_bookmark_id) 
                                values (?, ?, ?)
                             """;
        unnecessaryRequiredProductTypeIds.each() {
            db.executeUpdate(sqlAddNewQuery, [bookmarkId == null ? memberId : null, it, bookmarkId]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryRequiredProductTypeIds(final Long memberId, final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        if (bookmarkId == null) {
            def sqlQuery = """select required_product_type from export_filter_required_product_type where member_id=?""";
            rows = db.rows(sqlQuery,[memberId]);
        } else {
            def sqlQuery = """select required_product_type from export_filter_required_product_type where export_bookmark_id=?""";
            rows = db.rows(sqlQuery,[bookmarkId]);
        }
        def result = rows.collect{ it.required_product_type };
        db.close();
        result;
    }

    private def deleteSavedUserSettings(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_setting where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryRequiredProductTypeIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_required_product_type where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryGameTypeIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_game_type where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryGameIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_game where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryCountryIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_country where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryResolutionIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_resolution where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserUnnecessaryStoreIds(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_filter_store where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }

    private def deleteSavedUserCompareColumns(final Long bookmarkId) {

        def db = new Sql(dataSource);
        def rows;
        def sqlQuery = """delete from export_compare_column where export_bookmark_id=?""";
        db.executeUpdate(sqlQuery,[bookmarkId]);
        db.close();
    }
}