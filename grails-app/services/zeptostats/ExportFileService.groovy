package zeptostats

import java.util.List;

import javax.servlet.http.HttpServletResponse;

class ExportFileService {

    def grailsApplication;
    def timeService;

    private final static String EXPORT_FILENAME_FORMAT = "%s_%s_%s";
    private final static String COUNTRIES_PREFIX = "countries";
    private final static String DATAEXPORT_PREFIX = "dataexport_%s";
    private final static String CANDY_NONE = "None";
    private final static String CANDY_SEPARATOR = "_";

    public String configureCountriesExportFileName() {
        this.configureExportFileName(COUNTRIES_PREFIX);
    }

    public String configureDataExportFileName(final List<CompareColumn> compareColumnsUnique) {

        def candyStr = this.configureCandyString(compareColumnsUnique);
        def simpleFileName = String.format(DATAEXPORT_PREFIX, candyStr);
        this.configureExportFileName(simpleFileName);
    }

    private String configureExportFileName(final String simpleFileName) {

        def dateStr = timeService.formatSimpleTodayUtcTimestamp();
        def timeStr = timeService.formatSimpleTodayTimeUtcTimestamp();
        return String.format(EXPORT_FILENAME_FORMAT, simpleFileName, dateStr, timeStr);
    }

    private String configureCandyString(final List<CompareColumn> compareColumnsUnique) {

        def candyStr = CANDY_NONE;
        if (compareColumnsUnique != null && !compareColumnsUnique.isEmpty()) {
            def titles = compareColumnsUnique.collect() {
                return it.title;
            }
            candyStr = titles.join(CANDY_SEPARATOR);
        }

        return candyStr;
    }

    /**
     * Setups Response to have file attachment with need mimetype, filename, extension.
     * 
     * @param response HttpServletResponse
     * @param filename String need attachment filename
     * @param exportFileType ExportFileType
     */
    public void setupResponseAttachment(
        HttpServletResponse response, final String filename, final ExportFileType exportFileType
    ) {

        def contentType = this.getContentType(exportFileType);
        def extension = this.getExtension(exportFileType);

        if (contentType == null || filename == null || filename.isEmpty() || extension == null) {
            return;
        }

        this.setupResponseAttachment(response, filename, contentType, extension);
    }

    public String getExtension(final ExportFileType exportFileType) {
        return exportFileType?.extension;
    }

    /**
     * 
     * @param exportFileType ExportFileType
     * @return String contentType that is associated with exportFileType (from Configs)
     */
    public String getContentType(final ExportFileType exportFileType) {

        String contentType = null;
        String mimeType = this.getMimeType(exportFileType);
        if (mimeType) {
            contentType = grailsApplication.config.grails.mime.types[mimeType];
        }
        return contentType;
    }

    /**
     * 
     * @return String absolute path to tmp folder where export reports can be created, read, deleted (from Configs).
     */
    public String getTmpFolderAbsolutePath() {
        return grailsApplication.config.financeUiExport.tmpFolderAbsolutePath;
    }

    /**
     * 
     * @return String application hostname (from Configs).
     */
    public String getApplicationUrl() {
        return grailsApplication.config.grails.serverURL;
    }

    /**
     * @return String mimeType (one from Config grails.mime.types)
     */
    private String getMimeType(final ExportFileType exportFileType) {
        return exportFileType?.mimeType;
    }

    /**
     * Setups Response to have file attachment with need mime-tipe, filename.
     * 
     * @param response HttpServletResponse
     * @param filename String need attachment filename
     * @param contentType String content Type
     * @param extension String need attachment file's extension
     */
    private void setupResponseAttachment(
        HttpServletResponse response, final String filename, final String contentType, final String extension
    ) {

        response.setHeader("Content-disposition", "attachment; filename=${filename}.${extension}");
        response.setContentType(contentType);
    }
}
