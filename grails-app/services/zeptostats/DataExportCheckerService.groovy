package zeptostats
/**
 * Class that can check DataExport Settings.
 */
class DataExportCheckerService {

    private static final String ERROR_SAME_FILTER = "There are several columns for one filter (for CompareColumn '%s').";
    private static final String ERROR_IMPOSSIBLE_COMPARE_COLUMN = "There is unavailable CompareColumn (CompareColumn '%s' is unavailable for CompareColumn '%s').";
    private static final String ERROR_RANKINGS_IS_IMPOSSIBLE = "Unavailable curve is selected (Rankings).";

    public List<CompareColumn> configureCompareColumnsUnique(final List<CompareColumn> compareColumns) {

        def compareColumnsUnique = [];

        compareColumns?.each() {
            if (it != null && !compareColumnsUnique.contains(it)) {
                compareColumnsUnique.add(it)
            }
        }

        return compareColumnsUnique;
    }

    public void checkTimestamps(final Long fromTimestamp, final Long toTimestamp) throws DataExportTimestampException {

        if (fromTimestamp == null || toTimestamp == null) {
            throw new DataExportTimestampException();
        }
    }

    public List<ExportColumn> configureNeedSortedExportColumns(
        final List<CompareColumn> compareColumnsUnique,
        final Boolean isNeedDownloads,
        final Boolean isNeedRevenues,
        final Boolean isNeedUpdates,
        final Boolean isNeedRefunds,
        final Boolean isNeedRefundsMonetized,
        final Boolean isNeedRankings,
        final Boolean isNeedMonthlyFinancial,
        final Step needStepSafe
    ) throws DataExportNeedColumnException {

        def exportColumns = [];

        this.checkCompareColumnsStepAndCurves(compareColumnsUnique, isNeedRankings, needStepSafe);

        exportColumns = this.addExportColumnsForData(exportColumns);
        exportColumns = this.addExportColumnsForCompareColumns(exportColumns, compareColumnsUnique);
        exportColumns = this.addExportColumnsForCurves(
            exportColumns,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            isNeedMonthlyFinancial
        );

        exportColumns = this.sortExportColumns(exportColumns);

        return exportColumns;
    }

    private void checkCompareColumnsStepAndCurves(
        final List<CompareColumn> compareColumnsUnique,
        final Boolean isNeedRankings,
        final Step needStepSafe
    ) throws DataExportNeedColumnException {

        for (compareColumn1 in compareColumnsUnique) {
            //NOTE:: check that compareColumnsUnique does not contain several columns for one filter
            if (compareColumn1.filterColumn != null) {
                for (compareColumn2 in compareColumnsUnique) {
                    if (compareColumn1.equals(compareColumn2)) {
                        continue
                    }
                    if (compareColumn1.isSameFilter(compareColumn2)) {
                        throw new DataExportNeedColumnException(String.format(ERROR_SAME_FILTER, compareColumn1.code));
                    }
                }
            }

            //NOTE:: check that compareColumnsUnique does not CompareColumns that is not from justAvailableGroup
            if (compareColumn1.justAvailableGroup != null) {
                for (compareColumn2 in compareColumnsUnique) {
                    if (compareColumn1.equals(compareColumn2)) {
                        continue
                    }
                    if (!compareColumn1.justAvailableGroup.contains(compareColumn2)) {
                        throw new DataExportNeedColumnException(String.format(ERROR_IMPOSSIBLE_COMPARE_COLUMN, compareColumn2.code, compareColumn1.code));
                    }
                }
            }
        }

        if (isNeedRankings) {
            if (
                !Step.DAILY.equals(needStepSafe)
                || !compareColumnsUnique.contains(CompareColumn.SKU)
                || !compareColumnsUnique.contains(CompareColumn.COUNTRY)
            ) {
                throw new DataExportNeedColumnException(ERROR_RANKINGS_IS_IMPOSSIBLE);
            }
        }
    }

    private List<ExportColumn> addExportColumnsForData(final List<ExportColumn> exportColumns) {

        exportColumns.add(ExportColumn.START_DATE);
        exportColumns.add(ExportColumn.END_DATE);
        return exportColumns;
    }

    private List<ExportColumn> addExportColumnsForCompareColumns(
        final List<ExportColumn> exportColumns,
        final List<CompareColumn> compareColumnsUnique
    ) {

        if (compareColumnsUnique.contains(CompareColumn.SKU)) {
            exportColumns.add(ExportColumn.META_SKU)
            exportColumns.add(ExportColumn.SKU)
            exportColumns.add(ExportColumn.PARENT)
            exportColumns.add(ExportColumn.VERSION)
            exportColumns.add(ExportColumn.STYLE)
            exportColumns.add(ExportColumn.OS)
            exportColumns.add(ExportColumn.STORE)
            exportColumns.add(ExportColumn.REQUIRED_PRODUCT_TYPE)
        }
        if (compareColumnsUnique.contains(CompareColumn.METASKU)) {
            exportColumns.add(ExportColumn.META_SKU)
        }
        if (compareColumnsUnique.contains(CompareColumn.VERSION)) {
            exportColumns.add(ExportColumn.VERSION)
        }
        if (compareColumnsUnique.contains(CompareColumn.STYLE)) {
            exportColumns.add(ExportColumn.STYLE)
        }
        if (compareColumnsUnique.contains(CompareColumn.REQUIRED_PRODUCT_TYPE)) {
            exportColumns.add(ExportColumn.REQUIRED_PRODUCT_TYPE)
        }
        if (compareColumnsUnique.contains(CompareColumn.STORE)) {
            exportColumns.add(ExportColumn.OS)
            exportColumns.add(ExportColumn.STORE)
        }
        if (compareColumnsUnique.contains(CompareColumn.OS)) {
            exportColumns.add(ExportColumn.OS)
        }
        if (compareColumnsUnique.contains(CompareColumn.COUNTRY)) {
            exportColumns.add(ExportColumn.REGION)
            exportColumns.add(ExportColumn.COUNTRY)
        }
        if (compareColumnsUnique.contains(CompareColumn.REGION)) {
            exportColumns.add(ExportColumn.REGION)
        }

        return exportColumns;
    }

    private List<ExportColumn> addExportColumnsForCurves(
        final List<ExportColumn> exportColumns,
        final Boolean isNeedDownloads,
        final Boolean isNeedRevenues,
        final Boolean isNeedUpdates,
        final Boolean isNeedRefunds,
        final Boolean isNeedRefundsMonetized,
        final Boolean isNeedRankings,
        final Boolean isNeedMonthlyFinancial
    ) {

        if (isNeedDownloads) {
            exportColumns.add(ExportColumn.DOWNLOADS)
            exportColumns.add(ExportColumn.PROMO)
        }
        if (isNeedRevenues) {
            exportColumns.add(ExportColumn.REVENUE)
        }
        if (isNeedRefunds) {
            exportColumns.add(ExportColumn.REFUNDS)
        }
        if (isNeedRefundsMonetized) {
            exportColumns.add(ExportColumn.REFUNDS_MONETIZED)
        }
        if (isNeedUpdates) {
            exportColumns.add(ExportColumn.UPDATES)
        }
        if (isNeedRankings) {
            exportColumns.add(ExportColumn.RANKINGS)
        }
        if (isNeedMonthlyFinancial) {
            exportColumns.add(ExportColumn.MONTHLY_FINANCIAL)
        }

        return exportColumns;
    }

    private List<ExportColumn> sortExportColumns(final List<ExportColumn> exportColumns) {

        def exportColumnsSorted = [];

        ExportColumn.values().each () {
            if (exportColumns.contains(it) && !exportColumnsSorted.contains(it)) {
                exportColumnsSorted.add(it);
            }
        }

        return exportColumnsSorted;
    }
}