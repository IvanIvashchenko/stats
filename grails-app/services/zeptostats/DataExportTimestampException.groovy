package zeptostats;

/**
 * Is thrown when timestamp are wrong (are null).
 */
class DataExportTimestampException extends Exception {

    public DataExportTimestampException() {
        super()
    }

    public DataExportTimestampException(final String message) {
        super(message)
    }

    public DataExportTimestampException(final Throwable throwable) {
        super(throwable)
    }

    public DataExportTimestampException(final String message, final Throwable throwable) {
        super(message, throwable)
    }
}