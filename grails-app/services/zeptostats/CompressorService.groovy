package zeptostats;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.zip.GZIPOutputStream;
import java.io.OutputStreamWriter;

/**
 * Class which can compress files.
 */
class CompressorService {

    private static final String LOG_PREFIX = "DataExport: compressing:";

    private static final String LOG_ERROR = "$LOG_PREFIX compressing has not been finished because of %s (file %s).";
    private static final String LOG_NOTE = "$LOG_PREFIX reading-writing error(while closing) because of %s (file %s).";

    /**
     * Creates new compressed(GZIP) file by inFile in same folder with need extension.
     * 
     * @param inFile File which should be compressed
     * @param outExtension String need extension for compressed file
     * @return File compressed file or null if there were some errors
     */
    def File compressFile(final File inFile, final String outExtension) {

        File result = null;

        def inFileAbsolutePath = inFile.getAbsolutePath();

        BufferedWriter bufferedWriter = null;
        BufferedReader bufferedReader = null;
        String path = "${inFileAbsolutePath}.${outExtension}";
        File outFile = new File(path);
        try {

            FileOutputStream fileOutputStream = new FileOutputStream(outFile)
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(fileOutputStream)
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(gzipOutputStream)
            bufferedWriter = new BufferedWriter(outputStreamWriter);

            FileReader fileReader = new FileReader(inFile)
            bufferedReader = new BufferedReader(fileReader);

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                bufferedWriter.write(line);
                bufferedWriter.newLine();
            }

            result = outFile;

        } catch (FileNotFoundException exc) {
            log.error(String.format(LOG_ERROR, exc.getMessage(), path));
        } catch (IOException exc) {
            log.error(String.format(LOG_ERROR, exc.getMessage(), path));
        } catch (Exception exc) {
            log.error(String.format(LOG_ERROR, exc.getMessage(), path));
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException exc) {
                    log.error(String.format(LOG_NOTE, exc.getMessage(), path));
                }
            }

            if (bufferedReader != null ){
                try {
                    bufferedReader.close();
                } catch (IOException exc) {
                    log.error(String.format(LOG_NOTE, exc.getMessage(), path));
                }
            }
        }

        return result;
    }
}