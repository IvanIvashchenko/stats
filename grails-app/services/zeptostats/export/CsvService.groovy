package zeptostats.export


/**
 * Class to add CSV-data (Comma Separated Values) to outputStream.
 */
public class CsvService extends DsvService {

    private final static String ITEM_SEPARATOR_COMMA = ",";

    public CsvService() {
        super(ITEM_SEPARATOR_COMMA);
    }

    private CsvService(final String itemSeparator) {
        super(ITEM_SEPARATOR_COMMA);
    }
}