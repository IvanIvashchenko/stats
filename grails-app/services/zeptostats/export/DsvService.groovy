package zeptostats.export

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

/**
 * Class to add DSV-data (Delimiter Separated Values) to outputStream.
 */
public class DsvService {

    private final static String DSV_LINE_SEPARATOR = "\n";
    private final static String DSV_BLOCK_SEPARATOR = "\n\n";

    private BufferedWriter bufferedWriter;
    private String itemSeparator;

    /**
     * 
     * @param itemSeparator String separator which is used as item_separator.
     */
    public DsvService(final String itemSeparator) {
        this.setItemSeparator(itemSeparator);
    }

    /**
     * Should be used before write*, close* to need stream.
     * 
     * Closes previous stream itself.
     * 
     * @param outputStream OutputStream to which DSV-data should be added.
     */
    public void init(final OutputStream outputStream) {

        this.close();

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream)
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

        this.setBufferedWriter(bufferedWriter);
    }

    private String getItemSeparator() {
        return this.itemSeparator;
    }
    private void setItemSeparator(final String itemSeparator) {
        this.itemSeparator = itemSeparator;
    }

    private BufferedWriter getBufferedWriter() {
        return this.bufferedWriter;
    }
    private void setBufferedWriter(final BufferedWriter bufferedWriter) {
        this.bufferedWriter = bufferedWriter;
    }

    /**
     * Should be used after init
     * 
     * Adds DSV-item-data.
     * 
     * @param item Object item which will be added as DSV-item-data.
     */
    public void writeItem(final item) {
        this.bufferedWriter?.write(item + this.getItemSeparator());
    }

    /**
     * Should be used after init
     * 
     * Adds DSV-item-data and new line.
     * 
     * @param item Object item which will be added as DSV-item-data.
     */
    public void writelnItem(final item) {

        this.writeItem(item);
        this.writeln();
    }

    /**
     * Should be used after init
     * 
     * Adds DSV new line.
     */
    public void writeln() {
        this.writeAsIs(DSV_LINE_SEPARATOR);
    }

    /**
     * Should be used after init
     * 
     * Adds block of DSV lines.
     */
    public void writelnBlock() {
        this.writeAsIs(DSV_BLOCK_SEPARATOR);
    }

    /**
     * Should be used after init
     * 
     * Adds data as is.
     * 
     * @param item Object item which will be added as is.
     */
    protected void writeAsIs(final item) {
        this.bufferedWriter?.write(item);
    }

    /**
     * Should be used after init
     * 
     * Adds DSV-empty-data.
     */
    public void writeEmpty() {
        this.bufferedWriter?.write(this.getItemSeparator());
    }

    /**
     * Should be used after init
     */
    public void close() {

        this.bufferedWriter?.flush();
        this.bufferedWriter?.close();

        this.bufferedWriter = null;
    }
}