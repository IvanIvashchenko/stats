package zeptostats.export

/**
 * Class to add TSV-data (Comma Separated Values) to outputStream.
 */
public class TsvService extends DsvService {

    private final static String ITEM_SEPARATOR_TAB = "\t";

    public TsvService() {
        super(ITEM_SEPARATOR_TAB);
    }

    private TsvService(final String itemSeparator) {
        super(ITEM_SEPARATOR_TAB);
    }
}