package zeptostats;

import groovy.sql.Sql;

class FinanceFilterService {

    def timeService;
    def dataSource;
    def dataSource_zeptostats;

    def saveUserTimeFilter(final Long from, final Long to, Long memberId) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from finance_setting where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId.toString()]);
        def sqlAddNewQuery = """insert into finance_setting (member_id, from_time, to_time) values (?, ?, ?)""";
        db.executeUpdate(sqlAddNewQuery,[memberId.toString(), from, to]);
    }

    def fetchSavedTimeForFilter(Long memberId) {

        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select from_time,to_time from finance_setting where member_id=?""";
        def savedTime = db.firstRow(sqlSelectedItemsQuery, [memberId.toString()]);

        if (!savedTime) {

            def now = timeService.getTodayUtcTimestamp();
            def monthAgo = timeService.getMonthAgoUtcTimestamp();
            savedTime = [from_time: monthAgo, to_time: now];
        }

        savedTime;
    }

    def saveUserStoresSettings(memberId, storesIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from finance_stores_filter where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId.toString()]);
        def sqlAddNewQuery = """insert into finance_stores_filter (member_id,store_id) values (?, ?)""";
        storesIds.each() {

            if (!it.equals("")){
                db.executeUpdate(sqlAddNewQuery, [memberId.toString(), it]);
            }
        }
        db.close();
    }

    def fetchSavedUserStoresSettings(memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select store_id from finance_stores_filter where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId.toString()]);
        def result = rows.collect{ it.store_id };
        db.close();
        result;
    }

}