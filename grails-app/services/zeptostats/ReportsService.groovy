package zeptostats

import groovy.sql.Sql

class ReportsService {

    def dataSource;

    private defaultMailSubscriptions = ['weekly': true, 'everydayRating': true, 'everydayDownloads': true];

    def fetchUserMailSubscriptions(memberId) {
        
        def db = new Sql(dataSource);
        def sqlQuery = """select * from mail_subscription where member_id=?""";
        def rows = db.rows(sqlQuery, [memberId.toString()]);
        def result = rows.size == 0 ? defaultMailSubscriptions : rows.first();
        db.close();
        result;
    }

    def saveSubscriptions(weekly, everydayRating, everydayDownloads, memberId) {

        def db = new Sql(dataSource);

        def sqlDeleteOldQuery = """delete from mail_subscription where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId.toString()]);

        def sqlInsertQuery = """insert into mail_subscription 
                          (weekly,everyday_rating,everyday_downloads,member_id) values (?,?,?,?)""";
        def result = db.executeUpdate(sqlInsertQuery, [
            Boolean.valueOf(weekly),
                Boolean.valueOf(everydayRating),
                Boolean.valueOf(everydayDownloads),
                memberId.toString()
            ]);
        db.close();
        result;
    }
}