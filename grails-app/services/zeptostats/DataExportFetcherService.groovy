package zeptostats
import groovy.sql.GroovyRowResult
import groovy.sql.Sql

import java.sql.SQLException
/**
 * Class that can fetch all need data for DataExport.
 */
class DataExportFetcherService {

    def dataSource_zeptostats;

    /** ALIAS_* constants are used as aliases in query, are used as columnCodes in results */
    public static final String NAME_UNIT_TABLE = "unit";
    public static final String NAME_FINANCE_TABLE = "finance";
    public static final String ALIAS_UNIT_TABLE = "u";
    public static final String ALIAS_FINANCE_TABLE = "f";
    public static final String ALIAS_DATES = "Dates";
    public static final String ALIAS_START_DATE = ExportColumn.START_DATE.alias;
    public static final String ALIAS_END_DATE = ExportColumn.END_DATE.alias;
    public static final String ALIAS_EXTRA = ExportColumn.EXTRA.alias;
    public static final String ALIAS_DOWNLOADS = ExportColumn.DOWNLOADS.alias;
    public static final String ALIAS_PROMO = ExportColumn.PROMO.alias;
    public static final String ALIAS_UPDATES = ExportColumn.UPDATES.alias;
    public static final String ALIAS_REFUNDS = ExportColumn.REFUNDS.alias;
    public static final String ALIAS_REVENUE = ExportColumn.REVENUE.alias;
    public static final String ALIAS_REFUNDS_MONETIZED = ExportColumn.REFUNDS_MONETIZED.alias;
    public static final String ALIAS_IS_INAPP = "IsInapp";
    public static final String ALIAS_META_SKU = ExportColumn.META_SKU.alias;
    public static final String ALIAS_SKU = ExportColumn.SKU.alias;
    public static final String ALIAS_PARENT = ExportColumn.PARENT.alias;
    public static final String ALIAS_VERSION = ExportColumn.VERSION.alias;
    public static final String ALIAS_OS = ExportColumn.OS.alias;
    public static final String ALIAS_STORE = ExportColumn.STORE.alias;
    public static final String ALIAS_STYLE = ExportColumn.STYLE.alias;
    public static final String ALIAS_COUNTRY = ExportColumn.COUNTRY.alias;
    public static final String ALIAS_REGION = ExportColumn.REGION.alias;
    public static final String ALIAS_RANKINGS = ExportColumn.RANKINGS.alias;
    public static final String ALIAS_REQUIRED_PRODUCT_TYPE = ExportColumn.REQUIRED_PRODUCT_TYPE.alias;
    public static final String ALIAS_MONTHLY_FINANCIAL = ExportColumn.MONTHLY_FINANCIAL.alias;

    public static final String DATE_FORMAT_HUMAN_DAY = "'%d %b %Y'";
    public static final String DATE_FORMAT_HUMAN_MONTH = "'%b %Y'";
    public static final String DATE_FORMAT_HUMAN_SEPARATOR = "' - '";
    public static final String NONE = "-";
    public static final String NONE_STR = "'$NONE'";

    private static final String CODE_DATES = "Dates";
    private static final String CODE_START_DATE = ExportColumn.START_DATE.toString();
    private static final String CODE_END_DATE = ExportColumn.END_DATE.toString();
    private static final String CODE_PARTNER = "Partner";
    private static final String CODE_GAMETYPE = ExportColumn.VERSION.toString();
    private static final String CODE_RESOLUTION = ExportColumn.STYLE.toString();
    private static final String CODE_REGION = ExportColumn.REGION.toString();
    private static final String CODE_COUNTRY = ExportColumn.COUNTRY.toString();
    private static final String CODE_PLATFORM = ExportColumn.OS.toString();
    private static final String CODE_STORE = ExportColumn.STORE.toString();
    private static final String CODE_IS_INAPP = "IsInapp";
    private static final String CODE_METAGAME = ExportColumn.META_SKU.toString();
    private static final String CODE_GAME = ExportColumn.SKU.toString();
    private static final String CODE_PARENT_GAME = ExportColumn.PARENT.toString();
    private static final String CODE_EXTRA = ExportColumn.EXTRA.toString();
    private static final String CODE_DOWNLOADS = ExportColumn.DOWNLOADS.toString();
    private static final String CODE_PROMO = ExportColumn.PROMO.toString();
    private static final String CODE_REVENUES = ExportColumn.REVENUE.toString();
    private static final String CODE_REFUNDS = ExportColumn.REFUNDS.toString();
    private static final String CODE_REFUNDS_MONETIZED = ExportColumn.REFUNDS_MONETIZED.toString();
    private static final String CODE_UPDATES = ExportColumn.UPDATES.toString();
    private static final String CODE_RANKINGS = ExportColumn.RANKINGS.toString();
    private static final String CODE_REQUIRED_PRODUCT_TYPE = ExportColumn.REQUIRED_PRODUCT_TYPE.toString();
    private static final String CODE_MONTHLY_FINANCIAL = ExportColumn.MONTHLY_FINANCIAL.toString();

    private static final String ASC = "ASC";
    private static final String DESC = "DESC";

    private static final def DUMMY_ID = -1;

    /**
     * TODO:: add description about Rankings when it will be done.
     * TODO:: write full specification later
     *
     *
     * Method which selects into file (absoluteFilePath) all need ExportColumns by needStepSafe between fromTimestamp
     * and toTimestamp filtered by unnecessaryGameIds and unnecessaryCountryIds.
     *
     * Result file will be generated automatically, it should not exist before.
     * File contains data without column names.
     * Columns are ordered as orderedExportColumns.
     * Data is sorted as sortable columns from orderedExportColumns.
     *
     * @param absoluteFilePath String absolute path to future file that should contain result of query's select. File will be created by method, should not exist before.
     * @param unnecessaryGameIds ids of unnecessary Game(and Inapp). May be null, then will be like empty.
     * @param unnecessaryCountryIds ids of unnecessary Country. May be null, then will be like empty.
     * @param orderedExportColumns CORRECT ORDERED list of need ExportColumns. List cannot be null.
     * @param fromTimestamp start timestamp of need period. May not be null.
     * @param toTimestamp end timestamp of need period. May not be null.
     * @param needStepSafe need step (Daily, Weekly, Monthly etc.). May not be null.
     * @throws SQLException if some SQL error occurs
     */
    public void fetchIntoFile(
        final String absoluteFilePath,
        final List<Long> unnecessaryGameIds,
        final List<Long> unnecessaryCountryIds,
        final List<ExportColumn> orderedExportColumns,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Step needStepSafe
    ) throws SQLException {

        log.debug "Started fetchIntoFile()"

        def sqlQuery = this.configureFetchSql(
            absoluteFilePath,
            unnecessaryGameIds,
            unnecessaryCountryIds,
            orderedExportColumns,
            fromTimestamp,
            toTimestamp,
            needStepSafe
        );

        log.debug sqlQuery;

        Sql db = new Sql(dataSource_zeptostats);
        db.execute(
            sqlQuery,
            [
                from_Timestamp: fromTimestamp,
                to_Timestamp: toTimestamp
            ]
        );
        db.close();

        log.debug "Ended fetchIntoFile()"
    }

    protected String configureFetchSql(
        final String absoluteFilePath,
        final List<Long> unnecessaryGameIds,
        final List<Long> unnecessaryCountryIds,
        final List<ExportColumn> orderedExportColumns,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Step needStepSafe
    ) {

        def idGames = this.fromIdList(unnecessaryGameIds);
        def idCountries = this.fromIdList(unnecessaryCountryIds);

        def unitJoinList = [
            CODE_REGION, CODE_COUNTRY,
            CODE_PLATFORM, CODE_STORE, CODE_PARTNER,
            CODE_METAGAME, CODE_GAME, CODE_PARENT_GAME, CODE_RANKINGS, CODE_REQUIRED_PRODUCT_TYPE
        ];
        def unitSelectList = [
            CODE_DATES,
            CODE_IS_INAPP,
            CODE_REGION, CODE_COUNTRY,
            CODE_PLATFORM, CODE_STORE,
            CODE_METAGAME, CODE_GAME, CODE_GAMETYPE, CODE_RESOLUTION, CODE_PARENT_GAME, CODE_MONTHLY_FINANCIAL,
            CODE_DOWNLOADS, CODE_PROMO, CODE_UPDATES, CODE_REFUNDS, CODE_REFUNDS_MONETIZED, CODE_REVENUES, CODE_RANKINGS, CODE_REQUIRED_PRODUCT_TYPE
        ];
        def finalSelectList = [
            CODE_START_DATE,
            CODE_END_DATE,
            CODE_REGION, CODE_COUNTRY,
            CODE_PLATFORM, CODE_STORE,
            CODE_METAGAME, CODE_GAME, CODE_GAMETYPE, CODE_RESOLUTION, CODE_PARENT_GAME, CODE_MONTHLY_FINANCIAL,
            CODE_DOWNLOADS, CODE_PROMO, CODE_UPDATES, CODE_REFUNDS, CODE_REFUNDS_MONETIZED, CODE_REVENUES, CODE_RANKINGS, CODE_REQUIRED_PRODUCT_TYPE
        ];
        def unitGroupByList = [
            CODE_DATES,
            CODE_REGION, CODE_COUNTRY,
            CODE_PLATFORM, CODE_STORE,
            CODE_METAGAME, CODE_GAME, CODE_GAMETYPE, CODE_RESOLUTION, CODE_REQUIRED_PRODUCT_TYPE
        ];
        def unitJoin = this.configureMapByListWithEmptyValues(unitJoinList);
        def unitSelect = this.configureMapByListWithEmptyValues(unitSelectList);
        def finalSelect = this.configureMapByListWithEmptyValues(finalSelectList);
        def unitGroupBy = this.configureMapByListWithEmptyValues(unitGroupByList);
        
        def tableName = NAME_UNIT_TABLE
        def tableAlias = ALIAS_UNIT_TABLE
        def tableDateAlias = "date"
        def isNeedMonthlyFinancial = orderedExportColumns.contains(ExportColumn.MONTHLY_FINANCIAL)
        def dateBlock = this.configureDateBlock(isNeedMonthlyFinancial)
        if (isNeedMonthlyFinancial) {
            tableName = NAME_FINANCE_TABLE
            tableAlias = ALIAS_FINANCE_TABLE
            tableDateAlias = "start_date"
        } 

        def relations = this.configureNecessaryOrNotRelations(orderedExportColumns);
        if (relations[CODE_REGION])     unitJoin[CODE_REGION]      = "INNER JOIN region reg      ON reg.id = country.region_id"
        if (relations[CODE_COUNTRY])    unitJoin[CODE_COUNTRY]     = "INNER JOIN country country ON country.id = ${tableAlias}.country_id"
        if (relations[CODE_PLATFORM])   unitJoin[CODE_PLATFORM]    = "INNER JOIN platform plat   ON plat.id = st.platform_id"
        if (relations[CODE_STORE])      unitJoin[CODE_STORE]       = "INNER JOIN store st        ON st.id = g.store_id"
        if (relations[CODE_METAGAME])   unitJoin[CODE_METAGAME]    = "INNER JOIN meta_game mg    ON mg.id = g.meta_game_id"
        if (relations[CODE_GAME])       unitJoin[CODE_GAME]        = "INNER JOIN game g          ON g.id = ${tableAlias}.game_id"
        if (relations[CODE_PARENT_GAME])unitJoin[CODE_PARENT_GAME] = "LEFT  JOIN game parent     ON g.parent_game_id = parent.id"
        if (relations[CODE_PARTNER])    unitJoin[CODE_PARTNER]     = "LEFT  JOIN partner par     ON g.partner_id = par.id"
        if (relations[CODE_RANKINGS])   unitJoin[CODE_RANKINGS]    = "LEFT  JOIN rank rank       ON ${tableAlias}.${tableDateAlias} = rank.date AND ${tableAlias}.game_id = rank.game_id AND ${tableAlias}.country_id = rank.country_id"
        if (relations[CODE_REQUIRED_PRODUCT_TYPE]) {
            unitJoin[CODE_REQUIRED_PRODUCT_TYPE] = """
                LEFT  JOIN 
                (select id, if(parent_game_id is null, 'Application', 'In-App') as name from game where 1) 
                as rpt ON rpt.id = g.id
            """
        }  


        def calendarSelectDates = this.configureFirstSelectDates(needStepSafe, "c");
        def calendarGroupByFirstStep = this.configureFirstGroupByStep(needStepSafe, "c");

        unitSelect[CODE_DATES] = this.configureFirstSelectDates(needStepSafe, tableAlias);
        unitGroupBy[CODE_DATES] = this.configureFirstGroupByStep(needStepSafe, tableAlias);

        finalSelect[CODE_START_DATE] = this.configureFirstSelectStartDate(needStepSafe, "calendar");

        finalSelect[CODE_END_DATE] = this.configureFirstSelectEndDate(needStepSafe, "calendar");

        if (orderedExportColumns.contains(ExportColumn.DOWNLOADS)) {
            unitSelect[CODE_DOWNLOADS] = ", sum(u.downloads) AS $ALIAS_DOWNLOADS"
            finalSelect[CODE_DOWNLOADS] = this.configureSelectWithNoneFirst(ALIAS_DOWNLOADS)
            unitSelect[CODE_PROMO] = ", sum(u.promo) AS $ALIAS_PROMO"
            finalSelect[CODE_PROMO] = this.configureSelectWithNoneFirst(ALIAS_PROMO)
        }
        if (orderedExportColumns.contains(ExportColumn.UPDATES)) {
            unitSelect[CODE_UPDATES] = ", sum(u.updates) AS $ALIAS_UPDATES"
            finalSelect[CODE_UPDATES] = this.configureSelectWithNoneFirst(ALIAS_UPDATES)
        }
        if (orderedExportColumns.contains(ExportColumn.REFUNDS)) {
            unitSelect[CODE_REFUNDS] = ", sum(u.refunds) AS $ALIAS_REFUNDS"
            finalSelect[CODE_REFUNDS] = this.configureSelectWithNoneFirst(ALIAS_REFUNDS)
        }
        if (orderedExportColumns.contains(ExportColumn.REVENUE)) {
            def revenue = "u.downloads * gp.price * (1 - ifnull(st.commission, 0)) * (1 - ifnull(par.royalty, 0))"
            unitSelect[CODE_REVENUES] = this.configureSelectSumNullSafe(revenue, ALIAS_REVENUE)
            finalSelect[CODE_REVENUES] = this.configureSelectWithNoneFirst(ALIAS_REVENUE)
        }
        if (orderedExportColumns.contains(ExportColumn.REFUNDS_MONETIZED)) {
            def refundMonetized = "u.refunds * gp.price * (1 - ifnull(st.commission, 0)) * (1 - ifnull(par.royalty, 0))"
            unitSelect[CODE_REFUNDS_MONETIZED] = configureSelectSumNullSafe(refundMonetized, ALIAS_REFUNDS_MONETIZED)
            finalSelect[CODE_REFUNDS_MONETIZED] = this.configureSelectWithNoneFirst(ALIAS_REFUNDS_MONETIZED)
        }
        if (isNeedMonthlyFinancial) {
            
            unitSelect[CODE_MONTHLY_FINANCIAL] = ", sum(f.revenue) AS $ALIAS_MONTHLY_FINANCIAL"
            finalSelect[CODE_MONTHLY_FINANCIAL] = this.configureSelectWithNoneFirst(ALIAS_MONTHLY_FINANCIAL)
        }
        if (orderedExportColumns.contains(ExportColumn.VERSION) || orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_GAMETYPE] = ", g.game_type AS $ALIAS_VERSION"
            finalSelect[CODE_GAMETYPE] = this.configureSelectWithNoneFirst(ALIAS_VERSION)
            unitGroupBy[CODE_GAMETYPE] = this.configureGroupBy(ALIAS_VERSION)
        }
        if (orderedExportColumns.contains(ExportColumn.STYLE) || orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_RESOLUTION] = ", g.resolution AS $ALIAS_STYLE"
            finalSelect[CODE_RESOLUTION] = this.configureSelectWithNoneFirst(ALIAS_STYLE)
            unitGroupBy[CODE_RESOLUTION] = this.configureGroupBy(ALIAS_STYLE)
        }
        if (orderedExportColumns.contains(ExportColumn.REGION) || orderedExportColumns.contains(ExportColumn.COUNTRY)) {
            unitSelect[CODE_REGION] = ", reg.name AS $ALIAS_REGION"
            finalSelect[CODE_REGION] = this.configureSelectWithNoneFirst(ALIAS_REGION)
            unitGroupBy[CODE_REGION] = this.configureGroupBy(ALIAS_REGION)
        }
        if (orderedExportColumns.contains(ExportColumn.COUNTRY)) {
            unitSelect[CODE_COUNTRY] = ", country.name AS $ALIAS_COUNTRY"
            finalSelect[CODE_COUNTRY] = this.configureSelectWithNoneFirst(ALIAS_COUNTRY)
            unitGroupBy[CODE_COUNTRY] = this.configureGroupBy(ALIAS_COUNTRY)
        }
        if (orderedExportColumns.contains(ExportColumn.OS) || orderedExportColumns.contains(ExportColumn.STORE) || orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_PLATFORM] = ", plat.name AS $ALIAS_OS"
            finalSelect[CODE_PLATFORM] = this.configureSelectWithNoneFirst(ALIAS_OS)
            unitGroupBy[CODE_PLATFORM] = this.configureGroupBy(ALIAS_OS)
        }
        if (orderedExportColumns.contains(ExportColumn.STORE) || orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_STORE] = ", st.name AS $ALIAS_STORE"
            finalSelect[CODE_STORE] = this.configureSelectWithNoneFirst(ALIAS_STORE)
            unitGroupBy[CODE_STORE] = this.configureGroupBy(ALIAS_STORE)
        }
        if (orderedExportColumns.contains(ExportColumn.META_SKU) || orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_METAGAME] = ", mg.name AS $ALIAS_META_SKU"
            finalSelect[CODE_METAGAME] = this.configureSelectWithNoneFirst(ALIAS_META_SKU)
            unitGroupBy[CODE_METAGAME] = this.configureGroupBy(ALIAS_META_SKU)

            unitSelect[CODE_IS_INAPP] = ", mg.is_inapp AS $ALIAS_IS_INAPP"
        }
        if (orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_GAME] = ", g.title AS $ALIAS_SKU"
            finalSelect[CODE_GAME] = this.configureSelectWithNoneFirst(ALIAS_SKU)
            unitGroupBy[CODE_GAME] = this.configureGroupBy(ALIAS_SKU)
        }
        if (orderedExportColumns.contains(ExportColumn.SKU)) {
            unitSelect[CODE_PARENT_GAME] = ", parent.title AS $ALIAS_PARENT"
            finalSelect[CODE_PARENT_GAME] = this.configureSelectWithNoneFirst(ALIAS_PARENT)
        }
        if (orderedExportColumns.contains(ExportColumn.RANKINGS)) {
            unitSelect[CODE_RANKINGS] = ", rank.rank AS $ALIAS_RANKINGS"
            finalSelect[CODE_RANKINGS] = this.configureSelectWithNoneFirst(ALIAS_RANKINGS)
        }
        if (orderedExportColumns.contains(ExportColumn.REQUIRED_PRODUCT_TYPE)) {
            unitSelect[CODE_REQUIRED_PRODUCT_TYPE] = ", rpt.name AS $ALIAS_REQUIRED_PRODUCT_TYPE"
            finalSelect[CODE_REQUIRED_PRODUCT_TYPE] = this.configureSelectWithNoneFirst(ALIAS_REQUIRED_PRODUCT_TYPE)
            unitGroupBy[CODE_REQUIRED_PRODUCT_TYPE] = this.configureGroupBy(ALIAS_REQUIRED_PRODUCT_TYPE)
        }

        def finalOrderBy = this.configureFinalOrderBy(orderedExportColumns, "calendar.date", "metaTable")
        def finalSelectStr = this.configureFinalSelect(orderedExportColumns, finalSelect)
        def finalSelectIntoFileStr = "";
        if (absoluteFilePath) {
            finalSelectIntoFileStr = """
            INTO OUTFILE '${absoluteFilePath}'
                FIELDS TERMINATED BY '\t'
                OPTIONALLY ENCLOSED BY ''
                LINES TERMINATED BY '\n'
            """;
        }



        def sqlQuery =
        """
            SELECT ${finalSelectStr}
            FROM
            (
                (
                    SELECT
                        date
                        , $calendarSelectDates
                    FROM calendar c
                    WHERE c.date BETWEEN :from_Timestamp AND :to_Timestamp
                    GROUP BY $calendarGroupByFirstStep
                ) AS calendar
                LEFT JOIN
                (
                    SELECT
                        ${unitSelect[CODE_DATES]}
                        ${unitSelect[CODE_IS_INAPP]}
                        ${unitSelect[CODE_DOWNLOADS]}
                        ${unitSelect[CODE_PROMO]}
                        ${unitSelect[CODE_UPDATES]}
                        ${unitSelect[CODE_REVENUES]}
                        ${unitSelect[CODE_REFUNDS]}
                        ${unitSelect[CODE_REFUNDS_MONETIZED]}
                        ${unitSelect[CODE_MONTHLY_FINANCIAL]}
                        ${unitSelect[CODE_GAMETYPE]}
                        ${unitSelect[CODE_RESOLUTION]}
                        ${unitSelect[CODE_REGION]}
                        ${unitSelect[CODE_COUNTRY]}
                        ${unitSelect[CODE_PLATFORM]}
                        ${unitSelect[CODE_STORE]}
                        ${unitSelect[CODE_METAGAME]}
                        ${unitSelect[CODE_GAME]}
                        ${unitSelect[CODE_PARENT_GAME]}
                        ${unitSelect[CODE_RANKINGS]}
                        ${unitSelect[CODE_REQUIRED_PRODUCT_TYPE]}
                    FROM
                        ${tableName} ${tableAlias}
                        LEFT JOIN game_period gp USING(game_id)
                        ${unitJoin[CODE_GAME]}
                        ${unitJoin[CODE_STORE]}
                        ${unitJoin[CODE_COUNTRY]}
                        ${unitJoin[CODE_REGION]}
                        ${unitJoin[CODE_PLATFORM]}
                        ${unitJoin[CODE_METAGAME]}
                        ${unitJoin[CODE_PARENT_GAME]}
                        ${unitJoin[CODE_PARTNER]}
                        ${unitJoin[CODE_RANKINGS]}
                        ${unitJoin[CODE_REQUIRED_PRODUCT_TYPE]}
                    WHERE
                        ${tableAlias}.game_id NOT IN ($idGames)
                        AND ${tableAlias}.country_id NOT IN ($idCountries)
                        ${dateBlock}                        
                    GROUP BY
                        ${unitGroupBy[CODE_DATES]}
                        ${unitGroupBy[CODE_GAMETYPE]}
                        ${unitGroupBy[CODE_RESOLUTION]}
                        ${unitGroupBy[CODE_REGION]}
                        ${unitGroupBy[CODE_COUNTRY]}
                        ${unitGroupBy[CODE_PLATFORM]}
                        ${unitGroupBy[CODE_STORE]}
                        ${unitGroupBy[CODE_METAGAME]}
                        ${unitGroupBy[CODE_GAME]}
                        ${unitGroupBy[CODE_REQUIRED_PRODUCT_TYPE]}
                ) AS metaTable USING($ALIAS_DATES)
            )
            $finalOrderBy
            $finalSelectIntoFileStr
        """;

        return sqlQuery;
    }

    public List<GroovyRowResult> fetch(
        final List<Long> unnecessaryGameIds,
        final List<Long> unnecessaryCountryIds,
        final List<ExportColumn> orderedExportColumns,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Step needStepSafe
    ) throws SQLException {

        Long fetchStartTime = System.currentTimeMillis()

        def sqlQuery = this.configureFetchSql(
            null,
            unnecessaryGameIds,
            unnecessaryCountryIds,
            orderedExportColumns,
            fromTimestamp,
            toTimestamp,
            needStepSafe
        );

        Sql db = new Sql(dataSource_zeptostats);
        def result = db.rows(
            sqlQuery,
            [
                from_Timestamp: fromTimestamp,
                to_Timestamp: toTimestamp
            ]
        );
        db.close();

        Long totalTimeSpent = System.currentTimeMillis() - fetchStartTime
        log.debug "Time spent of fetch(): ${totalTimeSpent}"

        return result;
    }

    public GroovyRowResult fetchTotals(
        final List<Long> unnecessaryGameIds,
        final List<Long> unnecessaryCountryIds,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean isNeedMonthlyFinancial
    ) throws SQLException {

        def gameIdList = this.fromIdList(unnecessaryGameIds);
        def countryIdList = this.fromIdList(unnecessaryCountryIds);

        def tableName = isNeedMonthlyFinancial ? NAME_FINANCE_TABLE : NAME_UNIT_TABLE
        def tableAlias = isNeedMonthlyFinancial ? ALIAS_FINANCE_TABLE : ALIAS_UNIT_TABLE
        def dateBlock = this.configureDateBlock(isNeedMonthlyFinancial)
        def selectStr = ""

        if (isNeedMonthlyFinancial) {
            selectStr = "ifnull(cast(sum(f.revenue) AS CHAR), '-') AS $ALIAS_MONTHLY_FINANCIAL"
        } else {
            def revenue = "ROUND(sum(u.downloads * gp.price * (1 - ifnull(st.commission, 0)) * (1 - ifnull(par.royalty, 0))),2)"
            def refundsMonetized = "ROUND(sum(u.refunds * gp.price * (1 - ifnull(st.commission, 0)) * (1 - ifnull(par.royalty, 0))),2)"
            selectStr = """
                ifnull(cast(sum(u.downloads) AS CHAR), '-') AS $ALIAS_DOWNLOADS,
                ifnull(cast(sum(u.promo) AS CHAR), '-') AS $ALIAS_PROMO,
                if(
                    sum(gp.price IS NULL),
                    '-',
                    ifnull($revenue, '-')
                ) AS $ALIAS_REVENUE,
                ifnull(cast(sum(u.updates) AS CHAR), '-') AS $ALIAS_UPDATES,
                ifnull(cast(sum(u.refunds) AS CHAR), '-') AS $ALIAS_REFUNDS,
                if (
                    sum(gp.price IS NULL),
                    '-',
                    ifnull($refundsMonetized, '-')
                ) AS $ALIAS_REFUNDS_MONETIZED
            """
        }

        def sqlQuery = """
            SELECT
                ${selectStr}
            FROM ${tableName} ${tableAlias}
                INNER JOIN game g ON g.id = ${tableAlias}.game_id
                INNER JOIN store st ON st.id = g.store_id
                LEFT JOIN game_period gp USING(game_id)
                LEFT JOIN partner par ON par.id = g.partner_id
            WHERE
                ${tableAlias}.game_id NOT IN ($gameIdList)
                AND ${tableAlias}.country_id NOT IN ($countryIdList)
                ${dateBlock}
        """

        log.debug sqlQuery;

        Sql db = new Sql(dataSource_zeptostats);
        def result = db.rows(
            sqlQuery,
            [
                from_Timestamp: fromTimestamp,
                to_Timestamp: toTimestamp
            ]
        );
        db.close();

        def row = result == null || result.empty ? null : result.get(0);
        return row;
    }

    private String fromIdList(final List<Long> list) {
        return (list && list.size > 0) ? list.join(",").replaceAll(/[\[\]\{\}\(\)]/, "") : DUMMY_ID;
    }

    /**
     *
     * @param needStepSafe Step
     * @param tableAlias String table alias
     * @return String which contains FIRST(without leading comma) groupBY's part to group by date table with tableAlias for need step
     */
    private String configureFirstGroupByStep(final Step needStepSafe, final String tableAlias) {

        def dateAlias = tableAlias.equals("c") ? "date" : "start_date"
        def dateFromUnix = "FROM_UNIXTIME(${tableAlias}.${dateAlias} / 1000)"

        def grouping = ""
        switch (needStepSafe) {
            case Step.DAILY:
                grouping = "DATE($dateFromUnix)"
                break
            case Step.WEEKLY:
                grouping = "YEARWEEK($dateFromUnix, 1)"
                break
            case Step.MONTHLY:
                grouping = "CONCAT(YEAR($dateFromUnix),MONTH($dateFromUnix),'-')"
                break
            default:
                grouping = "DATE($dateFromUnix)"
        }

        return grouping
    }

    /**
     * @param needStepSafe Step
     * @param tableAlias String table alias
     * @return String which contains NON-FIRST(with leading comma) select's part to fetch from-to-string from table with tableAlias for need step
     */
    private String configureFirstSelectDates(final Step needStepSafe, final String tableAlias) {

        def dateAlias = tableAlias.equals("c") ? "date" : "start_date"
        def dateFromUnix = "FROM_UNIXTIME(${tableAlias}.${dateAlias} / 1000)"

        def from = null
        def to = null
        def separator = "''"
        def dateFormat = "''"
        switch (needStepSafe) {
            case Step.DAILY:
                from = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            case Step.WEEKLY:
                from = "DATE_SUB($dateFromUnix, INTERVAL WEEKDAY($dateFromUnix) DAY)"
                to   = "DATE_ADD($dateFromUnix, INTERVAL (6 - WEEKDAY($dateFromUnix)) DAY)"
                dateFormat = DATE_FORMAT_HUMAN_DAY
                separator = DATE_FORMAT_HUMAN_SEPARATOR
                break
            case Step.MONTHLY:
                from = "DATE_SUB(LAST_DAY($dateFromUnix), INTERVAL DAY(LAST_DAY($dateFromUnix)) - 1 DAY)"
                dateFormat = DATE_FORMAT_HUMAN_MONTH
                break
            default:
                from = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
        }

        from = from ? "DATE_FORMAT($from, $dateFormat)" : "''"
        to = to ? "DATE_FORMAT($to, $dateFormat)" : "''"

        return "CONCAT($from, $separator, $to) AS $ALIAS_DATES"
    }

    private String configureFirstSelectStartDate(final Step needStepSafe, final String tableAlias) {

        def dateFromUnix = "FROM_UNIXTIME(${tableAlias}.date / 1000)"

        def from = null
        def dateFormat = "''"
        switch (needStepSafe) {
            case Step.DAILY:
                from = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            case Step.WEEKLY:
                from = "DATE_SUB($dateFromUnix, INTERVAL WEEKDAY($dateFromUnix) DAY)"
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            case Step.MONTHLY:
                from = "DATE_SUB(LAST_DAY($dateFromUnix), INTERVAL DAY(LAST_DAY($dateFromUnix)) - 1 DAY)"
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            default:
                from = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
        }

        from = from ? "DATE_FORMAT($from, $dateFormat)" : "''"

        return "$from AS $ALIAS_START_DATE"
    }

    private String configureFirstSelectEndDate(final Step needStepSafe, final String tableAlias) {

        def dateFromUnix = "FROM_UNIXTIME(${tableAlias}.date / 1000)"

        def to = null
        def dateFormat = "''"
        switch (needStepSafe) {
            case Step.DAILY:
                to = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            case Step.WEEKLY:
                to   = "DATE_ADD($dateFromUnix, INTERVAL (6 - WEEKDAY($dateFromUnix)) DAY)"
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            case Step.MONTHLY:
                to = "LAST_DAY($dateFromUnix)"
                dateFormat = DATE_FORMAT_HUMAN_DAY
                break
            default:
                to = dateFromUnix
                dateFormat = DATE_FORMAT_HUMAN_DAY
        }

        to = to ? "DATE_FORMAT($to, $dateFormat)" : "''"

        return "$to AS $ALIAS_END_DATE"
    }

    private String configureFinalSelect(
        final List<ExportColumn> orderedExportColumns, final Map<String, String> finalSelect
    ) {

        List<String> selectItems = []
        orderedExportColumns.each() {
            def code = it.toString()
            def selectItem = finalSelect.get(code);
            if (selectItem != null && !selectItem.isEmpty()) {
                selectItems.add(selectItem)
            }
        }

        return selectItems?.isEmpty() ? "" : "${selectItems.join(", ")}"
    }

    private String configureFinalOrderBy(
        final List<ExportColumn> orderedExportColumns, final String dateAlias, final metaTableAlias
    ) {

        List<String> orderByItems = []
        orderedExportColumns.each() {
            if (it.isSortable) {
                def alias = it.alias;
                def order = ASC;
                if (ExportColumnType.DATE.equals(it.exportColumnType)) {
                    alias = dateAlias;
                    order = DESC;
                }
                if (ExportColumn.META_SKU.equals(it)) {
                    orderByItems.add("${metaTableAlias}.${ALIAS_IS_INAPP} $ASC")
                }
                if (!orderByItems.contains("${alias} ${order}")) {
                    orderByItems.add("${alias} ${order}")
                }
            }
        }

        return orderByItems?.isEmpty() ? "" : "ORDER BY ${orderByItems.join(", ")}"
    }

    private String configureSelectSumNullSafe(final String summand, final String alias) {
        return ", if(sum(${summand} is null), NULL, ROUND(sum(${summand}),2)) AS ${alias}"
    }

    /**
     *
     * @param alias alias
     * @return String which contains FIRST(without leading comma) select's part (value or None if value is null)
     */
    private String configureSelectWithNoneFirst(final String alias) {
        return "ifnull(${alias}, ${NONE_STR}) AS $alias"
    }

    private String configureGroupBy(final String alias) {
        return ", ${alias}"
    }

    private Map<String, String> configureMapByListWithEmptyValues(final List<String> needList) {

        def map = [:];
        needList?.each () {
            map.putAt(it, "");
        }

        return map;
    }

    private Map<String, Boolean> configureNecessaryOrNotRelations(final List<ExportColumn> orderedExportColumns) {

        def isGameTypeNecessary =            false;
        def isResolutionNecessary =          false;
        def isRegionNecessary =              false;
        def isCountryNecessary =             false;
        def isPlatformNecessary =            false;
        def isStoreNecessary =               false;
        def isMetaGameNecessary =            false;
        def isGameNecessary =                false;
        def isParentGameNecessary =          false;
        def isPartnerNecessary =             false;
        def isRankingNecessary =             false;
        def isRequiredProductTypeNecessary = false;

        if (orderedExportColumns.contains(ExportColumn.REVENUE) || orderedExportColumns.contains(ExportColumn.REFUNDS_MONETIZED)) {
            isGameNecessary = true;
            isStoreNecessary = true;
            isPartnerNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.SKU)) {
            isGameTypeNecessary = true;
            isResolutionNecessary = true;
            isPlatformNecessary = true;
            isStoreNecessary = true;
            isMetaGameNecessary = true;
            isGameNecessary = true;
            isParentGameNecessary = true;
            isRequiredProductTypeNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.META_SKU)) {
            isGameNecessary = true;
            isMetaGameNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.COUNTRY) || orderedExportColumns.contains(ExportColumn.REGION)) {
            isRegionNecessary = true;
            isCountryNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.VERSION)) {
            isGameNecessary = true;
            isGameTypeNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.STYLE)) {
            isGameNecessary = true;
            isResolutionNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.OS) || orderedExportColumns.contains(ExportColumn.STORE)) {
            isGameNecessary = true;
            isStoreNecessary = true;
            isPlatformNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.RANKINGS)) {
            isRankingNecessary = true;
        }
        if (orderedExportColumns.contains(ExportColumn.REQUIRED_PRODUCT_TYPE)) {
            isGameNecessary = true;
            isRequiredProductTypeNecessary = true;
        }

        return [
            (CODE_GAMETYPE): isGameTypeNecessary,
            (CODE_RESOLUTION): isResolutionNecessary,
            (CODE_REGION): isRegionNecessary,
            (CODE_COUNTRY): isCountryNecessary,
            (CODE_PLATFORM): isPlatformNecessary,
            (CODE_STORE): isStoreNecessary,
            (CODE_METAGAME): isMetaGameNecessary,
            (CODE_GAME): isGameNecessary,
            (CODE_PARENT_GAME): isParentGameNecessary,
            (CODE_PARTNER): isPartnerNecessary,
            (CODE_RANKINGS): isRankingNecessary,
            (CODE_REQUIRED_PRODUCT_TYPE): isRequiredProductTypeNecessary
        ]
    }

    private String configureDateBlock(final Boolean isNeedMonthlyFinancial) {
        def dateBlock = ""
        if (isNeedMonthlyFinancial) {
            dateBlock = """
                AND f.start_date BETWEEN :from_Timestamp AND :to_Timestamp
                AND f.end_date BETWEEN :from_Timestamp AND :to_Timestamp
                            AND (
                                (gp.start_date IS NULL AND gp.end_date IS NULL)
                                OR (gp.start_date IS NOT NULL AND gp.start_date <= f.end_date AND gp.end_date IS NULL)
                                OR (gp.start_date IS NULL AND gp.end_date IS NOT NULL AND gp.end_date >= f.start_date)
                                OR (gp.start_date IS NOT NULL AND gp.end_date IS NOT NULL 
                                    AND ((gp.start_date <= f.start_date AND gp.end_date >= f.start_date) 
                                        OR (gp.start_date >= f.end_date AND gp.end_date <= f.end_date)
                                        )
                                    )
                            )
            """
        } else {
            dateBlock = """
                AND u.start_date BETWEEN :from_Timestamp AND :to_Timestamp
                AND (
                    (gp.start_date IS NULL AND gp.end_date IS NULL)
                    OR (gp.start_date IS NOT NULL AND gp.start_date <= u.start_date AND gp.end_date IS NULL)
                    OR (gp.start_date IS NULL AND gp.end_date IS NOT NULL AND gp.end_date >= u.start_date)
                    OR (gp.start_date IS NOT NULL AND gp.end_date IS NOT NULL AND gp.start_date <= u.start_date AND gp.end_date >= u.start_date)
                )
            """
        }

        return dateBlock
    }
}