package zeptostats

import java.util.Calendar;
import org.apache.commons.lang.time.DateFormatUtils;
import java.text.SimpleDateFormat;


class TimeService {

    // Look at SQL-queries also. It should be same to firstDayOfWeek of used SQL mode.
    public static final int FIRST_DAY_OF_WEEK = Calendar.MONDAY;
    // Look at SQL-queries also.
    public static final int FIRST_DAY_OF_MONTH = 1;
    // Look at SQL-queries also. It is connected with FIRST_DAY_OF_WEEK.
    public static final int LAST_DAY_OF_WEEK = Calendar.SUNDAY;

    public final static int DAYS_AMOUNT_IN_WEEK = 7;

//    public final static String FULL_TIME_FORMAT_STRING = "EEE MMM dd yyyy hh:mm:ss z";
    public final static String TIME_FORMAT_SIMPLE = "hhmmss";
    public final static String DATE_FORMAT_SIMPLE = "yyyy-MM-dd";
    public final static String DATE_FORMAT_DAY_MONTH_YEAR = "d MMM ''yy";
    public final static String DATE_FORMAT_DAY_MONTH_YEAR_FULL = "d MMMM yyyy";
    public final static String DATE_FORMAT_DAY_MONTH_CURRENT_YEAR = "d MMM";
    public final static String DATE_FORMAT_DAY_MONTH_SHORT_YEAR_FULL = "d MMM yyyy";
    public final static String DATE_FORMAT_DAY_MONTH_SHORT_YEAR_FULL_TIME = "d MMM yyyy hh:mm:ss";

    public final static TimeZone TIMEZONE_UTC = TimeZone.getTimeZone("UTC");
    public final static Locale LOCALE = Locale.ENGLISH;

    def formatTimestampToUtc(final Long timestamp, final String format) {
        return this.formatTimestamp(timestamp, format, TIMEZONE_UTC);
    }

    private def formatTimestamp(final Long timestamp, final String format, final TimeZone timeZone) {

        if (timestamp == null) {
            return "";
        }

        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setTimeInMillis(timestamp);

        return DateFormatUtils.format(calendar, format, timeZone, LOCALE);
    }

//    def simpleFormatTimestamp(final Long timestamp) {
//        return this.formatTimestamp(timestamp, DATE_FORMAT_SIMPLE, TIMEZONE_UTC);
//    }

//    def fullFormatTimestamp(final Long timestamp) {
//        return this.formatTimestamp(timestamp, FULL_TIME_FORMAT_STRING, TIMEZONE_UTC);
//    }

//    def Long getTimestampByFullFormat(final String dateString) {
//
//        //TODO:: Think about removing this hardcode
//        Long timestamp = null;
//        if (dateString != null) {
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FULL_TIME_FORMAT_STRING, LOCALE);
//
//            def needDateString = "";
//            if (dateString.contains("+")) {
//                needDateString = dateString.substring(0, dateString.lastIndexOf("+"));
//            } else if (dateString.contains("-")) {
//                needDateString = dateString.substring(0, dateString.lastIndexOf("-"));
//            }
//
//            try {
//                def date = simpleDateFormat.parse(needDateString);
//                timestamp = date.getTime().toLong();
//            } catch (Exception exc) {
//            }
//        }
//        return timestamp;
//    }

    def Long getTimestamp(final String dateString, final String format) {

        Long timestamp = null;
        if (dateString != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, LOCALE);
            simpleDateFormat.setTimeZone(TIMEZONE_UTC);

            try {
                def date = simpleDateFormat.parse(dateString);
                timestamp = date.getTime().toLong();
            } catch (Exception exc) {
            }
        }
        return timestamp;
    }

    def isTimestampCurrentYear(final Long timestamp) {

        new Date().getYear().equals(new Date(timestamp).getYear());
    }

    def formatSimpleTodayUtcTimestamp() {

        def timestamp = this.getTodayUtcTimestamp();
        return this.formatTimestampToUtc(timestamp, DATE_FORMAT_SIMPLE);
    }

    def formatSimpleTodayTimeUtcTimestamp() {

        def timestamp = this.getTodayUtcTimestamp();
        return this.formatTimestampToUtc(timestamp, TIME_FORMAT_SIMPLE);
    }

    def getTodayUtcTimestamp() {

        Calendar calendar = this.configureCalendarByDate(new Date());
        calendar.getTimeInMillis();
    }

    def getMonthAgoUtcTimestamp() {

        //TODO:: fix it
        Calendar calendar = this.configureCalendarByDate(new Date().minus(30));
        calendar.getTimeInMillis();
    }

    def Long getTimestampNextDay(final timestamp) {

        def calendar = this.configureCalendar(timestamp);
        calendar.add(Calendar.DATE, 1);
        calendar.getTimeInMillis();
    }

    def Long getTimestampPreviousOrCurWeekDay(final timestamp, final int needWeekDay) {
        return this.getTimestampWeekDayWide(timestamp, needWeekDay, false, true);
    }

    def Long getTimestampNextOrCurWeekDay(final timestamp, final int needWeekDay) {
        return this.getTimestampWeekDayWide(timestamp, needWeekDay, true, true);
    }

    def Long getTimestampNextWeekDay(final timestamp, final int needWeekDay) {
        return this.getTimestampWeekDayWide(timestamp, needWeekDay, true, false);
    }

    /**
     * Returns calendar with timestamp:
     * 1. if isCurNeed = true and input timestamp is needWeekDay then input timestamp
     * 2. otherwise:
     * 2.b. if isNextNeed = false, then previous needWeekDay,
     * 2.c. if isNextNeed = true, then future needWeekDay
     * 
     * @param timestamp
     * @param needWeekDay
     * @param isNextNeed
     * @param isCurNeed
     * @return timestamp
     */
    private def Long getTimestampWeekDayWide(
        final timestamp, final int needWeekDay, final boolean isNextNeed, final boolean isCurNeed
    ) {

        def calendar = this.configureCalendar(timestamp);
        def weekDay = calendar.get(Calendar.DAY_OF_WEEK);

        if (weekDay != needWeekDay) {
            calendar.set(Calendar.DAY_OF_WEEK, needWeekDay);
            if (weekDay < needWeekDay) {
                calendar.add(Calendar.DATE, -DAYS_AMOUNT_IN_WEEK);
            }
        }
        //NOTE:: calendar contains needWeekDay of current or previous week.

        if (isCurNeed && weekDay == needWeekDay) {
            
        } else if (isNextNeed) {
            calendar.add(Calendar.DATE, DAYS_AMOUNT_IN_WEEK);
        }
        calendar.getTimeInMillis();
    }

    private def Calendar getCalendarPreviousOrCurMonthDay(final timestamp, final int needMonthDay) {
        return this.getTimestampMonthDayWide(timestamp, needMonthDay, false, true);
    }

    private def Calendar getCalendarNextMonthDay(final timestamp, final int needMonthDay) {
        return this.getTimestampMonthDayWide(timestamp, needMonthDay, true, false);
    }

    def Long getTimestampPreviousOrCurMonthDay(final timestamp, final int needMonthDay) {
        return this.getCalendarPreviousOrCurMonthDay(timestamp, needMonthDay).getTimeInMillis();
    }

    def Long getTimestampNextMonthDay(final timestamp, final int needMonthDay) {
        return this.getCalendarNextMonthDay(timestamp, needMonthDay).getTimeInMillis();
    }

    /**
     * Returns calendar with timestamp:
     * 1. if input timestamp is (nextNeedMonthDay - 1) day then input timestamp
     * 2. otherwise the future (nextNeedMonthDay - 1) day
     * 
     * @param timestamp
     * @param nextNeedMonthDay the next MonthDay after need one
     * @return timestamp
     */
    def Long getTimestampNextMonthPrevDay(final timestamp, final int nextNeedMonthDay) {

        def calendar = this.getCalendarNextMonthDay(timestamp, nextNeedMonthDay);
        calendar.add(Calendar.DATE, -1);
        return calendar.getTimeInMillis();
    }

    /**
     * Returns calendar with timestamp:
     * 1. if isCurNeed = true and input timestamp is needMonthDay then input timestamp
     * 2. otherwise:
     * 2.b. if isNextNeed = false, then previous needMonthDay,
     * 2.c. if isNextNeed = true, then future needMonthDay
     * 
     * @param timestamp
     * @param needMonthDay
     * @param isNextNeed
     * @param isCurNeed
     * @return timestamp
     */
    private def Calendar getTimestampMonthDayWide(
        final timestamp, final int needMonthDay, final boolean isNextNeed, final boolean isCurNeed
    ) {

        def calendar = this.configureCalendar(timestamp);
        def monthDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (monthDay != needMonthDay) {
            calendar.set(Calendar.DAY_OF_MONTH, needMonthDay);
            if (monthDay < needMonthDay) {
                calendar.add(Calendar.MONTH, -1);
            }
        }
        //NOTE:: calendar contains needMonthDay of current or previous month.

        if (isCurNeed && monthDay == needMonthDay) {
            
        } else if (isNextNeed) {
            calendar.add(Calendar.MONTH, 1);
        }
        return calendar;
    }

    def Calendar configureCalendar(timestamp) {

        Calendar calendar = Calendar.getInstance(TIMEZONE_UTC, LOCALE);
        calendar.setTimeInMillis(timestamp);
        calendar;
    }

    private def Calendar configureCalendarByDate(final Date date) {

        Calendar calendar = Calendar.getInstance(TIMEZONE_UTC, LOCALE);
        calendar.setTime(date);
        calendar;
    }

    /**
     * Rounds timestamps by need step.
     *   if step=Daily, then timestamps will not be modified.
     *   if step=Weekly, then timestamps will be modified. Period will become:
     *      from first day of first associated week
     *      to end day of end associated week.
     *   if step=Monthly, then timestamps will be modified. Period will become:
     *      from first day of first associated month
     *      to end day of end associated month.
     * 
     * @param validStep Step need Step
     * @param fromTimestamp Long timestamp from
     * @param toTimestamp Long timestamp to
     * @return map [
     *      fromTimestamp: Long round timestamp from
     *      toTimestamp: Long round timestamp to
     * ]
     */
    def roundTimestampsByStep(final Step validStep, final Long fromTimestamp, final Long toTimestamp) {

        def result = [:]

        def Long roundFromTimestamp = null;
        def Long roundToTimestamp = null;

        switch (validStep) {
            case Step.WEEKLY:
                roundFromTimestamp = this.getTimestampPreviousOrCurWeekDay(fromTimestamp, FIRST_DAY_OF_WEEK);
                roundToTimestamp = this.getTimestampNextOrCurWeekDay(toTimestamp, LAST_DAY_OF_WEEK);
                break;
            case Step.MONTHLY:
                roundFromTimestamp = this.getTimestampPreviousOrCurMonthDay(fromTimestamp, FIRST_DAY_OF_MONTH);
                roundToTimestamp = this.getTimestampNextMonthPrevDay(toTimestamp, FIRST_DAY_OF_MONTH);
                break;
            default:
                roundFromTimestamp = fromTimestamp;
                roundToTimestamp = toTimestamp;
        }

        result = [
            fromTimestamp: roundFromTimestamp,
            toTimestamp: roundToTimestamp
        ];

        return result
    }
}
