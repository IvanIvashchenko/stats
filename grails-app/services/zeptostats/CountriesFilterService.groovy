package zeptostats

import groovy.sql.Sql

class CountriesFilterService {

    def dataSource_zeptostats;
    def dataSource;
    def timeService;

    private String DEFAULT_COUNTRIES_CODE = "'US','RU','GB'";

    def fetchTopCountriesWithRegion() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                country.*
                , r.name as region
            from
                country as country
                inner join region r on r.id=country.region_id
            where country.is_top=true
        """;
        def rows = db.rows(sqlQuery);
        def result = rows.groupBy{ it.region };
        db.close();
        result;
    }

    def fetchAllTopCountriesIds() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """select country.id as cid from country as country
                            where country.is_top=true
                        """;
        def rows = db.rows(sqlQuery);
        def grouped = rows.groupBy{ it.cid };
        grouped.cid;
    }

    def fetchAllAppsWithReportUIName() {

        def db = new Sql(dataSource);
        def sqlQuery = """select game.*,reportUI.name from game as game
                            inner join report_ui reportUI on reportUI.id=game.report_ui_id""";
        db.rows(sqlQuery);
    }

    def saveUserCountriesFilter(countriesIds, Long memberId) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from countries_geo_filter where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId.toString()]);
        def sqlAddNewQuery = """insert into countries_geo_filter (member_id,country_id) values (?, ?)""";
        countriesIds.each() {

            if (!it.equals("")){
                db.executeUpdate(sqlAddNewQuery, [memberId.toString(), it]);
            }
        }
    }

    def saveUserGamesFilter(appsIds, Long memberId) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from countries_apps_filter where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId.toString()]);
        def sqlAddNewQuery = """insert into countries_apps_filter (member_id,game_id) values (?, ?)""";
        appsIds.each() {

            if (!it.equals("")){
              db.executeUpdate(sqlAddNewQuery, [memberId.toString(), it]);
            }
        }
    }

    def saveUserTimeFilter(final Long from, final Long to, Long memberId) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from countries_time_filter where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId.toString()]);
        def sqlAddNewQuery = """insert into countries_time_filter (member_id, from_time, to_time) values (?, ?, ?)""";
        db.executeUpdate(sqlAddNewQuery,[memberId.toString(), from, to]);
    }

    def saveUserTotalFilter(String total, Long memberId) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from countries_total_filter where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId.toString()]);
        def sqlAddNewQuery = """insert into financeui.countries_total_filter (member_id, total_filter) values (?, ?)""";
        db.executeUpdate(sqlAddNewQuery, [memberId.toString(), total]);
    }

    def selectDefaultFilterCountries() {
        def db = new Sql(dataSource_zeptostats);
        def sqlSelectedItemsQuery = """select id as country_id from country where code in ($DEFAULT_COUNTRIES_CODE)""";
        db.rows(sqlSelectedItemsQuery,[]);
    }

    def fetchSavedCountriesForFilter(Long memberId) {

        def result = null;
        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select country_id from countries_geo_filter where member_id=?""";
        def rows = db.rows(sqlSelectedItemsQuery,[memberId.toString()]);
        if (rows.size() == 0) {
            rows = this.selectDefaultFilterCountries();
        }
        rows.collect{ it.country_id };
    }

    def selectDefaultFilterGames() {
        def db = new Sql(dataSource_zeptostats);
        def sqlSelectedItemsQuery = """select id as game_id from game where parent_game_id is null""";
        db.rows(sqlSelectedItemsQuery,[]);
    }

    def fetchSavedGamesForFilter(Long memberId) {

        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select game_id from countries_apps_filter where member_id=?""";
        def rows = db.rows(sqlSelectedItemsQuery,[memberId.toString()]);
        if (rows.size() == 0) {
            rows = this.selectDefaultFilterGames();
        }
        rows.collect{ it.game_id };
    }

    def fetchSavedTotalForFilter(Long memberId) {

        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select total_filter from countries_total_filter where member_id=?""";
        def rows = db.rows(sqlSelectedItemsQuery,[memberId.toString()]);
        rows.size == 0 ? "choosen" : rows.first().total_filter
    }

    def fetchSavedTimeForFilter(Long memberId) {

        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select from_time,to_time from countries_time_filter where member_id=?""";
        def savedTime = db.firstRow(sqlSelectedItemsQuery, [memberId.toString()]);

        if (!savedTime) {

            def now = timeService.getTodayUtcTimestamp();
            def monthAgo = timeService.getMonthAgoUtcTimestamp();
            savedTime = [from_time: monthAgo, to_time: now];
        }

        savedTime;
    }

    private configureAppsForFilter(apps) {

        def result = [:];
        apps = apps.groupBy{ it.store };
        apps.each() {

           def storeName = it.key;
           def storeApps = it.value.groupBy{ it.title };
           result.putAt(storeName, storeApps);
        }
        result;
    }

    def getAppsForFilter() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """select store.name as store,game.id,game.extra,resolution.name as resolution,
                                    CONCAT(mgame.name,' ',game_type.name) as title from game as game
                            inner join meta_game mgame on game.meta_game_id=mgame.id
                            inner join store store on store.id=game.store_id
                            inner join game_type game_type on game_type.id=game.game_type_id
                            inner join resolution resolution on resolution.id=game.resolution_id
                            where game.parent_game_id is null
                        """;
        def rows = db.rows(sqlQuery);

        configureAppsForFilter(rows);
    }
}