package zeptostats

import groovy.sql.GroovyRowResult;
import groovy.sql.Sql

class FilterEntitiesService {

    private final static int CUT_NAME_MAX_LENGTH = 56;
    private final static String CUT_NAME_REPLACER = "...";

    private final static String DATEPICKER_STRING_DATE_FORMAT_NOT_RANGE = TimeService.DATE_FORMAT_DAY_MONTH_YEAR_FULL;
    private final static String DATEPICKER_STRING_DATE_FORMAT_RANGE = TimeService.DATE_FORMAT_DAY_MONTH_YEAR;
    private final static String DATEPICKER_STRING_DATE_FORMAT_RANGE_CURRENT_YEAR = TimeService.DATE_FORMAT_DAY_MONTH_CURRENT_YEAR;
    private final static String DATEPICKER_DATE_FORMAT = TimeService.DATE_FORMAT_SIMPLE;
    private final static String DATEPICKER_FORMAT_RANGE = "from %s to %s";
    private final static String DATEPICKER_FORMAT_NOT_RANGE = "%s";

    final static APPLICATION_TYPE = "Application";
    final static IN_APP_TYPE = "In-App";
    private final static List<String> REQUIRED_PRODUCT_TYPES = [APPLICATION_TYPE, IN_APP_TYPE];

    final static PAID = "PAID";
    final static LITE = "LITE";
    final static FREE = "FREE";
    private final static List<String> GAME_TYPES = [PAID, LITE, FREE];

    final static HD = "HD";
    final static SD = "SD";
    final static DESKTOP = "DESKTOP"
    private static final List<String> RESOLUTIONS = [HD, SD, DESKTOP];

    def dataSource_zeptostats;
    def timeService;
    def longService;

    def fetchAllSkusWithMetaGames() {

        def db = new Sql(dataSource_zeptostats);

/*
        def sqlQuery = """
            select
                mg.*
                , IF(
                    LENGTH(mg.name) <= :cutNameMaxLen,
                    mg.name,
                    CONCAT (SUBSTRING(mg.name FROM 1 FOR (:cutNameMaxLen - :cutNameReplacerLen)), :cutNameReplacer)
                ) as cutName
                , REPLACE(mg.name, ':', '') as logicName
            from
                meta_game mg
            order by mg.is_inapp, mg.name, logicName
        """;
*/

        def sqlQuery = """
            select
                g.*
                , IF(
                    LENGTH(g.title) <= :cutNameMaxLen,
                    g.title,
                    CONCAT (SUBSTRING(g.title FROM 1 FOR (:cutNameMaxLen - :cutNameReplacerLen)), :cutNameReplacer)
                ) as cutName
                , CONCAT ('[', s.name, '] ', g.title) as fullName
                , mg.name as metaSkuName
                , mg.id as metaSkuId
                , REPLACE(g.title, ':', '') as logicName
            from
                game g
                inner join meta_game mg on mg.id=g.meta_game_id
                inner join store s on s.id=g.store_id
            order by mg.is_inapp, mg.name, logicName, g.title
        """;
        def rows = db.rows(
            sqlQuery,
            [
                cutNameMaxLen: CUT_NAME_MAX_LENGTH,
                cutNameReplacerLen: CUT_NAME_REPLACER.length(),
                cutNameReplacer: CUT_NAME_REPLACER
            ]
        );
        def result = rows.groupBy{ it.metaSkuId };
        db.close();
        result;
    }

    def fetchAllStores() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select * from store
            order by name
        """;
        def rows = db.rows(sqlQuery);
        db.close();
        rows;
    }

    def fetchAllStoresWithPlatforms() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                  store.*
                , platform.name as platformName
                , platform.id as platformId
            from
                store
                inner join platform platform on platform.id=store.platform_id
            order by platform.name,store.name
        """;
        def rows = db.rows(sqlQuery);
        def result = rows.groupBy{ it.platformId };
        db.close();
        result;
    }

    def fetchAllGameTypes() {
        return GAME_TYPES;
    }

    def fetchAllResolutions() {
        return RESOLUTIONS;
    }

    def fetchAllCountriesWithRegionAndTopRegion() {

        def countriesAllWithRegion = fetchAllCountriesWithRegion();
        def countriesTop = fetchTopCountries();

        def resultList = countriesAllWithRegion;
        resultList.addAll(countriesTop);

        def result = resultList.groupBy{ it.regionId };

        result;
    }

    private def fetchAllCountriesWithRegion() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                country.*
                , r.name as regionName
                , r.id as regionId
            from
                country as country
                inner join region r on r.id=country.region_id
        """;
        def rows = db.rows(sqlQuery);
        def result = rows;
        db.close();
        result;
    }

    private def fetchTopCountries() {

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                country.*
                , "Top Countries" as regionName
                , 0 as regionId
            from
                country as country
            where country.is_top=true
        """;
        def rows = db.rows(sqlQuery);
        def result = rows;
        db.close();
        result;
    }

    /**
     *
     * @param ids List<Long> list of ids
     * @param table String name of table
     * @param column String name of column which will be selected as "name"
     * @param isIn boolean true if items with input ids are need; false if items with other ids are need
     * @return List<GroovyRowResult> list {
     *      GroovyRowResult {
     *          "id" => Long item's id,
     *          "name" => String item's name,
     *      },
     *      ...
     * }
     */
    def List<GroovyRowResult> fetchItemsByIds(final List<Long> ids, final String table, final String column, final boolean isIn) {
        return this.fetchByIds(ids, table, "${column} as name", isIn);
    }

    def List<Long> fetchItemsIdsByIds(final List<Long> ids, final String table, final boolean isIn) {

        def items = this.fetchByIds(ids, table, "", isIn);
        def idsOther = items.collect() { it.id }
        return idsOther;
    }

    def List<String> fetchRequiredProductTypes() {
        return REQUIRED_PRODUCT_TYPES;
    }

    protected def List<GroovyRowResult> fetchByIds(final List<Long> ids, final String table, final String andSelect, final boolean isIn) {

        def idsStr = longService.configureIdsString(ids);
        def notInStr = isIn ? "" : "not"
        def andSelectStr = andSelect == null || andSelect.isEmpty() ? "" : ", ${andSelect}";

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                id
                ${andSelectStr}
            from ${table} t
            where id ${notInStr} in (${idsStr})
        """;

        def result = db.rows(
            sqlQuery,
            []
        );
        db.close();

        return result;
    }

    def List<Long> fetchIndependentItemsIdsByIds(final List<Long> ids, final String table, final String mainTable, final boolean isIn) {

        def items = this.fetchIndependentByIds(ids, table, mainTable, "", isIn);
        def idsOther = items.collect() { it.id }
        return idsOther;
    }

    protected def fetchIndependentByIds(
        final List<Long> ids, final String table, final String mainTable, final String andSelect, final boolean isIn
    ) {

        def idsStr = longService.configureIdsString(ids);
        def notInStr = isIn ? "" : "not"
        def andSelectStr = andSelect == null || andSelect.isEmpty() ? "" : ", t.${andSelect}";

        def db = new Sql(dataSource_zeptostats);
        def sqlQuery = """
            select
                t.id
                ${andSelectStr}
            from
                ${table} t
                left join ${mainTable} main on t.id = main.${table}_id
            where
                t.id ${notInStr} in (${idsStr})
                and main.id IS NULL
        """;

        def result = db.rows(
            sqlQuery,
            []
        );
        db.close();

        return result;
    }

    def List<GroovyRowResult> fetchGamesInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "game", "title", true);
    }

    def List<GroovyRowResult> fetchGamesNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "game", "title", false);
    }

    def List<GroovyRowResult> fetchCountriesInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "country", "name", true);
    }

    def List<GroovyRowResult> fetchCountriesNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "country", "name", false);
    }

    def List<GroovyRowResult> fetchGameTypesInIds(final List<String> ids) {

        def list = ids == null ? [] : ids;

        list = GAME_TYPES.minus(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }

    def List<GroovyRowResult> fetchGameTypesNotInIds(final List<String> ids) {

        def list = ids == null ? [] : ids;

        list = GAME_TYPES.intersect(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }

    def List<GroovyRowResult> fetchResolutionsInIds(final List<String> ids) {

        def list = ids == null ? [] : ids;

        list = RESOLUTIONS.minus(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }

    def List<GroovyRowResult> fetchResolutionsNotInIds(final List<String> ids) {
        def list = ids == null ? [] : ids;

        list = RESOLUTIONS.intersect(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }

    def List<GroovyRowResult> fetchStoresInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "store", "name", true);
    }

    def List<GroovyRowResult> fetchStoresNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "store", "name", false);
    }

    def List<GroovyRowResult> fetchMetaGamesInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "meta_game", "name", true);
    }

    def List<GroovyRowResult> fetchMetaGamesNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "meta_game", "name", false);
    }

    def List<GroovyRowResult> fetchPlatformsInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "platform", "name", true);
    }

    def List<GroovyRowResult> fetchPlatformsNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "platform", "name", false);
    }

    def List<GroovyRowResult> fetchRegionsInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "region", "name", true);
    }

    def List<GroovyRowResult> fetchRegionsNotInIds(final List<Long> ids) {
        return this.fetchItemsByIds(ids, "region", "name", false);
    }

    //NOTE:: hack for required product type again
    def List<GroovyRowResult> fetchRequiredProductTypesInIds(final List<String> ids) {
        def list = ids == null ? [] : ids;

        list = REQUIRED_PRODUCT_TYPES.minus(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }

    def List<GroovyRowResult> fetchRequiredProductTypesNotInIds(final List<String> ids) {
        def list = ids == null ? [] : ids;

        list = REQUIRED_PRODUCT_TYPES.intersect(list);
        List<GroovyRowResult> resultList = [];
        list?.each() {
            resultList.add(new GroovyRowResult(name: it));
        }
        return resultList;
    }


    def List<Long> fetchGamesIdsIn(final List<Long> ids) {
        return this.fetchItemsIdsByIds(ids, "game", true);
    }

    def List<Long> fetchGamesIdsNotIn(final List<Long> ids) {
        return this.fetchItemsIdsByIds(ids, "game", false);
    }

    def List<Long> fetchCountriesIdsIn(final List<Long> ids) {
        return this.fetchItemsIdsByIds(ids, "country", true);
    }

    def List<Long> fetchCountriesIdsNotIn(final List<Long> ids) {
        return this.fetchItemsIdsByIds(ids, "country", false);
    }


    def List<Long> fetchIndependentGameTypesIn(final List<Long> ids) {
        def list = ids == null ? [] : ids;
        return GAME_TYPES.intersect(list);
    }

    def List<Long> fetchIndependentGameTypesNotIn(final List<Long> ids) {
        def list = ids == null ? [] : ids;
        return GAME_TYPES.minus(list);
    }

    def List<Long> fetchIndependentResolutionsIn(final List<Long> ids) {
        def list = ids == null ? [] : ids;
        return RESOLUTIONS.intersect(list);
    }

    def List<Long> fetchIndependentResolutionsNotIn(final List<Long> ids) {
        def list = ids == null ? [] : ids;
        return RESOLUTIONS.minus(list);
    }

    def List<Long> fetchIndependentStoresIdsIn(final List<Long> ids) {
        return this.fetchIndependentItemsIdsByIds(ids, "store", "game", true);
    }

    def List<Long> fetchIndependentStoresIdsNotIn(final List<Long> ids) {
        return this.fetchIndependentItemsIdsByIds(ids, "store", "game", false);
    }

    def List<String> fetchIndependentRequiredProductTypeIdsIn(final List<String> ids) {
        def list = ids == null ? [] : ids;
        return REQUIRED_PRODUCT_TYPES.intersect(list);
    }

    def List<String> fetchIndependentRequiredProductTypeIdsNotIn(final List<String> ids) {
        def list = ids == null ? [] : ids;
        return REQUIRED_PRODUCT_TYPES.minus(list);
    }


    def configureDatepickerString(final fromTimestamp, final toTimestamp) {

        def result = "";
        if (fromTimestamp != null && toTimestamp != null) {
            def fromFormat = null;
            def toFormat = null;
            if (fromTimestamp.equals(toTimestamp)) {
                fromFormat = DATEPICKER_STRING_DATE_FORMAT_NOT_RANGE;
                toFormat = DATEPICKER_STRING_DATE_FORMAT_NOT_RANGE;
            } else {
                toFormat = timeService.isTimestampCurrentYear(toTimestamp) ?
                    DATEPICKER_STRING_DATE_FORMAT_RANGE_CURRENT_YEAR :
                    DATEPICKER_STRING_DATE_FORMAT_RANGE;

                fromFormat = timeService.isTimestampCurrentYear(fromTimestamp) ?
                    DATEPICKER_STRING_DATE_FORMAT_RANGE_CURRENT_YEAR :
                    DATEPICKER_STRING_DATE_FORMAT_RANGE
            }

            def formattedDateToString = timeService.formatTimestampToUtc(toTimestamp, toFormat);
            def formattedDateFromString = timeService.formatTimestampToUtc(fromTimestamp, fromFormat);

            if (fromTimestamp.equals(toTimestamp)) {
                result = String.format(DATEPICKER_FORMAT_NOT_RANGE, formattedDateToString);
            } else {
                result = String.format(DATEPICKER_FORMAT_RANGE, formattedDateFromString, formattedDateToString);
            }
        }

        result;
    }

    def roundTimestampsByStep(final Step validStep, final Long fromTimestamp, final Long toTimestamp) {
        return timeService.roundTimestampsByStep(validStep, fromTimestamp, toTimestamp);
    }

    def formatDatepickerTimestamp(final timestamp) {
        return timeService.formatTimestampToUtc(timestamp, DATEPICKER_DATE_FORMAT);
    }

    def getDatepickerTimestamp(final dateString) {
        return timeService.getTimestamp(dateString, DATEPICKER_DATE_FORMAT);
    }
}