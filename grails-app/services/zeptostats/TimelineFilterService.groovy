package zeptostats;

import java.util.List;

import groovy.sql.Sql;

class TimelineFilterService {

    def dataSource;
    def timeService;
    def compareDependentColumnService;

    def fetchAllUserSettings(final Long memberId) {

        def unnecessaryGameTypeIdsAsIs = this.fetchSavedUserUnnecessaryGameTypeIds(memberId);
        def unnecessaryResolutionIdsAsIs = this.fetchSavedUserUnnecessaryResolutionIds(memberId);
        def unnecessaryGameIdsAsIs = this.fetchSavedUserUnnecessaryGameIds(memberId);
        def unnecessaryCountryIdsAsIs = this.fetchSavedUserUnnecessaryCountryIds(memberId);
        def unnecessaryStoreIdsAsIs = this.fetchSavedUserUnnecessaryStoreIds(memberId);
        def unnecessaryRequiredProductTypeIdsAsIs = this.fetchSavedUserUnnecessaryRequiredProductTypeIds(memberId);

        def Map<CompareColumn, Map<String, List<Long>>> idsInfo = compareDependentColumnService.correctIds(
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypeIdsAsIs,
            unnecessaryResolutionIdsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductTypeIdsAsIs
        );

        List<Long> unnecessaryGameIds = idsInfo?.get(CompareColumn.SKU)?.allStrikedIds;
        List<Long> unnecessaryCountryIds = idsInfo?.get(CompareColumn.COUNTRY)?.allStrikedIds;
        List<Long> unnecessaryGameTypeIds = idsInfo?.get(CompareColumn.VERSION)?.allStrikedIds;
        List<Long> unnecessaryResolutionIds = idsInfo?.get(CompareColumn.STYLE)?.allStrikedIds;
        List<Long> unnecessaryStoreIds = idsInfo?.get(CompareColumn.STORE)?.allStrikedIds;

        def settings = this.fetchSavedUserSettings(memberId);

        //TODO:: think about default values in Timeline
        if (!settings) {
            settings = this.fetchDefaultUserSettings();
        }

        def Step validStep = Step.parse(settings.step);
        def roundTimestamps = timeService.roundTimestampsByStep(validStep, settings.from_timestamp, settings.to_timestamp);

        [
            idsInfo: idsInfo,
            unnecessaryGameTypeIds: unnecessaryGameTypeIds,
            unnecessaryResolutionIds: unnecessaryResolutionIds,
            unnecessaryGameIds: unnecessaryGameIds,
            unnecessaryCountryIds: unnecessaryCountryIds,
            unnecessaryStoreIds: unnecessaryStoreIds,
            unnecessaryRequiredProductTypeIds: unnecessaryRequiredProductTypeIdsAsIs,
            fromTimestamp: roundTimestamps.fromTimestamp,
            toTimestamp: roundTimestamps.toTimestamp,
            step: validStep,
            compareColumn: CompareColumn.parse(settings.compare_column),
            ifNeedAppsDownloads: settings.if_need_apps_downloads,
            ifNeedAppsRevenue: settings.if_need_apps_revenue,
            ifNeedAppsCompare: settings.if_need_apps_compare,
            ifNeedInappsDownloads: settings.if_need_inapps_downloads,
            ifNeedInappsRevenue: settings.if_need_inapps_revenue,
            ifNeedInappsCompare: settings.if_need_inapps_compare,
            ifNeedUpdates: settings.if_need_updates,
            ifNeedUpdatesCompare: settings.if_need_updates_compare,
        ];
    }

    public boolean saveAllUserSettings(
        final Long memberId,
        final List<Long> unnecessaryGameIdsAsIs,
        final List<Long> unnecessaryCountryIdsAsIs,
        final List<Long> unnecessaryGameTypeIdsAsIs,
        final List<Long> unnecessaryResolutionIdsAsIs,
        final List<Long> unnecessaryStoreIdsAsIs,
        final List<String> unnecessaryRequiredProductTypeIdsAsIs,
        final CompareColumn compareColumn,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean ifNeedAppsDownloads,
        final Boolean ifNeedInappsDownloads,
        final Boolean ifNeedAppsRevenue,
        final Boolean ifNeedInappsRevenue,
        final Boolean ifNeedAppsCompare,
        final Boolean ifNeedInappsCompare,
        final Boolean ifNeedUpdates,
        final Boolean ifNeedUpdatesCompare,
        final Step needStep
    ) {

        boolean success = false;

        def Map<CompareColumn, Map<String, List<Long>>> idsInfo = compareDependentColumnService.correctIds(
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypeIdsAsIs,
            unnecessaryResolutionIdsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductTypeIdsAsIs
        );

        List<Long> unnecessaryGameIds = idsInfo?.get(CompareColumn.SKU)?.allStrikedIds;
        List<Long> unnecessaryCountryIds = idsInfo?.get(CompareColumn.COUNTRY)?.allStrikedIds;
        List<Long> unnecessaryGameTypeIds = idsInfo?.get(CompareColumn.VERSION)?.allStrikedIds;
        List<Long> unnecessaryResolutionIds = idsInfo?.get(CompareColumn.STYLE)?.allStrikedIds;
        List<Long> unnecessaryStoreIds = idsInfo?.get(CompareColumn.STORE)?.allStrikedIds;

        try {
            this.saveUserUnnecessaryGameTypeIds(memberId, unnecessaryGameTypeIds);
            this.saveUserUnnecessaryResolutionIds(memberId, unnecessaryResolutionIds);
            this.saveUserUnnecessaryCountryIds(memberId, unnecessaryCountryIds);
            this.saveUserUnnecessaryGameIds(memberId, unnecessaryGameIds);
            this.saveUserUnnecessaryStoreIds(memberId, unnecessaryStoreIds);
            this.saveUserUnnecessaryRequiredProductTypeIds(memberId, unnecessaryRequiredProductTypeIdsAsIs);
            this.saveUserSettings(
                memberId,
                compareColumn,
                fromTimestamp,
                toTimestamp,
                ifNeedAppsDownloads,
                ifNeedInappsDownloads,
                ifNeedAppsRevenue,
                ifNeedInappsRevenue,
                ifNeedAppsCompare,
                ifNeedInappsCompare,
                ifNeedUpdates,
                ifNeedUpdatesCompare,
                needStep
            );
            success = true;
        } catch (Exception exc) {
            System.out.println(exc);
        }

        return success;
    }

    private def fetchDefaultUserSettings() {

        def defaults = [:];
        defaults.put("from_timestamp", timeService.getMonthAgoUtcTimestamp());
        defaults.put("to_timestamp", timeService.getTodayUtcTimestamp());
        defaults.put("step", Step.DAILY.toString());
        defaults.put("compare_column", CompareColumn.VERSION.toString());
        defaults.put("if_need_apps_downloads", true);
        defaults.put("if_need_apps_revenue", false);
        defaults.put("if_need_apps_compare", true);
        defaults.put("if_need_inapps_downloads", true);
        defaults.put("if_need_inapps_revenue", false);
        defaults.put("if_need_inapps_compare", true);
        defaults.put("if_need_updates", true);
        defaults.put("if_need_updates_compare", true);

        defaults
    }

    private def fetchSavedUserSettings(Long memberId) {

        def db = new Sql(dataSource);
        def sqlSelectedItemsQuery = """select * from timeline_setting where member_id=?""";
        def settings = db.firstRow(sqlSelectedItemsQuery, [memberId]);
        db.close();
        settings;
    }

    private def saveUserSettings(
        final Long memberId,
        final CompareColumn compareColumn,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean ifNeedAppsDownloads,
        final Boolean ifNeedInappsDownloads,
        final Boolean ifNeedAppsRevenue,
        final Boolean ifNeedInappsRevenue,
        final Boolean ifNeedAppsCompare,
        final Boolean ifNeedInappsCompare,
        final Boolean ifNeedUpdates,
        final Boolean ifNeedUpdatesCompare,
        final Step needStep
    ) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_setting where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery, [memberId]);

        def sqlAddNewQuery = """
            insert into timeline_setting
                (member_id, if_need_apps_downloads, if_need_inapps_downloads, if_need_apps_revenue, if_need_inapps_revenue,
                    if_need_apps_compare, if_need_inapps_compare, if_need_updates, if_need_updates_compare, compare_column,
                    step, from_timestamp ,to_timestamp)
                values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """;
        db.executeUpdate(sqlAddNewQuery,[
            memberId,
            ifNeedAppsDownloads,
            ifNeedInappsDownloads,
            ifNeedAppsRevenue,
            ifNeedInappsRevenue,
            ifNeedAppsCompare,
            ifNeedInappsCompare,
            ifNeedUpdates,
            ifNeedUpdatesCompare,
            compareColumn?.toString(),
            needStep?.toString(),
            fromTimestamp,
            toTimestamp
        ]);
        db.close();
    }

    private def saveUserUnnecessaryGameTypeIds(final Long memberId, final List<Long> versionsIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_game_type where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_game_type (member_id, game_type_id) values (?, ?)""";
        versionsIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryGameTypeIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select game_type_id from timeline_filter_game_type where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.game_type_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryResolutionIds(final Long memberId, final List<Long> resolutionIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_resolution where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_resolution (member_id, resolution_id) values (?, ?)""";
        resolutionIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryResolutionIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select resolution_id from timeline_filter_resolution where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.resolution_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryCountryIds(final Long memberId, final List<Long> countriesIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_country where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_country (member_id, country_id) values (?, ?)""";
        countriesIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryCountryIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select country_id from timeline_filter_country where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.country_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryGameIds(final Long memberId, final List<Long> skuIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_game where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_game (member_id, game_id) values (?, ?)""";
        skuIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryGameIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select game_id from timeline_filter_game where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.game_id };
        db.close();
        result;
    }

    private def saveUserUnnecessaryStoreIds(final Long memberId, final List<Long> storesIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_store where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_store (member_id, store_id) values (?, ?)""";
        storesIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }

    private def fetchSavedUserUnnecessaryStoreIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select store_id from timeline_filter_store where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.store_id };
        db.close();
        result;
    }

    private def fetchSavedUserUnnecessaryRequiredProductTypeIds(final Long memberId) {

        def db = new Sql(dataSource);
        def sqlQuery = """select required_product_type from timeline_filter_required_product_type where member_id=?""";
        def rows = db.rows(sqlQuery,[memberId]);
        def result = rows.collect{ it.required_product_type };
        db.close();
        result;
    }

    private def saveUserUnnecessaryRequiredProductTypeIds(final Long memberId, final List<String> requiredProductTypeIds) {

        def db = new Sql(dataSource);
        def sqlDeleteOldQuery = """delete from timeline_filter_required_product_type where member_id=?""";
        db.executeUpdate(sqlDeleteOldQuery,[memberId]);
        def sqlAddNewQuery = """insert into timeline_filter_required_product_type (member_id, required_product_type) values (?, ?)""";
        requiredProductTypeIds.each() {
            db.executeUpdate(sqlAddNewQuery, [memberId, it]);
        }
        db.close();
    }
}