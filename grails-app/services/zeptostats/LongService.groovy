package zeptostats;

class LongService {

    private static final def DUMMY_ID = -1;
    private static final def STRING_ID_SEPARATOR = ",";

    /**
     *
     * @param str String which contains Long
     * @return null or valid parsed Long
     */
    def fetchValidLong(final String str) {

        Long validLong = null;
        try {
            validLong = Long.parseLong(str)
        } catch (Exception exc) {
        }

        return validLong
    }

    /**
     *
     * @param stringsList list of Strings which contains Long
     * @return list of valid parsed Long. It is not null always. It does not contain null as Long.
     */
    def fetchValidIdList(idStringsList) {

        List<Long> ids = new LinkedList<Long>()
        idStringsList?.each() {
            Long validId = fetchValidLong(it)
            if (validId != null) {
                ids.add(validId)
            }
        }

        return ids
    }

    /**
     * 
     * @param idList list (may be empty) with ids
     * @return string with separated ids from idList
     */
    def configureIdsString(idList) {
        return idList ? idList.join(STRING_ID_SEPARATOR) : DUMMY_ID;
    }

    /**
     *
     * @param idList list (may be empty) with strings ids
     * @return string with separated ids from idList
     */
    def configureStringIdsString(idList) {
        return idList ? idList.collect({'"' + it + '"'}).join(STRING_ID_SEPARATOR) : DUMMY_ID;
    }
}