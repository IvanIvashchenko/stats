package zeptostats

import groovy.sql.Sql
import java.util.Calendar
import java.util.TimeZone;
import java.text.SimpleDateFormat

class TimelineService {

    def dataSource_zeptostats;
    def timeService;
    def longService;

    def fetchGamesUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, final Step timeStep) {

        LinkedHashMap<String,Integer> blankUpdateMap = this.createBlankUpdateMap(fromTime, toTime, timeStep);
        def updates = this.fetchUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, timeStep);
        this.mergeUpdates(blankUpdateMap, updates);
    }

    def fetchInAppsDownloads(inappIds, countryIds, fromTime, toTime, final Step timeStep) {
        this.fetchGamesDownloads(inappIds, countryIds, fromTime, toTime, timeStep, false);
    }

    def fetchAppsDownloads(appIds, countryIds, fromTime, toTime, final Step timeStep) {
        this.fetchGamesDownloads(appIds, countryIds, fromTime, toTime, timeStep, true);
    }

    /**
     * 
     * @param gameIds
     * @param countryIds
     * @param fromTime timestamp
     * @param toTime timestamp
     * @param timeStep
     * @param ifApps=true if Apps are need only, ifApps=false if Inapps are need only, ifApps = null if Apps and Inapps are need.
     * @return
     */
    private def fetchGamesDownloads(gameIds, countryIds, fromTime, toTime, final Step timeStep, ifApps) {

        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def countryIdsStr = longService.configureIdsString(countryIds);

        def timeStepStr = this.configureTimeStepString(timeStep, "u.date");
        def andInnerJoinCondition = "";
        if (ifApps != null) {
            andInnerJoinCondition = ifApps ? "and g.parent_game_id is null" : "and g.parent_game_id is not null";
        }

        def sqlQuery = """
            select sum(CASE WHEN (u.downloads is null) THEN 0 ELSE u.downloads END) as downloads
            from
                unit u
                inner join game as g on g.id = u.game_id $andInnerJoinCondition
            where
                u.game_id in ($gameIdsStr)
                and u.country_id in ($countryIdsStr)
                and ((u.date >= :from_date) and (:to_date >= u.date))
            group by concat($timeStepStr)
            order by $timeStepStr
        """;
        def rows = db.rows(sqlQuery, [from_date: fromTime, to_date: toTime]);
        db.close();
        rows;
    }

    def configureUpdatesJson(downloads, totalDownloads, maxDownloads, timeStep, fromTime, toTime) {

        """{
           "height": 140,
           "from":"${fromTime}",
           "to":"${toTime}",
           "axis":[
              {
                 "max": ${maxDownloads},
                 "name":"Downloads",
                 "lines": true,
                 "segments": 3,
                 "step":"${timeStep}",
                 "total": ${totalDownloads}
              }
           ],
           "data":[
              {
                 "axis":0,
                 "name":"Downloads",
                 "fill": true,
                 "total": ${totalDownloads},
                 "type":"graph",
                 "color": "#8d8d8d",
                 "values": ${downloads}
              }
           ]
        }"""
    }

    private def createBlankUpdateMap(fromTime, toTime, final Step validStep) {

        def LinkedHashMap<String, Integer> blankUpdateMap = new LinkedHashMap<String, Integer>();
        def fromTimestamp = -1;
        def toTimestamp = -1;
        switch (validStep) {
            case Step.DAILY:
                fromTimestamp = fromTime;
                toTimestamp = toTime;
                break
            case Step.WEEKLY:
                fromTimestamp = timeService.getTimestampPreviousOrCurWeekDay(fromTime, TimeService.FIRST_DAY_OF_WEEK);
                toTimestamp = timeService.getTimestampPreviousOrCurWeekDay(toTime, TimeService.FIRST_DAY_OF_WEEK);
                break
            case Step.MONTHLY:
                fromTimestamp = timeService.getTimestampPreviousOrCurMonthDay(fromTime, TimeService.FIRST_DAY_OF_MONTH);
                toTimestamp = timeService.getTimestampPreviousOrCurMonthDay(toTime, TimeService.FIRST_DAY_OF_MONTH);
                break
        }

        def db = new Sql(dataSource_zeptostats);
        def timeStepStr = this.configureTimeStepString(validStep, "calendar.date");

        def sqlQuery = """
            SELECT DISTINCT
                CONCAT_WS("-", $timeStepStr) as startStepTime
            FROM calendar calendar
            WHERE
                :fromDate <= calendar.date
                AND calendar.date <= :toDate
            ORDER BY calendar.date
        """;
        def rows = db.rows(sqlQuery,
            [fromDate: fromTimestamp, toDate: toTimestamp]
        );
        db.close();
        rows.each() {
            blankUpdateMap.put(it.startStepTime, 0);
        };

        blankUpdateMap;
    }

    def fetchUpdates(metaGameIds, storeIds, resolutionIds, gameTypeIds, countryIds, fromTime, toTime, final Step timeStep) {

        def db = new Sql(dataSource_zeptostats);
        def timeStepStr = this.configureTimeStepString(timeStep, "u.date");

        def sqlQuery = """
            SELECT
                SUM(IFNULL(u.updates, 0)) AS updates,
                CONCAT_WS("-", $timeStepStr) as startStepTime
            FROM
                unit AS u
                RIGHT JOIN (
                    SELECT *
                    FROM (
                        SELECT g.id AS game_id
                        FROM game AS g
                        WHERE (
                            (g.store_id NOT IN (:storeIds))
                            AND (g.meta_game_id NOT IN (:metaGameIds))
                            AND (g.game_type_id NOT IN (:gameTypeIds))
                            AND (g.resolution_id NOT IN (:resolutionIds))
                        )
                    ) AS game_id,
                    (
                        SELECT c.id AS country_id
                        FROM country AS c
                        WHERE c.id NOT IN (:countryIds)
                    ) AS country_id
                ) AS game_country ON (game_country.game_id = u.game_id AND game_country.country_id = u.country_id)
            WHERE ((:fromDate <= u.date) AND (u.date <= :toDate))
            GROUP BY $timeStepStr
            ORDER BY u.date
        """;
        def rows = db.rows(sqlQuery,
            [fromDate: fromTime, toDate: toTime,storeIds: storeIds, metaGameIds: metaGameIds, gameTypeIds: gameTypeIds,
            resolutionIds: resolutionIds, countryIds: countryIds]
        );
        db.close();
        rows;
    }

    def mergeUpdates(blankUpdateMap, updates) {

        def mergedMap = blankUpdateMap?.clone();
        updates.each() {
            mergedMap[it.startStepTime] = it.updates;
        }
        mergedMap;
    }

    private def configureTimeStepString(final Step validStep, final String alias) {

        def result = null;

        switch (validStep) {
            case Step.DAILY:
                result = "date(FROM_UNIXTIME(${alias}/1000))";
                break
            case Step.WEEKLY:
                //NOTE:: mode=1 means First day of week: Monday; Range: 0-53; Week 1 is the first week: with more than 3 days this year.
                //NOTE:: look at TimeService.FIRST_DAY_OF_WEEK constant also. It should be same to firstDayOfWeek of used SQL mode.
                result = "yearweek(FROM_UNIXTIME(${alias}/1000), 1)";
                break
            case Step.MONTHLY:
                result = "year(FROM_UNIXTIME(${alias}/1000)), month(FROM_UNIXTIME(${alias}/1000))";
                break
        }

        result;
    }
}