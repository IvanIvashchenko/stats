package zeptostats;

/**
 * Class which can send email with export data.
 */
class ExportEmailService {

    private static final String SUBJECT_FORMAT_DATA_EXPORT = "FinanceUI: Data Export";
    private static final String BODY_FORMAT_DATA_EXPORT = "Hello, %s.\n\nThere is attached data-export file.\n\nThanks for using FinanceUI (%s).";

    private static final String LOG_PREFIX = "DataExport: email:";
    private static final String LOG_COMPRESSING = "$LOG_PREFIX file compressing...";
    private static final String LOG_COMPRESSED = "$LOG_PREFIX file has been compressed (file %s).";
    private static final String LOG_SENDING = "$LOG_PREFIX email sending...";
    private static final String LOG_SENT = "$LOG_PREFIX email has been sent.";
    private static final String LOG_DELETING = "$LOG_PREFIX files (report, compressed report) deleting...";
    private static final String LOG_DELETED = "$LOG_PREFIX files (report, compressed report) have been deleted.";

    private static final String LOG_ERROR_PREFIX = "$LOG_PREFIX email will not been sent because";
    private static final String LOG_ERROR_ASIS_FILE = "$LOG_ERROR_PREFIX tmp folder does not exists or is not readable or is not writtable or initial file does not exist or is not readable.";
    private static final String LOG_ERROR_COMPRESSED = "$LOG_ERROR_PREFIX file has not been compressed.";

    def mailService;
    def compressorService;
    def exportFileService;
    def fileService;

    /**
     * Sends email to member if all is ok with compressed DataExport attachment.
     * Deletes if all is ok data-export as-is and compressed files.
     * 
     * @param member Member member who should receive email with attach
     * @param tmpFolderAbsolutePath String absolute path(with end separator) to folder where as-is file is placed and new compressed file should be placed.
     * @param asIsFilePath String as-is file's absolute path
     * @param asIsFileName String as-is file's name
     * @param asIsExportFileType ExportFileType as-is file's ExportFileType
     * @param filenameForMember String file's name for member
     */
    def void sendDataExportEmail(
        final Member member,
        final String tmpFolderAbsolutePath,
        final String asIsFilePath,
        final String asIsFileName,
        final ExportFileType asIsExportFileType,
        final String filenameForMember
    ) {

        def emailTo = member.email;
        def nameTo = member.username;

        File folder = new File(tmpFolderAbsolutePath);
        File asIsFile = new File(asIsFilePath);
        if (
            folder && folder.exists() && folder.canRead() && folder.canWrite()
            && asIsFile && asIsFile.exists() && asIsFile.canRead()
        ) {

            ExportFileType compressedExportFileType = ExportFileType.GZIP;
            String compressedFileExtention = exportFileService.getExtension(compressedExportFileType);
            String compressedContentType = exportFileService.getContentType(compressedExportFileType);

            log.debug LOG_COMPRESSING;
            File compressedFile = compressorService.compressFile(asIsFile, compressedFileExtention);
            if (compressedFile) {
                def compressedFilePath = compressedFile.getAbsolutePath();
                log.debug String.format(LOG_COMPRESSED, compressedFilePath);

                def asIsFileExtention = exportFileService.getExtension(asIsExportFileType);
                def appUrl = exportFileService.getApplicationUrl();

                log.debug LOG_SENDING;
                mailService.sendMail {
                    multipart true
                    to emailTo
                    subject String.format(SUBJECT_FORMAT_DATA_EXPORT)
                    body String.format(BODY_FORMAT_DATA_EXPORT, nameTo, appUrl)
                    attachBytes "${filenameForMember}.${asIsFileExtention}.${compressedFileExtention}", compressedContentType, compressedFile.readBytes()
                }
                log.debug LOG_SENT;

                log.debug LOG_DELETING;
                fileService.delete(asIsFilePath);
                fileService.delete(compressedFilePath);
                log.debug LOG_DELETED;
            } else {
                log.debug LOG_ERROR_COMPRESSED;
            }
        } else {
            log.debug LOG_ERROR_ASIS_FILE;
        }
    }
}