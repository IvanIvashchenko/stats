package zeptostats
import groovy.sql.Sql

class CompareDependentColumnService {

    def dataSource_zeptostats;
    def longService;
    def filterEntitiesService;

    /**
     * Corrects sets of dependent ids.
     * For example, if Game1 belongs to Store1. If strikedGameIdsAsIs does not contain Game1, then Store1 will be unstriked in
     *  results no matter was Store1 in strikedCountryIdsAsIs or was not.
     *
     * Independent ids are possible.
     *
     * @param strikedGameIdsAsIs List<Long> striked Game ids from client
     * @param strikedCountryIdsAsIs List<Long> striked Country ids from client
     * @param strikedGameTypeIdsAsIs List<Long> striked GameType ids from client
     * @param strikedResolutionIdsAsIs List<Long> striked Resolution ids from client
     * @param strikedStoreIdsAsIs List<Long> striked Store ids from client
     * @return for all CompareColumns in dependence's tree (for dependence, its children, its parent, its parent's children) map {
     *      CompareColumn => map [
     *          "allStrikedIds": List<Long> all CORRECT ids to exclude from associated CompareColumn,
     *          "allUnstrikedIds": List<Long> all CORRECT ids to include to associated CompareColumn
     *      },
     *      ...
     * }
     */
    def Map<CompareColumn, Map<String, List<Long>>> correctIds(
        final List<Long> strikedGameIdsAsIs,
        final List<Long> strikedCountryIdsAsIs,
        final List<Long> strikedGameTypeIdsAsIs,
        final List<Long> strikedResolutionIdsAsIs,
        final List<Long> strikedStoreIdsAsIs,
        final List<String> strikedRequiredProductTypeIdsAsIs

    ) {

        List<Long> unnecessaryGameIds       = filterEntitiesService.fetchGamesIdsIn(strikedGameIdsAsIs);
        List<Long> necessaryGameIds         = filterEntitiesService.fetchGamesIdsNotIn(strikedGameIdsAsIs);

        List<Long> unnecessaryCountryIds    = filterEntitiesService.fetchCountriesIdsIn(strikedCountryIdsAsIs);
        List<Long> necessaryCountryIds      = filterEntitiesService.fetchCountriesIdsNotIn(strikedCountryIdsAsIs);

        List<Long> unnecessaryIndependentGameTypes   = filterEntitiesService.fetchIndependentGameTypesIn(strikedGameTypeIdsAsIs);
        List<Long> necessaryIndependentGameTypes     = filterEntitiesService.fetchIndependentGameTypesNotIn(strikedGameTypeIdsAsIs);

        List<Long> unnecessaryIndependentResolutions = filterEntitiesService.fetchIndependentResolutionsIn(strikedResolutionIdsAsIs);
        List<Long> necessaryIndependentResolutions   = filterEntitiesService.fetchIndependentResolutionsNotIn(strikedResolutionIdsAsIs);

        List<Long> unnecessaryIndependentStoreIds      = filterEntitiesService.fetchIndependentStoresIdsIn(strikedStoreIdsAsIs);
        List<Long> necessaryIndependentStoreIds        = filterEntitiesService.fetchIndependentStoresIdsNotIn(strikedStoreIdsAsIs);

        List<String> unnecessaryIndependentRequiredProductTypeIds = filterEntitiesService.fetchIndependentRequiredProductTypeIdsIn(strikedRequiredProductTypeIdsAsIs);
        List<String> necessaryIndependentRequiredProductTypeIds = filterEntitiesService.fetchIndependentRequiredProductTypeIdsNotIn(strikedRequiredProductTypeIdsAsIs);

        def Map<CompareColumn, Map<String, List<Long>>> correct = [:]
        CompareColumnDependence.getIndependentValues()?.each() { dependence ->
            def actionStrikedIds = [];
            def actionUnstrikedIds = [];
            switch (dependence) {
                case CompareColumnDependence.SKU:
                    actionStrikedIds = unnecessaryGameIds;
                    actionUnstrikedIds = necessaryGameIds;
                    break;
                case CompareColumnDependence.COUNTRY:
                    actionStrikedIds = unnecessaryCountryIds;
                    actionUnstrikedIds = necessaryCountryIds;
                    break;
            }

            def resultMap = this.fetchAllDependentStrikedUnstrikedIds(
                dependence,
                [
                    actionStrikedIds: actionStrikedIds,
                    actionUnstrikedIds: actionUnstrikedIds
                ],
                [:]
            );

            correct.putAll(resultMap);
        }

        //NOTE:: adding independent striked / unstriked ids
        CompareColumnDependence.values()?.each() { dependence ->
            CompareColumn compareColumn = dependence.column;
            def dependentResult = correct.get(compareColumn);
            def independentStrikedIds = [];
            def independentUnstrikedIds = [];
            def dependentStrikedIds = dependentResult == null ? [] : dependentResult.allStrikedIds;
            def dependentUnstrikedIds = dependentResult == null ? [] : dependentResult.allUnstrikedIds;

            switch (dependence) {
                case CompareColumnDependence.VERSION:
                    independentStrikedIds = unnecessaryIndependentGameTypes;
                    independentUnstrikedIds = necessaryIndependentGameTypes;
                    break;
                case CompareColumnDependence.STYLE:
                    independentStrikedIds = unnecessaryIndependentResolutions;
                    independentUnstrikedIds = necessaryIndependentResolutions;
                    break;
                case CompareColumnDependence.STORE:
                    independentStrikedIds = unnecessaryIndependentStoreIds;
                    independentUnstrikedIds = necessaryIndependentStoreIds;
                    break;
                case CompareColumnDependence.REQUIRED_PRODUCT_TYPE:
                    independentStrikedIds = unnecessaryIndependentRequiredProductTypeIds;
                    independentUnstrikedIds = necessaryIndependentRequiredProductTypeIds;
                    break;

                case CompareColumnDependence.COUNTRY:   //NOTE:: depends on self
                case CompareColumnDependence.SKU:       //NOTE:: depends on self
                case CompareColumnDependence.REGION:    //NOTE:: depends on COUNTRY
                case CompareColumnDependence.METASKU:   //NOTE:: depends on SKU
                case CompareColumnDependence.OS:        //NOTE:: depends on STORE
                    break;
            }

            dependentStrikedIds.removeAll({independentStrikedIds});
            dependentStrikedIds.addAll(independentStrikedIds);

            dependentUnstrikedIds.removeAll(independentUnstrikedIds);
            dependentUnstrikedIds.addAll(independentUnstrikedIds);
        }

        return correct;
    }

    /**
     * NOTE:: if action dependence is Top, then actionStrikedUnstriked contain ALL striked / unstriked items
     * NOTE:: if action dependence is not Top, then actionStrikedUnstriked contain ACTION striked / unstriked items only
     *
     * @param dependence CompareColumnDependence that is the cause of modifications
     * @param actionStrikedUnstriked map which is cause of tree's modifications map {
     *      "actionStrikedIds": List<Long> striked ids in dependence's CompareColumn
     *      "actionUnstrikedIds": List<Long> unstriked ids in dependence's CompareColumn
     * }
     * @param parentStrikedUnstriked map {
     *      "allStrikedIds": List<Long> all striked ids in dependence's parent's CompareColumn,
     *      "allUnstrikedIds": List<Long> all unstriked ids in dependence's parent's CompareColumn,
     * }
     * @return for all CompareColumns in dependence's tree (for dependence, its children, its parent, its parent's children) map {
     *      CompareColumn => map [
     *          "allStrikedIds": List<Long> all striked ids in associated CompareColumn,
     *          "allUnstrikedIds": List<Long> all unstriked ids in associated CompareColumn
     *      },
     *      ...
     * }
     */
    def Map<CompareColumn, Map<String, List<Long>>> fetchAllDependentStrikedUnstrikedIds(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked,
        final Map<String, List<Long>> topParentStrikedUnstriked
    ) {

        def result = [:];

        if (dependence == null || dependence.column == null || actionStrikedUnstriked == null || actionStrikedUnstriked.isEmpty()) {
            return result;
        }

        def resultCurrent = this.fetchStrikedUnstrikedOfCurrentDependence(dependence, actionStrikedUnstriked)
        result.putAll(resultCurrent)

        def resultOfTopParentTree = this.fetchStrikedUnstrikedOfTopParentDependenceTree(dependence, actionStrikedUnstriked, topParentStrikedUnstriked)
        result.putAll(resultOfTopParentTree)

        return result
    }

    protected Map<CompareColumn, Map<String, List<Long>>> fetchDependentStrikedUnstrikedIdsRecursiveDeep(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked,
        final List<CompareColumnDependence> except
    ) {

        def result = [:];

        if (dependence == null || dependence.column == null || actionStrikedUnstriked == null || actionStrikedUnstriked.isEmpty()) {
            return result;
        }

        def resultCurrent = this.fetchStrikedUnstrikedOfCurrentDependence(dependence, actionStrikedUnstriked)
        result.putAll(resultCurrent)

        def resultOfCurrentChildren = this.fetchStrikedUnstrikedOfCurrentDependenceChildren(dependence, actionStrikedUnstriked, except)
        result.putAll(resultOfCurrentChildren)

        return result
    }

    protected Map<CompareColumn, Map<String, List<Long>>> fetchStrikedUnstrikedOfCurrentDependence(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked
    ) {

        def resultOfCurrent = [:]

        CompareColumn compareColumn = dependence?.column
        if (compareColumn) {
            def oldCurrent = actionStrikedUnstriked

            resultOfCurrent.put(
                compareColumn,
                [
                    allStrikedIds: oldCurrent ? oldCurrent.actionStrikedIds : [],
                    allUnstrikedIds: oldCurrent ? oldCurrent.actionUnstrikedIds : []
                ]
            )
        }

        return resultOfCurrent
    }

    protected Map<CompareColumn, Map<String, List<Long>>> fetchStrikedUnstrikedOfCurrentDependenceChildren(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked,
        final List<CompareColumnDependence> except
    ) {

        def resultOfCurrentChildren = [:]

        CompareColumn compareColumn = dependence?.column
        List<CompareColumnDependence> childrenNeed = dependence?.getChildrenExcept(except)
        if (compareColumn && childrenNeed && !childrenNeed.isEmpty()) {
            resultOfCurrentChildren = this.fetchDependentStrikedUnstrikedIdsForDependence(dependence, actionStrikedUnstriked, childrenNeed);

            except.addAll(childrenNeed);

            childrenNeed.each() { child ->
                def modified = resultOfCurrentChildren[child.column]
                def actionStrikedUnstrikedChild = [
                    actionStrikedIds: modified?.allStrikedIds,
                    actionUnstrikedIds: modified?.allUnstrikedIds
                ]
                def resultOfCurrentChild = this.fetchDependentStrikedUnstrikedIdsRecursiveDeep(child, actionStrikedUnstrikedChild, except);

                resultOfCurrentChildren.putAll(resultOfCurrentChild);
            }
        }

        return resultOfCurrentChildren;
    }

    protected Map<CompareColumn, Map<String, List<Long>>> fetchStrikedUnstrikedOfTopParentDependenceTree(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked,
        final Map<String, List<Long>> topParentStrikedUnstriked
    ) {

        def resultOfTopParentTree = [:];

        if (dependence == null) {
            return resultOfTopParentTree;
        }

        CompareColumnDependence topParent = null;
        def actionStrikedUnstrikedTopParent = [:];
        def except = [];

        if (dependence.isTop()) {
            topParent = dependence;
            actionStrikedUnstrikedTopParent = actionStrikedUnstriked;
            except = [dependence];

        } else {
            topParent = dependence.topParent
            except = [dependence, topParent];

            def resultOfTopParent = this.fetchDependentStrikedUnstrikedIdsForDependence(dependence, actionStrikedUnstriked, [topParent]);
            def oldTopParent = topParentStrikedUnstriked
            def modified = resultOfTopParent[topParent?.column]

            def List<Long> mergedTopParentStriked = oldTopParent ? oldTopParent.allStrikedIds : []
            def List<Long> mergedTopParentUnstriked = oldTopParent ? oldTopParent.allUnstrikedIds : []
            if (modified) {
                if (modified.allStrikedIds) {
                    mergedTopParentStriked.removeAll(modified.allStrikedIds)
                    mergedTopParentStriked.addAll(modified.allStrikedIds)
                    mergedTopParentUnstriked.removeAll(modified.allStrikedIds)
                }
                if (modified.allUnstrikedIds) {
                    mergedTopParentStriked.removeAll(modified.allUnstrikedIds)
                    mergedTopParentUnstriked.removeAll(modified.allUnstrikedIds)
                    mergedTopParentUnstriked.addAll(modified.allUnstrikedIds)
                }
            }
            actionStrikedUnstrikedTopParent = [
                actionStrikedIds: mergedTopParentStriked,
                actionUnstrikedIds: mergedTopParentUnstriked
            ]
        }

        resultOfTopParentTree = this.fetchDependentStrikedUnstrikedIdsRecursiveDeep(topParent, actionStrikedUnstrikedTopParent, except);

        return resultOfTopParentTree;
    }

    protected def fetchDependentStrikedUnstrikedIdsForDependence(
        final CompareColumnDependence dependence,
        final Map<String, List<Long>> actionStrikedUnstriked,
        final List<CompareColumnDependence> need
    ) {

        def result = [:];

        FilterColumn filterColumn = dependence?.filterColumn
        if (filterColumn == null || actionStrikedUnstriked == null || actionStrikedUnstriked.isEmpty()) {
            return result;
        }

        def allStrikedIds = actionStrikedUnstriked.actionStrikedIds
        def allUnstrikedIds = actionStrikedUnstriked.actionUnstrikedIds

        if (filterColumn != null && need != null && !need.isEmpty()) {

            def allStrikedIdsStr = longService.configureIdsString(allStrikedIds);
            def allUnstrikedIdsStr = longService.configureIdsString(allUnstrikedIds);

            switch (filterColumn) {
                case FilterColumn.VERSION:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch (dependentCompareColumn) {
                            case CompareColumn.SKU:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedGameIdsByStrikedGameTypes(allStrikedIds),
                                    allUnstrikedIds: this.fetchUnstrikedGameIdsByUnstrikedGameTypes(allUnstrikedIds)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
                case FilterColumn.STYLE:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch (dependentCompareColumn) {
                            case CompareColumn.SKU:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedGameIdsByStrikedResolutions(allStrikedIds),
                                    allUnstrikedIds: this.fetchUnstrikedGameIdsByUnstrikedResolutions(allUnstrikedIds)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
                case FilterColumn.STORE:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch (dependentCompareColumn) {
                            case CompareColumn.SKU:

                                def info = [
                                    allStrikedIds:   this.fetchStrikedGameIdsByStrikedStoreIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedGameIdsByUnstrikedStoreIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                            case CompareColumn.OS:

                                def info = [
                                    allStrikedIds:   this.fetchStrikedPlatformIdsByStrikedStoreIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
                case FilterColumn.REQUIRED_PRODUCT_TYPE:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch(dependentCompareColumn) {
                            case CompareColumn.SKU:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedGameIdsByStrikedRequiredProductTypeIds(allStrikedIds),
                                    allUnstrikedIds: this.fetchUnstrikedGameIdsByUnstrikedRequiredProductTypeIds(allUnstrikedIds)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
                case FilterColumn.SKU:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch (dependentCompareColumn) {
                            case CompareColumn.VERSION:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedGameTypesByUnstrikedGameIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                            case CompareColumn.STYLE:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedResolutionsByUnstrikedGameIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                            case CompareColumn.STORE:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedStoreIdsByUnstrikedGameIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                            case CompareColumn.METASKU:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                            case CompareColumn.REQUIRED_PRODUCT_TYPE:
                                def info = [
                                    allStrikedIds:   this.fetchStrikedRequiredProductTypeIdsByStrikedGameIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedRequiredProductTypeIdsByUnstrikedGameIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
                case FilterColumn.COUNTRY:
                    need.each() {
                        def dependentCompareColumn = it?.column
                        switch (dependentCompareColumn) {
                            case CompareColumn.REGION:

                                def info = [
                                    allStrikedIds:   this.fetchStrikedRegionIdsByStrikedCountryIds(allStrikedIdsStr),
                                    allUnstrikedIds: this.fetchUnstrikedRegionIdsByUnstrikedCountryIds(allUnstrikedIdsStr)
                                ]
                                result.put(dependentCompareColumn, info)
                                break
                        }
                    }
                    break
            }
        }

        return result
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains GameType allStrikedIds separated by comma
     * @return List of Game dependentStrikedIds
     */
    protected def fetchStrikedGameIdsByStrikedGameTypes(allStrikedIds) {

        def allStrikedIdsStr = longService.configureStringIdsString(allStrikedIds)
        def sqlQuery = """
            select distinct g.id as id
            from
                game g
            where
                g.game_type in ($allStrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains GameType allUnstrikedIdsStr separated by comma
     * @return List of Game dependentUnstrikedIds
     */
    protected def fetchUnstrikedGameIdsByUnstrikedGameTypes(allUnstrikedIds) {

        def allUnstrikedIdsStr = longService.configureStringIdsString(allUnstrikedIds)

        def sqlQuery = """
            select distinct g.id as id
            from
                game g
            where
                g.game_type in ($allUnstrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Resolution allStrikedIds separated by comma
     * @return List of Game dependentStrikedIds
     */
    protected def fetchStrikedGameIdsByStrikedResolutions(allStrikedIds) {

        def allStrikedIdsStr = longService.configureStringIdsString(allStrikedIds)

        def sqlQuery = """
            select distinct g.id as id
            from
                game g
            where
                g.resolution in ($allStrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Resolution allUnstrikedIdsStr separated by comma
     * @return List of Game dependentUnstrikedIds
     */
    protected def fetchUnstrikedGameIdsByUnstrikedResolutions(allUnstrikedIds) {

        def allUnstrikedIdsStr = longService.configureStringIdsString(allUnstrikedIds)

        def sqlQuery = """
            select distinct g.id as id
            from
                game g
            where
                g.resolution in ($allUnstrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains RequiredProductType allStrikedIds separated by comma
     * allStrikedIds in requiredProductType is "Application" or/and "In-App"
     * Methods are the same :(((((((((((((((((((((((((((((((
     * @return List of Game dependentStrikedIds
     */
    protected def fetchStrikedGameIdsByStrikedRequiredProductTypeIds(allStrikedIdsStr) {

        def resultList = [];
        def sqlQuery = "";

        allStrikedIdsStr?.each() {
            if(it.equals(FilterEntitiesService.APPLICATION_TYPE)) {
                sqlQuery = """
                    select distinct g.id as id
                    from
                        game g
                    where
                        g.parent_game_id is null
                    order by g.id
                """;
                //TODO:: OMG, Change this!!!
                resultList.addAll(this.fetchIdList("${sqlQuery}"));
            } else if (it.equals(FilterEntitiesService.IN_APP_TYPE)) {
                sqlQuery = """
                    select distinct g.id as id
                    from
                        game g
                    where
                        g.parent_game_id is not null
                    order by g.id
                """;
                resultList.addAll(this.fetchIdList("${sqlQuery}"));
            }
        }

        return resultList;
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains RequiredProductType allUnstrikedIdsStr separated by comma
     * allStrikedIds in requiredProductType is "Application" or/and "In-App"
     * @return List of Game dependentUnstrikedIds
     */
    protected def fetchUnstrikedGameIdsByUnstrikedRequiredProductTypeIds(allUnstrikedIdsStr) {

        def resultList = [];
        def sqlQuery = "";

        allUnstrikedIdsStr?.each() {
            if(it.equals(FilterEntitiesService.APPLICATION_TYPE)) {
                sqlQuery = """
                    select distinct g.id as id
                    from
                        game g
                    where
                        g.parent_game_id is null
                    order by g.id
                """;
                resultList.addAll(this.fetchIdList("${sqlQuery}"));
            } else if (it.equals(FilterEntitiesService.IN_APP_TYPE)) {
                sqlQuery = """
                    select distinct g.id as id
                    from
                        game g
                    where
                        g.parent_game_id is not null
                    order by g.id
                """;
                resultList.addAll(this.fetchIdList("${sqlQuery}"));
            }
        }

        return resultList;
    }


    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Store allStrikedIds separated by comma
     * @return List of Game dependentStrikedIds
     */
    protected def fetchStrikedGameIdsByStrikedStoreIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct g.id as id
            from
                game g
                inner join store s on s.id = g.store_id
            where
                s.id in ($allStrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Store allUnstrikedIdsStr separated by comma
     * @return List of Game dependentUnstrikedIds
     */
    protected def fetchUnstrikedGameIdsByUnstrikedStoreIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct g.id as id
            from
                game g
                inner join store s on s.id = g.store_id
            where
                s.id in ($allUnstrikedIdsStr)
            order by g.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Store allStrikedIds separated by comma
     * @return List of Platform dependentStrikedIds
     */
    protected def fetchStrikedPlatformIdsByStrikedStoreIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct p.id as id
            from
                store s
                inner join platform p on p.id = s.platform_id
            where
                s.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        store sTmp
                        inner join platform pTmp on pTmp.id = sTmp.platform_id
                    where
                        p.id = pTmp.id
                        and sTmp.id not in ($allStrikedIdsStr)
                )
            order by p.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Store allUnstrikedIdsStr separated by comma
     * @return List of Platform dependentUnstrikedIds
     */
    protected def fetchUnstrikedPlatformIdsByUnstrikedStoreIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct p.id as id
            from
                store s
                inner join platform p on p.id = s.platform_id
            where
                s.id in ($allUnstrikedIdsStr)
            order by p.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allStrikedIds separated by comma
     * @return List of GameType dependentStrikedIds
     */
    protected def fetchStrikedGameTypesByStrikedGameIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct g.game_type as id
            from
                game g
            where
                g.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        game g1
                    where
                        g.game_type = g1.game_type
                        and g.id != g1.id
                        and g1.id not in ($allStrikedIdsStr)
                )
            order by id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allUnstrikedIdsStr separated by comma
     * @return List of GameType dependentUnstrikedIds
     */
    protected def fetchUnstrikedGameTypesByUnstrikedGameIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct g.game_type as id
            from
                game g
            where
                g.id in ($allUnstrikedIdsStr)
            order by id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allStrikedIds separated by comma
     * @return List of Resolution dependentStrikedIds
     */
    protected def fetchStrikedResolutionsByStrikedGameIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct g.resolution as id
            from
                game g
            where
                g.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        game g1
                    where
                        g.resolution = g1.resolution
                        and g.id != g1.id
                        and g1.id not in ($allStrikedIdsStr)
                )
            order by id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allUnstrikedIdsStr separated by comma
     * @return List of Resolution dependentUnstrikedIds
     */
    protected def fetchUnstrikedResolutionsByUnstrikedGameIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct g.resolution as id
            from
                game g
            where
                g.id in ($allUnstrikedIdsStr)
            order by id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allStrikedIds separated by comma
     * @return List of Store dependentStrikedIds
     */
    protected def fetchStrikedStoreIdsByStrikedGameIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct s.id as id
            from
                store s
                inner join game g on g.store_id = s.id
            where
                g.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        store sTmp
                        inner join game gTmp on gTmp.store_id = sTmp.id
                    where
                        s.id = sTmp.id
                        and g.id != gTmp.id
                        and gTmp.id not in ($allStrikedIdsStr)
                )
            order by s.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allUnstrikedIdsStr separated by comma
     * @return List of Store dependentUnstrikedIds
     */
    protected def fetchUnstrikedStoreIdsByUnstrikedGameIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct s.id as id
            from
                store s
                inner join game g on g.store_id = s.id
            where
                g.id in ($allUnstrikedIdsStr)
            order by s.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allStrikedIds separated by comma
     * @return List of MetaGame dependentStrikedIds
     */
    protected def fetchStrikedMetaGameIdsByStrikedGameIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct mg.id as id
            from
                meta_game mg
                inner join game g on g.meta_game_id = mg.id
            where
                g.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        meta_game mgTmp
                        inner join game gTmp on gTmp.meta_game_id = mgTmp.id
                    where
                        mg.id = mgTmp.id
                        and g.id != gTmp.id
                        and gTmp.id not in ($allStrikedIdsStr)
                )
            order by mg.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allUnstrikedIdsStr separated by comma
     * @return List of MetaGame dependentUnstrikedIds
     */
    protected def fetchUnstrikedMetaGameIdsByUnstrikedGameIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct mg.id as id
            from
                meta_game mg
                inner join game g on g.meta_game_id = mg.id
            where
                g.id in ($allUnstrikedIdsStr)
            order by mg.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allStrikedIds separated by comma
     * @return List of RequiredProductTypes dependentStrikedIds
     */
    protected def fetchStrikedRequiredProductTypeIdsByStrikedGameIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct (g.parent_game_id is not null) as id
            from
                game g
            where
                g.id in ($allStrikedIdsStr)
            order by id
        """;

        def idList = this.fetchIdList(sqlQuery);
        def resultList = [];
        idList?.each() {
            if(!it) {
                resultList.add(FilterEntitiesService.APPLICATION_TYPE);
            } else {
                resultList.add(FilterEntitiesService.IN_APP_TYPE);
            }
        }

        return resultList;
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Game allUnstrikedIdsStr separated by comma
     * @return List of RequiredProductTypes dependentUnstrikedIds
     */
    protected def fetchUnstrikedRequiredProductTypeIdsByUnstrikedGameIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct (g.parent_game_id is not null) as id
            from
                game g
            where
                g.id in ($allUnstrikedIdsStr)
            order by id
        """;

        def idList = this.fetchIdList(sqlQuery);
        def resultList = [];
        idList?.each() {
            if(!it) {
                resultList.add(FilterEntitiesService.APPLICATION_TYPE);
            } else {
                resultList.add(FilterEntitiesService.IN_APP_TYPE);
            }
        }

        return resultList;
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Country allStrikedIds separated by comma
     * @return List of Region dependentStrikedIds
     */
    protected def fetchStrikedRegionIdsByStrikedCountryIds(allStrikedIdsStr) {

        def sqlQuery = """
            select distinct r.id as id
            from
                region r
                inner join country c on r.id = c.region_id
            where
                c.id in ($allStrikedIdsStr)
                and not exists (
                    select null
                    from
                        region rTmp
                        inner join country cTmp on rTmp.id = cTmp.region_id
                    where
                        r.id = rTmp.id
                        and c.id != cTmp.id
                        and cTmp.id not in ($allStrikedIdsStr)
                )
            order by r.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param allStrikedIdsStr String not empty string which contains Country allUnstrikedIdsStr separated by comma
     * @return List of Region dependentUnstrikedIds
     */
    protected def fetchUnstrikedRegionIdsByUnstrikedCountryIds(allUnstrikedIdsStr) {

        def sqlQuery = """
            select distinct r.id as id
            from
                region r
                inner join country c on r.id = c.region_id
            where
                c.id in ($allUnstrikedIdsStr)
            order by r.id
        """;

        return this.fetchIdList(sqlQuery);
    }

    /**
     *
     * @param sqlQuery GString which is SQL query with select of id
     * @return list of fetched ids
     */
    protected def fetchIdList(final GString sqlQuery) {

        def ids = [];

        def db = new Sql(dataSource_zeptostats);
        def rows = db.rows(sqlQuery, []);
        rows?.each() {
            ids.add(it.id);
        }
        db.close();

        return ids;
    }
}