package zeptostats;

import groovy.sql.GroovyRowResult;
import groovy.sql.Sql;

import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import java.io.BufferedReader;

import zeptostats.export.TsvService;

/**
 * Class that can write to OutputStream fetched data for DataExport.
 */
class DataExportService {

    def TsvService tsvService;
    def DataExportFetcherService dataExportFetcherService;
    def FilterEntitiesService filterEntitiesService;
    def TimeService timeService;
    def DataExportCheckerService dataExportCheckerService;
    def FileService fileService;

    private static Random random = new Random();

    private static final String HEADER_TITLE = "Data Export";
    private static final String HEADER_STEP = "Step: \"%s\"";
    private static final String HEADER_COMPARED_BY = "Compared by: %s";
    private static final String HEADER_COMPARED_BY_ITEM = "\"%s\"";
    private static final String HEADER_COMPARED_BY_ITEM_SEPARATOR = ", ";
    private static final String HEADER_FILTER = "%s: %s";
    private static final String HEADER_FILTER_INCLUDE = "(included) %s";
    private static final String HEADER_FILTER_EXCLUDE = "(excluded) %s";
    private static final String HEADER_FILTER_MULTIPLE = "multiple elements";
    private static final String HEADER_FILTER_SOME = "\"%s\"";
    private static final String HEADER_FILTER_SOME_SEPARATOR = ", ";
    private static final String HEADER_FILTER_ALL = "All";
    private static final String HEADER_COLUMN_DOWNLOADS = "Downloads: %s";
    private static final String HEADER_COLUMN_REVENUES = "Revenues: %s";
    private static final String HEADER_COLUMN_UPDATES = "Downloads for update: %s";
    private static final String HEADER_COLUMN_REFUNDS = "Refunds: %s";
    private static final String HEADER_COLUMN_REFUNDS_MONETIZED = "Refunds in \$: %s";
    private static final String HEADER_COLUMN_RANKINGS = "Rankings: %s";
    private static final String HEADER_COLUMN_MONTHLY_FINANCIAL = "Monthly financial revenue: %s";
    private static final String HEADER_ROW_TOTAL = "Overtime Total: %s";
    private static final String HEADER_COLUMN_IS_INCLUDED = "included";
    private static final String HEADER_COLUMN_IS_EXCLUDED = "excluded";
    private static final String HEADER_DATE_RANGE = "Date Range: %s - %s";
    private static final String HEADER_GENERATION_DATETIME = "Generated: %s";
    private static final String HEADER_NON_CALENDAR_WARN = "There is non-calendar month in iTunes reports";
    private static final String HEADER_PREFIX = "------------------------------------------";
    private static final String HEADER_POSTFIX = "------------------------------------------";
    private static final Integer HEADER_FILTER_MAX = 8;

    private static final String UNKNOWN_VALUE = "X";
    private static final String UNNECESSARY_VALUE_TOTAL = "";
    private static final String TOTAL = "TOTAL";
    private static final String ERROR_SETTINGS = "ERROR: Set of Compared By Columns or (and) Step or (and) need info is unavailable.";
    private static final String ERROR_TIMESTAMPS = "ERROR: Selected From or (and) To dates are wrong.";
    private static final String ERROR_SQL = "ERROR: Report's fetching is failed.";
    private static final String ERROR = "ERROR: There was some error.";

    /** Should be 0 if prefix is unnecessary */
    private static final int TABLE_TOTAL_ROW_PREFIX_AMOUNT = 1;
    private static final int TABLE_ROW_PREFIX_AMOUNT = TABLE_TOTAL_ROW_PREFIX_AMOUNT + 1;

    private static final String LOG_PREFIX = "DataExport: file:";
    private static final String LOG_START = "$LOG_PREFIX start.";
    private static final String LOG_TABLE_FETCHING = "$LOG_PREFIX table fetching into temporary file (file %s)...";
    private static final String LOG_TABLE_WRITING = "$LOG_PREFIX table writing...";
    private static final String LOG_TMP_DELETING = "$LOG_PREFIX temporary file deleting...";
    private static final String LOG_TOTAL_FETCHING = "$LOG_PREFIX total fetching...";
    private static final String LOG_TOTAL_WRITING = "$LOG_PREFIX total writing...";
    private static final String LOG_END = "$LOG_PREFIX end.";

    private static final String LOG_FILE_ERROR = "$LOG_PREFIX reading-writing has not been finished because of %s (file %s).";
    private static final String LOG_FILE_NOTE = "$LOG_PREFIX reading-writing error(while closing) because of %s (file %s).";
    private static final String LOG_FILE_ACCESS = "$LOG_PREFIX reading-writing is impossible because file does not exist or is not readable (file %s).";

    /**
     * Writes to OutputStream fetched DataExport info in TSV format
     *
     * @param outputStream OutputStream stream to which report will be written.
     * @param tmpFolderAbsolutePath String absolute path(with end separator) to folder where all need files should be placed.
     * @param filename String need filename for data-export file
     * @param extension String need extension for data-export file
     * @param idsInfo Map<CompareColumn, Map<String, List<Long>>> CORRECT ids for all CompareColumns map {
     *      CompareColumn => map [
     *          "allStrikedIds": List<Long> all CORRECT ids to exclude from associated CompareColumn. May be null.
     *          "allUnstrikedIds": List<Long> all CORRECT ids to include to associated CompareColumn. May be null.
     *      },
     *      ...
     * }
     * @param compareColumns List<CompareColumn> list of need CompareColumn. List cannot be null.
     * @param fromTimestamp Long start timestamp of need period. May be null, but this is error situation.
     * @param toTimestamp Long end timestamp of need period. May be null, but this is error situation.
     * @param isNeedDownloads Boolean true if Downloads column is need. May be null, then will be like false.
     * @param isNeedRevenues Boolean true if Revenue column is need. May be null, then will be like false.
     * @param isNeedUpdates Boolean if Updates column is need. May be null, then will be like false.
     * @param isNeedRefunds Boolean Refunds column is needed. May be null, then will be like false.
     * @param isNeedRefundsMonetized Boolean true if RefundsMonetized column is need. May be null, then will be like false.
     * @param isNeedRankings Boolean true if Rankings column is need. May be null, then will be like false.
     * @param isNeedMonthlyFinancial Boolean true if Monthly financial revenue is need. May be null, then will be like false.
     * @param isNeedTotal Boolean true if Total line is need. May be null, then will be like false.
     * @param needStep Step need step (Daily, Weekly, Monthly etc.). May be null, then will be like DAILY.
     */
    public void generateTsvReport(
        final OutputStream outputStream,
        final String tmpFolderAbsolutePath,
        final String filename,
        final String extension,
        final Map<CompareColumn, Map<String, List<Long>>> idsInfo,
        final List<CompareColumn> compareColumns,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean isNeedDownloads,
        final Boolean isNeedRevenues,
        final Boolean isNeedUpdates,
        final Boolean isNeedRefunds,
        final Boolean isNeedRefundsMonetized,
        final Boolean isNeedRankings,
        final Boolean isNeedTotal,
        final Boolean isNeedMonthlyFinancial,
        final Step needStep
    ) {

        Long startTime = System.currentTimeMillis()
        log.debug LOG_START;
        tsvService.init(outputStream);

        def Step needStepSafe = (needStep == null) ? Step.DAILY : needStep;

        def compareColumnsUnique = dataExportCheckerService.configureCompareColumnsUnique(compareColumns);

        this.writeExportHeader(
            idsInfo,
            compareColumnsUnique,
            fromTimestamp,
            toTimestamp,
            isNeedDownloads,
            isNeedRevenues,
            isNeedUpdates,
            isNeedRefunds,
            isNeedRefundsMonetized,
            isNeedRankings,
            isNeedTotal,
            isNeedMonthlyFinancial,
            needStepSafe
        );

        try {
            dataExportCheckerService.checkTimestamps(fromTimestamp, toTimestamp);

            def List<ExportColumn> orderedExportColumns = dataExportCheckerService.configureNeedSortedExportColumns(
                compareColumnsUnique,
                isNeedDownloads,
                isNeedRevenues,
                isNeedUpdates,
                isNeedRefunds,
                isNeedRefundsMonetized,
                isNeedRankings,
                isNeedMonthlyFinancial,
                needStepSafe
            );
            this.writeTableHeader(orderedExportColumns);

            List<Long> unnecessaryGameIds = idsInfo?.get(CompareColumn.SKU)?.allStrikedIds;
            List<Long> unnecessaryCountryIds = idsInfo?.get(CompareColumn.COUNTRY)?.allStrikedIds;

            def temporaryFileName = filename + "-" + Math.abs(random.nextInt());
            def tmpAbsolutePath = "${tmpFolderAbsolutePath}${temporaryFileName}.${extension}"

            log.debug String.format(LOG_TABLE_FETCHING, tmpAbsolutePath);
            dataExportFetcherService.fetchIntoFile(
                tmpAbsolutePath,
                unnecessaryGameIds,
                unnecessaryCountryIds,
                orderedExportColumns,
                fromTimestamp,
                toTimestamp,
                needStepSafe
            );

            //TODO:: think about Rankings
            log.debug LOG_TABLE_WRITING;
            this.writeTableInnerFromFile(orderedExportColumns, tmpAbsolutePath);
            log.debug LOG_TMP_DELETING;
            fileService.delete(tmpAbsolutePath);
/*
            List<GroovyRowResult> exportList = dataExportFetcherService.fetch(
                unnecessaryGameIds,
                unnecessaryCountryIds,
                orderedExportColumns,
                fromTimestamp,
                toTimestamp,
                needStepSafe
            );
            this.writeTableInner(orderedExportColumns, exportList);
*/
            if (isNeedTotal) {
                log.debug LOG_TOTAL_FETCHING;
                GroovyRowResult exportTotal = dataExportFetcherService.fetchTotals(
                    unnecessaryGameIds,
                    unnecessaryCountryIds,
                    fromTimestamp,
                    toTimestamp,
                    isNeedMonthlyFinancial
                );
                log.debug LOG_TOTAL_WRITING;
                this.writeTableTotal(orderedExportColumns, exportTotal);
            }

            this.writeTableFooter();

        } catch (DataExportNeedColumnException exc) {
            this.writeError(ERROR_SETTINGS);
            log.debug ERROR_SETTINGS;
        } catch (DataExportTimestampException exc) {
            this.writeError(ERROR_TIMESTAMPS);
            log.debug ERROR_TIMESTAMPS;
        } catch (SQLException exc) {
            this.writeError(ERROR_SQL);
            log.debug(exc.getMessage())
            log.debug ERROR_SQL;
        } catch (Exception exc) {
            this.writeError(ERROR);
            log.debug ERROR;
        } finally {

            this.writeFileFooter();

            tsvService.close();

            log.debug LOG_END;
            Long totalTimeSpent = System.currentTimeMillis() - startTime;
            log.debug "Total time spent: ${totalTimeSpent}"
        }
    }

    private def writeExportHeader(
        final Map<CompareColumn, Map<String, List<Long>>> idsInfo,
        final List<CompareColumn> compareColumnsUnique,
        final Long fromTimestamp,
        final Long toTimestamp,
        final Boolean isNeedDownloads,
        final Boolean isNeedRevenues,
        final Boolean isNeedUpdates,
        final Boolean isNeedRefunds,
        final Boolean isNeedRefundsMonetized,
        final Boolean isNeedRankings,
        final Boolean isNeedTotal,
        final Boolean isNeedMonthlyFinancial,
        final Step needStepSafe
    ) {

        def fromDate = timeService.formatTimestampToUtc(fromTimestamp, timeService.DATE_FORMAT_DAY_MONTH_SHORT_YEAR_FULL);
        def toDate = timeService.formatTimestampToUtc(toTimestamp, timeService.DATE_FORMAT_DAY_MONTH_SHORT_YEAR_FULL);
        def nowTimestamp = timeService.getTodayUtcTimestamp();
        def nowDatetime = timeService.formatTimestampToUtc(nowTimestamp, timeService.DATE_FORMAT_DAY_MONTH_SHORT_YEAR_FULL_TIME);

        def comparedByItemsNamesStr = this.configureExportHeaderComparedByNames(compareColumnsUnique);

        tsvService.writelnItem(String.format(HEADER_PREFIX));
        tsvService.writeln();

        tsvService.writelnItem(String.format(HEADER_TITLE));
        tsvService.writelnItem(String.format(HEADER_GENERATION_DATETIME, nowDatetime));
        tsvService.writeln();

        tsvService.writelnItem(String.format(HEADER_STEP, needStepSafe.title));
        tsvService.writeln();

        tsvService.writelnItem(String.format(HEADER_COMPARED_BY, comparedByItemsNamesStr));
        tsvService.writeln();

        this.writeExportHeaderFilters(idsInfo);
        tsvService.writeln();

        this.writeExportHeaderColumn(HEADER_COLUMN_DOWNLOADS, isNeedDownloads);
        this.writeExportHeaderColumn(HEADER_COLUMN_REVENUES, isNeedRevenues);
        this.writeExportHeaderColumn(HEADER_COLUMN_UPDATES, isNeedUpdates);
        this.writeExportHeaderColumn(HEADER_COLUMN_REFUNDS, isNeedRefunds);
        this.writeExportHeaderColumn(HEADER_COLUMN_REFUNDS_MONETIZED, isNeedRefundsMonetized);
        this.writeExportHeaderColumn(HEADER_COLUMN_RANKINGS, isNeedRankings);
        this.writeExportHeaderColumn(HEADER_COLUMN_MONTHLY_FINANCIAL, isNeedMonthlyFinancial);
        tsvService.writeln();

        this.writeExportHeaderColumn(HEADER_ROW_TOTAL, isNeedTotal);
        tsvService.writeln();

        tsvService.writelnItem(String.format(HEADER_DATE_RANGE, fromDate, toDate));
        tsvService.writeln();

        if (isNeedMonthlyFinancial) {
            tsvService.writelnItem(String.format(HEADER_NON_CALENDAR_WARN));
            tsvService.writeln();
        }

        tsvService.writelnItem(String.format(HEADER_POSTFIX));
        tsvService.writelnBlock();
    }

    private def configureExportHeaderComparedByNames(final List<CompareColumn> compareColumns) {

        def names = compareColumns?.collect {
            String.format(HEADER_COMPARED_BY_ITEM, it.title)
        }
        def comparedByItemsNamesStr = names?.join(HEADER_COMPARED_BY_ITEM_SEPARATOR);

        return comparedByItemsNamesStr;
    }

    private def writeExportHeaderFilters(final Map<CompareColumn, Map<String, List<Long>>> idsInfo) {

        CompareColumn.values()?.each () { compareColumn ->
            def columnIdsInfo = idsInfo?.getAt(compareColumn);

            def excludeIds = columnIdsInfo?.allStrikedIds
            def includeIds = columnIdsInfo?.allUnstrikedIds

            def List<GroovyRowResult> excludeFilterItems;
            def List<GroovyRowResult> includeFilterItems;
            switch (compareColumn) {
                case CompareColumn.COUNTRY:
                    excludeFilterItems = filterEntitiesService.fetchCountriesInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchCountriesInIds(includeIds);
                    break;
                case CompareColumn.VERSION:
                    excludeFilterItems = filterEntitiesService.fetchGameTypesInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchGameTypesInIds(includeIds);
                    break;
                case CompareColumn.STYLE:
                    excludeFilterItems = filterEntitiesService.fetchResolutionsInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchResolutionsInIds(includeIds);
                    break;
                case CompareColumn.STORE:
                    excludeFilterItems = filterEntitiesService.fetchStoresInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchStoresInIds(includeIds);
                    break;
                case CompareColumn.SKU:
                    excludeFilterItems = filterEntitiesService.fetchGamesInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchGamesInIds(includeIds);
                    break;
                case CompareColumn.METASKU:
                    excludeFilterItems = filterEntitiesService.fetchMetaGamesInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchMetaGamesInIds(includeIds);
                    break;
                case CompareColumn.REGION:
                    excludeFilterItems = filterEntitiesService.fetchRegionsInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchRegionsInIds(includeIds);
                    break;
                case CompareColumn.OS:
                    excludeFilterItems = filterEntitiesService.fetchPlatformsInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchPlatformsInIds(includeIds);
                    break;
                case CompareColumn.REQUIRED_PRODUCT_TYPE:
                    excludeFilterItems = filterEntitiesService.fetchRequiredProductTypesInIds(excludeIds);
                    includeFilterItems = filterEntitiesService.fetchRequiredProductTypesNotInIds(includeIds);
                    break;
                default:
                    excludeFilterItems = [];
                    includeFilterItems = [];
            }
            this.writeExportHeaderFilter(excludeFilterItems, includeFilterItems, compareColumn);
        }
    }

    private def writeExportHeaderFilter(
        final List<GroovyRowResult> excludeItems,
        final List<GroovyRowResult> includeItems,
        final CompareColumn compareColumn
    ) {

        def excludeCount = excludeItems?.size();
        def includeCount = includeItems?.size();
        def allCount = excludeCount + includeCount;
        def isAllInclude = (allCount == includeCount);
        def isAllExclude = (allCount == excludeCount);

        def filterValueStr = "";
        if (isAllInclude) {
            filterValueStr = String.format(HEADER_FILTER_INCLUDE, HEADER_FILTER_ALL);
        } else if (isAllExclude) {
            filterValueStr = String.format(HEADER_FILTER_EXCLUDE, HEADER_FILTER_ALL);
        } else if (includeCount <= excludeCount) {
            def itemsNamesStr = this.configureExportHeaderSettingFilterItemsNames(includeItems, includeCount);
            filterValueStr = String.format(HEADER_FILTER_INCLUDE, itemsNamesStr);
        } else {
            def itemsNamesStr = this.configureExportHeaderSettingFilterItemsNames(excludeItems, excludeCount);
            filterValueStr = String.format(HEADER_FILTER_EXCLUDE, itemsNamesStr);
        }

        tsvService.writelnItem(String.format(HEADER_FILTER, compareColumn.title, filterValueStr));
    }

    private def configureExportHeaderSettingFilterItemsNames(final items, final itemsCount) {

        def itemsNamesStr = "";
        if (itemsCount <= HEADER_FILTER_MAX) {
            def names = items.collect () {
                String.format(HEADER_FILTER_SOME, it.name);
            }
            itemsNamesStr = names?.join(HEADER_FILTER_SOME_SEPARATOR);
        } else {
            itemsNamesStr = String.format(HEADER_FILTER_MULTIPLE);
        }

        return itemsNamesStr;
    }

    private def writeExportHeaderColumn(final String format, final Boolean isNeed) {
        tsvService.writelnItem(String.format(format, isNeed ? HEADER_COLUMN_IS_INCLUDED : HEADER_COLUMN_IS_EXCLUDED));
    }

    private def writeTableHeader(final List<ExportColumn> orderedExportColumns) {

        this.writeTableRowPrefix();
        orderedExportColumns?.each () { exportColumn ->
            tsvService.writeItem(exportColumn.title);
        }
        tsvService.writeln();
    }

    private def writeTableInnerFromFile(final List<ExportColumn> orderedExportColumns, final String path) {

        File inFile = new File(path);
        if (inFile?.exists() && inFile?.canRead()) {
            BufferedReader bufferedReader = null;
            try {
                FileReader fileReader = new FileReader(inFile)
                bufferedReader = new BufferedReader(fileReader);

                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    this.writeTableRowPrefix();
                    tsvService.writelnItem(line);
                }
            } catch (FileNotFoundException exc) {
                log.error(String.format(LOG_FILE_ERROR, exc.getMessage(), path));
            } catch (IOException exc) {
                log.error(String.format(LOG_FILE_ERROR, exc.getMessage(), path));
            } catch (Exception exc) {
                log.error(String.format(LOG_FILE_ERROR, exc.getMessage(), path));
            } finally {
                if (bufferedReader != null ){
                    try {
                        bufferedReader.close();
                    } catch (IOException exc) {
                        log.error(String.format(LOG_FILE_NOTE, exc.getMessage(), path));
                    }
                }
            }
        } else {
            log.error(String.format(LOG_FILE_ACCESS, path));
        }
    }
/*
    private def writeTableInner(final List<ExportColumn> orderedExportColumns, final List<GroovyRowResult> exportList) {

        exportList?.each() { row ->
            this.writeTableRowPrefix();
            orderedExportColumns?.each () { exportColumn ->
                def item = row.containsKey(exportColumn.alias) ? row.getAt(exportColumn.alias) : UNKNOWN_VALUE
                tsvService.writeItem(item);
            }
            tsvService.writeln();
        }
    }
*/
    private def writeTableTotal(final List<ExportColumn> orderedExportColumns, final GroovyRowResult exportTotal) {

        if (exportTotal != null) {
            this.writeTableTotalRowPrefix();
            tsvService.writeItem(TOTAL);
            orderedExportColumns?.each () { exportColumn ->
                def item = UNNECESSARY_VALUE_TOTAL;
                if (
                    ExportColumnType.CURVE.equals(exportColumn.exportColumnType)
                    && !ExportColumn.RANKINGS.equals(exportColumn)
                ) {
                    item = exportTotal.containsKey(exportColumn.alias) ? exportTotal.getAt(exportColumn.alias) : UNKNOWN_VALUE
                }
                tsvService.writeItem(item);
            }
            tsvService.writeln();
        }
    }

    private def writeTableFooter() {
    }

    private def writeFileFooter() {
        tsvService.writelnBlock();
    }

    private def writeTableRowPrefix() {

        for (int i = 0; i < TABLE_ROW_PREFIX_AMOUNT; i++) {
            tsvService.writeEmpty();
        }
    }

    private def writeTableTotalRowPrefix() {

        for (int i = 0; i < TABLE_TOTAL_ROW_PREFIX_AMOUNT; i++) {
            tsvService.writeEmpty();
        }
    }

    private def writeError(final String errorMsg) {

        tsvService.writeItem(errorMsg);
        tsvService.writelnBlock();
    }
}