package zeptostats;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import zeptostats.export.DsvService;
import zeptostats.export.TsvService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Listener class which generates and sends data-export-report when such message is received.
 */
class DataExportMessageListenerService {

    private static final String LOG_PREFIX = "DataExport:";
    private static final String LOG_START = "$LOG_PREFIX start.";
    private static final String LOG_REPORT_GENERATING = "$LOG_PREFIX report generating (file %s)...";
    private static final String LOG_EMAIL_SENDING = "$LOG_PREFIX report sending...";
    private static final String LOG_END = "$LOG_PREFIX end.";

    private static final String LOG_ERROR_PREFIX = "$LOG_PREFIX report will not been generated and sent because";
    private static final String LOG_ERROR_FOLDER = "$LOG_ERROR_PREFIX tmp folder does not exists or is not readable or is not writtable.";
    private static final String LOG_ERROR_SERVICE = "$LOG_ERROR_PREFIX some services are not defined.";
    private static final String LOG_ERROR_PARAMETER = "$LOG_ERROR_PREFIX some need parameters are absent (DataExport Settings or Member).";

    @Autowired
    def ExportFileService exportFileService;
    @Autowired
    def ExportEmailService exportEmailService;
    @Autowired
    def DataExportService dataExportService;

    @Autowired
    def DataExportCheckerService dataExportCheckerService;

    /**
     * 
     * @param message Map {
     *      "member" => Member which should receive email with report,
     *      "savedSettings" => map of settings for report
     * }
     */
    def void export(final Map<String, Object> message) {

        log.debug LOG_START;

        Member member = message.get("member");
        def savedSettings = message.get("savedSettings");

        if (savedSettings != null && member != null) {
            List<CompareColumn> compareColumns = savedSettings.compareColumns;
            def Map<CompareColumn, Map<String, List<Long>>> idsInfo = savedSettings.idsInfo;
            Long fromTimestamp = savedSettings.fromTimestamp;
            Long toTimestamp = savedSettings.toTimestamp;
            Step needStep = savedSettings.step;
            Boolean ifNeedDownloads = savedSettings.ifNeedDownloads;
            Boolean ifNeedRevenues = savedSettings.ifNeedRevenues;
            Boolean ifNeedUpdates = savedSettings.ifNeedUpdates;
            Boolean ifNeedRefunds = savedSettings.ifNeedRefunds;
            Boolean ifNeedRefundsMonetized = savedSettings.ifNeedRefundsMonetized;
            Boolean ifNeedRankings = savedSettings.ifNeedRankings;
            Boolean ifNeedTotal = savedSettings.ifNeedTotal;
            Boolean ifNeedMonthlyFinancial = savedSettings.ifNeedMonthlyFinancial;

            log.debug "$LOG_PREFIX memberId: ${member.getId()}"
            log.debug "$LOG_PREFIX idsInfo: ${idsInfo}"
            log.debug "$LOG_PREFIX compareColumns: ${compareColumns}"
            log.debug "$LOG_PREFIX fromTimestamp: ${fromTimestamp}"
            log.debug "$LOG_PREFIX toTimestamp: ${toTimestamp}"
            log.debug "$LOG_PREFIX ifNeedDownloads: ${ifNeedDownloads}"
            log.debug "$LOG_PREFIX ifNeedRevenues: ${ifNeedRevenues}"
            log.debug "$LOG_PREFIX ifNeedUpdates: ${ifNeedUpdates}"
            log.debug "$LOG_PREFIX ifNeedRefunds: ${ifNeedRefunds}"
            log.debug "$LOG_PREFIX ifNeedRefundsMonetized: ${ifNeedRefundsMonetized}"
            log.debug "$LOG_PREFIX ifNeedRankings: ${ifNeedRankings}"
            log.debug "$LOG_PREFIX ifNeedTotal: ${ifNeedTotal}"
            log.debug "$LOG_PREFIX ifNeedMonthlyFinancial: ${ifNeedMonthlyFinancial}"
            log.debug "$LOG_PREFIX needStep: ${needStep}"

            if (exportFileService && dataExportService && exportEmailService) {
                def tmpFolderAbsolutePath = exportFileService.getTmpFolderAbsolutePath();
                log.debug "$LOG_PREFIX tmpFolderAbsolutePath: ${tmpFolderAbsolutePath}"

                File tmpFolder = new File(tmpFolderAbsolutePath);
                if (tmpFolder && tmpFolder.exists() && tmpFolder.canWrite() && tmpFolder.canRead()) {

                    def compareColumnsUnique = dataExportCheckerService.configureCompareColumnsUnique(compareColumns);

                    def asIsExportFileType = ExportFileType.TSV;
                    def asIsFileName = exportFileService.configureDataExportFileName(compareColumnsUnique);

                    def asIsFileExtension = exportFileService.getExtension(asIsExportFileType);

                    def asIsFilePath = "${tmpFolderAbsolutePath}${asIsFileName}.${asIsFileExtension}";
                    log.debug String.format(LOG_REPORT_GENERATING, asIsFilePath);
                    File outFile = new File(asIsFilePath);
                    FileOutputStream fileOutputStream = new FileOutputStream(outFile);

                    dataExportService.generateTsvReport(
                        fileOutputStream,
                        tmpFolderAbsolutePath,
                        asIsFileName,
                        asIsFileExtension,
                        idsInfo,
                        compareColumns,
                        fromTimestamp,
                        toTimestamp,
                        ifNeedDownloads,
                        ifNeedRevenues,
                        ifNeedUpdates,
                        ifNeedRefunds,
                        ifNeedRefundsMonetized,
                        ifNeedRankings,
                        ifNeedTotal,
                        ifNeedMonthlyFinancial,
                        needStep
                    );

                    log.debug LOG_EMAIL_SENDING;
                    def filenameForMember = asIsFileName;
                    exportEmailService.sendDataExportEmail(
                        member,
                        tmpFolderAbsolutePath,
                        asIsFilePath,
                        asIsFileName,
                        asIsExportFileType,
                        filenameForMember
                    );
                } else {
                    log.debug LOG_ERROR_FOLDER;
                }
            } else {
                log.debug LOG_ERROR_SERVICE;
            }
        } else {
            log.debug LOG_ERROR_PARAMETER;
        }

        log.debug LOG_END;
    }
}