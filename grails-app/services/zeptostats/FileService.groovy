package zeptostats;

/**
 * Class that can work with files.
 */
class FileService {

    public def delete(final String path) {

        File inFile = new File(path);
        if (inFile?.exists() && inFile?.canWrite()) {
            try {
                if (!inFile.delete()) {
                    log.error("File $path has not been deleted");
                }
            } catch (SecurityException exc) {
                log.error("File $path has not been deleted because of ${exc.getMessage()}");
            } catch (Exception exc) {
                log.error("File $path has not been deleted because of ${exc.getMessage()}");
            }
        } else {
            log.error("File $path has not been deleted because it's writtable");
        }
    }
}