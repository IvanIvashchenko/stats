package zeptostats

import zeptostats.export.DsvService;
import zeptostats.export.CsvService;
import groovy.sql.Sql;
import jxl.*;
import jxl.write.*;

class CountriesService {

    def dataSource_zeptostats;
    def dataSource;
    def countriesFilterService;
    def filterEntitiesService;
    def longService;

    private String ALL_TOTAL = "all";

    private Long APP_STORE_ID = 2L;
    private Long GOOGLE_STORE_ID = 4L;
    private String TOP_STORE_IDS = "2,4";

    private def HEADER_XLS_SEPARATOR = 2;
    private def TABLES_XLS_SEPARATOR = 3;
    private def APP_STORE_NAME = "App Store";
    private def GOOGLE_PLAY_NAME = "Google Play";
    private def OTHER_STORES_TITLE = "Other Stores";
    private def XLS_COLUMN_WIDTH = 25;
    private def TOTAL_TITLE = "Total";

    /**
    * Configures list of positions of last element in each group (0-based). Result can contain positions = -1 also.
    * 
    * For example:
    *   group = [[a,b], [c], [d,e,f]]
    *   lastPositions = [1, 2, 5]
    * 
    * @param groups list of elements lists
    * @return List of Long numbers
    */
    protected def configureLastPositions(groups) {

        def lastPositions = [];

        def previousCount = 0;
        groups.each() {
            lastPositions.add(it.count + previousCount - 1);
            previousCount += it.count;
        }

        lastPositions;
    }

    /**
    * Fetches list of Apps characteristics by given game ids in given Top store.
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param storeId need Top store id
    * @return List of maps [
    *   [
    *       resolution: String name of App's resolution,
    *       id: Long App's id,
    *       extra: String App's extra,
    *       title: String App's title
    *   ],
    *   ...
    * ]
    */
    def fetchTopStoreAppsCharacteristics(gameIds, storeId) {

        def db = new Sql(dataSource_zeptostats);
        def ids = longService.configureIdsString(gameIds);
        def sqlQuery = """
            select
                resolution.name as resolution,
                game.id,
                game.extra,
                game.title
            from
                game
                inner join resolution resolution on resolution.id = game.resolution_id
            where
                game.parent_game_id is null
                and game.store_id = :store_id
                and game.id in ($ids)
            order by
                CONCAT(game.meta_game_id, ' ', game_type_id),
                game.id
        """;
        def result = db.rows(sqlQuery, [store_id: storeId.toString()]);

        db.close();
        result;
    }

    /**
    * Fetches downloads for given Apps in selected store and countries by selected time period
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param countryIds need country ids
    * @param storeId need Top store id
    * @param savedTime map [
    *   from_time: Long timestamp
    *   to_date: Long timestamp
    * ]
    * @return Map of lists grouped by country name [
    *   [
    *       country_name_i: [
    *           [
    *               code: String Country code,
    *               cname: String Country name,
    *               country_id: Long Country id,
    *               game_id: Long App id,
    *               downloads: Long
    *           ],
    *           ...
    *       ],
    *       ...
    *   ]
    * ]
    */
    protected def fetchTopStoreAppsDownloads(gameIds, countryIds, storeId, savedTime) {

        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def countryIdsStr = longService.configureIdsString(countryIds);
        def sqlQuery = """
            select
                sum(
                    CASE
                        WHEN (
                            unit.downloads IS NULL
                            or (
                                (unit.date is null) or
                                ((unit.date < :from_date) or (:to_date < unit.date))
                            )
                        )
                        THEN 0 
                        ELSE unit.downloads
                    END
                ) as downloads,
                game_country.code,
                game_country.cname,
                game_country.game_id,
                game_country.country_id
            from
                unit unit
                right join (
                    select *
                    from
                        (
                            select
                                g.id as game_id,
                                CONCAT(g.meta_game_id, ' ', g.game_type_id) as meta_game_id
                            from game g
                            where
                                g.parent_game_id is null
                                and g.store_id = :store_id
                                and g.id in ($gameIdsStr)
                            order by g.id
                        ) as game,
                        (
                            select
                                c.id as country_id,
                                c.name as cname,
                                c.code as code
                            from country c
                            where c.id in ($countryIdsStr)
                        ) as country
                ) as game_country on (
                    game_country.game_id = unit.game_id
                    and game_country.country_id = unit.country_id
                )
            group by
                game_country.game_id,
                game_country.country_id
            order by
                game_country.meta_game_id,
                game_country.game_id
        """;
        def result = db.rows(sqlQuery, [
            store_id: storeId.toString(),
            from_date: savedTime.from_time,
            to_date: savedTime.to_time
        ]);

        db.close();
        result.groupBy{ it.cname }.sort();
    }

    /**
    * Fetches info about MetaApps characteristics by given game ids in Top given store
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param storeId need Top store id
    * @return List of maps [
    *   [
    *       path: String path to MetaApp's icon,
    *       name: String name of MetaApp's icon,
    *       count: Long amount of Apps for MetaApp
    *   ],
    *   ...
    * ]
    */
    protected def fetchTopStoreMetaAppsCharacteristics(gameIds, storeId) {

        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def sqlQuery = """
            select
                mggt.icon_path as path,
                mggt.icon_name as name,
                count(game.id) as count
            from
                meta_game_game_type as mggt
                inner join game as game on (
                    mggt.meta_game_id = game.meta_game_id
                    and mggt.game_type_id = game.game_type_id
                )
            where
                game.parent_game_id is null
                and game.store_id = :store_id
                and game.id in ($gameIdsStr)
            group by mggt.icon_path
            order by
                CONCAT(game.meta_game_id, ' ', game.game_type_id),
                game.id
        """;
        def result = db.rows(sqlQuery, [store_id: storeId.toString()]);

        db.close();
        result;
    }

    /**
    * Fetches list of sum of Apps downloads by given game ids in given Top store in selected or all countries (depends on totalFilter parameter).
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param totalFilter
    * @param countryIds need country ids
    * @param storeId need Top store id
    * @param savedTime map [
    *   from_time: Long timestamp
    *   to_date: Long timestamp
    * ]
    * @return List of maps [
    *   [
    *       downloads: Long sum of Apps downloads
    *   ],
    *   ...
    * ]
    */
    protected def fetchTopStoreAppsTotals(gameIds, countryIds, totalFilter, storeId, savedTime) {

        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def countryIdsStr = longService.configureIdsString(countryIds);
        def sqlQuery = "";

        if (totalFilter.equals(ALL_TOTAL)) {

            //TODO::think about top countries
            sqlQuery = """
                select
                    sum(
                        CASE
                            WHEN (
                                unit.downloads IS NULL
                                or (
                                    (unit.date is null) or
                                    ((unit.date < :from_date) or (:to_date < unit.date))
                                )
                            )
                            THEN 0
                            ELSE unit.downloads
                        END
                    ) as downloads
                from
                    unit unit
                    right join game g on g.id = unit.game_id
                where
                    g.parent_game_id is null
                    and g.store_id = :store_id
                    and g.id in ($gameIdsStr)
                group by g.id
                order by
                    CONCAT(g.meta_game_id, ' ', g.game_type_id),
                    g.id
            """;

        } else {
            sqlQuery = """
                select
                    sum(
                        CASE
                            WHEN (
                                unit.downloads IS NULL
                                or (
                                    (unit.date is null) or
                                    ((unit.date < :from_date) or (:to_date < unit.date))
                                )
                            )
                            THEN 0
                            ELSE unit.downloads
                        END
                    ) as downloads
                from
                    unit unit
                    right join (
                        select *
                        from
                            (
                                select
                                    g.id as game_id,
                                    CONCAT(g.meta_game_id, ' ', g.game_type_id) as meta_game_id
                                from game g
                                where
                                    g.parent_game_id is null
                                    and g.store_id = :store_id
                                    and g.id in ($gameIdsStr)
                                order by g.id
                            ) as game,
                            (
                                select
                                    c.id as country_id,
                                    c.name as cname,
                                    c.code as code
                                from country c
                                where c.id in ($countryIdsStr)
                            ) as country
                    ) as game_country on (
                        game_country.game_id = unit.game_id
                        and game_country.country_id = unit.country_id
                    )
                group by game_country.game_id
                order by
                    game_country.meta_game_id,
                    game_country.game_id
            """;
        }

        def result = db.rows(sqlQuery, [
            store_id: storeId.toString(),
            from_date: savedTime.from_time,
            to_date: savedTime.to_time
        ]);

        db.close();
        result;
    }

    /**
    * Fetches list of Store characteristics by given game ids which do not belong to given Top stores (so belong to some store except of top).
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from Top stores. All of them will not be considered in results)
    * @param topStoreIdsStr String which contain Top store ids
    * @return List of maps [
    *   [
    *       store: String name of Store,
    *       name: String name of Store's icon
    *       path: String path to Store's icon
    *       count: Long amount of Apps,
    *   ],
    *   ...
    * ]
    */
    protected def fetchOtherStoreAppsCharacteristics(gameIds, topStoreIdsStr) {

        def db = new Sql(dataSource_zeptostats);
        String ids = longService.configureIdsString(gameIds);
        def sqlQuery = """
            select
                store.name as store,
                logo.name,
                logo.path,
                count(game.id) as count 
            from
                store_logo logo
                inner join store store on logo.id = store.store_logo_id
                inner join game game on game.store_id = store.id
            where (
                game.parent_game_id is null
                and game.id in ($ids)
                and game.store_id not in ($topStoreIdsStr)
            )
            group by logo.path
            order by
                store.id,
                CONCAT(game.meta_game_id, ' ', game.game_type_id),
                game.id
        """;
        def result = db.rows(sqlQuery,[]);

        db.close();
        result;
    }

    /**
    * Fetches list of Apps characteristics by given game ids which do not belong to given Top stores (so belong to some store except of top).
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from Top stores. All of them will not be considered in results)
    * @param topStoreIdsStr String which contain Top store ids
    * @return List of maps [
    *   [
    *       resolution: String name of App's resolution,
    *       title: String App's title,
    *       name: String name of Meta-App's icon
    *       path: String path to Meta-App's icon
    *   ],
    *   ...
    * ]
    */
    protected def fetchOtherAppsCharacteristics(gameIds, topStoreIdsStr) {

        def db = new Sql(dataSource_zeptostats);
        String ids = longService.configureIdsString(gameIds);
        def sqlQuery = """
            select
                game.title,
                mggt.icon_name as name,
                mggt.icon_path as path,
                resolution.name as resolution
            from
                game game
                inner join resolution resolution on game.resolution_id = resolution.id
                inner join meta_game_game_type mggt on (
                    game.meta_game_id = mggt.meta_game_id
                    and game.game_type_id = mggt.game_type_id
                )
            where
                game.parent_game_id is null
                and game.id in ($ids)
                and game.store_id not in ($topStoreIdsStr)
            order by
                game.store_id,
                CONCAT(game.meta_game_id, ' ', game.game_type_id),
                game.id
        """;
        def result = db.rows(sqlQuery,[]);
        db.close();
        result;
    }

    /**
    * Fetches list of sum of Apps downloads by given game ids which do not belong to given Top stores (so belong to some store except of top) in selected or all countries (depends on totalFilter parameter).
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param totalFilter
    * @param countryIds need country ids
    * @param topStoreIdsStr String which contain Top store ids
    * @param savedTime map [
    *   from_time: Long timestamp
    *   to_date: Long timestamp
    * ]
    * @return List of maps [
    *   [
    *       downloads: Long sum of Apps downloads
    *   ],
    *   ...
    * ]
    */
    protected def fetchOtherStoreAppsTotals(gameIds, countryIds, totalFilter, topStoreIdsStr, savedTime) {


        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def countryIdsStr = longService.configureIdsString(countryIds);
        def sqlQuery = "";

        if (totalFilter.equals(ALL_TOTAL)) {

            sqlQuery = """
                select
                    sum(
                        CASE
                            WHEN (
                                unit.downloads IS NULL
                                or (
                                    (unit.date is null) or
                                    ((unit.date < :from_date) or (:to_date < unit.date))
                                )
                            )
                            THEN 0
                            ELSE unit.downloads
                        END
                    ) as downloads
                from
                    unit unit
                    right join game g on g.id = unit.game_id
                where
                    g.parent_game_id is null
                    and g.store_id not in ($topStoreIdsStr)
                    and g.id in ($gameIdsStr)
                group by g.id
                order by
                    g.store_id,
                    CONCAT(g.meta_game_id, ' ', g.game_type_id),
                    g.id
            """;

        } else {
            sqlQuery = """
                select
                    sum(
                        CASE
                            WHEN (
                                unit.downloads IS NULL
                                or (
                                    (unit.date is null) or
                                    ((unit.date < :from_date) or (:to_date < unit.date))
                                )
                            )
                            THEN 0
                            ELSE unit.downloads
                        END
                    ) as downloads
                from
                    unit unit
                    right join (

                        select *
                        from
                            (
                                select
                                    g.id as game_id,
                                    CONCAT(g.meta_game_id, ' ', g.game_type_id) as meta_game_id,
                                    g.store_id as store_id
                                from game g
                                where
                                    g.parent_game_id is null
                                    and g.store_id not in ($topStoreIdsStr)
                                    and g.id in ($gameIdsStr)
                                order by g.id
                            ) as game,
                            (
                                select
                                    c.id as country_id,
                                    c.name as cname,
                                    c.code as code
                                from country c
                                where c.id in ($countryIdsStr)
                            ) as country
                    ) as game_country on (
                        game_country.game_id = unit.game_id
                        and game_country.country_id = unit.country_id
                    )
                group by game_country.game_id
                order by
                    game_country.store_id,
                    game_country.meta_game_id,
                    game_country.game_id
            """;
        }

        def result = db.rows(sqlQuery, [from_date: savedTime.from_time,to_date: savedTime.to_time]);

        db.close();
        result;
    }

    /**
    * Fetches downloads for given Apps in all stores (except top stores) and countries by selected time period
    * 
    * @param gameIds need game ids (can contain inappIds, can contain games from other stores. All of them will not be considered in results)
    * @param countryIds need country ids
    * @param topStoreIdsStr String which contain Top store ids
    * @param savedTime map [
    *   from_time: Long timestamp
    *   to_date: Long timestamp
    * ]
    * @return Map of lists grouped by country name [
    *   [
    *       country_name_i: [
    *           [
    *               code: String Country code,
    *               cname: String Country name,
    *               country_id: Long Country id,
    *               game_id: Long App id,
    *               downloads: Long
    *           ],
    *           ...
    *       ],
    *       ...
    *   ]
    * ]
    */
    protected def fetchOtherStoreAppsDownloads(gameIds, countryIds, topStoreIdsStr, savedTime) {

        def db = new Sql(dataSource_zeptostats);
        def gameIdsStr = longService.configureIdsString(gameIds);
        def countryIdsStr = longService.configureIdsString(countryIds);
        def sqlQuery = """
            select
                sum(
                    CASE
                        WHEN (
                            unit.downloads IS NULL
                            or (
                                (unit.date is null) or
                                ((unit.date < :from_date) or (:to_date < unit.date))
                            )
                        )
                        THEN 0
                        ELSE unit.downloads
                    END
                ) as downloads,
                game_country.code,
                game_country.cname,
                game_country.game_id,
                game_country.country_id
            from
                unit unit
                right join (
                        select *
                        from
                            (
                                select
                                    g.id as game_id,
                                    CONCAT(g.meta_game_id, ' ', g.game_type_id) as meta_game_id,
                                    g.store_id as store_id
                                from game g
                                where
                                    g.parent_game_id is null
                                    and g.store_id not in ($topStoreIdsStr)
                                    and g.id in ($gameIdsStr)
                                order by g.id
                            ) as game,
                            (
                                select
                                    c.id as country_id,
                                    c.name as cname,
                                    c.code as code
                                from country c
                                where c.id in ($countryIdsStr)
                            ) as country
                ) as game_country on (
                    game_country.game_id = unit.game_id
                    and game_country.country_id = unit.country_id
                )
            group by
                game_country.game_id,
                game_country.country_id
            order by
                store_id,
                game_country.meta_game_id,
                game_country.game_id
        """;
        def result = db.rows(sqlQuery,[from_date: savedTime.from_time,to_date: savedTime.to_time]);

        db.close();
        result.groupBy{ it.cname }.sort();
    }

    def fetchTablesData(total, selectedGames, selectedCountries, savedTime) {

        def appstoreAppsCharacteristics = this.fetchTopStoreAppsCharacteristics(selectedGames, APP_STORE_ID);
        def appstoreAppsDownloads = this.fetchTopStoreAppsDownloads(
            selectedGames,
            selectedCountries,
            APP_STORE_ID,
            savedTime
        );
        def appstoreMetaAppsCharacteristics = this.fetchTopStoreMetaAppsCharacteristics(selectedGames, APP_STORE_ID);
        def appstoreLastPositions = this.configureLastPositions(appstoreMetaAppsCharacteristics);
        def appstoreAppsTotal = this.fetchTopStoreAppsTotals(
            selectedGames,
            selectedCountries,
            total,
            APP_STORE_ID,
            savedTime
        );

        def googleAppsCharacteristics = this.fetchTopStoreAppsCharacteristics(selectedGames, GOOGLE_STORE_ID);
        def googleAppsDownloads = this.fetchTopStoreAppsDownloads(
            selectedGames,
            selectedCountries,
            GOOGLE_STORE_ID,
            savedTime
        );
        def googleMetaAppsCharacteristics = this.fetchTopStoreMetaAppsCharacteristics(selectedGames, GOOGLE_STORE_ID);
        def googleLastPositions = this.configureLastPositions(googleMetaAppsCharacteristics);
        def googleAppsTotal = this.fetchTopStoreAppsTotals(
            selectedGames,
            selectedCountries,
            total,
            GOOGLE_STORE_ID,
            savedTime
        );

        def otherAppsStoreCharacteristics = this.fetchOtherStoreAppsCharacteristics(selectedGames, TOP_STORE_IDS);
        def otherAppsCharacteristics = this.fetchOtherAppsCharacteristics(selectedGames, TOP_STORE_IDS);
        def otherLastPositions = this.configureLastPositions(otherAppsStoreCharacteristics);
        def otherAppsDownloads = this.fetchOtherStoreAppsDownloads(
            selectedGames,
            selectedCountries,
            TOP_STORE_IDS,
            savedTime
        );
        def otherAppsTotals = this.fetchOtherStoreAppsTotals(
            selectedGames,
            selectedCountries,
            total,
             TOP_STORE_IDS,
            savedTime
        );

        //TODO:: replace to config
        def zeptostatsHost = "http://localhost:8086";
        def renderAppStore = (appstoreAppsCharacteristics?.size() > 0 && appstoreAppsDownloads?.size() > 0);
        def renderGooglePlay = (googleAppsCharacteristics?.size() > 0 && googleAppsDownloads?.size() > 0);
        def renderOthers = (otherAppsCharacteristics?.size() > 0 && otherAppsDownloads?.size() > 0)

        def tablesData = [
            appstoreLogo: "/images/pages/countries/app-store.png",
            googleStoreLogo: "/images/pages/countries/google-play.png",
            appstoreAppsCharacteristics: appstoreAppsCharacteristics,
            appstoreAppsDownloads: appstoreAppsDownloads,
            appstoreMetaAppsCharacteristics: appstoreMetaAppsCharacteristics,
            appstoreLastPositions: appstoreLastPositions,
            appstoreAppsTotal: appstoreAppsTotal,
            googleAppsCharacteristics: googleAppsCharacteristics,
            googleAppsDownloads: googleAppsDownloads,
            googleMetaAppsCharacteristics: googleMetaAppsCharacteristics,
            googleLastPositions: googleLastPositions,
            googleAppsTotal: googleAppsTotal,
            renderAppStore: renderAppStore,
            renderGooglePlay: renderGooglePlay,
            renderData: (renderAppStore || renderGooglePlay || renderOthers),
            otherAppsStoreCharacteristics: otherAppsStoreCharacteristics,
            otherAppsCharacteristics: otherAppsCharacteristics,
            otherLastPositions: otherLastPositions,
            otherAppsDownloads: otherAppsDownloads,
            otherAppsTotals: otherAppsTotals,
            zeptostatsHost: zeptostatsHost,
            renderOthers: renderOthers
        ];
    }

    private def generateTopStoreXLSReport(sheet, storeName, headers, downloads, totals, row) {

        def col = 1;
        headers.each() {

            XlsExportUtils.addBoldColumnToSheetRow(sheet, storeName, 0, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addColumnToSheetRow(sheet, it.title, col, row, this.XLS_COLUMN_WIDTH);
            col++;
        }
        row += this.HEADER_XLS_SEPARATOR;

        downloads.each() {

            XlsExportUtils.addColumnToSheetRow(sheet, it.value.first().cname, 0, row, this.XLS_COLUMN_WIDTH);
            col = 1;
            it.value.each() {
                XlsExportUtils.addColumnToSheetRow(sheet, it.downloads, col, row, this.XLS_COLUMN_WIDTH);
                col++;
            }
            row++;
        }

        col = 1;
        totals.each() {

            XlsExportUtils.addBoldColumnToSheetRow(sheet, this.TOTAL_TITLE, 0, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addColumnToSheetRow(sheet, it.downloads, col, row, this.XLS_COLUMN_WIDTH);
            col++;
        }
        row++;
    }

    private def generateOtherStoresXLSReport(sheet, stores, games, downloads, totals, row) {

        int col = 1;
        stores.each() {

            XlsExportUtils.addBoldColumnToSheetRow(sheet, this.OTHER_STORES_TITLE, 0, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addBoldColumnToSheetRow(sheet, it.store, col, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addEmtyCellsToSheetRow(sheet, row, col+1, it.count)
            col += (it.count);
        }
        row ++;

        col = 1;
        games.each() {

            XlsExportUtils.addBoldColumnToSheetRow(sheet, "", 0, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addColumnToSheetRow(sheet, it.title, col, row, this.XLS_COLUMN_WIDTH);
            col++;
        }
        row += this.HEADER_XLS_SEPARATOR;

        downloads.each() {

            XlsExportUtils.addColumnToSheetRow(sheet, it.value.first().cname, 0, row, this.XLS_COLUMN_WIDTH);
            col = 1;
            it.value.each() {
                XlsExportUtils.addColumnToSheetRow(sheet, it.downloads, col, row, this.XLS_COLUMN_WIDTH);
                col++;
            }
            row++;
        }

        col = 1;
        totals.each() {

            XlsExportUtils.addBoldColumnToSheetRow(sheet, this.TOTAL_TITLE, 0, row, this.XLS_COLUMN_WIDTH);
            XlsExportUtils.addColumnToSheetRow(sheet, it.downloads, col, row, this.XLS_COLUMN_WIDTH);
            col++;
        }
        row++;
    }

    def void generateXlsReport(OutputStream outputStream, memberId) {

        def savedCountries = countriesFilterService.fetchSavedCountriesForFilter(memberId);
        def savedGames = countriesFilterService.fetchSavedGamesForFilter(memberId);
        def savedTotal = countriesFilterService.fetchSavedTotalForFilter(memberId);
        def savedTime = countriesFilterService.fetchSavedTimeForFilter(memberId);
        def tablesData = this.fetchTablesData(savedTotal, savedGames, savedCountries, savedTime);

        def workbook = XlsExportUtils.createWorkbook(outputStream);
        def formattedFromToString = filterEntitiesService.configureDatepickerString(
            savedTime.from_time.toLong(),
            savedTime.to_time.toLong()
        );
        def sheet = XlsExportUtils.addSheetToWorkbook(workbook, "Downloads by country", 0);
        def row = 0;
        row = this.generateTopStoreXLSReport(
            sheet,
            this.APP_STORE_NAME,
            tablesData.appstoreAppsCharacteristics,
            tablesData.appstoreAppsDownloads,
            tablesData.appstoreAppsTotal,
            row
        );
        row += this.TABLES_XLS_SEPARATOR;
        row = this.generateTopStoreXLSReport(
            sheet,
            this.GOOGLE_PLAY_NAME,
            tablesData.googleAppsCharacteristics,
            tablesData.googleAppsDownloads,
            tablesData.googleAppsTotal,
            row
        );
        row += this.TABLES_XLS_SEPARATOR;
        this.generateOtherStoresXLSReport(
            sheet,
            tablesData.otherAppsStoreCharacteristics,
            tablesData.otherAppsCharacteristics,
            tablesData.otherAppsDownloads,
            tablesData.otherAppsTotals,
            row
        );
        workbook.write();
        workbook.close();
    }

    private def generateTopStoreCsvReport(final DsvService dsvService, storeName, headers, downloads, totals) {

        dsvService.writeItem(storeName);
        headers.each() {
            dsvService.writeItem(it.title);
        }

        dsvService.writelnBlock();

        downloads.each() { countryName, countryDownloads ->
            dsvService.writeItem(countryName);
            countryDownloads.each() { gameDownloads ->
                dsvService.writeItem(gameDownloads.downloads);
            }
            dsvService.writeln();
        }

        dsvService.writeln();

        dsvService.writeItem(TOTAL_TITLE);
        totals.each() {
            dsvService.writeItem(it.downloads);
        }

        dsvService.writeln();
    }

    private def generateOtherStoresCsvReport(final DsvService dsvService, stores, games, downloads, totals) {

        dsvService.writeItem(this.OTHER_STORES_TITLE);
        stores.each() {
            dsvService.writeItem(it.store);
            def i = 1;
            while (i < it.count) {
                dsvService.writeItem("")
                i++;
            }
        }
        dsvService.writelnBlock();

        dsvService.writeItem("");
        games.each() {
            dsvService.writeItem(it.title);
        }
        dsvService.writelnBlock();

        downloads.each() { countryName, countryDownloads ->
            dsvService.writeItem(countryName);
            countryDownloads.each() { gameDownloads ->
                dsvService.writeItem(gameDownloads.downloads);
            }
            dsvService.writeln();
        }

        dsvService.writeln();

        dsvService.writeItem(TOTAL_TITLE);
        totals.each() {
            dsvService.writeItem(it.downloads);
        }

        dsvService.writeln();
    }

    def void generateCsvReport(OutputStream outputStream, memberId) {

        def DsvService dsvService = new CsvService();
        dsvService.init(outputStream);

        def savedCountries = countriesFilterService.fetchSavedCountriesForFilter(memberId);
        def savedGames = countriesFilterService.fetchSavedGamesForFilter(memberId);
        def savedTotal = countriesFilterService.fetchSavedTotalForFilter(memberId);
        def savedTime = countriesFilterService.fetchSavedTimeForFilter(memberId);
        def tablesData = this.fetchTablesData(savedTotal, savedGames, savedCountries, savedTime);

        this.generateTopStoreCsvReport(
            dsvService,
            this.APP_STORE_NAME,
            tablesData.appstoreAppsCharacteristics,
            tablesData.appstoreAppsDownloads,
            tablesData.appstoreAppsTotal
        );

        dsvService.writelnBlock();

        this.generateTopStoreCsvReport(
            dsvService,
            this.GOOGLE_PLAY_NAME,
            tablesData.googleAppsCharacteristics,
            tablesData.googleAppsDownloads,
            tablesData.googleAppsTotal
        );

        dsvService.writelnBlock();

        this.generateOtherStoresCsvReport(
            dsvService,
            tablesData.otherAppsStoreCharacteristics,
            tablesData.otherAppsCharacteristics,
            tablesData.otherAppsDownloads,
            tablesData.otherAppsTotals
        );

        dsvService.close();
    }

}