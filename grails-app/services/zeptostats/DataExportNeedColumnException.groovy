package zeptostats;

/**
 * Is thrown when need column is impossible for all selected CompareColumns or (and) step or (and) curves.
 */
class DataExportNeedColumnException extends Exception {

    public DataExportNeedColumnException() {
        super()
    }

    public DataExportNeedColumnException(final String message) {
        super(message)
    }

    public DataExportNeedColumnException(final Throwable throwable) {
        super(throwable)
    }

    public DataExportNeedColumnException(final String message, final Throwable throwable) {
        super(message, throwable)
    }
}