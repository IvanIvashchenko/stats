package zeptostats

class CountriesAppsFilter {

    Long gameId;
    Member member;

    static constraints = {
        gameId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}