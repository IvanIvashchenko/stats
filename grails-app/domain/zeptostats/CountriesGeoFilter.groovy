package zeptostats

class CountriesGeoFilter {
    
    Long countryId;
    Member member;
    
    static constraints = {
        countryId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}