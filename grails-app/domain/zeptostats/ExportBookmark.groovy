package zeptostats

/**
 * Contains saved bookmark in DataExport page
 */
class ExportBookmark {

    String name;

    static constraints = {
        name notnull: true, unique: true 
    }

    static mapping = {
        version false
    }
}