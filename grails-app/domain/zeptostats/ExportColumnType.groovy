package zeptostats

/**
 * Enum to keep all need info about Export column type.
 */
enum ExportColumnType {

    /** type for dates */
    DATE,
    /** type for SKU's properties */
    SKU,
    /** type for geo properties */
    GEO,
    /** type for curve properties */
    CURVE


    ExportColumnType() {
    }
}