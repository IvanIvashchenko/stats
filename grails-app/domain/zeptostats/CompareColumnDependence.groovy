package zeptostats

/**
 * Enum to keep info about dependence between CompareColumns.
 * 
 * The most specific CompareColumn should be in the top.
 * Tree should be without cycles.
 */
enum CompareColumnDependence {

    COUNTRY(CompareColumn.COUNTRY, null),
    REGION(CompareColumn.REGION, CompareColumnDependence.COUNTRY),

    SKU(CompareColumn.SKU, null),
    REQUIRED_PRODUCT_TYPE(CompareColumn.REQUIRED_PRODUCT_TYPE, CompareColumnDependence.SKU),
    METASKU(CompareColumn.METASKU, CompareColumnDependence.SKU),
    VERSION(CompareColumn.VERSION, CompareColumnDependence.SKU),
    STYLE(CompareColumn.STYLE, CompareColumnDependence.SKU),
    STORE(CompareColumn.STORE, CompareColumnDependence.SKU),
    OS(CompareColumn.OS, CompareColumnDependence.STORE)

    private final static String JSON_LIST_PREFIX = "[";
    private final static String JSON_LIST_POSTFIX = "]";
    private final static String JSON_LIST_OPTIONS_PREFIX = "\"";
    private final static String JSON_LIST_OPTIONS_POSTFIX = "\"";
    private final static String JSON_LIST_OPTIONS_SEPARATOR = JSON_LIST_OPTIONS_PREFIX + "," + JSON_LIST_OPTIONS_POSTFIX;


    CompareColumn column;

    /** parent CompareColumnDependence */
    CompareColumnDependence parent;

    /** top parent CompareColumnDependence */
    CompareColumnDependence topParent;

    FilterColumn filterColumn;


    CompareColumnDependence(final CompareColumn column, final CompareColumnDependence parent) {

        this.column = column;
        this.parent = parent;
        this.filterColumn = column?.filterColumn;

        def topParent = null;
        def cur = this.parent
        while (cur != null) {
            topParent = cur;
            cur = cur.parent
        }

        this.topParent = topParent;
    }

    public static List<CompareColumnDependence> getIndependentValues() {

        List<CompareColumnDependence> independentValues = [];

        CompareColumnDependence.values()?.each() {
            if (it?.parent == null) {
                independentValues.add(it);
            }
        }

        return independentValues;
    }

    public boolean isTop() {
        return (this.topParent == null);
    }

    public boolean hasDependentFilter() {

        def existingFilterColumn = this.column?.filterColumn;
        def topParent = this.isTop() ? this : this.topParent;

        if (topParent == null || existingFilterColumn == null) {
            return false;
        }
        return topParent.hasDependentFilterRecursive(existingFilterColumn);
    }

    protected boolean hasDependentFilterRecursive(final FilterColumn existingFilterColumn) {

        def curFilterColumn = this.column?.filterColumn;
        if (curFilterColumn == null) {
            return false;
        }
        if (!curFilterColumn.equals(existingFilterColumn)) {
            return true;
        }

        def children = this.getChildren();
        for (CompareColumnDependence child in children) {
            if (child.hasDependentFilterRecursive(existingFilterColumn)) {
                return true;
            }
        }
        return false;
    }

    public List<CompareColumnDependence> getChildren() {

        List<CompareColumnDependence> chilrden = [];

        CompareColumnDependence.values()?.each() {
            if (it?.parent?.column.equals(this.column)) {
                chilrden.add(it);
            }
        }

        return chilrden;
    }

    public List<CompareColumnDependence> getChildrenExcept(final List<CompareColumnDependence> except) {

        List<CompareColumnDependence> chilrdenExcept = this.getChildren()
        chilrdenExcept.removeAll(except)

        return chilrdenExcept;
    }

    /**
     * 
     * @param str String which contains name of some DependentColumn
     * @return null or valid parsed DependentColumn
     */
    public static CompareColumnDependence parse(final String str) {

        CompareColumnDependence valid = null
        try {
            valid = CompareColumnDependence.valueOf(str)
        } catch (Exception exc) {
        }

        return valid
    }

    public FilterColumn getTopParentFilterColumn() {

        if (this.topParent != null) {
            return this.topParent.filterColumn
        }

        return null;
    }
}