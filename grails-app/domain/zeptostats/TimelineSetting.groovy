package zeptostats

/**
 * Contains different settings for Member in Timeline page
 */
class TimelineSetting {

    Boolean ifNeedAppsDownloads;
    Boolean ifNeedInappsDownloads;
    Boolean ifNeedAppsRevenue;
    Boolean ifNeedInappsRevenue;
    Boolean ifNeedAppsCompare;
    Boolean ifNeedInappsCompare;
    Boolean ifNeedUpdates;
    Boolean ifNeedUpdatesCompare;
    CompareColumn compareColumn;
    Step step;
    Long fromTimestamp;
    Long toTimestamp;
    Member member;

    static constraints = {
        ifNeedAppsDownloads nullable: true
        ifNeedInappsDownloads nullable: true
        ifNeedAppsRevenue nullable: true
        ifNeedInappsRevenue nullable: true
        ifNeedAppsCompare nullable: true
        ifNeedInappsCompare nullable: true
        ifNeedUpdates nullable: true
        ifNeedUpdatesCompare nullable: true
        compareColumn nullable: true
        fromTimestamp nullable: true
        toTimestamp nullable: true
        step nullable: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}