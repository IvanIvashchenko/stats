package zeptostats

class CountriesTotalFilter {
    
    String totalFilter;
    Member member;
    
    static constraints = {
        totalFilter notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}