package zeptostats

import java.util.List;

/**
 * Enum to keep info about comparison column.
 */
enum CompareColumn {

    VERSION("version", "Versions", FilterColumn.VERSION, null),
    STYLE("style", "Style", FilterColumn.STYLE, null),
    REGION("region", "Regions", FilterColumn.COUNTRY, null),
    COUNTRY("country", "Countries", FilterColumn.COUNTRY, null),
    OS("os", "OS", FilterColumn.STORE, null),
    STORE("store", "Stores", FilterColumn.STORE, null),
    METASKU("metasku", "MetaSKUs", FilterColumn.SKU, null),
    REQUIRED_PRODUCT_TYPE("requiredproducttype", "SKU Type", FilterColumn.REQUIRED_PRODUCT_TYPE, null),
    SKU("sku", "SKUs", FilterColumn.SKU, [CompareColumn.REGION, CompareColumn.COUNTRY])

    private final static String JSON_LIST_PREFIX = "[";
    private final static String JSON_LIST_POSTFIX = "]";
    private final static String JSON_LIST_OPTIONS_PREFIX = "\"";
    private final static String JSON_LIST_OPTIONS_POSTFIX = "\"";
    private final static String JSON_LIST_OPTIONS_SEPARATOR = JSON_LIST_OPTIONS_PREFIX + "," + JSON_LIST_OPTIONS_POSTFIX;

    String code;
    String title;
    FilterColumn filterColumn;
    /** availableGroup is null available are all CompareColumns except of CompareColumns with same FilterColumn */
    List<CompareColumn> justAvailableGroup;

    CompareColumn(
        final String code, final String title, final FilterColumn filterColumn, final List<CompareColumn> justAvailableGroup
    ) {

        this.code = code;
        this.title = title;
        this.filterColumn = filterColumn;
        this.justAvailableGroup = justAvailableGroup;
    }

    public boolean isSameFilter(final CompareColumn compareColumn) {

        def filtersAreSame = false;

        if (compareColumn == null) {
            return filtersAreSame;
        }

        if (this.filterColumn == null || compareColumn.filterColumn == null) {
            return filtersAreSame;
        }

        filtersAreSame = this.filterColumn.equals(compareColumn.filterColumn);

        return filtersAreSame;
    }

    public boolean isIn(final List<CompareColumn> validList) {
        return validList != null && validList.contains(this);
    }

    /**
     * 
     * @param str String which contains name of some CompareColumn
     * @return null or valid parsed CompareColumn
     */
    public static CompareColumn parse(final String str) {

        CompareColumn valid = null
        try {
            valid = CompareColumn.valueOf(str)
        } catch (Exception exc) {
        }

        return valid
    }

    /**
     * 
     * @param str String which contains name of some CompareColumn
     * @return String name of valid parsed CompareColumn (or "" if it was invalid)
     */
    public static CompareColumn parseName(final String str) {

        CompareColumn valid = CompareColumn.parse(str)
        return valid == null ? "" : valid.toString()
    }

    /**
     * 
     * @param stringsList list of Strings which contains name of some CompareColumn
     * @return list of valid parsed CompareColumn. It is not null always. It does not contain null as CompareColumn.
     */
    public static List<CompareColumn> parseList(stringsList) {

        List<CompareColumn> validList = new LinkedList<CompareColumn>()
        stringsList?.each() {
            CompareColumn valid = CompareColumn.parse(it)
            if (valid != null) {
                validList.add(valid)
            }
        }

        return validList
    }

    /**
     * 
     * @return String which contains JSON with list of just available CompareColumns
     */
    public String getJustAvailableCompareColumnsJsonStr() {

        def strings = this.justAvailableGroup?.collect () {
            it.toString()
        }
        def jsonListStr = strings == null ? "" : JSON_LIST_OPTIONS_PREFIX + strings.join(JSON_LIST_OPTIONS_SEPARATOR) + JSON_LIST_OPTIONS_POSTFIX

        return JSON_LIST_PREFIX + jsonListStr + JSON_LIST_POSTFIX
    }
}