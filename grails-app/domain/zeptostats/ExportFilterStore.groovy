package zeptostats

/**
 * Contains unnecessary Store for Member in DataExport page
 */
class ExportFilterStore {

    Long storeId;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        storeId notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}