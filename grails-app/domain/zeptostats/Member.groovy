package zeptostats

class Member {

    transient springSecurityService
    transient boolean isPasswordEncoded = false

    String username
    String password
    String email
    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static constraints = {
        username blank: false, unique: true
        password blank: false
        email blank: false, email: true
    }

    static mapping = {
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        MemberRole.findAllByMember(this).collect { it.role } as Set
    }

    def beforeInsert() {
        if ( !isPasswordEncoded ) {
            isPasswordEncoded = true
            encodePassword()
        }
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            isPasswordEncoded = false
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }
}
