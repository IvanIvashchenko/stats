package zeptostats

/**
 * Contains different settings for Member in DataExport page
 */
class ExportSetting {

    Boolean ifNeedDownloads;
    Boolean ifNeedRevenues;
    Boolean ifNeedUpdates;
    Boolean ifNeedRankings;
    Boolean ifNeedRefunds;
    Boolean ifNeedRefundsMonetized;
    Boolean ifNeedTotal;
    Boolean ifNeedMonthlyFinancial;
    Long fromTimestamp;
    Long toTimestamp;
    Step step;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        ifNeedDownloads nullable: true
        ifNeedRevenues nullable: true
        ifNeedUpdates nullable: true
        ifNeedRankings nullable: true
        ifNeedRefunds nullable: true
        ifNeedRefundsMonetized nullable: true
        ifNeedTotal nullable: true
        ifNeedMonthlyFinancial nullable: true
        fromTimestamp nullable: true
        toTimestamp nullable: true
        step nullable: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}