package zeptostats

/**
 * Contains unnecessary GameType for Member in DataExport page
 */
class ExportFilterGameType {

    String gameType;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        gameType notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}