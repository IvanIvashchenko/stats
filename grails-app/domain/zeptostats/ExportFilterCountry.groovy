package zeptostats

/**
 * Contains unnecessary Country for Member in DataExport page
 */
class ExportFilterCountry {

    Long countryId;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        countryId notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}