package zeptostats

/**
 * Enum to keep info about filter column.
 */
enum FilterColumn {

    VERSION,
    STYLE,
    COUNTRY,
    STORE,
    REQUIRED_PRODUCT_TYPE,
    SKU
}