package zeptostats

/**
 * Contains unnecessary Store for Member in Timeline page
 */
class TimelineFilterStore {

    Long storeId;
    Member member;

    static constraints = {
        storeId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}