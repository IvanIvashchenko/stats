package zeptostats

class CountriesTimeFilter {
    
    Long fromTime;
    Long toTime;
    Member member;
    
    static constraints = {
        fromTime notnull: true
        toTime notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}