package zeptostats

class FinanceStoresFilter {

    Long storeId;
    Member member;

    static constraints = {
        storeId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}