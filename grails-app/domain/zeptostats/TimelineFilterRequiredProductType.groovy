package zeptostats

class TimelineFilterRequiredProductType {

    String requiredProductType;
    Member member;

    static constraints = {
        requiredProductType notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }
}
