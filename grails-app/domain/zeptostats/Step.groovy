package zeptostats

/**
 * Enum to keep info about time-step.
 */
enum Step {

    DAILY("date", "Daily"),
    WEEKLY("week", "Weekly"),
    MONTHLY("month", "Monthly")

    String code;
    String title;

    Step(final String code, final String title) {

        this.code = code;
        this.title = title;
    }

    /**
     * 
     * @param str String which contains name of some Step
     * @return null or valid parsed Step
     */
    public static Step parse(final String str) {

        Step valid = null
        try {
            valid = Step.valueOf(str)
        } catch (Exception exc) {
        }

        return valid
    }

    /**
     * 
     * @param str String which contains name of some Step
     * @return String name of valid parsed Step (or "" if it was invalid)
     */
    public static Step parseName(final String str) {

        Step valid = Step.parse(str)
        return valid == null ? "" : valid.toString()
    }
}