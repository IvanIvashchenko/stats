package zeptostats

class MailSubscription {

    Member member;
    boolean weekly;
    boolean everydayRating;
    boolean everydayDownloads;

    static constraints = {
        member notnull: true
    }

    static mapping = {
        version false
    }

}