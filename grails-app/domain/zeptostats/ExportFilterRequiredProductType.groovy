package zeptostats

class ExportFilterRequiredProductType {

    String requiredProductType;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        requiredProductType notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }
}
