package zeptostats

/**
 * Enum to keep all need info about Export column.
 */
enum ExportColumn {

    /** START_DATE is formatted start date */
    START_DATE(ExportColumnType.DATE, "StartDate", "Start Date", true),
    /** END_DATE is formatted end date */
    END_DATE(ExportColumnType.DATE, "EndDate", "End Date", true),

    /** META_SKU is title of MetaGame or MetaInapp */
    META_SKU(ExportColumnType.SKU, "MetaSKU", "Meta SKU", true),
    /** SKU is title of Game or Inapp */
    SKU(ExportColumnType.SKU, "FullName", "Full Name", true),
    /** PARENT is title of perent Game (is actual for Inapp only) */
    PARENT(ExportColumnType.SKU, "Parent", "Parent Name", true),
    /** VERSION is GameType's name for Game or Inapp (Free, Paid, Lite) */
    VERSION(ExportColumnType.SKU, "Version", "Version", true),
    /** STYLE is Resolution's name for Game or Inapp (SD, HD, Desktop) */
    STYLE(ExportColumnType.SKU, "Style", "Style", true),
    /** EXTRA is extra for Game or Inapp */
    EXTRA(ExportColumnType.SKU, "Extra", "Add", true),
    /** OS is Paltform's name for for Game or Inapp */
    OS(ExportColumnType.SKU, "OS", "OS", true),
    /** STORE is Store's name for for Game or Inapp */
    STORE(ExportColumnType.SKU, "Store", "Store", true),
    REQUIRED_PRODUCT_TYPE(ExportColumnType.SKU, "RequiredProductType", "Required Product Type", true),

    /** REGION is Region's name */
    REGION(ExportColumnType.GEO, "Region", "Region", true),
    /** COUNTRY is Country's name */
    COUNTRY(ExportColumnType.GEO, "Country", "Country", true),

    /** DOWNLOADS is amount of Downloads (for DateRange and ComparedColumns) */
    DOWNLOADS(ExportColumnType.CURVE, "Downloads", "Downloads", false),
    /** DOWNLOADS is amount of Downloads (for DateRange and ComparedColumns) */
    PROMO(ExportColumnType.CURVE, "Promo", "Promo", false),
    /** REVENUE is CALCULATED revenue = Downloads * net_price (for DateRange and ComparedColumns) */
    REVENUE(ExportColumnType.CURVE, "Revenue", "Revenue", false),
    /** REFUNDS is amount of Refunds (for DateRange and ComparedColumns) */
    REFUNDS(ExportColumnType.CURVE, "Refunds", "Refunds", false),
    /** REFUNDS_MONETIZED is CALCULATED monetized refunds = Refunds * net_price (for DateRange and ComparedColumns) */
    REFUNDS_MONETIZED(ExportColumnType.CURVE, "RefundsMonetized", "Refunds Monetized", false),
    /** UPDATES is amount of Updates (for DateRange and ComparedColumns) */
    UPDATES(ExportColumnType.CURVE, "Updates", "Updates", false),
    /** RANKINGS is rank (for DATE and Game and Country) */
    RANKINGS(ExportColumnType.CURVE, "Rankings", "Rankings", false),
    /** MONTHLY_FINANCIAL is revenue from monyhly financial reports */
    MONTHLY_FINANCIAL(ExportColumnType.CURVE, "MonthlyFinancialRevenue", "Monthly Financial Revenue", false)

    ExportColumnType exportColumnType;

    /** alias should not have whitespaces. It is used as alias in SQL queries. */
    String alias;

    /** title is column's title in export file. */
    String title;

    Boolean isSortable;


    ExportColumn(final ExportColumnType exportColumnType, final String alias, final String title, final Boolean isSortable) {

        this.exportColumnType = exportColumnType;
        this.alias = alias;
        this.title = title;
        this.isSortable = isSortable;
    }
}