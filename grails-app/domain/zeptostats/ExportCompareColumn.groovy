package zeptostats

/**
 * Contains selected compareColumn for Member in DataExport page
 */
class ExportCompareColumn {

    CompareColumn compareColumn;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
    	//NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}