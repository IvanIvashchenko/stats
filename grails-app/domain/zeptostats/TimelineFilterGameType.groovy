package zeptostats

/**
 * Contains unnecessary GameType for Member in Timeline page
 */
class TimelineFilterGameType {

    Long gameTypeId;
    Member member;

    static constraints = {
        gameTypeId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}