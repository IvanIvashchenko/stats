package zeptostats

/**
 * Contains unnecessary Game for Member in Timeline page
 */
class TimelineFilterGame {

    Long gameId;
    Member member;

    static constraints = {
        gameId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}