package zeptostats

/**
 * Contains unnecessary Resolution for Member in Timeline page
 */
class TimelineFilterResolution {

    Long resolutionId;
    Member member;

    static constraints = {
        resolutionId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}