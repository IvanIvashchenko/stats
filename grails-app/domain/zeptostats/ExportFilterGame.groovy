package zeptostats

/**
 * Contains unnecessary Game for Member in DataExport page
 */
class ExportFilterGame {

    Long gameId;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        gameId notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}