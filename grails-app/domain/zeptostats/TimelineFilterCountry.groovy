package zeptostats

/**
 * Contains unnecessary Country for Member in Timeline page
 */
class TimelineFilterCountry {

    Long countryId;
    Member member;

    static constraints = {
        countryId notnull: true
        member notnull: true
    }

    static mapping = {
        version false
    }

}