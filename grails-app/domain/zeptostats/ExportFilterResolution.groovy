package zeptostats

/**
 * Contains unnecessary Resolution for Member in DataExport page
 */
class ExportFilterResolution {

    String resolution;
    Member member;
    ExportBookmark exportBookmark;

    static constraints = {
        resolution notnull: true
        //NOTE:: because bookmarks exist for all users
        member nullable: true
        exportBookmark nullable: true
    }

    static mapping = {
        version false
    }

}