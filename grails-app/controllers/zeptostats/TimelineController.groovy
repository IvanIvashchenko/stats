package zeptostats

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_USER'])
class TimelineController {

    def springSecurityService;
    def timelineService;
    def filterEntitiesService;
    def timelineFilterService;
    def longService;

    def prefix = "/home/sevenbits/projects/grails/zeptofinance/web-app/";

    def fetchFilterData(final savedSettings) {

        def countriesInfo = filterEntitiesService.fetchAllCountriesWithRegionAndTopRegion();
        def gamesInfo = filterEntitiesService.fetchAllSkusWithMetaGames();
        def storesInfo = filterEntitiesService.fetchAllStoresWithPlatforms();
        def gameTypesInfo = filterEntitiesService.fetchAllGameTypes();
        def resolutionsInfo = filterEntitiesService.fetchAllResolutions();
        def requiredProductTypes = filterEntitiesService.fetchRequiredProductTypes();

        def formattedDateFrom = filterEntitiesService.formatDatepickerTimestamp(savedSettings.fromTimestamp);
        def formattedDateTo = filterEntitiesService.formatDatepickerTimestamp(savedSettings.toTimestamp);

        def formattedFromToString = filterEntitiesService.configureDatepickerString(
            savedSettings.fromTimestamp,
            savedSettings.toTimestamp
        );

        def filterData = [
            gameTypes: gameTypesInfo,
            resolutions: resolutionsInfo,
            countries: countriesInfo,
            games: gamesInfo,
            stores: storesInfo,
            formattedDateFrom: formattedDateFrom,
            formattedDateTo: formattedDateTo,
            formattedFromToString: formattedFromToString,
            requiredProductTypes: requiredProductTypes
        ]
    }

    def index() {

        Long memberId = springSecurityService.principal.id;
        def savedSettings = timelineFilterService.fetchAllUserSettings(memberId);

        def filterData = this.fetchFilterData(savedSettings);

        [
            filterData: filterData,
            savedSettings: savedSettings
        ]
    }

    def updateGraphs() {

        Long memberId = springSecurityService.principal.id;
        def unnecessaryGameIdList = request.getParameterValues('skuIds[]');
        def unnecessaryCountryIdList = request.getParameterValues('countryIds[]');
        def unnecessaryGameTypeIdList = request.getParameterValues('typeIds[]');
        def unnecessaryResolutionIdList = request.getParameterValues('resolutionIds[]');
        def unnecessaryStoreIdList = request.getParameterValues('storeIds[]');
        List<String> unnecessaryRequiredProductType = (List<String>) request.getParameterValues('requiredProductTypeIds[]');

        List<Long> unnecessaryGameIdsAsIs = longService.fetchValidIdList(unnecessaryGameIdList);
        List<Long> unnecessaryCountryIdsAsIs = longService.fetchValidIdList(unnecessaryCountryIdList);
        List<Long> unnecessaryGameTypeIdsAsIs = longService.fetchValidIdList(unnecessaryGameTypeIdList);
        List<Long> unnecessaryResolutionIdsAsIs = longService.fetchValidIdList(unnecessaryResolutionIdList);
        List<Long> unnecessaryStoreIdsAsIs = longService.fetchValidIdList(unnecessaryStoreIdList);

        Long fromTimestamp = filterEntitiesService.getDatepickerTimestamp(params.fromDate);
        Long toTimestamp = filterEntitiesService.getDatepickerTimestamp(params.toDate);

        CompareColumn compareColumn = CompareColumn.parse(params.compareColumn);
        Step needStep = Step.parse(params.step);
        Boolean ifNeedAppsDownloads = params.ifNeedAppsDownloads?.toBoolean();
        Boolean ifNeedInappsDownloads = params.ifNeedInappsDownloads?.toBoolean();
        Boolean ifNeedAppsRevenue = params.ifNeedAppsRevenue?.toBoolean();
        Boolean ifNeedInappsRevenue = params.ifNeedInappsRevenue?.toBoolean();
        Boolean ifNeedAppsCompare = params.ifNeedAppsCompare?.toBoolean();
        Boolean ifNeedInappsCompare = params.ifNeedInappsCompare?.toBoolean();
        Boolean ifNeedUpdates = params.ifNeedUpdates?.toBoolean();
        Boolean ifNeedUpdatesCompare = params.ifNeedUpdatesCompare?.toBoolean();

        boolean successSettings = timelineFilterService.saveAllUserSettings(
            memberId,
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypeIdsAsIs,
            unnecessaryResolutionIdsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductType,
            compareColumn,
            fromTimestamp,
            toTimestamp,
            ifNeedAppsDownloads,
            ifNeedInappsDownloads,
            ifNeedAppsRevenue,
            ifNeedInappsRevenue,
            ifNeedAppsCompare,
            ifNeedInappsCompare,
            ifNeedUpdates,
            ifNeedUpdatesCompare,
            needStep
        );

        def savedSettings = timelineFilterService.fetchAllUserSettings(memberId);
        def filterData = this.fetchFilterData(savedSettings);
        def modelParameters = [
            filterData: filterData,
            savedSettings: savedSettings
        ]


        //TODO:: add query here later
        boolean successQuery = true;
        def downloads = [276, 252, 196, 255, 265, 281, 242, 261, 251, 285, 250, 255, 304, 345, 320, 349, 359, 299, 267, 243, 215, 247, 209, 207, 210, 245, 232, 213, 164, 198, 236, 242, 217, 179, 154, 183, 210, 152, 161, 159, 219, 249, 195, 192, 204, 218, 186, 209, 261, 279, 241, 253, 276, 305, 305, 359, 316, 315, 284, 270, 297, 339, 375, 357, 350, 309, 270, 318, 360, 301, 292, 317, 265, 228, 180, 198, 174, 162, 167, 212, 199, 205, 261, 210, 252, 301, 279, 304, 355, 303, 360, 411, 446, 409, 393, 394, 347, 367, 309, 298, 273, 320, 372, 425, 448, 455, 475, 429, 372, 374, 383, 428, 380, 361, 385, 353, 311, 317, 321, 345, 326, 321, 301, 247, 296, 328, 380, 341, 330, 319, 376, 356, 321, 357, 348, 378, 426, 389, 394, 375, 413, 386, 413, 408, 392, 423, 483, 463, 469, 472, 492, 441, 436, 413, 389, 443, 440, 440, 426, 419, 364, 388, 442, 451, 401, 449, 415, 376, 346, 291, 310, 326, 288, 294, 299, 323, 381, 385, 353, 351, 332, 353, 349, 295, 239, 227, 175, 187, 171, 178, 177, 216, 239, 243, 241, 185, 158, 168, 199, 246];
        def total = 43000;
        def max = 600;
        def step = "day";

        def renderingData = timelineService.configureUpdatesJson(downloads, total, max, step, params.from, params.to);

//        def file = new File(prefix + 'js/data/updates.js');
//        file.write(renderingData)

        //TODO:: add error handlers.


        def graphs = g.render(
            template: 'graphs',
            model: modelParameters
        ).toString();

        def resultMap = ['successSettings': successSettings, 'successQuery': successQuery];

        render(contentType:"text/json") {
            success = successSettings && successQuery;
            html = graphs;
            result = resultMap;
        }
    }
}
