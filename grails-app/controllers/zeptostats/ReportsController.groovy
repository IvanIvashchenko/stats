package zeptostats

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_USER'])
class ReportsController {

    def reportsService;
    def springSecurityService;

    def index() {
       
       def memberId = springSecurityService.principal.id;
       def subscritions = reportsService.fetchUserMailSubscriptions(memberId);
       [subscritions: subscritions];
    }

    def saveSubscriptions() {

        def memberId = springSecurityService.principal.id;
        def weekly = params.weekly;
        def everydayRating = params.everydayRating;
        def everydayDownloads = params.everydayDownloads;
        def result = reportsService.saveSubscriptions(weekly, everydayRating, everydayDownloads, memberId);
        render(contentType:"text/json") {
            success = (result > 0);
        }
    }
}
