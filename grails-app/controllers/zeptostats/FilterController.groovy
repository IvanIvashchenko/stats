package zeptostats
import grails.plugins.springsecurity.Secured

@Secured(['ROLE_USER'])
class FilterController {

    def springSecurityService;
    def compareDependentColumnService;
    def longService;
    def filterEntitiesService;

    def ajaxFetchDependentStrikedIds() {

        def resultMap = [:];

        def memberId = springSecurityService.principal.id;

        def actionFilterColumnStr = params.actionFilterColumn;

        CompareColumnDependence dependence = CompareColumnDependence.parse(actionFilterColumnStr)

        if (dependence) {
            Map<String, List<Long>> actionStrikedUnstriked = [:];
            def actionFilterColumn = dependence.filterColumn
            if (actionFilterColumn) {
                //NOTE:: if action dependence is Top, then lists contain ALL striked / unstriked items
                //NOTE:: if action dependence is not Top, then lists contain ACTION striked / unstriked items only
                def actionStrikedIdStringsList = request.getParameterValues("${actionFilterColumn}[strikedIds][]");
                def actionUnstrikedIdStringsList = request.getParameterValues("${actionFilterColumn}[unstrikedIds][]");
                //NOTE:: hack for required product type
                if (actionFilterColumnStr.equals("REQUIRED_PRODUCT_TYPE")
                    || actionFilterColumnStr.equals("STYLE")
                    || actionFilterColumnStr.equals("VERSION")
                ) {
                    actionStrikedUnstriked = [
                        actionStrikedIds: actionStrikedIdStringsList == null ? [] : actionStrikedIdStringsList,
                        actionUnstrikedIds: actionUnstrikedIdStringsList == null ? [] : actionUnstrikedIdStringsList
                    ]
                } else {
                    actionStrikedUnstriked = [
                        actionStrikedIds: longService.fetchValidIdList(actionStrikedIdStringsList),
                        actionUnstrikedIds: longService.fetchValidIdList(actionUnstrikedIdStringsList)
                    ]
                }
            }

            Map<String, List<Long>> topParentStrikedUnstriked = [:];
            if (!dependence.isTop()) {
                def topParentFilterColumn = dependence.getTopParentFilterColumn()
                if (topParentFilterColumn) {
                    def allStrikedIdStringsList = request.getParameterValues("${topParentFilterColumn}[strikedIds][]");
                    def allUnstrikedIdStringsList = request.getParameterValues("${topParentFilterColumn}[unstrikedIds][]");
                    topParentStrikedUnstriked = [
                        allStrikedIds: longService.fetchValidIdList(allStrikedIdStringsList),
                        allUnstrikedIds: longService.fetchValidIdList(allUnstrikedIdStringsList)
                    ]
                }
            }

            resultMap = compareDependentColumnService.fetchAllDependentStrikedUnstrikedIds(
                dependence,
                actionStrikedUnstriked,
                topParentStrikedUnstriked
            );
        }

        render(contentType:"text/json") {
            success = true;
            result = resultMap;
        }
    }

    def ajaxRoundDatesByStep() {

        def memberId = springSecurityService.principal.id;

        Step validStep = Step.parse(params.step);
        Long fromTimestamp = filterEntitiesService.getDatepickerTimestamp(params.fromDate);
        Long toTimestamp = filterEntitiesService.getDatepickerTimestamp(params.toDate);

        def roundTimestamps = filterEntitiesService.roundTimestampsByStep(validStep, fromTimestamp, toTimestamp);

        def roundFromFormatted = filterEntitiesService.formatDatepickerTimestamp(roundTimestamps.fromTimestamp);
        def roundToFormatted = filterEntitiesService.formatDatepickerTimestamp(roundTimestamps.toTimestamp);

        def resultMap = [
            roundFromFormatted: roundFromFormatted,
            roundToFormatted: roundToFormatted
        ];

        render(contentType:"text/json") {
            success = true;
            result = resultMap;
        }
    }
}
