package zeptostats

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_USER'])
class FinancesController {

    def financeFilterService;
    def springSecurityService;
    def filterEntitiesService;

    private String CALENDAR_DATE_FORMAT = "yyyy-MM-dd";

    private def configureFilterParams(strikedStores, selectedPeriod) {

        def storesFilter = filterEntitiesService.fetchAllStores();

        def formattedDateTo = filterEntitiesService.formatDatepickerTimestamp(selectedPeriod.to_time.toLong());
        def formattedDateFrom = filterEntitiesService.formatDatepickerTimestamp(selectedPeriod.from_time.toLong());

        def formattedFromToString = filterEntitiesService.configureDatepickerString(
            selectedPeriod.from_time.toLong(),
            selectedPeriod.to_time.toLong()
        );

        [
            storesFilter: storesFilter,
            strikedStores: strikedStores,
            formattedDateFrom: formattedDateFrom,
            formattedDateTo: formattedDateTo,
            formattedFromToString: formattedFromToString
        ]
    }

    def index() {

        def memberId = springSecurityService.principal.id;
        def savedStores = financeFilterService.fetchSavedUserStoresSettings(memberId);
        def savedTime = financeFilterService.fetchSavedTimeForFilter(memberId);
        this.configureFilterParams(savedStores, savedTime);
    }

    def updateStores() {

        def memberId = springSecurityService.principal.id;
        def storesIds = request.getParameterValues('storeIds[]');
        financeFilterService.saveUserStoresSettings(memberId, storesIds);
        def savedTime = financeFilterService.fetchSavedTimeForFilter(memberId);

        def graphs = g.render(
                    template: 'graphs',
                    model: [:]
            ).toString();
        render(contentType:"text/json") {
            success = true;
            html = graphs;
        }
    }

    def updateTime() {

        def memberId = springSecurityService.principal.id;
        def savedStores = financeFilterService.fetchSavedUserStoresSettings(memberId);
        def from = filterEntitiesService.getDatepickerTimestamp(params.from);
        def to = filterEntitiesService.getDatepickerTimestamp(params.to);
        financeFilterService.saveUserTimeFilter(from, to, memberId);

        def graphs = g.render(
                    template: 'graphs',
                    model: [:]
            ).toString();
        render(contentType:"text/json") {
            success = true;
            html = graphs;
        }
    }
}
