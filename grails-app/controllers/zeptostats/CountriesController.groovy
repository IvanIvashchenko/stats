package zeptostats

import grails.plugins.springsecurity.Secured;

@Secured(['ROLE_USER'])
class CountriesController {

    def countriesService;
    def countriesFilterService;
    def springSecurityService;
    def filterEntitiesService;
    def exportFileService;

    private final static String CALENDAR_DATE_FORMAT = "yyyy-MM-dd";

    private def configureCountriesTablesParams(total, selectedGames, selectedCountries, savedTime) {

        def countriesFilter = countriesFilterService.fetchTopCountriesWithRegion();
        def appsFilter = countriesFilterService.getAppsForFilter();

        def formattedDateTo = filterEntitiesService.formatDatepickerTimestamp(savedTime.to_time.toLong());
        def formattedDateFrom = filterEntitiesService.formatDatepickerTimestamp(savedTime.from_time.toLong());

        def formattedFromToString = filterEntitiesService.configureDatepickerString(
            savedTime.from_time.toLong(),
            savedTime.to_time.toLong()
        );

        def tablesData = countriesService.fetchTablesData(total, selectedGames, selectedCountries, savedTime);

        [
            countriesFilter: countriesFilter,
            appsFilter: appsFilter,
            savedCountries: selectedCountries,
            savedApps: selectedGames,
            total: total,
            tablesData: tablesData,
            formattedDateFrom: formattedDateFrom,
            formattedDateTo: formattedDateTo,
            formattedFromToString: formattedFromToString
        ]
    }

    def index() {

        def memberId = springSecurityService.principal.id;
        def savedCountries = countriesFilterService.fetchSavedCountriesForFilter(memberId);
        def savedGames = countriesFilterService.fetchSavedGamesForFilter(memberId);
        def savedTotal = countriesFilterService.fetchSavedTotalForFilter(memberId);
        def savedTime = countriesFilterService.fetchSavedTimeForFilter(memberId);

        configureCountriesTablesParams(savedTotal, savedGames, savedCountries, savedTime);
    }

    def updateFilter() {

        def memberId = springSecurityService.principal.id;
        def countries = request.getParameterValues('geoSettings[]');
        def selectedGames = request.getParameterValues('appsSettings[]');
        def total = params.total;
        def savedTime = countriesFilterService.fetchSavedTimeForFilter(memberId);
        countriesFilterService.saveUserTotalFilter(total, memberId);
        countriesFilterService.saveUserCountriesFilter(countries, memberId);
        countriesFilterService.saveUserGamesFilter(selectedGames, memberId);
        def tablesData = configureCountriesTablesParams(total, selectedGames, countries, savedTime)['tablesData'];
        def tables = g.render(
                template: 'countriesTables',
                model: tablesData
            ).toString();
        render(contentType:"text/json") {
            success = true;
            html = tables;
        }
    }

    def updateDateRange() {

        def memberId = springSecurityService.principal.id;
        def savedCountries = countriesFilterService.fetchSavedCountriesForFilter(memberId);
        def savedselectedGames = countriesFilterService.fetchSavedGamesForFilter(memberId);
        def savedTotal = countriesFilterService.fetchSavedTotalForFilter(memberId);
        def from = filterEntitiesService.getDatepickerTimestamp(params.from);
        def to = filterEntitiesService.getDatepickerTimestamp(params.to);
        countriesFilterService.saveUserTimeFilter(from, to, memberId);
        def tables = g.render(
                    template: 'countriesTables',
                    model: configureCountriesTablesParams(
                        savedTotal,
                        savedselectedGames,
                        savedCountries,
                        [from_time: from, to_time: to]
                    )['tablesData']
            ).toString();
        render(contentType:"text/json") {
            success = true;
            html = tables;
        }
    }

    def exportXls() {

        def memberId = springSecurityService.principal.id;

        def filename = exportFileService.configureCountriesExportFileName();
        exportFileService.setupResponseAttachment(response, filename, ExportFileType.XLS);

        countriesService.generateXlsReport(response.outputStream, memberId);
        response.flushBuffer();
    }

    def exportCsv() {

        def memberId = springSecurityService.principal.id;

        def filename = exportFileService.configureCountriesExportFileName();
        exportFileService.setupResponseAttachment(response, filename, ExportFileType.CSV);

        countriesService.generateCsvReport(response.outputStream, memberId);
        response.flushBuffer();
    }
}
