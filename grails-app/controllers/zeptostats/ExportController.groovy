package zeptostats

import java.util.List;
import java.util.Map;

import grails.plugins.springsecurity.Secured;


@Secured(['ROLE_USER'])
class ExportController {

    def springSecurityService;
    def filterEntitiesService;
    def exportFilterService;
    def longService;

    def fetchFilterData(final savedSettings) {

        def countriesInfo = filterEntitiesService.fetchAllCountriesWithRegionAndTopRegion();
        def gamesInfo = filterEntitiesService.fetchAllSkusWithMetaGames();
        def storesInfo = filterEntitiesService.fetchAllStoresWithPlatforms();
        def gameTypesInfo = filterEntitiesService.fetchAllGameTypes();
        def resolutionsInfo = filterEntitiesService.fetchAllResolutions();
        def requiredProductTypes = filterEntitiesService.fetchRequiredProductTypes();

        def formattedDateFrom = filterEntitiesService.formatDatepickerTimestamp(savedSettings.fromTimestamp);
        def formattedDateTo = filterEntitiesService.formatDatepickerTimestamp(savedSettings.toTimestamp);

        def formattedFromToString = filterEntitiesService.configureDatepickerString(
            savedSettings.fromTimestamp,
            savedSettings.toTimestamp
        );
        def filterData = [
            gameTypes: gameTypesInfo,
            resolutions: resolutionsInfo,
            countries: countriesInfo,
            games: gamesInfo,
            stores: storesInfo,
            requiredProductTypes: requiredProductTypes,
            formattedDateFrom: formattedDateFrom,
            formattedDateTo: formattedDateTo,
            formattedFromToString: formattedFromToString,
            bookmarks: savedSettings.bookmarks
        ]
    }


    def index() {

        Long memberId = springSecurityService.principal.id?.toLong();
        def bookmarkId = params.bookmarkId?.toLong();
        def savedSettings = exportFilterService.fetchAllUserSettings(memberId, bookmarkId);

        def filterData = this.fetchFilterData(savedSettings);

        [
            filterData: filterData,
            savedSettings: savedSettings
        ]
    }

    def deleteBookmark() {

        def bookmarkId = params.bookmarkId?.toLong();
        exportFilterService.removeBookmark(bookmarkId);

        render(view: "index", contentType:"text/json") {
            success = true;
        }
    }

    def loadBookmark() {

        Long memberId = springSecurityService.principal.id?.toLong();
        def bookmarkId = params.bookmarkId?.toLong();
        def savedSettings = exportFilterService.fetchAllUserSettings(memberId, bookmarkId);

        savedSettings.unnecessaryGameIds?.each() {
            it?.toLong()
        }
        savedSettings.unnecessaryCountryIds?.each() {
            it?.toLong()
        }
        savedSettings.unnecessaryStoreIds?.each() {
            it?.toLong()
        }
        savedSettings.compareColumns?.each() {
            it?.toString()
        }


        exportFilterService.saveAllUserSettings(
            memberId,
            savedSettings.unnecessaryGameIds,
            savedSettings.unnecessaryCountryIds,
            savedSettings.unnecessaryGameTypeIds,
            savedSettings.unnecessaryResolutionIds,
            savedSettings.unnecessaryStoreIds,
            savedSettings.unnecessaryRequiredProductTypeIds,
            savedSettings.compareColumns,
            savedSettings.fromTimestamp?.toLong(),
            savedSettings.toTimestamp?.toLong(),
            savedSettings.ifNeedDownloads?.toBoolean(),
            savedSettings.ifNeedRevenues?.toBoolean(),
            savedSettings.ifNeedUpdates?.toBoolean(),
            savedSettings.ifNeedRefunds?.toBoolean(),
            savedSettings.ifNeedRefundsMonetized?.toBoolean(),
            savedSettings.ifNeedRankings?.toBoolean(),
            savedSettings.ifNeedTotal?.toBoolean(),
            savedSettings.ifNeedMonthlyFinancial?.toBoolean(),
            Step.parse(savedSettings.step?.toString()),
            null
        );


        def data = [
            savedSettings: savedSettings
        ]

        render(view: "index", contentType:"text/json") {
            data = data;
        }

    }

    def saveSettings() {

        Long memberId = springSecurityService.principal.id;
        def unnecessaryGameIdList = request.getParameterValues('skuIds[]');
        def unnecessaryCountryIdList = request.getParameterValues('countryIds[]');
        def unnecessaryGameTypeIdList = request.getParameterValues('typeIds[]');
        def unnecessaryResolutionIdList = request.getParameterValues('resolutionIds[]');
        def unnecessaryStoreIdList = request.getParameterValues('storeIds[]');
        List<String> unnecessaryRequiredProductTypes = (List<String>) request.getParameterValues('requiredProductTypeIds[]');
        def compareColumnList = request.getParameterValues('compareColumns[]');

        List<CompareColumn> compareColumns = CompareColumn.parseList(compareColumnList);
        List<Long> unnecessaryGameIdsAsIs = longService.fetchValidIdList(unnecessaryGameIdList);
        List<Long> unnecessaryCountryIdsAsIs = longService.fetchValidIdList(unnecessaryCountryIdList);
        List<Long> unnecessaryGameTypeIdsAsIs = longService.fetchValidIdList(unnecessaryGameTypeIdList);
        List<Long> unnecessaryResolutionIdsAsIs = longService.fetchValidIdList(unnecessaryResolutionIdList);
        List<Long> unnecessaryStoreIdsAsIs = longService.fetchValidIdList(unnecessaryStoreIdList);

        Long fromTimestamp = filterEntitiesService.getDatepickerTimestamp(params.fromDate);
        Long toTimestamp = filterEntitiesService.getDatepickerTimestamp(params.toDate);

        Step needStep = Step.parse(params.step);
        Boolean ifNeedDownloads = params.ifNeedDownloads?.toBoolean();
        Boolean ifNeedRevenues = params.ifNeedRevenues?.toBoolean();
        Boolean ifNeedUpdates = params.ifNeedUpdates?.toBoolean();
        Boolean ifNeedRefunds = params.ifNeedRefunds?.toBoolean();
        Boolean ifNeedRefundsMonetized = params.ifNeedRefundsMonetized?.toBoolean();
        Boolean ifNeedRankings = params.ifNeedRankings?.toBoolean();
        Boolean ifNeedTotal = params.ifNeedTotal?.toBoolean();
        Boolean ifNeedMonthlyFinancial = params.ifNeedMonthlyFinancial?.toBoolean();
        String bookmarkName = params.bookmarkName?.toString();

        boolean successSettings = exportFilterService.saveAllUserSettings(
            memberId,
            unnecessaryGameIdsAsIs,
            unnecessaryCountryIdsAsIs,
            unnecessaryGameTypeIdsAsIs,
            unnecessaryResolutionIdsAsIs,
            unnecessaryStoreIdsAsIs,
            unnecessaryRequiredProductTypes,
            compareColumns,
            fromTimestamp,
            toTimestamp,
            ifNeedDownloads,
            ifNeedRevenues,
            ifNeedUpdates,
            ifNeedRefunds,
            ifNeedRefundsMonetized,
            ifNeedRankings,
            ifNeedTotal,
            ifNeedMonthlyFinancial,
            needStep,
            bookmarkName
        );
        Long id = exportFilterService.fetchBookmarkId(bookmarkName);

        render(contentType:"text/json") {
            success = successSettings;
            bookmarkId = id;
        }
    }

    def export() {

        Long memberId = springSecurityService.principal.id;
        Member member = Member.get(memberId);
        def savedSettings = exportFilterService.fetchAllUserSettings(memberId, null);

        def message = [
            "member": member,
            "savedSettings": savedSettings
        ];
        sendMessage("seda:input.dataExportQueue", message)

        render(contentType:"text/json") {
            success = true;
        }
    }
}
