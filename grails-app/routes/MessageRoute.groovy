

import org.apache.camel.builder.RouteBuilder

class MessageRoute extends RouteBuilder {
    def grailsApplication

    @Override
    void configure() {
        def config = grailsApplication?.config

        from("seda:input.dataExportQueue").to("bean:zeptostats.DataExportMessageListenerService?method=export")
    }
}
